import sys
sys.path.append('/home/ubuntu/market/dc-66-python-2.0/SelfModule')

from urllib.parse import unquote
from PIL import Image, ImageDraw, ImageFont
import tempfile
import mysql.connector
from sqlalchemy import create_engine
import warnings
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from selfdefinfunc import Render_Mpl_to_Pil  # 自定义封装函数
from tradedate import *  # 自定义封装函数
from matplotlib.font_manager import FontProperties
from matplotlib.ticker import PercentFormatter, FuncFormatter, MaxNLocator
from chinese_calendar import is_workday
import os
import io
import math
from pdf2image import convert_from_path
import tempfile
from reportlab.lib.pagesizes import letter
from api_log import create_cos_client
import logging

# 设置日志
logging.basicConfig(filename='/home/ubuntu/market/dc-66-python-2.0/execution.log', 
                    level=logging.INFO, 
                    format='%(asctime)s - %(levelname)s - %(message)s', 
                    filemode='a')  # 设置为追加模式

# 设置日志级别为 WARNING
logging.getLogger('qcloud_cos').setLevel(logging.WARNING)

def get_fill_color(value):  
    if value < 0:  
        return 'green'  
    else:  
        return 'red'

# 定义一个函数来将刻度值乘以100  
def multiply_by_100(x, pos):  
    return '{:.1f}%'.format(x * 100)  

plt.rcParams['font.sans-serif'] = ['SimHei']  # 用来正常显示中文标签
plt.rcParams['axes.unicode_minus'] = False  # 用来正常显示负号
warnings.filterwarnings('ignore')

# 获取当前文件夹的父文件夹路径
parent_dir = os.path.dirname('/home/ubuntu/market/dc-66-python-2.0/')
# 构建目标文件夹路径
target_dir = os.path.join(parent_dir, "DocTemplate")
# 修改系统默认文件夹路径
os.chdir(target_dir)

# 环境变量读取数据库秘钥信息
host = os.environ.get('MYSQL_HOST')   
user = os.environ.get('MYSQL_USERNAME')  
password = os.environ.get('MYSQL_PASSWORD')
port = os.environ.get('MYSQL_PORT')
# 数据库存储操作
cnx = mysql.connector.connect(user=user, password=password,
                              host=host,  port=port, database='同业产品数据库')
engine = create_engine(f'mysql+mysqlconnector://{user}:{password}@{host}:{port}/同业产品数据库')
cursor = cnx.cursor()

secret_id = os.environ.get('COS_SECRET_ID')  # 用户的 SecretId
secret_key = os.environ.get('COS_SECRET_KEY')  # 用户的 SecretKey
region = 'ap-guangzhou'                      # 桶归属的 region
token = None                                # 临时密钥的 token
scheme = 'https'                            # 访问 COS 的协议
bucket_name = 'xwabstore-1320558538'

cos_client = create_cos_client(secret_id, secret_key, region, token, scheme)

# 字体路径初定义
font_title_path = "素材/SourceHanSansCN-Heavy.otf"
font_subtitle_path = "素材/SourceHanSansCN-Normal.otf"

# 模版路径
pdf_moban_path = 'tongye/'

summary_df = pd.read_sql_query("SELECT * FROM %s" % '所有产品计算指标汇总表', engine)

# 从目标路径提取报告模版
initial_image = convert_from_path(pdf_moban_path + '普通产品周报模版.pdf')
# image.show()

for n in range(len(summary_df)):
    if summary_df.loc[n, '是否更新周报'] == '是':
        continue
    if '指增' in summary_df.loc[n, '策略']:
        continue
    # 产品全称匹配
    product_full_name = summary_df.loc[n, '产品名称']

    # 判断所属保存路径
    report_save_path = '私募产品周报/产品净值周报/'
    
    # 提取产品净值信息
    list_chanpin_info = pd.read_sql_query("SELECT * FROM %s" % product_full_name, engine)
    list_chanpin_info = list_chanpin_info.loc[list_chanpin_info['净值日期'] <= time_last_Friday_time]
    # 这里保证全部都是周频净值
    list_chanpin_info = list_chanpin_info.loc[list_chanpin_info['是否周末净值'] == '是'].reset_index().drop('index', axis=1)
    
    # 处理可能为 None 的值
    manager = summary_df.loc[n, '管理人'] if summary_df.loc[n, '管理人'] is not None else ''
    strategy = summary_df.loc[n, '策略'] if summary_df.loc[n, '策略'] is not None else ''
    strategy_label = summary_df.loc[n, '策略标签'] if summary_df.loc[n, '策略标签'] is not None else ''

    if summary_df.loc[n, '是否平台净值产品'] == '是':
        # 如果 strategy_label 为空，则不添加括号
        if strategy_label:
            product_name = manager + strategy + '(' + strategy_label + ')' + '产品净值周报'
        else:
            product_name = manager + strategy + '产品净值周报'
    else:
        product_name = product_full_name + '产品净值周报'

    latest_date = list_chanpin_info['净值日期'].iloc[-1].strftime("%Y年%m月%d日")
    product_unit_nav = list_chanpin_info['复权净值'].iloc[-1]
    product_return_week = summary_df.loc[n, '本周收益率']
    product_return_last4week = summary_df.loc[n, '近四周收益率']
    product_return_last3month = summary_df.loc[n, '近三月收益率']
    product_return_ytd = summary_df.loc[n, '今年以来收益率']
    
    image = initial_image[0].copy()
    # 创建Image对象
    draw = ImageDraw.Draw(image)
    # 产品标题
    # 确定字体
    font = ImageFont.truetype(font_title_path, size=48)  # 选择适当的字体大小
    subtitle_font = FontProperties(fname=font_title_path)
    # 计算产品标题文本宽度
    text_width, text_height = font.font.getsize(product_name)[0]
    # 计算产品标题文本居中位置
    image_width, image_height = image.size
    x_centered = (image_width - text_width) / 2
    draw.text((x_centered, 160), product_name, fill='black', font=font)
    
    # 净值时间
    # 绘制名称
    font = ImageFont.truetype(font_title_path, size=32)  # 选择适当的字体大小
    font_sub = ImageFont.truetype(font_subtitle_path, size=24)  # 选择适当的字体大小
    # 计算文本宽度
    text_width, text_height = font.font.getsize(latest_date)[0]

    draw.text((120, 250), latest_date, fill='black', font=font)

# 左侧基础信息
    # 单位净值
    target_x, target_y = 290, 318
    draw.text((target_x, target_y), str(product_unit_nav), fill='#262626', font=font)
    # 本周收益率
    target_x, target_y = 290, 395
    formatted_return = "{:.2%}".format(product_return_week)   # 格式化数值为小数点后两位   
    fill_color = get_fill_color(product_return_week)  # 调用函数获取字体颜色  
    draw.text((target_x, target_y), formatted_return, fill=fill_color, font=font)
    # 四周收益率
    target_x, target_y = 290, 473
    formatted_return = "{:.2%}".format(product_return_last4week)   # 格式化数值为小数点后两位   
    fill_color = get_fill_color(product_return_last4week)  # 调用函数获取字体颜色  
    draw.text((target_x, target_y), formatted_return, fill=fill_color, font=font)
    # 近三月收益率
    if not math.isnan(product_return_last3month):
        target_x, target_y = 290, 551
        formatted_return = "{:.2%}".format(product_return_last3month)   # 格式化数值为小数点后两位   
        fill_color = get_fill_color(product_return_last3month)  # 调用函数获取字体颜色  
        draw.text((target_x, target_y), formatted_return, fill=fill_color, font=font)

    # 今年以来收益率
    if not math.isnan(product_return_ytd):
        target_x, target_y = 290, 629
        formatted_return = "{:.2%}".format(product_return_ytd)   # 格式化数值为小数点后两位   
        fill_color = get_fill_color(product_return_ytd)  # 调用函数获取字体颜色  
        draw.text((target_x, target_y), formatted_return, fill=fill_color, font=font)
    
# 柱状图
    # 本周收益率
    rates = list_chanpin_info['本周收益率'].dropna().tolist()
    if len(rates) > 156:
        rates = rates[-156:]
    
    # 生成柱状图的 x 轴坐标
    x = np.arange(len(rates))
    # 根据收益率的正负性设置柱状图的颜色
    colors = ['green' if rate < 0 else 'red' for rate in rates]
    
    # 创建带透明背景的图形
    fig_NavDraw = plt.figure(figsize=(6.5, 1.8), dpi=130)
    plt.bar(x, rates, color=colors)  # 绘制柱状图
    plt.xticks([])  # 隐藏 x 轴
    ax = plt.gca()
    ax.yaxis.set_major_formatter(FuncFormatter(multiply_by_100))
    ax.yaxis.set_major_locator(MaxNLocator(nbins=5))
    plt.tight_layout()
    
    # 将 Matplotlib 图形转换为 PIL 图像
    img_buf = io.BytesIO()
    fig_NavDraw.savefig(img_buf, format='png', transparent=True)
    img_buf.seek(0)
    fig_NavDraw = Image.open(img_buf).convert('RGBA')
    
    # 手动修改需要添加图表的坐标点
    fig_width_abscissa, fig_ordinate = 470, 460
    image.paste(fig_NavDraw, (fig_width_abscissa, fig_ordinate), fig_NavDraw)
    plt.close()
    
    # 列名和坐标点信息
    columns_to_plot = {
        '近四周收益率': (480, 290),
        '近三月收益率': (790, 290),
        '今年以来收益率': (1100, 290)
    }
    for column, (abscissa, ordinate) in columns_to_plot.items():
        if column in list_chanpin_info.columns and not list_chanpin_info[column].isna().all():
            fig_NavDraw = plt.figure(figsize=(2.4, 1.4), dpi=130)
            plt.plot(list_chanpin_info['净值日期'], list_chanpin_info[column], color='#AB545A', linewidth=2)
            plt.xticks([])
            # 设置y轴刻度为最少三个
            y_locator = MaxNLocator(nbins=3)
            plt.gca().yaxis.set_major_locator(y_locator)
            plt.gca().yaxis.set_major_formatter(PercentFormatter(xmax=1, decimals=1))  # 格式化纵坐标刻度 
            plt.tight_layout()
            # 将 Matplotlib 图形转换为 PIL 图像
            fig_NavDraw = Render_Mpl_to_Pil(fig_NavDraw)
            fig_width, fig_height = fig_NavDraw.size
            fig_NavDraw = fig_NavDraw.convert('RGB')
            # 手动修改需要添加图表的坐标点
            fig_width_abscissa, fig_ordinate = abscissa, ordinate  # 根据坐标点信息修改坐标
            image.paste(fig_NavDraw, (fig_width_abscissa, fig_ordinate, fig_width_abscissa + fig_width, fig_ordinate + fig_height))
            plt.close()  # 清除当前图以便绘制下一张图
    
    # 初始化净值曲线图  
    fig_NavDraw = plt.figure(figsize=(5.5, 4), dpi=130)
    # 提取'成立以来收益率'列
    if '成立以来收益率' in list_chanpin_info.columns and not list_chanpin_info['成立以来收益率'].isna().all():
        plt.plot(list_chanpin_info['净值日期'], list_chanpin_info['成立以来收益率'], label='运作以来收益率', color='#AB545A', linewidth=2)
    # 显示图例，并将图例放到左上角
    plt.legend(loc='upper left')
    # 使用PercentFormatter来格式化纵坐标轴刻度
    plt.gca().yaxis.set_major_formatter(PercentFormatter(xmax=1, decimals=1))  # xmax为最大刻度，decimals为小数位数
    plt.xticks(rotation=45)
    # 清除图的边界
    plt.tight_layout()
    # 将 Matplotlib 图形转换为 PIL 图像  
    fig_NavDraw = Render_Mpl_to_Pil(fig_NavDraw)
    fig_width, fig_height = fig_NavDraw.size
    fig_NavDraw = fig_NavDraw.convert('RGB')
    # 手动修改需要添加图表的坐标点  
    fig_width_abscissa, fig_ordinate = 40, 820
    image.paste(fig_NavDraw, (fig_width_abscissa, fig_ordinate, fig_width_abscissa + fig_width, fig_ordinate + fig_height))
    plt.close()
    
    
    # 初始化回撤曲线图
    fig_NavDraw = plt.figure(figsize=(5.5, 2.7), dpi=130)
    # 提取'成立以来最大回撤'列
    if '成立以来最大回撤' in list_chanpin_info.columns and not list_chanpin_info['成立以来最大回撤'].isna().all():
        plt.fill_between(list_chanpin_info['净值日期'], list_chanpin_info['成立以来最大回撤'], alpha=0.5, color='#AB545A', label='运作以来最大回撤')
    # 显示图例
    plt.legend()
    # 使用PercentFormatter来格式化纵坐标轴刻度
    plt.gca().yaxis.set_major_formatter(PercentFormatter(xmax=1, decimals=1))  # xmax为最大刻度，decimals为小数位数
    plt.xticks(rotation=45)
    # 清除图的边界
    plt.tight_layout()
    plt.legend(loc='lower left')
    # 将 Matplotlib 图形转换为 PIL 图像
    fig_NavDraw = Render_Mpl_to_Pil(fig_NavDraw)
    fig_width, fig_height = fig_NavDraw.size
    fig_NavDraw = fig_NavDraw.convert('RGB')
    # 手动修改需要添加图表的坐标点
    fig_width_abscissa, fig_ordinate = 40, 1330
    image.paste(fig_NavDraw, (fig_width_abscissa, fig_ordinate, fig_width_abscissa + fig_width, fig_ordinate + fig_height))
    plt.close()
    
    # 获取当前收益指标
    product_data = summary_df[['策略', '成立日期', '成立以来收益率', '累计年化收益率']].loc[n]
    # 转换为datetime
    product_data['成立日期'] = pd.to_datetime(product_data['成立日期'])
    # 格式化
    product_data['成立日期'] = product_data['成立日期'].to_pydatetime().strftime('%Y年%m月%d日')
    # 百分比格式化
    product_data['成立以来收益率'] = "{:.2%}".format(product_data['成立以来收益率'])
    product_data['累计年化收益率'] ="{:.2%}".format(product_data['累计年化收益率'])
    # 转换为DataFrame
    product_df = product_data.to_frame().T
    product_df.rename(columns={'成立日期': '运作日期', '成立以来收益率': '运作以来收益率'}, inplace=True)
    # 创建一个新的Figure对象
    fig_table = plt.figure(figsize=(5.6, 1.3), dpi=130)
    plt.tight_layout()
    plt.axis('off')
    # 创建表格
    tb = plt.table(cellText=product_df.values, colLabels=product_df.columns, cellLoc="center", loc='center', bbox=[0, 0, 1, 0.75])
    plt.tight_layout()
    # 隐藏表格的坐标轴
    # 渲染表格
    tb.auto_set_font_size(False)
    tb.set_fontsize(12)
    tb.auto_set_column_width([i for i in range(len(product_data))])
    # 图表添加标题
    plt.text(0.0, 0.9, '基础指标', fontproperties=subtitle_font, fontsize=16, weight='bold', ha='left', va='center', color='black')
    # 获取渲染后的图像
    table_image = Render_Mpl_to_Pil(fig_table)
    table_image = table_image.convert('RGB')
    # 
    fig_width, fig_height = table_image.size
    # 手动修改需要添加图表的坐标点
    fig_width_abscissa, fig_ordinate = 750, 840
    # 粘贴图片
    image.paste(table_image, (fig_width_abscissa, fig_ordinate, fig_width_abscissa + fig_width, fig_ordinate + fig_height))
    plt.close()
    
    
    # 获取当前风险指标
    product_data = summary_df[['成立以来最大回撤', '周胜率', '夏普比率', '索提诺比率', '卡玛比率']].loc[n]
    # 格式化
    product_data['成立以来最大回撤'] = "{:.2%}".format(product_data['成立以来最大回撤'])
    product_data['周胜率'] = "{:.0%}".format(product_data['周胜率'])
    product_data['夏普比率'] ="{:.2f}".format(product_data['夏普比率'])
    product_data['索提诺比率'] ="{:.2f}".format(product_data['索提诺比率'])
    product_data['卡玛比率'] ="{:.2f}".format(product_data['卡玛比率'])
    # 转换为DataFrame
    product_df = product_data.to_frame().T
    product_df.rename(columns={'成立以来最大回撤': '运作以来最大回撤'}, inplace=True)
    # 创建一个新的Figure对象
    fig_table = plt.figure(figsize=(5.6, 1.3), dpi=130)
    plt.tight_layout()
    plt.axis('off')
    # 创建表格
    tb = plt.table(cellText=product_df.values, colLabels=product_df.columns, cellLoc="center", loc='center', bbox=[0, 0, 1, 0.75])
    plt.tight_layout()
    # 隐藏表格的坐标轴
    # 渲染表格
    tb.auto_set_font_size(False)
    tb.set_fontsize(12)
    tb.auto_set_column_width([i for i in range(len(product_data))])
    # 图表添加标题
    plt.text(0.0, 0.9, '风险指标', fontproperties=subtitle_font, fontsize=16, weight='bold', ha='left', va='center', color='black')
    # 获取渲染后的图像
    table_image = Render_Mpl_to_Pil(fig_table)
    table_image = table_image.convert('RGB')
    # 
    fig_width, fig_height = table_image.size
    # 手动修改需要添加图表的坐标点
    fig_width_abscissa, fig_ordinate = 750, 1010
    # 粘贴图片
    image.paste(table_image, (fig_width_abscissa, fig_ordinate, fig_width_abscissa + fig_width, fig_ordinate + fig_height))
    plt.close()
    
    # 区间收益列名和行名
    column_names = ['区间收益率', '区间最大回撤', '夏普比率', '卡玛比率']
    row_names = ['近三月', '近六月', '近一年', '近两年', '近三年']
    # 创建一个空的数据框，稍后将填充数据
    product_df = pd.DataFrame(columns=column_names, index=row_names)
    # 年化参数数据数组
    values = [1, 1, 1, 2, 3]
    # 循环填充数据
    for i, row_name in enumerate(row_names):
        # 在此处填充每个单元格的数据
        # 例如：product_return_3months, max_drawdown_3months, sharpe_ratio_3months, sortino_ratio_3months
        # 根据您的数据源来获取相应的值，假设这些值在不同的变量中
        interval_return = summary_df.loc[n, row_name + '收益率']
        if math.isnan(interval_return):
            continue
        # 提取相应区间的收益率数据
        interval_returns = list_chanpin_info.loc[~list_chanpin_info[row_name + '收益率'].isna(), '本周收益率']
        # 计算波动率
        interval_volatility = np.std(interval_returns) * math.sqrt(52)

        interval_max_drawdown = summary_df.loc[n, row_name + '最大回撤']
        interval_sharpe_ratio = ((1+interval_return)**(1/values[i])-1)/interval_volatility
        interval_sortino_ratio = -((1+interval_return)**(1/values[i])-1)/interval_max_drawdown
        data = [interval_return, interval_max_drawdown, interval_sharpe_ratio, interval_sortino_ratio]
        # 将数据添加到数据框
        product_df.loc[row_name] = data
        product_df.loc[row_name,'区间收益率'] = "{:.2%}".format(product_df.loc[row_name]['区间收益率'])
        product_df.loc[row_name, '区间最大回撤'] ="{:.2%}".format(product_df.loc[row_name]['区间最大回撤'])
        product_df.loc[row_name, '夏普比率'] = "{:.2f}".format(product_df.loc[row_name]['夏普比率'])
        product_df.loc[row_name, '卡玛比率'] ="{:.2f}".format(product_df.loc[row_name]['卡玛比率'])
    product_df.insert(0, '统计区间', row_names)
    product_df = product_df.dropna(subset=['区间收益率'])
    product_df = product_df.fillna('')  # 将所有 NaN 值替代为空值
    if not product_df.empty:
        table_height = (len(product_df) +1)*0.52 + 0.1  # 每行高度为0.2
        # 创建一个新的Figure对象
        fig_table = plt.figure(figsize=(5.6, table_height), dpi=130)
        plt.tight_layout()
        plt.axis('off')
        # 创建表格
        tb = plt.table(cellText=product_df.values, colLabels=product_df.columns, cellLoc="center", loc='center', bbox=[0, 0, 1, 0.9-(5-len(product_df))*0.025])
        plt.tight_layout()
        # 隐藏表格的坐标轴
        # 渲染表格
        tb.auto_set_font_size(False)
        tb.set_fontsize(12)
        tb.auto_set_column_width([i for i in range(len(column_names))])
        # 图表添加标题
        plt.text(0.0, 0.95, '区间指标', fontproperties=subtitle_font, fontsize=16, weight='bold', ha='left', va='center', color='black')
        # 获取渲染后的图像
        table_image = Render_Mpl_to_Pil(fig_table)
        table_image = table_image.convert('RGB')
        # 手动修改需要添加图表的坐标点
        fig_width_abscissa, fig_ordinate = 750, 1180
        # 粘贴图片
        image.paste(table_image, (fig_width_abscissa, fig_ordinate, fig_width_abscissa + table_image.width, fig_ordinate + table_image.height))
        plt.close()
    
    # 获取最近四周净值数据
    nav_df = list_chanpin_info.iloc[-5:, list_chanpin_info.columns.get_indexer(['净值日期', '单位净值', '复权净值'])].reset_index(drop=True)
    for x in range(1, len(nav_df)):
        nav_df.loc[x, '涨跌幅'] = nav_df.loc[x, '复权净值'] / nav_df.loc[x-1, '复权净值'] - 1
    nav_df['净值日期'] = pd.to_datetime(nav_df['净值日期']).dt.strftime('%Y/%m/%d')
    nav_df['涨跌幅'] = (nav_df['涨跌幅'] * 100).round(2).astype(str) + '%'
    # 删除第一行
    nav_df = nav_df.drop(0).reset_index(drop=True)
    # 创建一个新的Figure对象
    fig_table = plt.figure(figsize=(4.2, 2.3), dpi=130)
    plt.tight_layout()
    plt.axis('off')
    # 创建表格
    tb = plt.table(cellText=nav_df.values, colLabels=nav_df.columns, cellLoc="center", loc='center', bbox=[0, 0, 1, 0.87])
    # 渲染表格
    tb.auto_set_font_size(False)
    tb.set_fontsize(12)
    # 自适应宽度
    tb.auto_set_column_width([i for i in range(len(nav_df.columns))])
    plt.tight_layout()
    # 图表添加标题
    plt.text(0.0, 0.96, '近四周净值', fontproperties=subtitle_font, fontsize=16, weight='bold', ha='left', va='center', color='black')
    # 获取渲染后的图像
    table_image = Render_Mpl_to_Pil(fig_table)
    # 转换为RGBA模式
    table_image = table_image.convert('RGBA')
    # 手动修改需要添加图表的坐标点
    fig_width_abscissa, fig_ordinate = 50, 1800
    # 粘贴图片
    image.paste(table_image, (fig_width_abscissa, fig_ordinate), table_image)
    plt.close()
    
    # 创建临时文件
    with tempfile.NamedTemporaryFile(delete=False, suffix='.png') as temp_file:
        temp_file_path = temp_file.name
        # 保存图像到临时文件
        image.save(temp_file_path)

    # 上传临时文件到云存储桶的指定路径
    key = f'{report_save_path}{product_full_name}.png'
    with open(temp_file_path, 'rb') as f:
        cos_client.put_object(
            Bucket=bucket_name,
            Body=f,
            Key=key
        )

    # 删除临时文件
    os.remove(temp_file_path)

    print(product_full_name + '生成：'+ '{:.1%}'.format((n+1)/len(summary_df)))
    summary_df.loc[n, '是否更新周报'] = '是'

# 从目标路径提取报告模版
initial_image = convert_from_path(pdf_moban_path + '指增产品周报模版.pdf')

for n in range(len(summary_df)):
    if summary_df.loc[n, '是否更新周报'] == '是':
        continue
    if '指增' not in summary_df.loc[n, '策略']:
        continue

    # 产品全称匹配
    product_full_name = product_full_name = summary_df.loc[n, '产品名称']

    report_save_path = '私募产品周报/产品净值周报/'
    # 提取产品净值信息
    list_chanpin_info = pd.read_sql_query("SELECT * FROM %s" % product_full_name, engine)
    list_chanpin_info = list_chanpin_info.loc[list_chanpin_info['净值日期'] <= time_last_Friday_time]
    # 这里保证全部都是周频净值
    list_chanpin_info = list_chanpin_info.loc[list_chanpin_info['是否周末净值'] == '是'].reset_index().drop('index', axis=1)
    
    # 提取产品信息

    # 处理可能为 None 的值
    manager = summary_df.loc[n, '管理人'] if summary_df.loc[n, '管理人'] is not None else ''
    strategy = summary_df.loc[n, '策略'] if summary_df.loc[n, '策略'] is not None else ''
    strategy_label = summary_df.loc[n, '策略标签'] if summary_df.loc[n, '策略标签'] is not None else ''

    # 如果 strategy_label 为空，则不添加括号
    if summary_df.loc[n, '是否平台净值产品'] == '是':
        # 如果 strategy_label 为空，则不添加括号
        if strategy_label:
            product_name = manager + strategy + '(' + strategy_label + ')' + '产品净值周报'
        else:
            product_name = manager + strategy + '产品净值周报'
    else:
        product_name = product_full_name + '产品净值周报'
    
    latest_date = list_chanpin_info['净值日期'].iloc[-1].strftime("%Y年%m月%d日")
    product_unit_nav = list_chanpin_info['复权净值'].iloc[-1]
    product_week_return = summary_df.loc[n, '本周收益率']
    product_week_return_excess = summary_df.loc[n, '本周超额']
    product_week_return_last4week = summary_df.loc[n, '近四周收益率']
    product_week_return_excess_last4week = summary_df.loc[n, '近四周超额（除法）']
    
    image = initial_image[0].copy()
    # 创建Image对象
    draw = ImageDraw.Draw(image)
    # 产品标题
    # 绘制产品名称
    font = ImageFont.truetype(font_title_path, size=48)  # 选择适当的字体大小
    # 计算产品标题文本宽度
    text_width, text_height = font.font.getsize(product_name)[0]
    # 计算产品标题文本居中位置
    image_width, image_height = image.size
    x_centered = (image_width - text_width) / 2
    draw.text((x_centered, 160), product_name, fill='black', font=font)

    # 净值时间
    # 绘制名称
    font = ImageFont.truetype(font_title_path, size=32)  # 选择适当的字体大小
    font_sub = ImageFont.truetype(font_subtitle_path, size=24)  # 选择适当的字体大小
    subtitle_font = FontProperties(fname=font_title_path)
    # 计算文本宽度
    text_width, text_height = font.font.getsize(latest_date)[0]

    draw.text((120, 250), latest_date, fill='black', font=font)

# 左侧基础信息
    # 单位净值
    target_x, target_y = 290, 318
    draw.text((target_x, target_y), str(product_unit_nav), fill='#262626', font=font)
    # 本周收益率
    target_x, target_y = 280, 395
    formatted_return = "{:.2%}".format(product_week_return)   # 格式化数值为小数点后两位   
    fill_color = get_fill_color(product_week_return)  # 调用函数获取字体颜色  
    draw.text((target_x, target_y), formatted_return, fill=fill_color, font=font)
    # 本周超额
    target_x, target_y = 280, 473
    formatted_return = "{:.2%}".format(product_week_return_excess)   # 格式化数值为小数点后两位   
    fill_color = get_fill_color(product_week_return_excess)  # 调用函数获取字体颜色  
    draw.text((target_x, target_y), formatted_return, fill=fill_color, font=font)
    # 本周收益率
    target_x, target_y = 280, 551
    formatted_return = "{:.2%}".format(product_week_return_last4week)   # 格式化数值为小数点后两位   
    fill_color = get_fill_color(product_week_return_last4week)  # 调用函数获取字体颜色  
    draw.text((target_x, target_y), formatted_return, fill=fill_color, font=font)
    # 本周超额
    target_x, target_y = 280, 629
    formatted_return = "{:.2%}".format(product_week_return_excess_last4week)   # 格式化数值为小数点后两位   
    fill_color = get_fill_color(product_week_return_excess_last4week)  # 调用函数获取字体颜色  
    draw.text((target_x, target_y), formatted_return, fill=fill_color, font=font)

# 柱状图
    # 本周收益率
    rates = list_chanpin_info['本周收益率'].dropna().tolist()  # 提取非NaN的收益率数据
    if len(rates) > 156:
        rates = rates[-156:]  # 如果rates的长度超过156，只保留最后的156个值
    # 生成柱状图的x轴坐标  
    x = np.arange(len(rates))
    # 根据收益率的正负性设置柱状图的颜色  
    colors = ['green' if rate < 0 else 'red' for rate in rates]  
    fig_NavDraw = plt.figure(figsize=(6.5, 1.8), dpi=130)
    plt.bar(x, rates, color=colors) # 绘制柱状图
    plt.xticks([])  # 隐藏x轴 
    # 设置y轴格式为保留一位小数的百分比  
    ax = plt.gca()  
    ax.yaxis.set_major_formatter(FuncFormatter(multiply_by_100))
    ax.yaxis.set_major_locator(MaxNLocator(nbins=5))  # 设置y轴刻度数量为5
    # 清除图的边界
    plt.tight_layout() 
    # 将 Matplotlib 图形转换为 PIL 图像  
    fig_NavDraw = Render_Mpl_to_Pil(fig_NavDraw)  
    fig_width, fig_height = fig_NavDraw.size  
    fig_NavDraw = fig_NavDraw.convert('RGB')  
    # 手动修改需要添加图表的坐标点  
    fig_width_abscissa, fig_ordinate = 470, 230  
    image.paste(fig_NavDraw, (fig_width_abscissa, fig_ordinate, fig_width_abscissa + fig_width, fig_ordinate + fig_height))  
    plt.close()
    
    # 本周超额收益率
    rates = list_chanpin_info['本周超额'].dropna().tolist()
    # 生成柱状图的 x 轴坐标
    x = np.arange(len(rates))
    # 根据收益率的正负性设置柱状图的颜色
    colors = ['green' if rate < 0 else 'red' for rate in rates]
    
    # 创建带透明背景的图形
    fig_NavDraw = plt.figure(figsize=(6.5, 1.8), dpi=130)
    plt.bar(x, rates, color=colors)  # 绘制柱状图
    plt.xticks([])  # 隐藏 x 轴
    ax = plt.gca()
    ax.yaxis.set_major_formatter(FuncFormatter(multiply_by_100))
    ax.yaxis.set_major_locator(MaxNLocator(nbins=5))
    plt.tight_layout()
    
    # 将 Matplotlib 图形转换为 PIL 图像
    img_buf = io.BytesIO()
    fig_NavDraw.savefig(img_buf, format='png', transparent=True)
    img_buf.seek(0)
    fig_NavDraw = Image.open(img_buf).convert('RGBA')
    
    # 手动修改需要添加图表的坐标点
    fig_width_abscissa, fig_ordinate = 470, 460
    image.paste(fig_NavDraw, (fig_width_abscissa, fig_ordinate), fig_NavDraw)
    plt.close()
    

    # 初始化净值曲线图  
    fig_NavDraw = plt.figure(figsize=(5.5, 4), dpi=130)  
    # 添加曲线图
    cols_to_plot = ['成立以来收益率', '对标指数成立以来收益率', '成立以来超额（除法）']
    colors = ['#AB545A', '#676662', '#9A7549']  # 定义每条线的颜色
    alphas = [1, 1, 0.5]  # 定义面积图的透明度
    linewidths = [2, 1, 2]  # 定义线的宽度
    legends = ['运作以来收益率', '对标指数运作以来收益率', '运作以来超额（除法）']  # 添加图例名称
    
    for col, color, alpha, linewidth, legend in zip(cols_to_plot, colors, alphas, linewidths, legends):
        if col in list_chanpin_info.columns and not pd.isna(list_chanpin_info[col]).all():
            if color == '#9A7549':  # 如果是面积图，使用 fill_between 方法
                plt.fill_between(list_chanpin_info['净值日期'], list_chanpin_info[col], alpha=alpha, color=color, linewidth=0, label=legend)
            else:  # 如果是线图，使用 plot 方法
                plt.plot(list_chanpin_info['净值日期'], list_chanpin_info[col], label=legend, color=color, linewidth=linewidth)
    # 显示图例，并将图例放到左上角
    plt.legend(loc='upper left')
    # 使用PercentFormatter来格式化纵坐标轴刻度
    plt.gca().yaxis.set_major_formatter(PercentFormatter(xmax=1, decimals=1))  # xmax为最大刻度，decimals为小数位数
    plt.xticks(rotation=45)
    # 清除图的边界
    plt.tight_layout() 
    # 将 Matplotlib 图形转换为 PIL 图像  
    fig_NavDraw = Render_Mpl_to_Pil(fig_NavDraw)  
    fig_width, fig_height = fig_NavDraw.size  
    fig_NavDraw = fig_NavDraw.convert('RGB')  
    # 手动修改需要添加图表的坐标点  
    fig_width_abscissa, fig_ordinate = 40, 820  
    image.paste(fig_NavDraw, (fig_width_abscissa, fig_ordinate, fig_width_abscissa + fig_width, fig_ordinate + fig_height))  
    plt.close()
    
    
    # 初始化回撤曲线图
    fig_NavDraw = plt.figure(figsize=(5.5, 2.7), dpi=130)
    # 添加曲线图
    cols_to_plot = ['成立以来最大回撤', '成立以来最大回撤（超额）']
    colors = ['#AB545A', '#9A7549']  # 定义每条线的颜色，移除面积图的颜色定义
    alphas = [0.5, 0.7]  # 定义线条的透明度，'成立以来最大回撤'透明度较低，'成立以来最大回撤（超额）'透明度较高
    linewidths = [0, 0]  # 定义线的宽度，将它们设置为0，以创建面积图
    legends = ['运作以来最大回撤', '运作以来最大回撤（超额）']  # 修改图例名称
    
    for col, color, alpha, linewidth, legend in zip(cols_to_plot, colors, alphas, linewidths, legends):
        if col in list_chanpin_info.columns and not pd.isna(list_chanpin_info[col]).all():
            plt.fill_between(list_chanpin_info['净值日期'], list_chanpin_info[col], alpha=alpha, color=color, label=legend)
    
    # 调整图例的顺序
    handles, labels = plt.gca().get_legend_handles_labels()
    handles = [handles[1], handles[0]]  # 调整图例的顺序
    labels = [labels[1], labels[0]]
    plt.legend(handles, labels)
    plt.legend(handles, labels, loc='lower left')
    # 使用PercentFormatter来格式化纵坐标轴刻度
    plt.gca().yaxis.set_major_formatter(PercentFormatter(xmax=1, decimals=1))  # xmax为最大刻度，decimals为小数位数
    plt.xticks(rotation=45)
    # 清除图的边界
    plt.tight_layout()
    # 将 Matplotlib 图形转换为 PIL 图像
    fig_NavDraw = Render_Mpl_to_Pil(fig_NavDraw)
    fig_width, fig_height = fig_NavDraw.size
    fig_NavDraw = fig_NavDraw.convert('RGB')
    # 手动修改需要添加图表的坐标点
    fig_width_abscissa, fig_ordinate = 40, 1330
    image.paste(fig_NavDraw, (fig_width_abscissa, fig_ordinate, fig_width_abscissa + fig_width, fig_ordinate + fig_height))
    plt.close()
    
    
    # 获取当前收益指标
    product_data = summary_df[['策略', '成立日期', '成立以来收益率', '累计年化收益率']].loc[n]
    # 转换为datetime
    product_data['成立日期'] = pd.to_datetime(product_data['成立日期'])
    # 格式化
    product_data['成立日期'] = product_data['成立日期'].to_pydatetime().strftime('%Y年%m月%d日')
    # 百分比格式化
    product_data['成立以来收益率'] = "{:.2%}".format(product_data['成立以来收益率'])
    product_data['累计年化收益率'] ="{:.2%}".format(product_data['累计年化收益率'])
    # 转换为DataFrame
    product_df = product_data.to_frame().T
    product_df.rename(columns={'成立日期': '运作日期', '成立以来收益率': '运作以来收益率'}, inplace=True)
    # 创建一个新的Figure对象
    fig_table = plt.figure(figsize=(5.6, 1.3), dpi=130)
    plt.tight_layout()
    plt.axis('off')
    # 创建表格
    tb = plt.table(cellText=product_df.values, colLabels=product_df.columns, cellLoc="center", loc='center', bbox=[0, 0, 1, 0.75])
    plt.tight_layout()
    # 隐藏表格的坐标轴
    # 渲染表格
    tb.auto_set_font_size(False)
    tb.set_fontsize(12)
    tb.auto_set_column_width([i for i in range(len(product_data))])
    # 图表添加标题
    subtitle_font = FontProperties(fname=font_title_path)
    plt.text(0.0, 0.9, '基础指标', fontproperties=subtitle_font, fontsize=16, weight='bold', ha='left', va='center', color='black')
    # 获取渲染后的图像
    table_image = Render_Mpl_to_Pil(fig_table)
    table_image = table_image.convert('RGB')
    # 
    fig_width, fig_height = table_image.size
    # 手动修改需要添加图表的坐标点
    fig_width_abscissa, fig_ordinate = 750, 840
    # 粘贴图片
    image.paste(table_image, (fig_width_abscissa, fig_ordinate, fig_width_abscissa + fig_width, fig_ordinate + fig_height))
    plt.close()


    # 获取当前风险指标
    product_data = summary_df[['成立以来最大回撤', '周胜率', '夏普比率', '索提诺比率', '卡玛比率']].loc[n]
    # 格式化
    product_data['成立以来最大回撤'] = "{:.2%}".format(product_data['成立以来最大回撤'])
    product_data['周胜率'] = "{:.0%}".format(product_data['周胜率'])
    product_data['夏普比率'] ="{:.2f}".format(product_data['夏普比率'])
    product_data['索提诺比率'] ="{:.2f}".format(product_data['索提诺比率'])
    product_data['卡玛比率'] ="{:.2f}".format(product_data['卡玛比率'])
    # 转换为DataFrame
    product_df = product_data.to_frame().T
    # 创建一个新的Figure对象
    fig_table = plt.figure(figsize=(5.6, 1.3), dpi=130)
    plt.tight_layout()
    plt.axis('off')
    # 创建表格
    tb = plt.table(cellText=product_df.values, colLabels=product_df.columns, cellLoc="center", loc='center', bbox=[0, 0, 1, 0.75])
    plt.tight_layout()
    # 隐藏表格的坐标轴
    # 渲染表格
    tb.auto_set_font_size(False)
    tb.set_fontsize(12)
    tb.auto_set_column_width([i for i in range(len(product_data))])
    # 图表添加标题
    plt.text(0.0, 0.9, '风险指标', fontproperties=subtitle_font, fontsize=16, weight='bold', ha='left', va='center', color='black')
    # 获取渲染后的图像
    table_image = Render_Mpl_to_Pil(fig_table)
    table_image = table_image.convert('RGB')
    # 
    fig_width, fig_height = table_image.size
    # 手动修改需要添加图表的坐标点
    fig_width_abscissa, fig_ordinate = 750, 1000
    # 粘贴图片
    image.paste(table_image, (fig_width_abscissa, fig_ordinate, fig_width_abscissa + fig_width, fig_ordinate + fig_height))
    plt.close()
    
    
    # 超额指标
    product_data = summary_df[['成立以来超额（除法）', '成立以来最大回撤（超额）', '累计年化超额收益率', '超额周胜率', '超额夏普比率', '超额索提诺比率']].loc[n]
    # 百分比格式化
    product_data['成立以来超额（除法）'] = "{:.2%}".format(product_data['成立以来超额（除法）'])
    product_data['累计年化超额收益率'] = "{:.2%}".format(product_data['累计年化超额收益率'])
    product_data['超额周胜率'] ="{:.0%}".format(product_data['超额周胜率'])
    product_data['成立以来最大回撤（超额）'] ="{:.2%}".format(product_data['成立以来最大回撤（超额）'])
    # 小数点转化
    product_data['超额夏普比率'] = "{:.2f}".format(product_data['超额夏普比率'])
    product_data['超额索提诺比率'] ="{:.2f}".format(product_data['超额索提诺比率'])
    # 转换为DataFrame
    product_df = product_data.to_frame().T
    product_df.rename(columns={'成立以来超额（除法）': '运作以来超额', '累计年化超额收益率': '年化超额', '成立以来最大回撤（超额）': '超额最大回撤', '超额夏普比率': '超额夏普', '超额索提诺比率': '超额索提诺'}, inplace=True)
    # 创建一个新的Figure对象
    fig_table = plt.figure(figsize=(5.6, 1.3), dpi=130)
    plt.tight_layout()
    plt.axis('off')
    # 创建表格
    tb = plt.table(cellText=product_df.values, colLabels=product_df.columns, cellLoc="center", loc='center', bbox=[0, 0, 1, 0.75])
    plt.tight_layout()
    # 隐藏表格的坐标轴
    # 渲染表格
    tb.auto_set_font_size(False)
    tb.set_fontsize(12)
    tb.auto_set_column_width([i for i in range(len(product_data))])
    # 图表添加标题
    plt.text(0.0, 0.9, '超额指标', fontproperties=subtitle_font, fontsize=16, weight='bold', ha='left', va='center', color='black')
    # 获取渲染后的图像
    table_image = Render_Mpl_to_Pil(fig_table)
    table_image = table_image.convert('RGB')
    # 
    fig_width, fig_height = table_image.size
    # 手动修改需要添加图表的坐标点
    fig_width_abscissa, fig_ordinate = 750, 1160
    # 粘贴图片
    image.paste(table_image, (fig_width_abscissa, fig_ordinate, fig_width_abscissa + fig_width, fig_ordinate + fig_height))
    plt.close()
    
    # 区间收益列名和行名
    column_names = ['区间超额', '区间超额回撤', '超额夏普', '超额卡玛比']
    row_names = ['近三月', '近六月', '近一年', '近两年', '近三年']
    # 创建一个空的数据框，稍后将填充数据
    product_df = pd.DataFrame(columns=column_names, index=row_names)
    # 年化参数数据数组
    values = [1, 1, 1, 2, 3]
    # 循环填充数据
    for i, row_name in enumerate(row_names):
        # 在此处填充每个单元格的数据
        # 例如：product_return_3months, max_drawdown_3months, sharpe_ratio_3months, sortino_ratio_3months
        # 根据您的数据源来获取相应的值，假设这些值在不同的变量中
        interval_return = summary_df.loc[n, row_name + '超额（除法）']
        if math.isnan(interval_return):
            continue
        # 提取相应区间的收益率数据
        interval_returns = list_chanpin_info.loc[~list_chanpin_info[row_name + '收益率'].isna(), '本周超额']
        # 计算波动率
        interval_volatility = np.std(interval_returns) * math.sqrt(52)

        interval_max_drawdown = summary_df.loc[n, row_name + '最大回撤（超额）']
        interval_sharpe_ratio = ((1+interval_return)**(1/values[i])-1)/interval_volatility
        interval_sortino_ratio = -((1+interval_return)**(1/values[i])-1)/interval_max_drawdown
        data = [interval_return, interval_max_drawdown, interval_sharpe_ratio, interval_sortino_ratio]
        # 将数据添加到数据框
        product_df.loc[row_name] = data
        product_df.loc[row_name,'区间超额'] = "{:.2%}".format(product_df.loc[row_name]['区间超额'])
        product_df.loc[row_name, '区间超额回撤'] ="{:.2%}".format(product_df.loc[row_name]['区间超额回撤'])
        product_df.loc[row_name, '超额夏普'] = "{:.2f}".format(product_df.loc[row_name]['超额夏普'])
        product_df.loc[row_name, '超额卡玛比'] ="{:.2f}".format(product_df.loc[row_name]['超额卡玛比'])
    product_df.insert(0, '统计区间', row_names)
    product_df = product_df.dropna(subset=['区间超额'])
    product_df = product_df.fillna('')  # 将所有 NaN 值替代为空值
    if not product_df.empty:
        table_height = (len(product_df) +1)*0.52 + 0.1  # 每行高度为0.2
        # 创建一个新的Figure对象
        fig_table = plt.figure(figsize=(5.6, table_height), dpi=130)
        plt.tight_layout()
        plt.axis('off')
        # 创建表格
        tb = plt.table(cellText=product_df.values, colLabels=product_df.columns, cellLoc="center", loc='center', bbox=[0, 0, 1, 0.9-(5-len(product_df))*0.025])
        plt.tight_layout()
        # 隐藏表格的坐标轴
        # 渲染表格
        tb.auto_set_font_size(False)
        tb.set_fontsize(12)
        tb.auto_set_column_width([i for i in range(len(column_names))])
        # 图表添加标题
        plt.text(0.0, 0.95, '区间超额指标', fontproperties=subtitle_font, fontsize=16, weight='bold', ha='left', va='center', color='black')
        # 获取渲染后的图像
        table_image = Render_Mpl_to_Pil(fig_table)
        table_image = table_image.convert('RGB')
        # 手动修改需要添加图表的坐标点
        fig_width_abscissa, fig_ordinate = 750, 1310
        # 粘贴图片
        image.paste(table_image, (fig_width_abscissa, fig_ordinate, fig_width_abscissa + table_image.width, fig_ordinate + table_image.height))
        plt.close()
    
    # 获取最近四周净值数据
    nav_df = list_chanpin_info.iloc[-5:, list_chanpin_info.columns.get_indexer(['净值日期', '单位净值', '复权净值'])].reset_index(drop=True)
    for x in range(1, len(nav_df)):
        nav_df.loc[x, '涨跌幅'] = nav_df.loc[x, '复权净值'] / nav_df.loc[x-1, '复权净值'] - 1
    nav_df['净值日期'] = pd.to_datetime(nav_df['净值日期']).dt.strftime('%Y/%m/%d')
    nav_df['涨跌幅'] = (nav_df['涨跌幅'] * 100).round(2).astype(str) + '%'
    # 删除第一行
    nav_df = nav_df.drop(0).reset_index(drop=True)
    # 创建一个新的Figure对象
    fig_table = plt.figure(figsize=(4.2, 2.3), dpi=130)
    plt.tight_layout()
    plt.axis('off')
    # 创建表格
    tb = plt.table(cellText=nav_df.values, colLabels=nav_df.columns, cellLoc="center", loc='center', bbox=[0, 0, 1, 0.87])
    # 渲染表格
    tb.auto_set_font_size(False)
    tb.set_fontsize(12)
    # 自适应宽度
    tb.auto_set_column_width([i for i in range(len(nav_df.columns))])
    plt.tight_layout()
    # 图表添加标题
    plt.text(0.0, 0.96, '近四周净值', fontproperties=subtitle_font, fontsize=16, weight='bold', ha='left', va='center', color='black')
    # 获取渲染后的图像
    table_image = Render_Mpl_to_Pil(fig_table)
    # 转换为RGBA模式
    table_image = table_image.convert('RGBA')
    # 手动修改需要添加图表的坐标点
    fig_width_abscissa, fig_ordinate = 50, 1800
    # 粘贴图片
    image.paste(table_image, (fig_width_abscissa, fig_ordinate), table_image)
    plt.close()

    # 创建临时文件
    with tempfile.NamedTemporaryFile(delete=False, suffix='.png') as temp_file:
        temp_file_path = temp_file.name
        # 保存图像到临时文件
        image.save(temp_file_path)

    # 上传临时文件到云存储桶的指定路径
    key = f'{report_save_path}{product_full_name}.png'
    with open(temp_file_path, 'rb') as f:
        cos_client.put_object(
            Bucket=bucket_name,
            Body=f,
            Key=key
        )

    # 删除临时文件
    os.remove(temp_file_path)

    print(product_full_name + '生成：'+ '{:.1%}'.format((n+1)/len(summary_df)))
    summary_df.loc[n, '是否更新周报'] = '是'

summary_df.to_sql(name = '所有产品计算指标汇总表', con=engine, if_exists='replace', index=False)

# # 更新每周各策略综合净值周报pdf
# sheet_list = ['中性策略指数', '300指增策略指数', '500指增策略指数', '1000指增策略指数', '空气指增策略指数', '中短周期策略指数', '中周期策略指数', '中长周期策略指数', '100亿元以上策略指数', '50-100亿元策略指数', '50亿元以下策略指数']
# # 分策略绘制曲线以及绩效表格
# for celue_index in sheet_list:
#     celue_name = celue_index.strip('策略指数')
#     period_word = '近一年'
#     zhuguan_type = ''
#     if '周期' in celue_name:
#         list_celue = list_cta.copy()
#         save_path = 'cta策略图表'
#         key_withdraw_data_word = '最大回撤'
#         columns = [period_word + '收益率', period_word + key_withdraw_data_word]
#     elif '亿元' in celue_name:
#         zhuguan_type = '主观多头'
#         list_celue = list_zhuguan.copy()
#         save_path = '主观策略图表'
#         key_yield_data_word = '超额（除法）'
#         key_withdraw_data_word = '最大回撤（超额）'
#         columns = [period_word + '收益率', period_word + key_yield_data_word, '对标指数' + period_word + '收益率', period_word + key_withdraw_data_word]
#     else:
#         list_celue = list_alpha.copy()
#         save_path = 'alpha策略图表'
#         if '中性' in celue_name:
#             key_withdraw_data_word = '最大回撤'
#             columns = [period_word + '收益率', period_word + key_withdraw_data_word]
#         else:
#             key_yield_data_word = '超额（除法）'
#             key_withdraw_data_word = '最大回撤（超额）'
#             columns = [period_word + '收益率', period_word + key_yield_data_word, '对标指数' + period_word + '收益率', period_word + key_withdraw_data_word]

#     if '亿元' in celue_name:
#         celue_type = zhuguan_type
#         list_celue = list_zhuguan.copy()
#         list_celue = list_celue.loc[list_celue['管理人规模'] == celue_name].reset_index(drop=True)
#     else:
#         list_celue = list_celue.loc[list_celue['策略'] == celue_name].reset_index(drop=True)
    
#     list_celue = list_celue.sort_values(by='近四周收益率', ascending=False).reset_index(drop=True)
#     list_celue = list_celue.dropna(subset=['近一周收益率'])
    
#     if '亿元' not in celue_name:
#         totol_report_image_path = '私募业绩统计/' + save_path + '/' + celue_name + '综合表(' + datetime.strftime(time_last_Friday, '%m.%d') + ').png'
#     else:
#         totol_report_image_path = '私募业绩统计/' + save_path + '/' + celue_name + '综合表(' + datetime.strftime(time_last_Friday, '%m.%d') + ').png'
    
#     pdf_canvas = canvas.Canvas('私募业绩统计/' + zhuguan_type + celue_name + '管理人净值周报(' + datetime.strftime(time_last_Friday, '%m.%d') + ').pdf', pagesize=letter)
    
#     page_width, page_height = letter
#     temp_df = pd.read_sql_query(f"SELECT * FROM `{celue_index}`", engine).set_index('净值日期')
#     # 绘制首页策略图表
#     if '指增' not in celue_name and '亿元' not in celue_name:
#         temp_df = temp_df.loc[:, columns].dropna()
#         fig, (ax, ax2) = plt.subplots(2, 1, figsize=(4, 2.5), dpi=200, gridspec_kw={'height_ratios': [4, 1]})
#         ax.set_frame_on(True)
#         ax.yaxis.set_major_formatter(mtick.PercentFormatter(1))
#         ax.yaxis.set_major_locator(MaxNLocator(nbins=8))
#         ax.xaxis.set_major_formatter(mdates.DateFormatter('%Y-%m'))
#         ax.set_facecolor('lightgrey')
#         ax.grid(axis='both', c='white') 
#         ax.tick_params(labelsize='6.5')
#         ax.tick_params(axis='x', labelsize='6.5', rotation=45)
        
#         x = temp_df.index
#         ax.minorticks_off()        
#         ax.plot(x, temp_df[period_word + '收益率'], color='#2A4C65', label=period_word + '收益率')
        
#         # 找到最大回撤区间
#         max_drawdown_end = temp_df.loc[temp_df[period_word + '最大回撤'].idxmin()].name
#         try:
#             max_drawdown_start = temp_df[temp_df.index <= max_drawdown_end].loc[temp_df[period_word + '最大回撤'] == 0].index[-1]
#         except:
#             max_drawdown_start = temp_df.index[0]
        
#         # 在最大回撤区间内绘制红色部分
#         ax.plot(x[(x >= max_drawdown_start) & (x <= max_drawdown_end)],
#                 temp_df[period_word + '收益率'][(x >= max_drawdown_start) & (x <= max_drawdown_end)],
#                 color='#B95756', linewidth=1.6)  # 根据需要调整线宽
#         # 在起点和终点加上标志
#         ax.scatter([max_drawdown_start, max_drawdown_end], 
#                    [temp_df.loc[max_drawdown_start, period_word + '收益率'], temp_df.loc[max_drawdown_end, period_word + '收益率']], 
#                    color='#B95756', marker='*', s=10, zorder=3)  # 根据需要调整标记点的大小
#         # 在y=0轴以下的部分填充淡绿色
#         ax.fill_between(x, 0, temp_df[period_word + '收益率'], where=(temp_df[period_word + '收益率'] <= 0), color='#678F74', alpha=1, zorder=-1)
#         ax.legend()
#         # 关闭子图的白色网格线
#         ax.grid(False)
        
#         celue_data_info = celue_total.loc[celue_total['产品名称'] == celue_index, ['产品名称', '近一周收益率', '近四周收益率', period_word + '收益率', period_word + key_withdraw_data_word]]
#         # 将celue_data_info的信息显示在ax的下方，以表格的形式展示
#         table_data = celue_data_info.values
#         table_columns = celue_data_info.columns
#         table_cell_text = []
#         for row in table_data:
#             table_cell_text.append([f'{cell:.2%}' if isinstance(cell, float) else cell for cell in row])
        
#         table = ax2.table(cellText=table_cell_text,
#                          colLabels=table_columns,
#                          cellLoc = 'center',
#                          loc='bottom',
#                          bbox=[0, -0.8, 1.0, 1])  # 调整bbox的值以适应表格的位置
#         ax2.axis('off')  # 隐藏坐标轴
#         # 设置表格字体大小
#         table.auto_set_font_size(False)
#         table.set_fontsize(5)
#         # 保存为内存中的BytesIO对象
#         fig_buffer = BytesIO()
#         fig.savefig(fig_buffer, format='png', bbox_inches='tight', pad_inches=0.1)
#         plt.close(fig)
#         # BytesIO对象的seek方法将文件指针移回开头
#         fig_buffer.seek(0)
#         # 创建Image对象
#         fig_image = Image.open(fig_buffer)
    
#     else:
#         temp_df = temp_df.loc[:, columns].dropna()
#         fig, (ax, ax2) = plt.subplots(2, 1, figsize=(4, 2.5), dpi=200, gridspec_kw={'height_ratios': [4, 1]})
#         ax.set_frame_on(True)
#         ax.yaxis.set_major_formatter(mtick.PercentFormatter(1))
#         ax.yaxis.set_major_locator(MaxNLocator(nbins=8))
#         ax.xaxis.set_major_formatter(mdates.DateFormatter('%Y-%m'))
#         ax.set_facecolor('lightgrey')
#         ax.grid(axis='both', c='white') 
#         ax.tick_params(labelsize='6.5')
#         ax.tick_params(axis='x', labelsize='6.5', rotation=45)
        
#         x = temp_df.index
#         ax.minorticks_off()        
#         ax.plot(x, temp_df[period_word + '收益率'], color='#2A4C65', label=period_word + '收益率')
#         ax.plot(x, temp_df[period_word + key_yield_data_word], color='#F77E66', label=period_word + key_yield_data_word)
#         ax.plot(x, temp_df['对标指数' + period_word + '收益率'], color='#53565C', label='对标指数' + period_word + '收益率')
#         # 找到超额最大回撤区间
#         max_drawdown_end = temp_df.loc[temp_df[period_word + key_withdraw_data_word].idxmin()].name
#         try:
#             max_drawdown_start = temp_df[temp_df.index <= max_drawdown_end].loc[temp_df[period_word + key_withdraw_data_word] == 0].index[-1]
#         except:
#             max_drawdown_start = temp_df.index[0]
        
#         # 在最大回撤区间内绘制红色部分
#         ax.plot(x[(x >= max_drawdown_start) & (x <= max_drawdown_end)],
#                 temp_df[period_word + key_yield_data_word][(x >= max_drawdown_start) & (x <= max_drawdown_end)],
#                 color='#B95756', linewidth=1.6)  # 根据需要调整线宽
#         # 在起点和终点加上标志
#         ax.scatter([max_drawdown_start, max_drawdown_end], 
#                    [temp_df.loc[max_drawdown_start, period_word + key_yield_data_word], temp_df.loc[max_drawdown_end, period_word + key_yield_data_word]], 
#                    color='#B95756', marker='*', s=10, zorder=3)  # 根据需要调整标记点的大小
#         # 在y=0轴以下的部分填充淡绿色
#         ax.fill_between(x, 0, temp_df[period_word + '收益率'], where=(temp_df[period_word + '收益率'] <= 0), color='#678F74', alpha=1, zorder=-1)
#         # 添加图例，放在左下角
#         ax.legend(loc='lower left', fontsize='6')
#         # 关闭子图的白色网格线
#         ax.grid(False)
        
#         celue_data_info = celue_total.loc[celue_total['产品名称'] == celue_index, ['产品名称', '近一周超额（除法）', '近四周超额（除法）', period_word + '超额（除法）', period_word + key_withdraw_data_word]]
#         if '亿元' in celue_name:
#             celue_data_info = celue_total.loc[celue_total['产品名称'] == celue_index, ['产品名称', '近一周收益率', '近四周收益率', period_word + '收益率', period_word + '最大回撤']]
        
#         # 将celue_data_info的信息显示在ax的下方，以表格的形式展示
#         table_data = celue_data_info.values
#         table_columns = celue_data_info.columns
#         table_columns = [col.replace('（除法）', '') if isinstance(col, str) else col for col in table_columns]
#         table_columns = [col.replace('最大回撤（超额）', '超额回撤') if isinstance(col, str) else col for col in table_columns]
#         table_cell_text = []
#         for row in table_data:
#             table_cell_text.append([f'{cell:.2%}' if isinstance(cell, float) else cell for cell in row])
        
#         table = ax2.table(cellText=table_cell_text,
#                          colLabels=table_columns,
#                          cellLoc = 'center',
#                          loc='bottom',
#                          bbox=[0, -0.8, 1.0, 1])  # 调整bbox的值以适应表格的位置
#         ax2.axis('off')  # 隐藏坐标轴
#         # 设置表格字体大小
#         table.auto_set_font_size(False)
#         table.set_fontsize(5)
#         # 保存为内存中的BytesIO对象
#         fig_buffer = BytesIO()
#         fig.savefig(fig_buffer, format='png', bbox_inches='tight', pad_inches=0.1)
#         plt.close(fig)
#         # BytesIO对象的seek方法将文件指针移回开头
#         fig_buffer.seek(0)
#         # 创建Image对象
#         fig_image = Image.open(fig_buffer)
    
#     if fig_image.mode != 'RGB':
#         fig_image = fig_image.convert('RGB')
#     # 获取图片尺寸
#     img_width, img_height = fig_image.size
    
#     # 计算图片在PDF页面上的位置和尺寸
#     # 这里我们将图片缩放以适应PDF页面
#     aspect_ratio = min(page_width / img_width, page_height / img_height)
#     new_width = img_width * aspect_ratio
#     new_height = img_height * aspect_ratio
#     x = (page_width - new_width) / 2
#     y = (page_height - new_height) / 2
#     pdf_canvas.drawInlineImage(fig_image, x, y, new_width, new_height)
#     # 添加一个新的PDF页面
#     pdf_canvas.showPage()
    
    
#     image = Image.open(totol_report_image_path)
#     if image.mode != 'RGB':
#         image = image.convert('RGB')
#     # 获取图片尺寸
#     img_width, img_height = image.size
    
#     # 计算图片在PDF页面上的位置和尺寸
#     # 这里我们将图片缩放以适应PDF页面
#     aspect_ratio = min(page_width / img_width, page_height / img_height)
#     new_width = img_width * aspect_ratio
#     new_height = img_height * aspect_ratio
#     x = (page_width - new_width) / 2
#     y = (page_height - new_height) / 2
#     # 将图片添加到PDF页面
#     pdf_canvas.drawInlineImage(totol_report_image_path, x, y, new_width, new_height)
#     # 添加一个新的PDF页面
#     pdf_canvas.showPage()
    
#     for chanpin_name in list_celue['产品名称']:
#         image_path = '私募业绩统计/' + save_path + '/' + '产品净值周报/' + chanpin_name + '.png'
#         try:
#             image = Image.open(image_path)
#         except:
#             continue
#         if image.mode != 'RGB':
#             image = image.convert('RGB')
#         # 获取图片尺寸
#         img_width, img_height = image.size
#         # 计算图片在PDF页面上的位置和尺寸
#         # 这里我们将图片缩放以适应PDF页面
#         aspect_ratio = min(page_width / img_width, page_height / img_height)
#         new_width = img_width * aspect_ratio
#         new_height = img_height * aspect_ratio
#         x = (page_width - new_width) / 2
#         y = (page_height - new_height) / 2
#         # 将图片添加到PDF页面
#         pdf_canvas.drawInlineImage(image_path, x, y, new_width, new_height)
#         # 添加一个新的PDF页面
#         pdf_canvas.showPage()
            
#     pdf_canvas.save()
