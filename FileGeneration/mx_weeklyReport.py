import sys
sys.path.append('/home/ubuntu/market/dc-66-python-2.0/SelfModule')

from urllib.parse import unquote
from PIL import Image, ImageDraw, ImageFont
import tempfile
import mysql.connector
from sqlalchemy import create_engine
import warnings
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from selfdefinfunc import Render_Mpl_to_Pil  # 自定义封装函数
from tradedate import *  # 自定义封装函数
from matplotlib.font_manager import FontProperties
from matplotlib.ticker import PercentFormatter, FuncFormatter, MaxNLocator
from chinese_calendar import is_workday
import os
import io
import math
from pdf2image import convert_from_path 
from api_log import create_cos_client
import logging

# 设置日志
logging.basicConfig(filename='/home/ubuntu/market/dc-66-python-2.0/execution.log', 
                    level=logging.INFO, 
                    format='%(asctime)s - %(levelname)s - %(message)s', 
                    filemode='a')  # 设置为追加模式

# 设置日志级别为 WARNING
logging.getLogger('qcloud_cos').setLevel(logging.WARNING)

def get_fill_color(value):  
    if value < 0:  
        return 'green'  
    else:  
        return 'red'
# 定义一个函数来将刻度值乘以100  
def multiply_by_100(x, pos):  
    return '{:.1f}%'.format(x * 100)  

def style_table(tb, product_df):
    # 设置表格样式
    row_colors = ['white', '#DFEBF7'] * len(product_df)
    
    for i, key in enumerate(tb.get_celld().keys()):
        cell = tb[key]
        cell.PAD = 0.05  # 调整单元格内边距
        cell.set_facecolor(row_colors[key[0]])
        
        # 设置边框宽度
        if key[0] == 0:  # 第一行（表头）
            cell.set_linewidth(0)  # 设置边框宽度为1
        elif key[0] == len(product_df):  # 最后一行
            cell.set_linewidth(0)  # 设置边框宽度为1
        else:
            cell.set_linewidth(0)  # 其他单元格没有边框
    
    # 获取表格的边界框（bounding box）
    bbox = tb.get_window_extent(plt.gcf().canvas.get_renderer())
    x0, y0, width, height = bbox.x0, bbox.y0, bbox.width, bbox.height
    
    # 将 bbox 的位置和尺寸转换为坐标轴中的相对值
    x0_rel, y0_rel = plt.gca().transData.inverted().transform((x0, y0))
    x1_rel, y1_rel = plt.gca().transData.inverted().transform((x0 + width, y0 + height))
    
    # 手动绘制上下边框
    plt.gca().add_line(plt.Line2D([x0_rel, x1_rel], [y1_rel, y1_rel], color='black', linewidth=1))  # 上边框
    plt.gca().add_line(plt.Line2D([x0_rel, x1_rel], [y0_rel, y0_rel], color='black', linewidth=2))  # 下边框

plt.rcParams['font.sans-serif'] = ['SimHei']  # 用来正常显示中文标签
plt.rcParams['axes.unicode_minus'] = False  # 用来正常显示负号
warnings.filterwarnings('ignore')

# 获取当前文件夹的父文件夹路径
parent_dir = os.path.dirname('/home/ubuntu/market/dc-66-python-2.0/')
# 构建目标文件夹路径
target_dir = os.path.join(parent_dir, "DocTemplate")
# 修改系统默认文件夹路径
os.chdir(target_dir)

# 环境变量读取数据库秘钥信息
host = os.environ.get('MYSQL_HOST')   
user = os.environ.get('MYSQL_USERNAME')  
password = os.environ.get('MYSQL_PASSWORD')
port = os.environ.get('MYSQL_PORT')
# 数据库存储操作
cnx = mysql.connector.connect(user=user, password=password,
                              host=host,  port=port, database='鸣熙产品净值数据库')
engine = create_engine(f'mysql+mysqlconnector://{user}:{password}@{host}:{port}/鸣熙产品净值数据库')
cursor = cnx.cursor()

secret_id = os.environ.get('COS_SECRET_ID')  # 用户的 SecretId
secret_key = os.environ.get('COS_SECRET_KEY')  # 用户的 SecretKey
region = 'ap-guangzhou'                      # 桶归属的 region
token = None                                # 临时密钥的 token
scheme = 'https'                            # 访问 COS 的协议
bucket_name = 'xwabstore-1320558538'

cos_client = create_cos_client(secret_id, secret_key, region, token, scheme)

# 字体路径初定义
font_title_path = "素材/SourceHanSansCN-Heavy.otf"
font_subtitle_path = "素材/SourceHanSansCN-Normal.otf"

# 模版路径
pdf_moban_path = 'chanpin/'
report_save_path = '鸣熙定期披露报告/产品净值周报/'

list_celue = pd.read_sql_query("SELECT * FROM %s" % '产品策略对照表', engine)

summary_df = pd.read_sql_query("SELECT * FROM %s" % '产品净值周报汇总表', engine)
summary_df = summary_df[summary_df['策略类型'].str.contains('中性|CTA|其他', regex=True)].reset_index(drop=True)
chanpin_elements_df = pd.read_sql_query("SELECT * FROM %s" % '所有产品清洗后要素表', engine)

# 读取所有产品规模表中净值日期字段小于等于 time_last_month_time 的行
all_chanpin_aum = pd.read_sql_query("SELECT * FROM 所有产品规模表 WHERE 净值日期 <= %s", engine, params=(time_last_Friday_time,))

# 从目标路径提取报告模版
initial_image = convert_from_path(pdf_moban_path + '鸣熙普通产品周报模版.pdf')
# image.show()

for n in range(len(summary_df)):
    if summary_df.loc[n, '最新净值日期'] < pd.Timestamp(time_last_Friday_time):
        continue
    # 产品全称匹配
    product_full_name = list_celue.loc[list_celue['基金简称'] == summary_df.loc[n, '产品名称'], '基金全称'].values[0]
    # 提取产品净值信息
    table_name = product_full_name + '净值周报'
    # 构建 SQL 查询，并在表名周围加上反引号
    query = f"SELECT * FROM `{table_name}`"
    list_chanpin_info = pd.read_sql_query(query, engine)
    list_chanpin_info = list_chanpin_info.loc[list_chanpin_info['净值日期'] <= time_last_Friday_time]
    # 这里保证全部都是周频净值
    list_chanpin_info = list_chanpin_info.loc[list_chanpin_info['是否周末净值'] == '是'].reset_index().drop('index', axis=1)
    
    # 按照 '净值日期' 合并 list_chanpin_info 和 all_chanpin_aum
    list_chanpin_info = pd.merge(list_chanpin_info, all_chanpin_aum[['净值日期', product_full_name]], on='净值日期', how='left').rename(columns={product_full_name: '产品规模'})
    list_chanpin_info['产品规模'] = list_chanpin_info['产品规模'].fillna(method='bfill')
    # 将 '产品规模' 列转换为以万为单位的整数
    list_chanpin_info['产品规模'] = list_chanpin_info['产品规模'].str.replace(',', '').astype(float) / 10000
    list_chanpin_info['产品规模'] = list_chanpin_info['产品规模'].astype(int)
    
    list_chanpin_info_val = pd.read_sql_query("SELECT * FROM `%s`" % (product_full_name + '(波动率)'), engine)
    product_name = summary_df.loc[n, '产品名称'] + '净值周报'
    latest_date = list_chanpin_info['净值日期'].iloc[-1].strftime("%Y年%m月%d日")
    product_unit_nav = list_chanpin_info['单位净值'].iloc[-1]
    product_cum_nav = list_chanpin_info['累计净值'].iloc[-1]
    product_return_week = summary_df.loc[n, '本周收益率']
    product_return_last4week = summary_df.loc[n, '近四周收益率']
    product_return_last3month = summary_df.loc[n, '近三月收益率']
    product_return_ytd = summary_df.loc[n, '今年以来收益率']
    
    image = initial_image[0].copy()
    # 创建Image对象
    draw = ImageDraw.Draw(image)
    # 产品标题
    # 确定字体
    font = ImageFont.truetype(font_title_path, size=48)  # 选择适当的字体大小
    subtitle_font = FontProperties(fname=font_title_path)
    # 计算产品标题文本宽度
    text_width, text_height = font.font.getsize(product_name)[0]
    # 计算产品标题文本居中位置
    image_width, image_height = image.size
    x_centered = (image_width - text_width) / 2
    draw.text((x_centered, 170), product_name, fill='black', font=font)
    
    # 净值时间
    # 绘制名称
    font = ImageFont.truetype(font_title_path, size=32)  # 选择适当的字体大小
    font_sub = ImageFont.truetype(font_subtitle_path, size=24)  # 选择适当的字体大小
    # 计算文本宽度
    text_width, text_height = font.font.getsize(latest_date)[0]

    draw.text((120, 250), latest_date, fill='black', font=font)

# 左侧基础信息
    # 单位净值
    target_x, target_y = 290, 318
    draw.text((target_x, target_y), str(product_unit_nav), fill='#262626', font=font)
    # 累计净值
    target_x, target_y = 303, 350
    draw.text((target_x, target_y), '(' + str(product_cum_nav) + ')', fill='#262626', font=font_sub)
    # 本周收益率
    target_x, target_y = 290, 395
    formatted_return = "{:.2%}".format(product_return_week)   # 格式化数值为小数点后两位   
    fill_color = get_fill_color(product_return_week)  # 调用函数获取字体颜色  
    draw.text((target_x, target_y), formatted_return, fill=fill_color, font=font)
    # 四周收益率
    target_x, target_y = 290, 473
    formatted_return = "{:.2%}".format(product_return_last4week)   # 格式化数值为小数点后两位   
    fill_color = get_fill_color(product_return_last4week)  # 调用函数获取字体颜色  
    draw.text((target_x, target_y), formatted_return, fill=fill_color, font=font)
    # 近三月收益率
    if not math.isnan(product_return_last3month):
        target_x, target_y = 290, 551
        formatted_return = "{:.2%}".format(product_return_last3month)   # 格式化数值为小数点后两位   
        fill_color = get_fill_color(product_return_last3month)  # 调用函数获取字体颜色  
        draw.text((target_x, target_y), formatted_return, fill=fill_color, font=font)

    # 今年以来收益率
    if not math.isnan(product_return_ytd):
        target_x, target_y = 290, 629
        formatted_return = "{:.2%}".format(product_return_ytd)   # 格式化数值为小数点后两位   
        fill_color = get_fill_color(product_return_ytd)  # 调用函数获取字体颜色  
        draw.text((target_x, target_y), formatted_return, fill=fill_color, font=font)
    
# 柱状图
    # 本周收益率
    rates = list_chanpin_info['本周收益率'].dropna().tolist()
    if len(rates) > 156:
        rates = rates[-156:]
    
    # 生成柱状图的 x 轴坐标
    x = np.arange(len(rates))
    # 根据收益率的正负性设置柱状图的颜色
    colors = ['green' if rate < 0 else 'red' for rate in rates]
    
    # 创建带透明背景的图形
    fig_NavDraw = plt.figure(figsize=(6.9, 1.8), dpi=130)
    plt.bar(x, rates, color=colors)  # 绘制柱状图
    plt.xticks([])  # 隐藏 x 轴
    ax = plt.gca()
    ax.yaxis.set_major_formatter(FuncFormatter(multiply_by_100))
    ax.yaxis.set_major_locator(MaxNLocator(nbins=5))
    # 去除图表的边框，只保留坐标轴
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    ax.spines['bottom'].set_visible(False)  # 保留底部边框（即 x 轴）
    ax.spines['left'].set_visible(True)  # 保留左侧边框（即 y 轴）
    # 设置 x 轴为零轴
    ax.axhline(0, color='black', linewidth=1)
    
    plt.tight_layout()
    
    # 将 Matplotlib 图形转换为 PIL 图像
    img_buf = io.BytesIO()
    fig_NavDraw.savefig(img_buf, format='png', transparent=True)
    img_buf.seek(0)
    fig_NavDraw = Image.open(img_buf).convert('RGBA')
    
    # 手动修改需要添加图表的坐标点
    fig_width_abscissa, fig_ordinate = 470, 460
    image.paste(fig_NavDraw, (fig_width_abscissa, fig_ordinate), fig_NavDraw)
    plt.close()
    
    # 列名和坐标点信息
    columns_to_plot = {
        '近四周收益率': (480, 290),
        '近三月收益率': (790, 290),
        '今年以来收益率': (1100, 290)
    }
    for column, (abscissa, ordinate) in columns_to_plot.items():
        if column in list_chanpin_info.columns and not list_chanpin_info[column].isna().all():
            fig_NavDraw = plt.figure(figsize=(2.4, 1.4), dpi=130)
            plt.plot(list_chanpin_info['净值日期'], list_chanpin_info[column], color='#AB545A', linewidth=2)
            plt.xticks([])
            # 设置y轴刻度为最少三个
            y_locator = MaxNLocator(nbins=3)
            plt.gca().yaxis.set_major_locator(y_locator)
            plt.gca().yaxis.set_major_formatter(PercentFormatter(xmax=1, decimals=1))  # 格式化纵坐标刻度
            # 去除图表的边框，只保留坐标轴
            ax = plt.gca()
            ax.spines['top'].set_visible(False)
            ax.spines['right'].set_visible(False)
            ax.spines['bottom'].set_visible(False)  # 保留底部边框（即 x 轴）
            ax.spines['left'].set_visible(True)  # 保留左侧边框（即 y 轴）
            # 设置 x 轴为零轴
            ax.axhline(0, color='black', linewidth=1)
            
            plt.tight_layout()
            # 将 Matplotlib 图形转换为 PIL 图像
            fig_NavDraw = Render_Mpl_to_Pil(fig_NavDraw)
            fig_width, fig_height = fig_NavDraw.size
            fig_NavDraw = fig_NavDraw.convert('RGB')
            # 手动修改需要添加图表的坐标点
            fig_width_abscissa, fig_ordinate = abscissa, ordinate  # 根据坐标点信息修改坐标
            image.paste(fig_NavDraw, (fig_width_abscissa, fig_ordinate, fig_width_abscissa + fig_width, fig_ordinate + fig_height))
            plt.close()  # 清除当前图以便绘制下一张图
    
    # 初始化净值曲线图  
    fig_NavDraw = plt.figure(figsize=(5.5, 4), dpi=130)
    # 提取'成立以来收益率'列
    if '成立以来收益率' in list_chanpin_info.columns and not list_chanpin_info['成立以来收益率'].isna().all():
        plt.plot(list_chanpin_info['净值日期'], list_chanpin_info['成立以来收益率'], label='运作以来收益率', color='#AB545A', linewidth=2)
    # 显示图例，并将图例放到左上角
    plt.legend(loc='upper left')
    # 使用PercentFormatter来格式化纵坐标轴刻度
    plt.gca().yaxis.set_major_formatter(PercentFormatter(xmax=1, decimals=1))  # xmax为最大刻度，decimals为小数位数
    plt.xticks(rotation=45)
    
    # 添加产品规模曲线
    ax1 = plt.gca()
    ax2 = ax1.twinx()
    product_scale_color = '#BCA295'  # 较浅的颜色
    product_scale_alpha = 0.5  # 透明度
    ax2.plot(list_chanpin_info['净值日期'], list_chanpin_info['产品规模'], label='产品规模', color=product_scale_color, alpha=product_scale_alpha, linewidth=1.5, linestyle='--')
    ax2.get_yaxis().set_visible(False)  # 隐藏右轴
    # 在最后一个数据点显示产品规模的值
    last_date = list_chanpin_info['净值日期'].iloc[-1]
    last_scale = list_chanpin_info['产品规模'].iloc[-1]
    ax2.text(last_date, last_scale, f'{last_scale:.0f}万元', verticalalignment='top', horizontalalignment='right', color='black')
    
    # 去除图表的边框，只保留坐标轴
    ax1.spines['top'].set_visible(False)
    ax1.spines['right'].set_visible(False)
    ax2.spines['top'].set_visible(False)
    ax2.spines['right'].set_visible(False)
    ax1.spines['bottom'].set_visible(True)  # 保留底部边框（即 x 轴）
    ax1.spines['left'].set_visible(True)  # 保留左侧边框（即 y 轴）
    
# =============================================================================
#     # 显示产品规模曲线的图例
#     lines, labels = ax1.get_legend_handles_labels()
#     lines2, labels2 = ax2.get_legend_handles_labels()
#     ax1.legend(lines + lines2, labels + labels2, loc='upper center', bbox_to_anchor=(0.5, -0.1), ncol=3, frameon=False)
# =============================================================================
    
    # 清除图的边界
    plt.tight_layout()
    # 将 Matplotlib 图形转换为 PIL 图像  
    fig_NavDraw = Render_Mpl_to_Pil(fig_NavDraw)
    fig_width, fig_height = fig_NavDraw.size
    fig_NavDraw = fig_NavDraw.convert('RGB')
    # 手动修改需要添加图表的坐标点  
    fig_width_abscissa, fig_ordinate = 40, 820
    image.paste(fig_NavDraw, (fig_width_abscissa, fig_ordinate, fig_width_abscissa + fig_width, fig_ordinate + fig_height))
    plt.close()
    
    
    # 初始化回撤曲线图
    fig_NavDraw = plt.figure(figsize=(5.5, 2.7), dpi=130)
    # 提取'成立以来最大回撤'列
    if '成立以来最大回撤' in list_chanpin_info.columns and not list_chanpin_info['成立以来最大回撤'].isna().all():
        plt.fill_between(list_chanpin_info['净值日期'], list_chanpin_info['成立以来最大回撤'], alpha=0.5, color='#AB545A', label='运作以来最大回撤')
    # 显示图例
    plt.legend()
    # 使用PercentFormatter来格式化纵坐标轴刻度
    plt.gca().yaxis.set_major_formatter(PercentFormatter(xmax=1, decimals=1))  # xmax为最大刻度，decimals为小数位数
    plt.xticks(rotation=45)
    
    ax = plt.gca()
    # 去除图表的边框，只保留坐标轴
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    ax.spines['bottom'].set_visible(True)  # 保留底部边框（即 x 轴）
    ax.spines['left'].set_visible(True)  # 保留左侧边框（即 y 轴）
    
    # 清除图的边界
    plt.tight_layout()
    plt.legend(loc='lower left')
    # 将 Matplotlib 图形转换为 PIL 图像
    fig_NavDraw = Render_Mpl_to_Pil(fig_NavDraw)
    fig_width, fig_height = fig_NavDraw.size
    fig_NavDraw = fig_NavDraw.convert('RGB')
    # 手动修改需要添加图表的坐标点
    fig_width_abscissa, fig_ordinate = 40, 1330
    image.paste(fig_NavDraw, (fig_width_abscissa, fig_ordinate, fig_width_abscissa + fig_width, fig_ordinate + fig_height))
    plt.close()
    
    # 假设 summary_df 和 n 已经定义
    product_data = summary_df[['策略类型', '成立日期', '成立以来收益率', '累计年化收益率']].loc[n]
    # 转换为datetime
    product_data['成立日期'] = pd.to_datetime(product_data['成立日期'])
    # 格式化
    product_data['成立日期'] = product_data['成立日期'].to_pydatetime().strftime('%Y年%m月%d日')
    # 百分比格式化
    product_data['成立以来收益率'] = "{:.2%}".format(product_data['成立以来收益率'])
    product_data['累计年化收益率'] = "{:.2%}".format(product_data['累计年化收益率'])
    
    # 转换为DataFrame
    product_df = product_data.to_frame().T
    product_df.rename(columns={'成立日期': '运作日期', '成立以来收益率': '运作以来收益率'}, inplace=True)
    
    # 创建一个新的Figure对象
    fig_table = plt.figure(figsize=(5.4, 1.3), dpi=130)
    plt.tight_layout()
    plt.axis('off')
    
    # 创建表格
    tb = plt.table(cellText=product_df.values, colLabels=product_df.columns, cellLoc="center", loc='center', bbox=[0, 0, 1, 0.75])
    # 调用样式调整函数
    style_table(tb, product_df)
    
    # 隐藏表格的坐标轴
    plt.tight_layout()
    
    # 渲染表格
    tb.auto_set_font_size(False)
    tb.set_fontsize(12)
    tb.auto_set_column_width([i for i in range(len(product_data))])
    
    # 图表添加标题
    plt.text(0.0, 0.9, '基础指标', fontproperties=subtitle_font, fontsize=16, weight='bold', ha='left', va='center', color='black')
    
    # 获取渲染后的图像
    table_image = Render_Mpl_to_Pil(fig_table)
    table_image = table_image.convert('RGB')
    
    # 获取图像尺寸
    fig_width, fig_height = table_image.size
    
    # 手动修改需要添加图表的坐标点
    fig_width_abscissa, fig_ordinate = 750, 840
    
    # 粘贴图片
    image.paste(table_image, (fig_width_abscissa, fig_ordinate, fig_width_abscissa + fig_width, fig_ordinate + fig_height))
    
    plt.close()
    
    
    # 获取当前风险指标
    product_data = summary_df[['成立以来最大回撤', '周胜率', '夏普比率', '索提诺比率', '卡玛比率']].loc[n]
    # 格式化
    product_data['成立以来最大回撤'] = "{:.2%}".format(product_data['成立以来最大回撤'])
    product_data['周胜率'] = "{:.0%}".format(product_data['周胜率'])
    product_data['夏普比率'] ="{:.2f}".format(product_data['夏普比率'])
    product_data['索提诺比率'] ="{:.2f}".format(product_data['索提诺比率'])
    product_data['卡玛比率'] ="{:.2f}".format(product_data['卡玛比率'])
    # 转换为DataFrame
    product_df = product_data.to_frame().T
    product_df.rename(columns={'成立以来最大回撤': '运作以来最大回撤'}, inplace=True)
    # 创建一个新的Figure对象
    fig_table = plt.figure(figsize=(5.4, 1.3), dpi=130)
    plt.tight_layout()
    plt.axis('off')
    # 创建表格
    tb = plt.table(cellText=product_df.values, colLabels=product_df.columns, cellLoc="center", loc='center', bbox=[0, 0, 1, 0.75])
    # 调用样式调整函数
    style_table(tb, product_df)
    
    plt.tight_layout()
    # 隐藏表格的坐标轴
    # 渲染表格
    tb.auto_set_font_size(False)
    tb.set_fontsize(12)
    tb.auto_set_column_width([i for i in range(len(product_data))])
    # 图表添加标题
    plt.text(0.0, 0.9, '风险指标', fontproperties=subtitle_font, fontsize=16, weight='bold', ha='left', va='center', color='black')
    # 获取渲染后的图像
    table_image = Render_Mpl_to_Pil(fig_table)
    table_image = table_image.convert('RGB')
    # 
    fig_width, fig_height = table_image.size
    # 手动修改需要添加图表的坐标点
    fig_width_abscissa, fig_ordinate = 750, 1010
    # 粘贴图片
    image.paste(table_image, (fig_width_abscissa, fig_ordinate, fig_width_abscissa + fig_width, fig_ordinate + fig_height))
    plt.close()
    
    
    # 区间收益列名和行名
    column_names = ['区间收益率', '区间最大回撤', '夏普比率', '卡玛比率']
    row_names = ['近三月', '近六月', '近一年', '近两年', '近三年']
    # 创建一个空的数据框，稍后将填充数据
    product_df = pd.DataFrame(columns=column_names, index=row_names)
    # 年化参数数据数组
    values = [1, 1, 1, 2, 3]
    # 循环填充数据
    for i, row_name in enumerate(row_names):
        # 在此处填充每个单元格的数据
        # 例如：product_return_3months, max_drawdown_3months, sharpe_ratio_3months, sortino_ratio_3months
        # 根据您的数据源来获取相应的值，假设这些值在不同的变量中
        interval_return = summary_df.loc[n, row_name + '收益率']
        if math.isnan(interval_return):
            continue
        interval_max_drawdown = summary_df.loc[n, row_name + '最大回撤']
        interval_sharpe_ratio = ((1+interval_return)**(1/values[i])-1)/(np.std(list_chanpin_info_val[row_name + '波动率'])*math.sqrt(52))
        interval_sortino_ratio = -((1+interval_return)**(1/values[i])-1)/interval_max_drawdown
        data = [interval_return, interval_max_drawdown, interval_sharpe_ratio, interval_sortino_ratio]
        # 将数据添加到数据框
        product_df.loc[row_name] = data
        product_df.loc[row_name,'区间收益率'] = "{:.2%}".format(product_df.loc[row_name]['区间收益率'])
        product_df.loc[row_name, '区间最大回撤'] ="{:.2%}".format(product_df.loc[row_name]['区间最大回撤'])
        product_df.loc[row_name, '夏普比率'] = "{:.2f}".format(product_df.loc[row_name]['夏普比率'])
        product_df.loc[row_name, '卡玛比率'] ="{:.2f}".format(product_df.loc[row_name]['卡玛比率'])
    product_df.insert(0, '统计区间', row_names)
    product_df = product_df.dropna(subset=['区间收益率'])
    product_df = product_df.fillna('')  # 将所有 NaN 值替代为空值
    if not product_df.empty:
        table_height = (len(product_df) +1)*0.52 + 0.1  # 每行高度为0.2
        # 创建一个新的Figure对象
        fig_table = plt.figure(figsize=(5.4, table_height), dpi=130)
        plt.tight_layout()
        plt.axis('off')
        # 创建表格
        tb = plt.table(cellText=product_df.values, colLabels=product_df.columns, cellLoc="center", loc='center', bbox=[0, 0, 1, 0.9-(5-len(product_df))*0.025])
        # 调用样式调整函数
        style_table(tb, product_df)
        
        plt.tight_layout()
        # 隐藏表格的坐标轴
        # 渲染表格
        tb.auto_set_font_size(False)
        tb.set_fontsize(12)
        tb.auto_set_column_width([i for i in range(len(column_names))])
        # 图表添加标题
        plt.text(0.0, 0.95, '区间指标', fontproperties=subtitle_font, fontsize=16, weight='bold', ha='left', va='center', color='black')
        # 获取渲染后的图像
        table_image = Render_Mpl_to_Pil(fig_table)
        table_image = table_image.convert('RGB')
        # 手动修改需要添加图表的坐标点
        fig_width_abscissa, fig_ordinate = 750, 1180
        # 粘贴图片
        image.paste(table_image, (fig_width_abscissa, fig_ordinate, fig_width_abscissa + table_image.width, fig_ordinate + table_image.height))
        plt.close()
    
# =============================================================================
#     # 获取最近四周净值数据
#     nav_df = list_chanpin_info.iloc[-5:, list_chanpin_info.columns.get_indexer(['净值日期', '单位净值', '累计净值'])].reset_index(drop=True)
#     for x in range(1, len(nav_df)):
#         nav_df.loc[x, '涨跌幅'] = (nav_df.loc[x, '单位净值'] + (nav_df.loc[x, '累计净值'] - nav_df.loc[x, '单位净值']) - (nav_df.loc[x-1, '累计净值'] - nav_df.loc[x-1, '单位净值'])) / nav_df.loc[x-1, '单位净值'] - 1
#     nav_df['净值日期'] = pd.to_datetime(nav_df['净值日期']).dt.strftime('%Y/%m/%d')
#     nav_df['涨跌幅'] = (nav_df['涨跌幅'] * 100).round(2).astype(str) + '%'
#     # 删除第一行
#     nav_df = nav_df.drop(0).reset_index(drop=True)
#     # 创建一个新的Figure对象
#     fig_table = plt.figure(figsize=(4.2, 2.3), dpi=130)
#     plt.tight_layout()
#     plt.axis('off')
#     # 创建表格
#     tb = plt.table(cellText=nav_df.values, colLabels=nav_df.columns, cellLoc="center", loc='center', bbox=[0, 0, 1, 0.87])
#     # 渲染表格
#     tb.auto_set_font_size(False)
#     tb.set_fontsize(12)
#     # 自适应宽度
#     tb.auto_set_column_width([i for i in range(len(nav_df.columns))])
#     plt.tight_layout()
#     # 图表添加标题
#     plt.text(0.0, 0.96, '近四周净值', fontproperties=subtitle_font, fontsize=16, weight='bold', ha='left', va='center', color='black')
#     # 获取渲染后的图像
#     table_image = Render_Mpl_to_Pil(fig_table)
#     # 转换为RGBA模式
#     table_image = table_image.convert('RGBA')
#     # 手动修改需要添加图表的坐标点
#     fig_width_abscissa, fig_ordinate = 50, 1800
#     # 粘贴图片
#     image.paste(table_image, (fig_width_abscissa, fig_ordinate), table_image)
#     plt.close()
#     # image.show()
# =============================================================================
    
    # # 获取当前要素数据
    # # 第一行要素
    # elements_df = chanpin_elements_df[['基金经理', '托管人', '预警线清洗', '止损线清洗']].loc[chanpin_elements_df['基金简称'] == summary_df.loc[n, '产品名称']]
    # # 检查是否需要模糊匹配
    # if elements_df.empty:# 获取要匹配的产品名称
    #     product_name_to_match = summary_df.loc[n, '产品名称']
    #     # 获取基金简称列
    #     fund_names = chanpin_elements_df['基金简称']
    #     # 使用模糊匹配找到最接近的基金简称
    #     result = process.extractOne(product_name_to_match, fund_names)
    #     # 使用最接近的基金简称进行条件筛选
    #     elements_df = chanpin_elements_df[['基金经理', '托管人', '预警线清洗', '止损线清洗']].loc[chanpin_elements_df['基金简称'] == result[0]]
    
    # elements_df = elements_df.applymap(lambda x: x.replace('\r\n', '\n'))
    # elements_df = elements_df.rename(columns={
    #     '预警线清洗': '预警线',
    #     '止损线清洗': '止损线'
    # })
    # # 创建一个新的Figure对象
    # fig_table = plt.figure(figsize=(5.5, 1.3), dpi=130)
    # plt.tight_layout()
    # plt.axis('off')
    # # 创建表格
    # tb = plt.table(cellText=elements_df.values, colLabels=elements_df.columns, cellLoc="center", loc='center', bbox=[0, 0, 1, 0.75])
    # # 渲染表格
    # tb.auto_set_font_size(False)
    # tb.set_fontsize(12)
    # # 自适应第1和第3列的宽度
    # tb.auto_set_column_width([i for i in range(len(elements_df.columns))])
    # plt.tight_layout()
    # # 图表添加标题
    # plt.text(0.0, 0.9, '基本要素', fontproperties=subtitle_font, fontsize=16, weight='bold', ha='left', va='center', color='black')
    # # 获取渲染后的图像
    # table_image = Render_Mpl_to_Pil(fig_table)
    # table_image = table_image.convert('RGB')
    # fig_width, fig_height = table_image.size
    # # 手动修改需要添加图表的坐标点
    # fig_width_abscissa, fig_ordinate = 600, 1800
    # # 粘贴图片
    # image.paste(table_image, (fig_width_abscissa, fig_ordinate, fig_width_abscissa + fig_width, fig_ordinate + fig_height))
    # plt.close()
    
    # # 第二行要素
    # elements_df = chanpin_elements_df[['申赎规则']].loc[chanpin_elements_df['基金简称'] == summary_df.loc[n, '产品名称']]
    # # 检查是否需要模糊匹配
    # if elements_df.empty:# 获取要匹配的产品名称
    #     product_name_to_match = summary_df.loc[n, '产品名称']
    #     # 获取基金简称列
    #     fund_names = chanpin_elements_df['基金简称']
    #     # 使用模糊匹配找到最接近的基金简称
    #     result = process.extractOne(product_name_to_match, fund_names)
    #     # 使用最接近的基金简称进行条件筛选
    #     elements_df = chanpin_elements_df[['基金经理', '托管人', '预警线清洗', '止损线清洗']].loc[chanpin_elements_df['基金简称'] == result[0]]
    # elements_df = elements_df.applymap(lambda x: x.replace('\r\n', '\n'))
    # elements_df = elements_df.rename(columns={
    #     '预警线清洗': '预警线',
    #     '止损线清洗': '止损线'
    # })
    # # 创建一个新的Figure对象
    # fig_table = plt.figure(figsize=(5.5, 1), dpi=130)
    # plt.tight_layout()
    # plt.axis('off')
    # # 创建表格
    # tb = plt.table(cellText=elements_df.values, colLabels=elements_df.columns, cellLoc="center", loc='center', bbox=[0, 0, 1, 0.95])
    # # 渲染表格
    # tb.auto_set_font_size(False)
    # tb.set_fontsize(12)
    # # 自适应第1和第3列的宽度
    # tb.auto_set_column_width([i for i in range(len(elements_df.columns))])
    # plt.tight_layout()
    # # 获取渲染后的图像
    # table_image = Render_Mpl_to_Pil(fig_table)
    # table_image = table_image.convert('RGB')
    # fig_width, fig_height = table_image.size
    # # 手动修改需要添加图表的坐标点
    # fig_width_abscissa, fig_ordinate = 600, 1950
    # # 粘贴图片
    # image.paste(table_image, (fig_width_abscissa, fig_ordinate, fig_width_abscissa + fig_width, fig_ordinate + fig_height))
    # plt.close()
    # # image.show()
    
    # 创建临时文件
    with tempfile.NamedTemporaryFile(delete=False, suffix='.png') as temp_file:
        temp_file_path = temp_file.name
        # 保存图像到临时文件
        image.save(temp_file_path)

    # 上传临时文件到云存储桶的指定路径
    key = f'{report_save_path}{product_full_name}.png'
    with open(temp_file_path, 'rb') as f:
        cos_client.put_object(
            Bucket=bucket_name,
            Body=f,
            Key=key
        )

    # 删除临时文件
    os.remove(temp_file_path)

    logging.info(product_name + '生成：'+ '{:.1%}'.format((n+1)/len(summary_df)))
    plt.close()



# 指增产品周报
summary_df = pd.read_sql_query("SELECT * FROM %s" % '产品净值周报汇总表', engine)
summary_df = summary_df[summary_df['策略类型'].str.contains('指增', regex=True)].reset_index(drop=True)
chanpin_elements_df = pd.read_sql_query("SELECT * FROM %s" % '所有产品清洗后要素表', engine)

# 从目标路径提取报告模版
initial_image = convert_from_path(pdf_moban_path + '鸣熙指增产品周报模版.pdf')

for n in range(len(summary_df)):
    if summary_df.loc[n, '最新净值日期'] < pd.Timestamp(time_last_Friday_time):
        continue
    # 产品全称匹配
    product_full_name = list_celue.loc[list_celue['基金简称'] == summary_df.loc[n, '产品名称'], '基金全称'].values[0]
    # 提取产品净值信息
    table_name = product_full_name + '净值周报'
    # 构建 SQL 查询，并在表名周围加上反引号
    query = f"SELECT * FROM `{table_name}`"
    list_chanpin_info = pd.read_sql_query(query, engine)
    list_chanpin_info = list_chanpin_info.loc[list_chanpin_info['净值日期'] <= time_last_Friday_time]
    # 这里保证全部都是周频净值
    list_chanpin_info = list_chanpin_info.loc[list_chanpin_info['是否周末净值'] == '是'].reset_index().drop('index', axis=1)
        
    # 按照 '净值日期' 合并 list_chanpin_info 和 all_chanpin_aum
    list_chanpin_info = pd.merge(list_chanpin_info, all_chanpin_aum[['净值日期', product_full_name]], on='净值日期', how='left').rename(columns={product_full_name: '产品规模'})
    list_chanpin_info['产品规模'] = list_chanpin_info['产品规模'].fillna(method='bfill')
    # 将 '产品规模' 列转换为以万为单位的整数
    list_chanpin_info['产品规模'] = list_chanpin_info['产品规模'].str.replace(',', '').astype(float) / 10000
    list_chanpin_info['产品规模'] = list_chanpin_info['产品规模'].astype(int)
    
    list_chanpin_info_val = pd.read_sql_query("SELECT * FROM `%s`" % (product_full_name + '(波动率)'), engine)
    # 提取产品信息

    product_name = summary_df.loc[n, '产品名称'] + '净值周报'
    latest_date = list_chanpin_info['净值日期'].iloc[-1].strftime("%Y年%m月%d日")
    product_unit_nav = list_chanpin_info['单位净值'].iloc[-1]
    product_cum_nav = list_chanpin_info['累计净值'].iloc[-1]
    product_week_return = summary_df.loc[n, '本周收益率']
    product_week_return_excess = summary_df.loc[n, '本周超额']
    product_week_return_last4week = summary_df.loc[n, '近四周收益率']
    product_week_return_excess_last4week = summary_df.loc[n, '近四周超额（除法）']
    
    image = initial_image[0].copy()
    # 创建Image对象
    draw = ImageDraw.Draw(image)
    # 产品标题
    # 绘制产品名称
    font = ImageFont.truetype(font_title_path, size=48)  # 选择适当的字体大小
    # 计算产品标题文本宽度
    text_width, text_height = font.font.getsize(product_name)[0]
    # 计算产品标题文本居中位置
    image_width, image_height = image.size
    x_centered = (image_width - text_width) / 2
    draw.text((x_centered, 170), product_name, fill='black', font=font)

    # 净值时间
    # 绘制名称
    font = ImageFont.truetype(font_title_path, size=32)  # 选择适当的字体大小
    font_sub = ImageFont.truetype(font_subtitle_path, size=24)  # 选择适当的字体大小
    subtitle_font = FontProperties(fname=font_title_path)
    # 计算文本宽度
    text_width, text_height = font.font.getsize(latest_date)[0]

    draw.text((120, 250), latest_date, fill='black', font=font)

# 左侧基础信息
    # 单位净值
    target_x, target_y = 290, 318
    draw.text((target_x, target_y), str(product_unit_nav), fill='#262626', font=font)
    # 累计净值
    target_x, target_y = 303, 350
    draw.text((target_x, target_y), '(' + str(product_cum_nav) + ')', fill='#262626', font=font_sub)
    # 本周收益率
    target_x, target_y = 290, 395
    formatted_return = "{:.2%}".format(product_week_return)   # 格式化数值为小数点后两位   
    fill_color = get_fill_color(product_week_return)  # 调用函数获取字体颜色  
    draw.text((target_x, target_y), formatted_return, fill=fill_color, font=font)
    # 本周超额
    target_x, target_y = 290, 473
    formatted_return = "{:.2%}".format(product_week_return_excess)   # 格式化数值为小数点后两位   
    fill_color = get_fill_color(product_week_return_excess)  # 调用函数获取字体颜色  
    draw.text((target_x, target_y), formatted_return, fill=fill_color, font=font)
    # 本周收益率
    target_x, target_y = 290, 551
    formatted_return = "{:.2%}".format(product_week_return_last4week)   # 格式化数值为小数点后两位   
    fill_color = get_fill_color(product_week_return_last4week)  # 调用函数获取字体颜色  
    draw.text((target_x, target_y), formatted_return, fill=fill_color, font=font)
    # 本周超额
    target_x, target_y = 290, 629
    formatted_return = "{:.2%}".format(product_week_return_excess_last4week)   # 格式化数值为小数点后两位   
    fill_color = get_fill_color(product_week_return_excess_last4week)  # 调用函数获取字体颜色  
    draw.text((target_x, target_y), formatted_return, fill=fill_color, font=font)

# 柱状图
    # 本周收益率
    rates = list_chanpin_info['本周收益率'].dropna().tolist()  # 提取非NaN的收益率数据
    if len(rates) > 156:
        rates = rates[-156:]  # 如果rates的长度超过156，只保留最后的156个值
    # 生成柱状图的x轴坐标
    x = np.arange(len(rates))
    # 根据收益率的正负性设置柱状图的颜色  
    colors = ['green' if rate < 0 else 'red' for rate in rates]  
    fig_NavDraw = plt.figure(figsize=(6.9, 1.8), dpi=130)
    plt.bar(x, rates, color=colors) # 绘制柱状图
    plt.xticks([])  # 隐藏x轴 
    # 设置y轴格式为保留一位小数的百分比  
    ax = plt.gca()  
    ax.yaxis.set_major_formatter(FuncFormatter(multiply_by_100))
    ax.yaxis.set_major_locator(MaxNLocator(nbins=5))  # 设置y轴刻度数量为5
    # 清除图的边界
    plt.tight_layout() 
    # 将 Matplotlib 图形转换为 PIL 图像  
    fig_NavDraw = Render_Mpl_to_Pil(fig_NavDraw)  
    fig_width, fig_height = fig_NavDraw.size  
    fig_NavDraw = fig_NavDraw.convert('RGB')  
    # 手动修改需要添加图表的坐标点  
    fig_width_abscissa, fig_ordinate = 470, 230  
    image.paste(fig_NavDraw, (fig_width_abscissa, fig_ordinate, fig_width_abscissa + fig_width, fig_ordinate + fig_height))  
    plt.close()
    
    # 本周超额收益率
    rates = list_chanpin_info['本周超额'].dropna().tolist()
    # 生成柱状图的 x 轴坐标
    x = np.arange(len(rates))
    # 根据收益率的正负性设置柱状图的颜色
    colors = ['green' if rate < 0 else 'red' for rate in rates]
    
    # 创建带透明背景的图形
    fig_NavDraw = plt.figure(figsize=(6.9, 1.8), dpi=130)
    plt.bar(x, rates, color=colors)  # 绘制柱状图
    plt.xticks([])  # 隐藏 x 轴
    ax = plt.gca()
    ax.yaxis.set_major_formatter(FuncFormatter(multiply_by_100))
    ax.yaxis.set_major_locator(MaxNLocator(nbins=5))
    plt.tight_layout()
    
    # 将 Matplotlib 图形转换为 PIL 图像
    img_buf = io.BytesIO()
    fig_NavDraw.savefig(img_buf, format='png', transparent=True)
    img_buf.seek(0)
    fig_NavDraw = Image.open(img_buf).convert('RGBA')
    
    # 手动修改需要添加图表的坐标点
    fig_width_abscissa, fig_ordinate = 470, 460
    image.paste(fig_NavDraw, (fig_width_abscissa, fig_ordinate), fig_NavDraw)
    plt.close()
    

    # 初始化净值曲线图  
    fig_NavDraw = plt.figure(figsize=(5.5, 4), dpi=130)  
    # 添加曲线图
    cols_to_plot = ['成立以来收益率', '对标指数成立以来收益率', '成立以来超额（除法）']
    colors = ['#AB545A', '#676662', '#B5C1D0']  # 定义每条线的颜色
    alphas = [1, 1, 1]  # 定义面积图的透明度
    linewidths = [2, 1, 2]  # 定义线的宽度
    legends = ['运作以来收益率', '对标指数运作以来收益率', '运作以来超额（除法）']  # 添加图例名称
    
    for col, color, alpha, linewidth, legend in zip(cols_to_plot, colors, alphas, linewidths, legends):
        if col in list_chanpin_info.columns and not pd.isna(list_chanpin_info[col]).all():
            if color == '#B5C1D0':  # 如果是面积图，使用 fill_between 方法
                plt.fill_between(list_chanpin_info['净值日期'], list_chanpin_info[col], alpha=alpha, color=color, linewidth=0, label=legend)
            else:  # 如果是线图，使用 plot 方法
                plt.plot(list_chanpin_info['净值日期'], list_chanpin_info[col], label=legend, color=color, linewidth=linewidth)
    # 显示图例，并将图例放到左上角
    plt.legend(loc='upper left')
    # 使用PercentFormatter来格式化纵坐标轴刻度
    plt.gca().yaxis.set_major_formatter(PercentFormatter(xmax=1, decimals=1))  # xmax为最大刻度，decimals为小数位数
    plt.xticks(rotation=45)
    
    # 添加产品规模曲线
    ax1 = plt.gca()
    ax2 = ax1.twinx()
    product_scale_color = '#BCA295'  # 较浅的颜色
    product_scale_alpha = 0.5  # 透明度
    ax2.plot(list_chanpin_info['净值日期'], list_chanpin_info['产品规模'], label='产品规模', color=product_scale_color, alpha=product_scale_alpha, linewidth=1.5, linestyle='--')
    ax2.get_yaxis().set_visible(False)  # 隐藏右轴
    # 在最后一个数据点显示产品规模的值
    last_date = list_chanpin_info['净值日期'].iloc[-1]
    last_scale = list_chanpin_info['产品规模'].iloc[-1]
    ax2.text(last_date, last_scale, f'{last_scale:.0f}万元', verticalalignment='top', horizontalalignment='right', color='black')
    
    # 去除图表的边框，只保留坐标轴
    ax1.spines['top'].set_visible(False)
    ax1.spines['right'].set_visible(False)
    ax2.spines['top'].set_visible(False)
    ax2.spines['right'].set_visible(False)
    ax1.spines['bottom'].set_visible(True)  # 保留底部边框（即 x 轴）
    ax1.spines['left'].set_visible(True)  # 保留左侧边框（即 y 轴）
    
# =============================================================================
#     # 显示产品规模曲线的图例
#     lines, labels = ax1.get_legend_handles_labels()
#     lines2, labels2 = ax2.get_legend_handles_labels()
#     ax1.legend(lines + lines2, labels + labels2, loc='upper center', bbox_to_anchor=(0.5, -0.1), ncol=3, frameon=False)
# =============================================================================
    
    # 清除图的边界
    plt.tight_layout() 
    # 将 Matplotlib 图形转换为 PIL 图像  
    fig_NavDraw = Render_Mpl_to_Pil(fig_NavDraw)  
    fig_width, fig_height = fig_NavDraw.size  
    fig_NavDraw = fig_NavDraw.convert('RGB')  
    # 手动修改需要添加图表的坐标点  
    fig_width_abscissa, fig_ordinate = 40, 820  
    image.paste(fig_NavDraw, (fig_width_abscissa, fig_ordinate, fig_width_abscissa + fig_width, fig_ordinate + fig_height))  
    plt.close()
    
    
    # 初始化回撤曲线图
    fig_NavDraw = plt.figure(figsize=(5.5, 2.7), dpi=130)
    # 添加曲线图
    cols_to_plot = ['成立以来最大回撤', '成立以来最大回撤（超额）']
    colors = ['#AB545A', '#6B83A1']  # 定义每条线的颜色，移除面积图的颜色定义
    alphas = [0.5, 0.5]  # 定义线条的透明度，'成立以来最大回撤'透明度较低，'成立以来最大回撤（超额）'透明度较高
    linewidths = [0, 0]  # 定义线的宽度，将它们设置为0，以创建面积图
    legends = ['运作以来最大回撤', '运作以来最大回撤（超额）']  # 修改图例名称
    
    for col, color, alpha, linewidth, legend in zip(cols_to_plot, colors, alphas, linewidths, legends):
        if col in list_chanpin_info.columns and not pd.isna(list_chanpin_info[col]).all():
            plt.fill_between(list_chanpin_info['净值日期'], list_chanpin_info[col], alpha=alpha, color=color, label=legend)
    
    # 调整图例的顺序
    handles, labels = plt.gca().get_legend_handles_labels()
    handles = [handles[1], handles[0]]  # 调整图例的顺序
    labels = [labels[1], labels[0]]
    plt.legend(handles, labels)
    plt.legend(handles, labels, loc='lower left')
    # 使用PercentFormatter来格式化纵坐标轴刻度
    plt.gca().yaxis.set_major_formatter(PercentFormatter(xmax=1, decimals=1))  # xmax为最大刻度，decimals为小数位数
    plt.xticks(rotation=45)
    
    ax = plt.gca()
    # 去除图表的边框，只保留坐标轴
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    ax.spines['bottom'].set_visible(True)  # 保留底部边框（即 x 轴）
    ax.spines['left'].set_visible(True)  # 保留左侧边框（即 y 轴）
    
    # 清除图的边界
    plt.tight_layout()
    # 将 Matplotlib 图形转换为 PIL 图像
    fig_NavDraw = Render_Mpl_to_Pil(fig_NavDraw)
    fig_width, fig_height = fig_NavDraw.size
    fig_NavDraw = fig_NavDraw.convert('RGB')
    # 手动修改需要添加图表的坐标点
    fig_width_abscissa, fig_ordinate = 40, 1330
    image.paste(fig_NavDraw, (fig_width_abscissa, fig_ordinate, fig_width_abscissa + fig_width, fig_ordinate + fig_height))
    plt.close()
    
    
    # 获取当前收益指标
    product_data = summary_df[['策略类型', '成立日期', '成立以来收益率', '累计年化收益率']].loc[n]
    # 转换为datetime
    product_data['成立日期'] = pd.to_datetime(product_data['成立日期'])
    # 格式化
    product_data['成立日期'] = product_data['成立日期'].to_pydatetime().strftime('%Y年%m月%d日')
    # 百分比格式化
    product_data['成立以来收益率'] = "{:.2%}".format(product_data['成立以来收益率'])
    product_data['累计年化收益率'] ="{:.2%}".format(product_data['累计年化收益率'])
    # 转换为DataFrame
    product_df = product_data.to_frame().T
    product_df.rename(columns={'成立日期': '运作日期', '成立以来收益率': '运作以来收益率'}, inplace=True)

    # 创建一个新的Figure对象
    fig_table = plt.figure(figsize=(5.4, 1.3), dpi=130)
    plt.tight_layout()
    plt.axis('off')
    # 创建表格
    tb = plt.table(cellText=product_df.values, colLabels=product_df.columns, cellLoc="center", loc='center', bbox=[0, 0, 1, 0.75])
    # 调用样式调整函数
    style_table(tb, product_df)
    
    plt.tight_layout()
    # 隐藏表格的坐标轴
    # 渲染表格
    tb.auto_set_font_size(False)
    tb.set_fontsize(12)
    tb.auto_set_column_width([i for i in range(len(product_data))])
    # 图表添加标题
    subtitle_font = FontProperties(fname=font_title_path)
    plt.text(0.0, 0.9, '基础指标', fontproperties=subtitle_font, fontsize=16, weight='bold', ha='left', va='center', color='black')
    # 获取渲染后的图像
    table_image = Render_Mpl_to_Pil(fig_table)
    table_image = table_image.convert('RGB')
    # 
    fig_width, fig_height = table_image.size
    # 手动修改需要添加图表的坐标点
    fig_width_abscissa, fig_ordinate = 750, 840
    # 粘贴图片
    image.paste(table_image, (fig_width_abscissa, fig_ordinate, fig_width_abscissa + fig_width, fig_ordinate + fig_height))
    plt.close()


    # 获取当前风险指标
    product_data = summary_df[['成立以来最大回撤', '周胜率', '夏普比率', '索提诺比率', '卡玛比率']].loc[n]
    # 格式化
    product_data['成立以来最大回撤'] = "{:.2%}".format(product_data['成立以来最大回撤'])
    product_data['周胜率'] = "{:.0%}".format(product_data['周胜率'])
    product_data['夏普比率'] ="{:.2f}".format(product_data['夏普比率'])
    product_data['索提诺比率'] ="{:.2f}".format(product_data['索提诺比率'])
    product_data['卡玛比率'] ="{:.2f}".format(product_data['卡玛比率'])
    # 转换为DataFrame
    product_df = product_data.to_frame().T
    # 创建一个新的Figure对象
    fig_table = plt.figure(figsize=(5.4, 1.3), dpi=130)
    plt.tight_layout()
    plt.axis('off')
    # 创建表格
    tb = plt.table(cellText=product_df.values, colLabels=product_df.columns, cellLoc="center", loc='center', bbox=[0, 0, 1, 0.75])
    # 调用样式调整函数
    style_table(tb, product_df)
    
    plt.tight_layout()
    # 隐藏表格的坐标轴
    # 渲染表格
    tb.auto_set_font_size(False)
    tb.set_fontsize(12)
    tb.auto_set_column_width([i for i in range(len(product_data))])
    # 图表添加标题
    plt.text(0.0, 0.9, '风险指标', fontproperties=subtitle_font, fontsize=16, weight='bold', ha='left', va='center', color='black')
    # 获取渲染后的图像
    table_image = Render_Mpl_to_Pil(fig_table)
    table_image = table_image.convert('RGB')
    # 
    fig_width, fig_height = table_image.size
    # 手动修改需要添加图表的坐标点
    fig_width_abscissa, fig_ordinate = 750, 1000
    # 粘贴图片
    image.paste(table_image, (fig_width_abscissa, fig_ordinate, fig_width_abscissa + fig_width, fig_ordinate + fig_height))
    plt.close()
    
    
    # 超额指标
    product_data = summary_df[['成立以来超额（除法）', '成立以来最大回撤（超额）', '累计年化超额收益率', '超额周胜率', '超额夏普比率', '超额索提诺比率']].loc[n]
    # 百分比格式化
    product_data['成立以来超额（除法）'] = "{:.2%}".format(product_data['成立以来超额（除法）'])
    product_data['累计年化超额收益率'] = "{:.2%}".format(product_data['累计年化超额收益率'])
    product_data['超额周胜率'] ="{:.0%}".format(product_data['超额周胜率'])
    product_data['成立以来最大回撤（超额）'] ="{:.2%}".format(product_data['成立以来最大回撤（超额）'])
    # 小数点转化
    product_data['超额夏普比率'] = "{:.2f}".format(product_data['超额夏普比率'])
    product_data['超额索提诺比率'] ="{:.2f}".format(product_data['超额索提诺比率'])
    # 转换为DataFrame
    product_df = product_data.to_frame().T
    product_df.rename(columns={'成立以来超额（除法）': '运作以来超额', '累计年化超额收益率': '年化超额', '成立以来最大回撤（超额）': '超额最大回撤', '超额夏普比率': '超额夏普', '超额索提诺比率': '超额索提诺'}, inplace=True)
    # 创建一个新的Figure对象
    fig_table = plt.figure(figsize=(5.4, 1.3), dpi=130)
    plt.tight_layout()
    plt.axis('off')
    # 创建表格
    tb = plt.table(cellText=product_df.values, colLabels=product_df.columns, cellLoc="center", loc='center', bbox=[0, 0, 1, 0.75])
    # 调用样式调整函数
    style_table(tb, product_df)
    
    plt.tight_layout()
    # 隐藏表格的坐标轴
    # 渲染表格
    tb.auto_set_font_size(False)
    tb.set_fontsize(12)
    tb.auto_set_column_width([i for i in range(len(product_data))])
    # 图表添加标题
    plt.text(0.0, 0.9, '超额指标', fontproperties=subtitle_font, fontsize=16, weight='bold', ha='left', va='center', color='black')
    # 获取渲染后的图像
    table_image = Render_Mpl_to_Pil(fig_table)
    table_image = table_image.convert('RGB')
    # 
    fig_width, fig_height = table_image.size
    # 手动修改需要添加图表的坐标点
    fig_width_abscissa, fig_ordinate = 750, 1160
    # 粘贴图片
    image.paste(table_image, (fig_width_abscissa, fig_ordinate, fig_width_abscissa + fig_width, fig_ordinate + fig_height))
    plt.close()
    
    
    # 区间收益列名和行名
    column_names = ['区间超额', '区间超额回撤', '超额夏普', '超额卡玛比']
    row_names = ['近三月', '近六月', '近一年', '近两年', '近三年']
    # 创建一个空的数据框，稍后将填充数据
    product_df = pd.DataFrame(columns=column_names, index=row_names)
    # 年化参数数据数组
    values = [1, 1, 1, 2, 3]
    # 循环填充数据
    for i, row_name in enumerate(row_names):
        # 在此处填充每个单元格的数据
        # 例如：product_return_3months, max_drawdown_3months, sharpe_ratio_3months, sortino_ratio_3months
        # 根据您的数据源来获取相应的值，假设这些值在不同的变量中
        interval_return = summary_df.loc[n, row_name + '超额（除法）']
        if math.isnan(interval_return):
            continue
        interval_max_drawdown = summary_df.loc[n, row_name + '最大回撤（超额）']
        interval_sharpe_ratio = ((1+interval_return)**(1/values[i])-1)/(np.std(list_chanpin_info_val[row_name + '超额波动率'])*math.sqrt(52))
        interval_sortino_ratio = -((1+interval_return)**(1/values[i])-1)/interval_max_drawdown
        data = [interval_return, interval_max_drawdown, interval_sharpe_ratio, interval_sortino_ratio]
        # 将数据添加到数据框
        product_df.loc[row_name] = data
        product_df.loc[row_name,'区间超额'] = "{:.2%}".format(product_df.loc[row_name]['区间超额'])
        product_df.loc[row_name, '区间超额回撤'] ="{:.2%}".format(product_df.loc[row_name]['区间超额回撤'])
        product_df.loc[row_name, '超额夏普'] = "{:.2f}".format(product_df.loc[row_name]['超额夏普'])
        product_df.loc[row_name, '超额卡玛比'] ="{:.2f}".format(product_df.loc[row_name]['超额卡玛比'])
    product_df.insert(0, '统计区间', row_names)
    product_df = product_df.dropna(subset=['区间超额'])
    product_df = product_df.fillna('')  # 将所有 NaN 值替代为空值
    if not product_df.empty:
        table_height = (len(product_df) +1)*0.52 + 0.1  # 每行高度为0.2
        # 创建一个新的Figure对象
        fig_table = plt.figure(figsize=(5.4, table_height), dpi=130)
        plt.tight_layout()
        plt.axis('off')
        # 创建表格
        tb = plt.table(cellText=product_df.values, colLabels=product_df.columns, cellLoc="center", loc='center', bbox=[0, 0, 1, 0.9-(5-len(product_df))*0.025])
        # 调用样式调整函数
        style_table(tb, product_df)
        
        plt.tight_layout()
        # 隐藏表格的坐标轴
        # 渲染表格
        tb.auto_set_font_size(False)
        tb.set_fontsize(12)
        tb.auto_set_column_width([i for i in range(len(column_names))])
        # 图表添加标题
        plt.text(0.0, 0.95, '区间超额指标', fontproperties=subtitle_font, fontsize=16, weight='bold', ha='left', va='center', color='black')
        # 获取渲染后的图像
        table_image = Render_Mpl_to_Pil(fig_table)
        table_image = table_image.convert('RGB')
        # 手动修改需要添加图表的坐标点
        fig_width_abscissa, fig_ordinate = 750, 1310
        # 粘贴图片
        image.paste(table_image, (fig_width_abscissa, fig_ordinate, fig_width_abscissa + table_image.width, fig_ordinate + table_image.height))
        plt.close()
    
    # # 获取最近四周净值数据
    # nav_df = list_chanpin_info.iloc[-5:, list_chanpin_info.columns.get_indexer(['净值日期', '单位净值', '累计净值'])].reset_index(drop=True)
    # for x in range(1, len(nav_df)):
    #     nav_df.loc[x, '涨跌幅'] = (nav_df.loc[x, '单位净值'] + (nav_df.loc[x, '累计净值'] - nav_df.loc[x, '单位净值']) - (nav_df.loc[x-1, '累计净值'] - nav_df.loc[x-1, '单位净值'])) / nav_df.loc[x-1, '单位净值'] - 1
    # nav_df['净值日期'] = pd.to_datetime(nav_df['净值日期']).dt.strftime('%Y/%m/%d')
    # nav_df['涨跌幅'] = (nav_df['涨跌幅'] * 100).round(2).astype(str) + '%'
    # # 删除第一行
    # nav_df = nav_df.drop(0).reset_index(drop=True)
    # # 创建一个新的Figure对象
    # fig_table = plt.figure(figsize=(4.2, 2.3), dpi=130)
    # plt.tight_layout()
    # plt.axis('off')
    # # 创建表格
    # tb = plt.table(cellText=nav_df.values, colLabels=nav_df.columns, cellLoc="center", loc='center', bbox=[0, 0, 1, 0.87])
    # # 渲染表格
    # tb.auto_set_font_size(False)
    # tb.set_fontsize(12)
    # # 自适应宽度
    # tb.auto_set_column_width([i for i in range(len(nav_df.columns))])
    # plt.tight_layout()
    # # 图表添加标题
    # plt.text(0.0, 0.96, '近四周净值', fontproperties=subtitle_font, fontsize=16, weight='bold', ha='left', va='center', color='black')
    # # 获取渲染后的图像
    # table_image = Render_Mpl_to_Pil(fig_table)
    # # 转换为RGBA模式
    # table_image = table_image.convert('RGBA')
    # # 手动修改需要添加图表的坐标点
    # fig_width_abscissa, fig_ordinate = 50, 1800
    # # 粘贴图片
    # image.paste(table_image, (fig_width_abscissa, fig_ordinate), table_image)
    # plt.close()
    
    # # 获取当前要素数据
    # # 第一行要素
    # elements_df = chanpin_elements_df[['基金经理', '托管人', '预警线清洗', '止损线清洗']].loc[chanpin_elements_df['基金简称'] == summary_df.loc[n, '产品名称']]
    # # 检查是否需要模糊匹配
    # if elements_df.empty:# 获取要匹配的产品名称
    #     product_name_to_match = summary_df.loc[n, '产品名称']
    #     # 获取基金简称列
    #     fund_names = chanpin_elements_df['基金简称']
    #     # 使用模糊匹配找到最接近的基金简称
    #     result = process.extractOne(product_name_to_match, fund_names)
    #     # 使用最接近的基金简称进行条件筛选
    #     elements_df = chanpin_elements_df[['基金经理', '托管人', '预警线清洗', '止损线清洗']].loc[chanpin_elements_df['基金简称'] == result[0]]
    # elements_df = elements_df.applymap(lambda x: x.replace('\r\n', '\n'))
    # # 去掉表头中的“清洗”
    # elements_df = elements_df.rename(columns={
    #     '预警线清洗': '预警线',
    #     '止损线清洗': '止损线'
    # })
    # # 创建一个新的Figure对象
    # fig_table = plt.figure(figsize=(5.5, 1.3), dpi=130)
    # plt.tight_layout()
    # plt.axis('off')
    # # 创建表格
    # tb = plt.table(cellText=elements_df.values, colLabels=elements_df.columns, cellLoc="center", loc='center', bbox=[0, 0, 1, 0.75])
    # # 渲染表格
    # tb.auto_set_font_size(False)
    # tb.set_fontsize(12)
    # # 自适应第1和第3列的宽度
    # tb.auto_set_column_width([i for i in range(len(elements_df.columns))])
    # plt.tight_layout()
    # # 图表添加标题
    # plt.text(0.0, 0.9, '基本要素', fontproperties=subtitle_font, fontsize=16, weight='bold', ha='left', va='center', color='black')
    # # 获取渲染后的图像
    # table_image = Render_Mpl_to_Pil(fig_table)
    # table_image = table_image.convert('RGB')
    # fig_width, fig_height = table_image.size
    # # 手动修改需要添加图表的坐标点
    # fig_width_abscissa, fig_ordinate = 600, 1800
    # # 粘贴图片
    # image.paste(table_image, (fig_width_abscissa, fig_ordinate, fig_width_abscissa + fig_width, fig_ordinate + fig_height))
    # plt.close()
    
    # # 第二行要素
    # elements_df = chanpin_elements_df[['申赎规则']].loc[chanpin_elements_df['基金简称'] == summary_df.loc[n, '产品名称']]
    # # 检查是否需要模糊匹配
    # if elements_df.empty:# 获取要匹配的产品名称
    #     product_name_to_match = summary_df.loc[n, '产品名称']
    #     # 获取基金简称列
    #     fund_names = chanpin_elements_df['基金简称']
    #     # 使用模糊匹配找到最接近的基金简称
    #     result = process.extractOne(product_name_to_match, fund_names)
    #     # 使用最接近的基金简称进行条件筛选
    #     elements_df = chanpin_elements_df[['基金经理', '托管人', '预警线清洗', '止损线清洗']].loc[chanpin_elements_df['基金简称'] == result[0]]
    # elements_df = elements_df.applymap(lambda x: x.replace('\r\n', '\n'))
    # elements_df = elements_df.rename(columns={
    #     '预警线清洗': '预警线',
    #     '止损线清洗': '止损线'
    # })
    # # 创建一个新的Figure对象
    # fig_table = plt.figure(figsize=(5.5, 1), dpi=130)
    # plt.tight_layout()
    # plt.axis('off')
    # # 创建表格
    # tb = plt.table(cellText=elements_df.values, colLabels=elements_df.columns, cellLoc="center", loc='center', bbox=[0, 0, 1, 0.95])
    # # 渲染表格
    # tb.auto_set_font_size(False)
    # tb.set_fontsize(12)
    # # 自适应第1和第3列的宽度
    # tb.auto_set_column_width([i for i in range(len(elements_df.columns))])
    # plt.tight_layout()
    # # 获取渲染后的图像
    # table_image = Render_Mpl_to_Pil(fig_table)
    # table_image = table_image.convert('RGB')
    # fig_width, fig_height = table_image.size
    # # 手动修改需要添加图表的坐标点
    # fig_width_abscissa, fig_ordinate = 600, 1950
    # # 粘贴图片
    # image.paste(table_image, (fig_width_abscissa, fig_ordinate, fig_width_abscissa + fig_width, fig_ordinate + fig_height))
    # plt.close()
    # # image.show()
    
    # 创建临时文件
    with tempfile.NamedTemporaryFile(delete=False, suffix='.png') as temp_file:
        temp_file_path = temp_file.name
        # 保存图像到临时文件
        image.save(temp_file_path)

    # 上传临时文件到云存储桶的指定路径
    key = f'{report_save_path}{product_full_name}.png'
    with open(temp_file_path, 'rb') as f:
        cos_client.put_object(
            Bucket=bucket_name,
            Body=f,
            Key=key
        )

    # 删除临时文件
    os.remove(temp_file_path)

    logging.info(product_name + '生成：'+ '{:.1%}'.format((n+1)/len(summary_df)))
    plt.close()
