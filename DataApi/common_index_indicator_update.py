import sys
sys.path.append('/home/ubuntu/market/dc-66-python-2.0/SelfModule')

from sqlalchemy import create_engine
from selfdefinfunc import check_a_share_trading_day
import warnings
import mysql.connector
import numpy as np
import pandas as pd
from datetime import datetime, timedelta
import os
import requests
import json
import logging

# 设置日志
logging.basicConfig(filename='/home/ubuntu/market/dc-66-python-2.0/execution.log', 
                    level=logging.INFO, 
                    format='%(asctime)s - %(levelname)s - %(message)s', 
                    filemode='a')  # 设置为追加模式

warnings.filterwarnings('ignore')

# 检查是否非交易日
check_a_share_trading_day()

# 环境变量读取数据库秘钥信息
host = os.environ.get('MYSQL_HOST')   
user = os.environ.get('MYSQL_USERNAME')  
password = os.environ.get('MYSQL_PASSWORD')
port = os.environ.get('MYSQL_PORT')
refreshToken = os.environ.get('iFind_refreshToken')

# 数据库存储操作
cnx = mysql.connector.connect(user=user, password=password,
                              host=host,  port=port, database='指数行情数据库')
engine = create_engine(f'mysql+mysqlconnector://{user}:{password}@{host}:{port}/指数行情数据库')
cursor = cnx.cursor()

getAccessTokenUrl = 'https://ft.10jqka.com.cn/api/v1/get_access_token'
getAccessTokenHeader = {"Content-Type":"application/json","refresh_token":refreshToken}
getAccessTokenResponse = requests.post(url=getAccessTokenUrl, headers=getAccessTokenHeader)
accessToken = json.loads(getAccessTokenResponse.content)['data']['access_token']
thsHeaders = {"Content-Type":"application/json","access_token": accessToken}


# '指数总市值数据库'数据更新
# 构建 SQL 查询，选择表的倒数第一行数据
query = "SELECT 日期 FROM {} ORDER BY ROW_NUMBER() OVER() DESC LIMIT 1".format('指数总市值数据库')
# 使用 SQL 查询从数据库中读取倒数第一行数据
list_chanpin_info = pd.read_sql_query(query, engine)

product_update_date = list_chanpin_info.iloc[-1]['日期']
formatted_end_date = (datetime.now() - timedelta(days=1)).strftime("%Y%m%d")
formatted_start_date = (product_update_date+timedelta(1)).strftime("%Y%m%d")

try:
    product_ifind_num = "IXIC.GI,NYA.GI,SPX.GI"
    
    para = {"codes":product_ifind_num,"startdate":formatted_start_date,"enddate":formatted_end_date,"functionpara":{"Fill":"Blank"},"indipara":[{"indicator":"ths_market_value_hb_index","indiparams":[""]}]}
    thsResponse = requests.post(url=thsUrl, json=para, headers=thsHeaders)
    parsed_data = json.loads(thsResponse.content)
    product_date = parsed_data["tables"][0]['time']
    ths_total_cap_IXIC = parsed_data["tables"][0]['table']["ths_market_value_hb_index"]
    ths_total_cap_NYA = parsed_data["tables"][1]['table']["ths_market_value_hb_index"]
    ths_total_cap_SPX = parsed_data["tables"][2]['table']["ths_market_value_hb_index"]
    
    # 将数据转换为DataFrame
    list_chanpin_info = pd.DataFrame({
        '日期': pd.to_datetime(product_date),
        '纳斯达克综合指数': ths_total_cap_IXIC,
        '纽约证交所综指': ths_total_cap_NYA,
        '标普500': ths_total_cap_SPX,
    })
    
    list_chanpin_info.to_sql(name='指数总市值数据库', con=engine, if_exists='append', index=False)
except:
    logging.info('#指数总市值数据库最新数据截止：' + formatted_end_date + '，无需更新')