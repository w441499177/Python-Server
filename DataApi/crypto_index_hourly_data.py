from sqlalchemy import create_engine, Table, Column, Integer, Float, DateTime, MetaData
import requests
import pandas as pd
import os
import mysql.connector
from datetime import datetime, timedelta
import logging
import time

# 设置日志
logging.basicConfig(filename='/home/ubuntu/market/dc-66-python-2.0/execution.log', 
                    level=logging.INFO, 
                    format='%(asctime)s - %(levelname)s - %(message)s', 
                    filemode='a')  # 设置为追加模式

# List of cryptocurrencies
coins = ['BTC', 'ETH', 'BNB', 'SOL', 'XRP', 'DOGE']

# 从环境变量读取数据库秘钥信息
MYSQL_HOST = os.environ.get('MYSQL_HOST')   
MYSQL_USERNAME = os.environ.get('MYSQL_USERNAME')  
MYSQL_PASSWORD = os.environ.get('MYSQL_PASSWORD')
MYSQL_PORT = os.environ.get('MYSQL_PORT')

def connect_to_database():
    return mysql.connector.connect(
        user=MYSQL_USERNAME,
        password=MYSQL_PASSWORD,
        host=MYSQL_HOST,
        port=MYSQL_PORT,
        database='指数行情数据库'
    )

# Function to get historical data
def get_historical_data(coin, limit, aggregate):
    url = f'https://min-api.cryptocompare.com/data/v2/histohour?fsym={coin}&tsym=USD&limit={limit}&aggregate={aggregate}'
    response = requests.get(url)
    data = response.json()
    if data['Response'] == 'Success':
        return data['Data']['Data']
    else:
        raise Exception(f"Error fetching data for {coin}: {data['Message']}")

# Function to get current price data
def get_current_price_data(coin):
    url = f'https://min-api.cryptocompare.com/data/price?fsym={coin}&tsyms=USD'
    response = requests.get(url)
    data = response.json()
    return data['USD']

# Create the table if it doesn't exist
def create_table(engine):
    metadata = MetaData()
    table = Table(
        '加密货币小时频率行情序列表', metadata,
        Column('timestamp', Integer, primary_key=True),
        Column('datetime', DateTime),
        Column('BTC', Float),
        Column('ETH', Float),
        Column('BNB', Float),
        Column('SOL', Float),
        Column('XRP', Float),
        Column('DOGE', Float),
    )
    metadata.create_all(engine)

# Save latest price data to the database
def save_latest_price_to_database(engine, data):
    df = pd.DataFrame(data)
    df.to_sql('加密货币小时频率行情序列表', con=engine, if_exists='append', index=False)

# Get the latest timestamp from the database
def get_latest_timestamp(conn):
    cursor = conn.cursor()
    query = "SELECT MAX(timestamp) FROM 加密货币小时频率行情序列表"
    cursor.execute(query)
    result = cursor.fetchone()
    cursor.close()
    return result[0] if result[0] else None

# Main function to download and save data
def main():
    engine = create_engine(f'mysql+mysqlconnector://{MYSQL_USERNAME}:{MYSQL_PASSWORD}@{MYSQL_HOST}:{MYSQL_PORT}/指数行情数据库')
    create_table(engine)

    conn = connect_to_database()
    latest_timestamp = get_latest_timestamp(conn)
    current_time = datetime.now().replace(minute=0, second=0, microsecond=0)

    # Check if the current hour is an odd number
    if current_time.hour % 4 == 0:
        return

    if latest_timestamp:
        # Calculate the difference in hours
        hours_diff = int((current_time - datetime.fromtimestamp(latest_timestamp)).total_seconds() / 3600)
    else:
        # If no data in the database, start from the beginning
        hours_diff = 24  # For example, fetch the last 24 hours

    if hours_diff > 0:
        combined_data = []

        # Get historical data
        for coin in coins:
            data = get_historical_data(coin, hours_diff, 1)
            for entry in data:
                dt = datetime.fromtimestamp(entry['time'])
                if entry['time'] > latest_timestamp:
                    combined_data.append({
                        'timestamp': entry['time'],
                        'datetime': dt,
                        coin: entry['open']
                    })

        # Get current price data
        current_prices = {coin: get_current_price_data(coin) for coin in coins}
        current_timestamp = int(current_time.timestamp())
        if current_timestamp > latest_timestamp:
            current_data = {
                'timestamp': current_timestamp,
                'datetime': current_time
            }
            for coin, price in current_prices.items():
                current_data[coin] = price
            combined_data.append(current_data)

        # Remove duplicates and merge data
        merged_data = {}
        for entry in combined_data:
            timestamp = entry['timestamp']
            if timestamp not in merged_data:
                merged_data[timestamp] = {
                    'timestamp': timestamp,
                    'datetime': entry['datetime']
                }
            for coin in coins:
                if coin in entry:
                    merged_data[timestamp][coin] = entry[coin]

        # Convert merged data to list
        final_data = list(merged_data.values())

        # Save all data to the database
        with engine.begin() as connection:
            save_latest_price_to_database(connection, final_data)

    conn.commit()
    cursor = conn.cursor()
    # Calculate returns and update the frontend real-time table
    for coin in coins:
        query = f"""
        SELECT datetime, {coin} FROM 加密货币小时频率行情序列表
        WHERE datetime IN (%s, %s, %s, %s, %s)
        ORDER BY datetime DESC
        """
        one_hour_ago = current_time - timedelta(hours=1)
        one_day_ago = current_time - timedelta(days=1)
        seven_days_ago = current_time - timedelta(days=7)
        thirty_days_ago = current_time - timedelta(days=30)
        cursor.execute(query, (current_time, one_hour_ago, one_day_ago, seven_days_ago, thirty_days_ago))
        data = cursor.fetchall()

        if len(data) > 0:
            latest_price = data[0][1] 
            returns_1h = (latest_price / data[1][1] - 1)* 100
            returns_24h = (latest_price / data[2][1] - 1)* 100
            returns_7d = (latest_price / data[3][1] - 1)* 100
            returns_30d = (latest_price / data[4][1] - 1)* 100

            insert_query = """
            REPLACE INTO 加密货币前端实时行情 (Coin, `UpdateTimestamp`, `LatestPrice(USD)`, `1hReturn(%)`, `24hReturn(%)`, `7dReturn(%)`, `30dReturn(%)`)
            VALUES (%s, %s, %s, %s, %s, %s, %s)
            """
            cursor.execute(insert_query, (coin, current_time, latest_price, returns_1h, returns_24h, returns_7d, returns_30d))

    conn.commit()
    cursor.close()
    conn.close()

if __name__ == '__main__':
    main()
    logging.info('加密货币小时行情数据已更新')