import sys
sys.path.append('/home/ubuntu/market/dc-66-python-2.0/SelfModule')

import warnings
import akshare as ak
import pandas as pd
import numpy as np
import os
import mysql.connector
from sqlalchemy import create_engine
from fund_indicator_caculate import calculate_fund_metrics # 自定义封装函数
from tradedate import *  # 自定义封装函数
from selfdefinfunc import Calculate_Reinvested_Nav # 自定义封装函数
from downloadindex import BenchmarkIndex_mapping
from concurrent.futures import ThreadPoolExecutor, TimeoutError
import logging

warnings.filterwarnings('ignore')

# 设置日志
logging.basicConfig(filename='/home/ubuntu/market/dc-66-python-2.0/execution.log', 
                    level=logging.INFO, 
                    format='%(asctime)s - %(levelname)s - %(message)s', 
                    filemode='a')  # 设置为追加模式

def get_db_connection():
    # 环境变量读取数据库秘钥信息
    host = os.environ.get('MYSQL_HOST')
    user = os.environ.get('MYSQL_USERNAME')
    password = os.environ.get('MYSQL_PASSWORD')
    port = os.environ.get('MYSQL_PORT')

    # 数据库存储操作
    cnx = mysql.connector.connect(user=user, password=password,
                                  host=host, port=port, database='同业产品数据库')
    engine = create_engine(f'mysql+mysqlconnector://{user}:{password}@{host}:{port}/同业产品数据库')
    engine_index = create_engine(f'mysql+mysqlconnector://{user}:{password}@{host}:{port}/指数行情数据库')
    cursor = cnx.cursor()
    
    return cursor, engine, engine_index, cnx

# 是否需要更新所有基金信息 True or False
if_update_up_date_all_fund_info = False

mf_strategy_list = ['300指增', '500指增', '1000指增', '国证2000指增']

# 指定要保留的产品名称
target_products = [
    '公募300指增策略指数',
    '公募500指增策略指数',
    '公募1000指增策略指数',
    '公募国证2000指增策略指数'
]

# 为每个策略设置一个截止日期
cutoff_date_dict = {
    '300指增': '2012-12-31',
    '500指增': '2015-12-31',
    '1000指增': '2018-12-28',
    '国证2000指增': '2022-12-30'
}

# 列名列表按照你的顺序
new_columns = ['成立以来收益率', '对标指数成立以来收益率', '成立以来超额（除法）', '成立以来最大回撤', '成立以来最大回撤（超额）', '成立以来波动率', '成立以来超额波动率',
               '近一周收益率', '对标指数近一周收益率', '近一周超额（除法）', '近四周收益率', '对标指数近四周收益率', '近四周超额（除法）', 
               '今年以来收益率', '对标指数今年以来收益率', '今年以来超额（除法）', '今年以来最大回撤', '今年以来最大回撤（超额）', '今年以来波动率', '今年以来超额波动率',
               '近三月收益率', '对标指数近三月收益率', '近三月超额（除法）', '近三月最大回撤', '近三月最大回撤（超额）', '近三月波动率', '近三月超额波动率',
               '近六月收益率', '对标指数近六月收益率', '近六月超额（除法）', '近六月最大回撤', '近六月最大回撤（超额）', '近六月波动率', '近六月超额波动率',
               '近一年收益率', '对标指数近一年收益率', '近一年超额（除法）', '近一年最大回撤', '近一年最大回撤（超额）', '近一年波动率', '近一年超额波动率',
               '近两年收益率', '对标指数近两年收益率', '近两年超额（除法）', '近两年最大回撤', '近两年最大回撤（超额）', '近两年波动率', '近两年超额波动率',
               '近三年收益率', '对标指数近三年收益率', '近三年超额（除法）', '近三年最大回撤', '近三年最大回撤（超额）', '近三年波动率', '近三年超额波动率']

if if_update_up_date_all_fund_info == True:
    # 获取所有基金的名称
    fund_name_em_df = ak.fund_name_em()
    
    # 确保 '基金代码' 列存在，然后转换为列表
    if '基金代码' in fund_name_em_df.columns:
        fund_codes = fund_name_em_df['基金代码'].tolist()
    else:
        logging.info("DataFrame does not contain '基金代码' column")
        exit()
        
    df = pd.DataFrame()
    
    # 计算总基金数量用于进度跟踪
    total_funds = len(fund_codes)
    
    # 使用线程池来处理并发请求
    with ThreadPoolExecutor(max_workers=10) as executor:
        futures = {}
        
        for i, fund_code in enumerate(fund_codes):
            # 提交任务到线程池
            future = executor.submit(ak.fund_individual_basic_info_xq, symbol=fund_code)
            futures[future] = fund_code
            
            try:
                # 获取结果并设置超时时间为10秒
                fund_individual_basic_info_xq_df = future.result(timeout=10)
                
                # 处理获取到的基金信息
                transposed_df = fund_individual_basic_info_xq_df.T
                transposed_df.columns = transposed_df.iloc[0]
                transposed_df = transposed_df.drop(transposed_df.index[0])
                transposed_df.index = range(len(transposed_df))
                
                # 将获取到的基金信息添加到数据框中
                df = pd.concat([df, transposed_df], axis=0, ignore_index=True)
                
                # 打印进度信息
                logging.info(f"已处理 {i+1}/{total_funds} 个基金，当前基金代码：{fund_code}")
                
            except TimeoutError:
                logging.info(f"Timeout fetching info for fund code {fund_code}")
            except Exception as e:
                logging.info(f"Error fetching info for fund code {fund_code}: {e}")

    cursor, engine, engine_index, cnx = get_db_connection()
    df.to_sql(name='公募所有产品明细表', con=engine, if_exists='replace', index=False)
    
    # 更新所有指增产品基本信息
    fund_info_index_em_df = ak.fund_info_index_em(symbol="沪深指数", indicator="增强指数型")
    
    list_mf_strategy = pd.read_sql_query("SELECT * FROM %s" % '公募产品汇总表', engine)
    list_mf_strategy = list_mf_strategy.drop(columns=['基金代码'])
    list_mf_strategy = pd.merge(fund_info_index_em_df[['基金代码', '基金名称']], list_mf_strategy, how='left', left_on='基金名称', right_on='基金名称')
    
    # 逐行循环 list_mf_strategy，只处理策略类型有文字的行
    for index, row in list_mf_strategy.iterrows():
        if pd.notna(row['策略类型']) and row['策略类型'] != '':
            # 从数据库的 '公募所有产品明细表' 中查询匹配的基金代码
            query = "SELECT 最新规模, 成立时间, 基金公司, 基金经理 FROM 公募所有产品明细表 WHERE 基金代码 = %s" % row['基金代码']
            matched_data = pd.read_sql_query(query, engine)
            
            # 如果找到匹配的数据
            if not matched_data.empty:
                matched_row = matched_data.iloc[0]
                # 更新 list_mf_strategy 中的相应字段
                list_mf_strategy.at[index, '产品规模'] = matched_row['最新规模']
                list_mf_strategy.at[index, '成立日期'] = matched_row['成立时间']
                list_mf_strategy.at[index, '管理人'] = matched_row['基金公司']
                list_mf_strategy.at[index, '基金经理'] = matched_row['基金经理']
    
    # 按照 '成立日期' 列升序排列
    list_mf_strategy = list_mf_strategy.sort_values(by='成立日期', ascending=True)
    list_mf_strategy.to_sql(name='公募产品汇总表', con=engine, if_exists='replace', index=False)

# 连接数据库
cursor, engine, engine_index, cnx = get_db_connection()
list_mf_strategy = pd.read_sql_query("SELECT * FROM %s" % '公募产品汇总表', engine)

# 公募基金净值数据更新
list_unit_nav_list = pd.DataFrame()
list_unit_nav_list['净值日期'] = pd.Series(dtype='datetime64[ns]')

list_cum_nav_list = pd.DataFrame()
list_cum_nav_list['净值日期'] = pd.Series(dtype='datetime64[ns]')

stock_zh_index_daily_em_df = ak.stock_zh_index_daily_em(symbol="sh000001")
stock_zh_index_daily_em_df.rename(columns={'date': '净值日期'}, inplace=True)

list_unit_nav_list = pd.merge(list_unit_nav_list, stock_zh_index_daily_em_df['净值日期'], on='净值日期', how='right')
list_cum_nav_list = pd.merge(list_cum_nav_list, stock_zh_index_daily_em_df['净值日期'], on='净值日期', how='right')

list_unit_nav_list['净值日期'] = pd.to_datetime(list_unit_nav_list['净值日期'])
list_cum_nav_list['净值日期'] = pd.to_datetime(list_cum_nav_list['净值日期'])

# =============================================================================
# # 首先使用净值总表进行更新
# fund_open_fund_rank_em_df = ak.fund_open_fund_rank_em(symbol="全部")
# # 计算最新收市的交易日
# last_trade_day = pd.to_datetime(stock_zh_index_daily_em_df.iloc[-1, 0])
# # 获取 list_mf_strategy 中的基金代码列表
# fund_codes_in_list_mf_strategy = list_mf_strategy['基金代码'].tolist()
# 
# # 筛选 fund_open_fund_rank_em_df
# fund_open_fund_rank_em_df = fund_open_fund_rank_em_df.loc[
#     (pd.to_datetime(fund_open_fund_rank_em_df['日期']) == last_trade_day) &
#     (fund_open_fund_rank_em_df['基金代码'].isin(fund_codes_in_list_mf_strategy))
# ]
# 
# # 按照每一个列名进行匹配
# =============================================================================

# 单位净值数据表更新
for mf_strategy in mf_strategy_list:
    list_strategy = list_mf_strategy[list_mf_strategy['策略类型'] == mf_strategy]
    for mf_code in list_strategy['基金代码']:
        mf_unit_nav_df = ak.fund_open_fund_info_em(symbol=mf_code, indicator="单位净值走势")
        # 将 mf_unit_nav_df 的净值日期列转换为 datetime 格式
        mf_unit_nav_df['净值日期'] = pd.to_datetime(mf_unit_nav_df['净值日期'])
        # 只保留‘单位净值’列并重命名为 mf_code
        mf_unit_nav_df = mf_unit_nav_df[['净值日期', '单位净值']]
        mf_unit_nav_df.rename(columns={'单位净值': mf_code}, inplace=True)
        # 按照净值日期列合并 list_unit_nav_list 和 mf_unit_nav_df
        list_unit_nav_list = pd.merge(list_unit_nav_list, mf_unit_nav_df, on='净值日期', how='left')
        logging.info('新增加' + mf_code + '单位净值序列')
list_unit_nav_list.to_sql(name='公募基金单位净值数据表', con=engine, if_exists='replace', index=False)

# 累计净值数据表更新
for mf_strategy in mf_strategy_list:
    list_strategy = list_mf_strategy[list_mf_strategy['策略类型'] == mf_strategy]
    for mf_code in list_strategy['基金代码']:
        mf_cum_nav_df = ak.fund_open_fund_info_em(symbol=mf_code, indicator="累计净值走势")
        # 将 mf_unit_nav_df 的净值日期列转换为 datetime 格式
        mf_cum_nav_df['净值日期'] = pd.to_datetime(mf_cum_nav_df['净值日期'])
        # 只保留‘单位净值’列并重命名为 mf_code
        mf_cum_nav_df = mf_cum_nav_df[['净值日期', '累计净值']]
        mf_cum_nav_df.rename(columns={'累计净值': mf_code}, inplace=True)
        # 按照净值日期列合并 list_cum_nav_list 和 mf_cum_nav_df
        list_cum_nav_list = pd.merge(list_cum_nav_list, mf_cum_nav_df, on='净值日期', how='left')
        logging.info('新增加' + mf_code + '累计净值序列')
list_cum_nav_list.to_sql(name='公募基金累计净值数据表', con=engine, if_exists='replace', index=False)


# 连接数据库
cursor, engine, engine_index, cnx = get_db_connection()
list_mf_strategy = pd.read_sql_query("SELECT * FROM %s" % '公募产品汇总表', engine)
celue_total_list = pd.read_sql_query("SELECT * FROM %s" % '策略指数汇总表', engine)
list_cum_nav_list = pd.read_sql_query("SELECT * FROM %s" % '公募基金累计净值数据表', engine)
list_unit_nav_list = pd.read_sql_query("SELECT * FROM %s" % '公募基金单位净值数据表', engine)

# 使用 loc 方法筛选出指定产品名称的行
celue_total_list = celue_total_list.loc[~celue_total_list['产品名称'].isin(target_products)].reset_index(drop=True)

# 创建公募产品指数数据表
for mf_strategy in mf_strategy_list:
    list_strategy = list_mf_strategy[list_mf_strategy['策略类型'] == mf_strategy]
    mf_strategy_index_name = '公募' + mf_strategy + '策略指数'
    df_strategy_index = BenchmarkIndex_mapping[mf_strategy].copy()
    df_strategy_index = df_strategy_index[df_strategy_index['date'] <= np.datetime64(time_latest_Friday)]
    # 获取当前策略的截止日期
    cutoff_date = cutoff_date_dict[mf_strategy]
    # 截取 DataFrame 的 date 列，保留 cutoff_date 之后的日期
    df_strategy_index = df_strategy_index[df_strategy_index['date'] >= cutoff_date].reset_index(drop=True)
    df_strategy_index.rename(columns={'close': '对标指数', 'date': '净值日期'}, inplace=True)
    df_strategy_index[['样本数量', '复权净值']] = np.nan

    # 添加新列名到 df_strategy_index
    df_strategy_index = df_strategy_index.reindex(columns=list(df_strategy_index.columns) + new_columns)
    
    # 净值序列匹配 #       
    for mf_code in list_strategy['基金代码']:
        # 从 list_unit_nav_list 中提取单位净值和净值日期
        unit_nav = list_unit_nav_list[['净值日期', mf_code]].rename(columns={mf_code: '单位净值'})
        
        # 从 list_cum_nav_list 中提取累计净值和净值日期
        cum_nav = list_cum_nav_list[['净值日期', mf_code]].rename(columns={mf_code: '累计净值'})
        
        # 合并单位净值和累计净值
        list_chanpin_info = pd.merge(unit_nav, cum_nav, on='净值日期', how='outer')
        # 删除包含 NaN 值的行
        list_chanpin_info = list_chanpin_info.dropna(how='any')
        list_chanpin_info['复权净值'] = Calculate_Reinvested_Nav(list_chanpin_info, '单位净值', '累计净值')
        
        # 将结果添加到 list_chanpin_info 中
        temp_nav = pd.merge(df_strategy_index['净值日期'], list_chanpin_info[['净值日期', '复权净值']])
        temp_nav = pd.merge(df_strategy_index['净值日期'], temp_nav, how='outer')
        df_strategy_index[mf_code] = temp_nav['复权净值']
    
    # 指数编制计算和分析
    df_strategy_index.loc[:, '成立以来收益率':'近三年超额波动率'] = np.nan
    
    # 计算成立以来数据 #
    df_strategy_index.loc[0, '复权净值'] = 1
    df_strategy_index.loc[0, '样本数量'] = 0
    df_strategy_index.loc[0, '成立以来收益率'] = 0
    df_strategy_index.loc[0, '成立以来最大回撤'] = 0
    df_strategy_index.loc[0, '成立以来最大回撤（超额）'] = 0
    df_strategy_index[['复权净值', '对标指数', '成立以来收益率', '对标指数成立以来收益率', '成立以来超额（除法）', '成立以来最大回撤', '成立以来最大回撤（超额）']] = df_strategy_index[['复权净值', '对标指数', '成立以来收益率', '对标指数成立以来收益率', '成立以来超额（除法）', '成立以来最大回撤', '成立以来最大回撤（超额）']].astype(float)
    
    # 计算成立以来数据 #
    if '指增' in mf_strategy or '亿元' in mf_strategy:
        df_strategy_index.loc[0, '对标指数成立以来收益率'] = 0
        df_strategy_index.loc[0, '成立以来超额（除法）'] = 0
    num_strategy = list_mf_strategy['策略类型'].value_counts()[mf_strategy]
    for date_length in range(len(df_strategy_index)-1):
        n = 0
        cum_return = 0
        # 指数等权重编制计算
        for celue_length in range(num_strategy):            
            if np.isnan(df_strategy_index.iloc[date_length, 55 + celue_length]*df_strategy_index.iloc[date_length + 1, 55 + celue_length]) == False:
                cum_return = cum_return + df_strategy_index.iloc[date_length + 1, 55 + celue_length]/df_strategy_index.iloc[date_length, 55 + celue_length] - 1
                n = n + 1

        df_strategy_index.loc[date_length + 1, '样本数量'] = n
        try:
            df_strategy_index.loc[date_length + 1, '复权净值'] = float(df_strategy_index.loc[date_length, '复权净值']*(cum_return/n + 1))
        except:
            continue
        df_strategy_index.loc[date_length + 1, '成立以来收益率'] = float(df_strategy_index.loc[date_length + 1, '复权净值'] - 1)
        # 最大回撤
        df_strategy_index.loc[date_length + 1, '成立以来最大回撤'] = (df_strategy_index.loc[date_length + 1,'复权净值'])/max(df_strategy_index.loc[0: (date_length + 1), '复权净值']) - 1
        if df_strategy_index.loc[date_length + 1, '成立以来最大回撤'] > 0:
            df_strategy_index.loc[date_length + 1, '成立以来最大回撤'] = 0
        # 绝对收益波动率
        df_strategy_index.loc[date_length + 1, '成立以来波动率'] = (df_strategy_index.loc[date_length + 1, '成立以来收益率'] + 1)/(df_strategy_index.loc[date_length, '成立以来收益率'] + 1) - 1
        
        # 相对收益指标计算
        if '指增' in mf_strategy or '亿元' in mf_strategy:
            # 基准指数以及超额计算
            df_strategy_index.loc[date_length + 1, '对标指数成立以来收益率'] = df_strategy_index.loc[date_length + 1, '对标指数']/df_strategy_index.loc[0, '对标指数'] - 1
            df_strategy_index.loc[date_length + 1, '成立以来超额（除法）'] = (df_strategy_index.loc[date_length + 1, '成立以来收益率'] + 1)/(df_strategy_index.loc[date_length + 1, '对标指数成立以来收益率'] + 1) - 1
            # 最大超额回撤
            df_strategy_index.loc[date_length + 1, '成立以来最大回撤（超额）'] = (df_strategy_index.loc[date_length + 1,'成立以来超额（除法）'] + 1)/max(df_strategy_index.loc[0: (date_length + 1), '成立以来超额（除法）'] + 1) - 1
            if df_strategy_index.loc[date_length + 1, '成立以来最大回撤（超额）'] > 0:
                df_strategy_index.loc[date_length + 1, '成立以来最大回撤（超额）'] = 0
            # 超额波动率
            df_strategy_index.loc[date_length + 1, '成立以来超额波动率'] = (df_strategy_index.loc[date_length + 1, '成立以来超额（除法）'] + 1)/(df_strategy_index.loc[date_length, '成立以来超额（除法）'] + 1) - 1
        
        # 其他区间字段计算
        for date_start, com_columns, spe_columns, draw_columns, bench_columns, chanpin_draw_columns, vol_columns, excess_vol_columns in zip(
            [time_last_Friday_lag1_time, time_time_last_Friday_lag4_time, time_last_year_time, time_lag3_month_time, time_lag6_month_time, time_lag_year_time, time_lag2_year_time, time_lag3_year_time],
            ['近一周收益率', '近四周收益率', '今年以来收益率', '近三月收益率', '近六月收益率', '近一年收益率', '近两年收益率', '近三年收益率'],
            ['近一周超额（除法）', '近四周超额（除法）', '今年以来超额（除法）', '近三月超额（除法）', '近六月超额（除法）', '近一年超额（除法）', '近两年超额（除法）', '近三年超额（除法）'],
            ['近一周最大回撤（超额）', '近四周最大回撤（超额）', '今年以来最大回撤（超额）', '近三月最大回撤（超额）', '近六月最大回撤（超额）', '近一年最大回撤（超额）', '近两年最大回撤（超额）', '近三年最大回撤（超额）'],
            ['对标指数近一周收益率', '对标指数近四周收益率', '对标指数今年以来收益率', '对标指数近三月收益率', '对标指数近六月收益率', '对标指数近一年收益率', '对标指数近两年收益率', '对标指数近三年收益率'],
            ['近一周最大回撤', '近四周最大回撤', '今年以来最大回撤', '近三月最大回撤', '近六月最大回撤', '近一年最大回撤', '近两年最大回撤', '近三年最大回撤'],
            ['近一周波动率', '近四周波动率', '今年以来波动率', '近三月波动率', '近六月波动率', '近一年波动率', '近两年波动率', '近三年波动率'],
            ['近一周超额波动率', '近四周超额波动率', '今年以来超额波动率', '近三月超额波动率', '近六月超额波动率', '近一年超额波动率', '近两年超额波动率', '近三年超额波动率']
        ):
                                                                                  
            if df_strategy_index.iloc[date_length + 1, 0] >= date_start:
                # 绝对收益字段数据计算 
                first_index = df_strategy_index.loc[df_strategy_index['净值日期'] >= date_start].index[0]
                temp = df_strategy_index.loc[df_strategy_index['净值日期'] >= date_start].reset_index(drop=True)
                # 普通收益率字段计算
                df_strategy_index.loc[date_length + 1, com_columns] = df_strategy_index.loc[date_length + 1, '复权净值']/temp.loc[0, '复权净值']-1
                if date_start != time_time_last_Friday_lag4_time and date_start != time_last_Friday_lag1_time:
                    # 最大回撤
                    df_strategy_index.loc[date_length + 1, chanpin_draw_columns] = (df_strategy_index.loc[date_length + 1,'复权净值'])/max(df_strategy_index.loc[first_index:(date_length + 1), '复权净值']) - 1
                    if df_strategy_index.loc[date_length + 1, chanpin_draw_columns] > 0:
                        df_strategy_index.loc[date_length + 1, chanpin_draw_columns] = 0
                    # 波动率
                    if first_index != (date_length + 1):
                        df_strategy_index.loc[date_length + 1, vol_columns] = (df_strategy_index.loc[date_length + 1,'复权净值'])/(df_strategy_index.loc[date_length, '复权净值']) - 1
                
                # 相对收益字段数据计算
                if '指增' in mf_strategy or '亿元' in mf_strategy:
                # 相对收益
                    # 超额收益字段计算
                    df_strategy_index.loc[date_length + 1, spe_columns] = (df_strategy_index.loc[date_length + 1, com_columns] + 1)/(df_strategy_index.loc[date_length + 1, '对标指数']/temp.loc[0, '对标指数']) - 1
                    # 基准收益
                    df_strategy_index.loc[date_length + 1, bench_columns] = df_strategy_index.loc[date_length + 1, '对标指数']/df_strategy_index.loc[first_index, '对标指数'] - 1 
                    # 指增最大回撤及波动率字段计算
                    if date_start != time_time_last_Friday_lag4_time and date_start != time_last_Friday_lag1_time:
                        # 最大回撤
                        df_strategy_index.loc[date_length + 1, draw_columns] = (df_strategy_index.loc[date_length + 1,'成立以来超额（除法）'] + 1)/max(df_strategy_index.loc[first_index:(date_length + 1), '成立以来超额（除法）'] + 1) - 1
                        if df_strategy_index.loc[date_length + 1, draw_columns] > 0:
                            df_strategy_index.loc[date_length + 1, draw_columns] = 0
                        # 波动率
                        if first_index != (date_length + 1):
                            df_strategy_index.loc[date_length + 1, excess_vol_columns] = (df_strategy_index.loc[date_length + 1, '成立以来超额（除法）'] + 1)/(df_strategy_index.loc[date_length, '成立以来超额（除法）'] + 1) - 1
    
    # 填写总表数据 #
    celue_total_list = celue_total_list._append(pd.Series(np.nan, index=celue_total_list.columns), ignore_index=True)  # 添加空白行
    row_num = celue_total_list.index[-1]
    celue_total_list.loc[row_num, '产品名称'] = mf_strategy_index_name 
    cols = df_strategy_index.loc[:, '成立以来收益率':'近三年超额波动率'].columns
    celue_total_list.loc[row_num, '成立日期'] = df_strategy_index.loc[0, '净值日期']
    celue_total_list.loc[row_num, '最新净值日期'] = df_strategy_index.iloc[-1]['净值日期']
    celue_total_list.loc[row_num, '净值更新率'] = "{:.0%}".format((n+1) / num_strategy)
# =============================================================================
#     celue_total_list.loc[row_num, '近一周收益率'] = df_strategy_index.iloc[-1]['复权净值'] / df_strategy_index.loc[df_strategy_index['净值日期'] == time_last_Friday_lag1_time, '复权净值'].values[0] - 1
#     celue_total_list.loc[row_num, '近一周超额（除法）'] = (df_strategy_index.iloc[-1]['成立以来超额（除法）'] + 1) / (1 + df_strategy_index.loc[df_strategy_index['净值日期'] == time_last_Friday_lag1_time, '成立以来超额（除法）'].values[0]) - 1
#     celue_total_list.loc[row_num, '对标指数近一周收益率'] = df_strategy_index.iloc[-1]['对标指数'] / df_strategy_index.loc[df_strategy_index['净值日期'] == time_last_Friday_lag1_time,'对标指数'].values[0] - 1
# =============================================================================
    for col in cols:
        celue_total_list.loc[row_num, col] = df_strategy_index.iloc[-1][col]
    for col in ['成立以来波动率', '今年以来波动率', '近三月波动率', '近六月波动率', '近一年波动率', '近两年波动率', '近三年波动率']:
        celue_total_list.loc[row_num, col] = df_strategy_index[col].dropna().std() * np.sqrt(52) 
    for col in ['成立以来超额波动率', '今年以来超额波动率', '近三月超额波动率', '近六月超额波动率', '近一年超额波动率', '近两年超额波动率', '近三年超额波动率']:
        celue_total_list.loc[row_num, col] = df_strategy_index[col].dropna().std() * np.sqrt(52)
    for col in ['成立以来最大回撤', '今年以来最大回撤', '近三月最大回撤', '近六月最大回撤', '近一年最大回撤', '近两年最大回撤', '近三年最大回撤']:
        celue_total_list.loc[row_num, col] = df_strategy_index[col].dropna().min()
    for col in ['成立以来最大回撤（超额）', '今年以来最大回撤（超额）', '近三月最大回撤（超额）', '近六月最大回撤（超额）', '近一年最大回撤（超额）', '近两年最大回撤（超额）', '近三年最大回撤（超额）']:
        celue_total_list.loc[row_num, col] = df_strategy_index[col].dropna().min()
    
    df_strategy_index.to_sql(name=mf_strategy_index_name, con=engine, if_exists='replace', index=False)
    logging.info(mf_strategy_index_name + '更新完成')
    # 策略指数保存到指数数据库
    df_strategy_index.rename(columns={'净值日期': '日期', '复权净值': '收盘', '样本数量': '成交额'}, inplace=True)
    df_strategy_index.to_sql(name=mf_strategy_index_name, con=engine_index, if_exists='replace', index=False)

celue_total_list.to_sql(name='策略指数汇总表', con=engine, if_exists='replace', index=False)

# 计算所有公募基金的指标
cursor, engine, engine_index, cnx = get_db_connection()
list_mf_strategy = pd.read_sql_query("SELECT * FROM %s" % '公募产品汇总表', engine)

for mf_strategy in mf_strategy_list:
    df_strategy_index = pd.read_sql_query("SELECT * FROM %s" % '公募' + mf_strategy + '策略指数', engine)
    list_strategy = list_mf_strategy[list_mf_strategy['策略类型'] == mf_strategy]
    benchmark_data = BenchmarkIndex_mapping[mf_strategy]
    benchmark_data = benchmark_data.rename(columns={'date': '净值日期', 'close': '对标指数'})
    for mf_code in list_strategy['基金代码']:
        fund_nav_info_df = df_strategy_index[['净值日期', mf_code]]
        # 删除 mf_code 列中包含 NaN 值的行
        fund_nav_info_df = fund_nav_info_df.dropna(subset=[mf_code])
        # 日期参数生成
        periods_dict = {
            '成立以来': (fund_nav_info_df.iloc[0, 0], time_last_Friday_time),
            '近一周': (time_last_Friday_lag1_time, time_last_Friday_time),
            '近四周': (time_last_Friday_lag4_time, time_last_Friday_time),
            '今年以来': (time_last_year_time, time_last_Friday_time),
            '近三月': (time_lag3_month_time, time_last_Friday_time),
            '近六月': (time_lag6_month_time, time_last_Friday_time),
            '近一年': (time_lag_year_time, time_last_Friday_time),
            '近两年': (time_lag2_year_time, time_last_Friday_time),
            '近三年': (time_lag3_year_time, time_last_Friday_time)
        }
        # 根据净值日期找到对应的对标指数值
        merged_df = pd.merge(fund_nav_info_df, benchmark_data, how='left', on='净值日期')
        fund_nav_indicator_df = calculate_fund_metrics(merged_df['净值日期'], merged_df[mf_code], periods_dict, merged_df['对标指数'])
        # 找到基金代码为 mf_code 的行的索引
        index_to_update = list_mf_strategy.index[list_mf_strategy['基金代码'] == mf_code]
        # 将计算得到的指标数据按列名赋值给对应的列
        for col in fund_nav_indicator_df.columns:
            list_mf_strategy.loc[index_to_update, col] = fund_nav_indicator_df[col].values[0]
            
list_mf_strategy.to_sql(name='公募产品汇总表', con=engine, if_exists='replace', index=False)
logging.info('公募基金收益指标更新计算完成')

# 关闭游标
cursor.close()
# 关闭连接
cnx.close()