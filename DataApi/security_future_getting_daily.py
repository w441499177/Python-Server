import sys
sys.path.append('/home/ubuntu/market/dc-66-python-2.0/SelfModule')

import matplotlib.pyplot as plt
import akshare as ak
import pandas as pd
import numpy as np
from datetime import time, datetime, date, timedelta
from downloadindex import data_CSI50, data_CSI300, data_CSI500, data_CSI1000  # 自定义封装函数
from selfdefinfunc import check_a_share_trading_day  # 自定义封装函数
import matplotlib.dates as mdates
import os
from lxml import etree
import time as dtime
from chinese_calendar import is_workday
from openpyxl import load_workbook, Workbook
import mysql.connector
import warnings
from sqlalchemy import create_engine
import os
import sys
import logging

# 检查是否非交易日
check_a_share_trading_day()

warnings.filterwarnings('ignore')

# 设置日志
logging.basicConfig(filename='/home/ubuntu/market/dc-66-python-2.0/execution.log', 
                    level=logging.INFO, 
                    format='%(asctime)s - %(levelname)s - %(message)s', 
                    filemode='a')  # 设置为追加模式

# 环境变量读取数据库秘钥信息
host = os.environ.get('MYSQL_HOST')   
user = os.environ.get('MYSQL_USERNAME')  
password = os.environ.get('MYSQL_PASSWORD')
port = os.environ.get('MYSQL_PORT')

# 数据库存储操作
engine = create_engine(f'mysql+mysqlconnector://{user}:{password}@{host}:{port}/期货行情数据库')

# 更新历史数据
query = "SELECT * FROM {} ORDER BY ROW_NUMBER() OVER() DESC LIMIT 1".format('sec_futures_derivatives_daily')
# 使用 SQL 查询从数据库中读取倒数第一行数据
get_futures_daily_df = pd.read_sql_query(query, engine)
date_latest = (datetime.strptime(get_futures_daily_df.iloc[-1,1], '%Y%m%d') + timedelta(1)).strftime('%Y%m%d')
# 截止日期计算
now = datetime.now() # 获取当前时间
if now.hour >= 16: # 如果当前时间大于下午16点，则减去一天
    today = now 
else:
    today = now - timedelta(days=1)
date_today = datetime.strftime(today, '%Y%m%d') # 更新到今日最新数据
get_futures_daily_df = ak.get_futures_daily(start_date=str(date_latest), end_date=date_today, market="CFFEX")
if get_futures_daily_df is None:
    logging.info('期货数据库最新数据无需更新')
    sys.exit()

# 读取指数数据 #
data_CSI50 = data_CSI50.rename(columns={'close':'spot_index'})
data_CSI50['daily_ret'] = data_CSI50['spot_index'].pct_change()
data_CSI300 = data_CSI300.rename(columns={'close':'spot_index'})
data_CSI300['daily_ret'] = data_CSI300['spot_index'].pct_change()
data_CSI500 = data_CSI500.rename(columns={'close':'spot_index'})
data_CSI500['daily_ret'] = data_CSI500['spot_index'].pct_change()
data_CSI1000 = data_CSI1000.rename(columns={'close':'spot_index'})
data_CSI1000['daily_ret'] = data_CSI1000['spot_index'].pct_change()

# 历史数据转化为不同合约序列
contract_name = {'上证50当月':'IH', '上证50下月':'IH', '上证50当季':'IH', '上证50下季':'IH',
                 '沪深300当月':'IF', '沪深300下月':'IF', '沪深300当季':'IF', '沪深300下季':'IF',
                 '中证500当月':'IC', '中证500下月':'IC', '中证500当季':'IC', '中证500下季':'IC',
                 '中证1000当月':'IM', '中证1000下月':'IM', '中证1000当季':'IM', '中证1000下季':'IM',
    }

for i, m in zip(contract_name.values(), contract_name): #i为IX，m为XX指数X月/季
    mask = get_futures_daily_df['variety'] == i 
    temp = get_futures_daily_df.loc[mask]
    temp = temp.sort_values(by=['date', 'symbol'])
    temp.reset_index(drop=True, inplace=True)
    # 匹配对应主力合约
    if m[-2:] == '当月':
        mask = temp.index % 4 == 0
        temp = temp.loc[mask]
    elif m[-2:] == '下月':
        mask = temp.index % 4 == 1
        temp = temp.loc[mask]
    elif m[-2:] == '当季':
        mask = temp.index % 4 == 2
        temp = temp.loc[mask]
    elif m[-2:] == '下季':
        mask = temp.index % 4 == 3
        temp = temp.loc[mask]
    temp = temp.loc[ : , ~temp.columns.str.contains("^Unnamed")] #删除Unnamed列
    temp.insert(0, 'date', temp.pop('date')) #将前两列调换顺序
    temp['date'] = temp['date'].apply(lambda x: datetime.strptime(str(x), '%Y%m%d'))
    # 匹配对应指数
    if i == 'IH':
        temp = temp.merge(data_CSI50[['date', 'spot_index', 'daily_ret']], on='date', how='left')       
    elif i == 'IF':
        temp = temp.merge(data_CSI300[['date', 'spot_index', 'daily_ret']], on='date', how='left')  
    elif i == 'IC':
        temp = temp.merge(data_CSI500[['date', 'spot_index', 'daily_ret']], on='date', how='left')  
    elif i == 'IM':
        temp = temp.merge(data_CSI1000[['date', 'spot_index', 'daily_ret']], on='date', how='left')
    # 防止出现指数数据缺失的情况   
    if temp['spot_index'].isnull().any():
        logging.error("spot_index column contains null values. Stopping execution.")
        raise SystemExit
    temp['basis'] = temp['settle'] - temp['spot_index']
    # 计算对冲成本
    temp['basis_cost'] = temp['daily_ret'] - (temp['settle']/temp['pre_settle'] - 1)
    temp = temp.drop('daily_ret', axis=1)
    # 到期日，到期时间，年化基差
    temp['expiring_date'] = ''
    temp['due_to_expiring'] = ''
    temp['annual_basis'] = ''
    for n in range(len(temp)): 
        date_exp_first = date(year=2000+int(str(temp.iloc[n, 1])[2:4]),month=int(str(temp.iloc[n, 1])[4:6]),day=1)
        if date.weekday(date_exp_first) < 4:
            date_exp =  date_exp_first + timedelta(days=14 + 4 - date.weekday(date_exp_first))
        elif date.weekday(date_exp_first) == 7:
            date_exp =  date_exp_first + timedelta(days=14 + 4)
        else:
            date_exp =  date_exp_first + timedelta(days=19 + 7 - date.weekday(date_exp_first))
        if date_exp.year == date.today().year:
            for tt in range(10):
                if is_workday(date_exp) == True:
                    temp.loc[n, 'expiring_date'] = date_exp
                    break
                else:
                    date_exp = date_exp + timedelta(days=tt+1)
        else:
            temp.loc[n, 'expiring_date'] = date_exp
        
        temp.loc[n, 'due_to_expiring'] = (datetime.combine(temp.loc[n, 'expiring_date'] ,time()) - temp.loc[n, 'date'].to_pydatetime()).days
        if temp.loc[n, 'due_to_expiring'] != 0:
            temp.loc[n, 'annual_basis'] = (temp.loc[n, 'basis']/temp.loc[n, 'spot_index'])/(temp.loc[n, 'due_to_expiring']/365) 
        else:
            temp.loc[n, 'annual_basis'] = 0
    temp.to_sql(name=m, con=engine, if_exists='append', index=False)
    print(m + '合约行情更新')

get_futures_daily_df.to_sql(name='sec_futures_derivatives_daily', con=engine, if_exists='append', index=False)
logging.info('每日股指期货数据更新完毕')