import sys
sys.path.append('/home/ubuntu/market/dc-66-python-2.0/SelfModule')

from tradedate import *  # 自定义封装函数
from selfdefinfunc import check_a_share_trading_day
import akshare as ak
import numpy as np
import pandas as pd
from datetime import datetime, date, timedelta
import warnings
import mysql.connector
from selenium.webdriver.chrome.options import Options
from sqlalchemy import create_engine, text
from sqlalchemy.exc import NoSuchTableError
import requests
import json
import time
import os
import logging

# 设置日志
logging.basicConfig(filename='/home/ubuntu/market/dc-66-python-2.0/execution.log', 
                    level=logging.INFO, 
                    format='%(asctime)s - %(levelname)s - %(message)s', 
                    filemode='a')  # 设置为追加模式

warnings.filterwarnings('ignore')

# 检查是否非交易日
check_a_share_trading_day()

# 环境变量读取数据库秘钥信息
host = os.environ.get('MYSQL_HOST')   
user = os.environ.get('MYSQL_USERNAME')  
password = os.environ.get('MYSQL_PASSWORD')
port = os.environ.get('MYSQL_PORT')
refreshToken = os.environ.get('iFind_refreshToken')

# 数据库存储操作
cnx = mysql.connector.connect(user=user, password=password,
                              host=host,  port=port, database='同业产品数据库')
engine = create_engine(f'mysql+mysqlconnector://{user}:{password}@{host}:{port}/同业产品数据库')
cursor = cnx.cursor()

# 获取access_token
getAccessTokenUrl = 'https://quantapi.51ifind.com/api/v1/get_access_token'
getAccessTokenHeader = {"ContentType":"application/json","refresh_token":refreshToken}
getAccessTokenResponse = requests.post(url=getAccessTokenUrl,headers=getAccessTokenHeader)
accessToken = json.loads(getAccessTokenResponse.content)['data']['access_token']
thsHeaders = {"Content-Type":"application/json","access_token": accessToken}

# 1. 从数据库中读取 "ETF基础信息表"
ETF_df = pd.read_sql("ETF基础信息表", con=engine)
# 2. 通过接口 ak.fund_etf_category_sina(symbol="ETF基金") 获取数据
etf_data = ak.fund_etf_spot_em()
# 3. 将接口获取的数据与 "ETF基础信息表" 进行合并
# 首先，将 etf_data 中的新代码添加到 ETF_df 中
new_codes = etf_data[~etf_data['代码'].isin(ETF_df['代码'])]
ETF_df = pd.concat([ETF_df, new_codes], ignore_index=True)
# 4. 更新 ETF_df 中的现有代码数据
# 使用 merge 进行合并，并直接覆盖重复的列
ETF_df = ETF_df.merge(etf_data, on='代码', how='left', suffixes=('', '_new'))
# 5. 更新列数据
for col in etf_data.columns:
    if col != '代码':
        ETF_df[col] = ETF_df[col + '_new']
# 6. 删除多余的列
ETF_df = ETF_df.drop(columns=[col + '_new' for col in etf_data.columns if col != '代码'])
# 提取 '代码' 列的前两个字母，并在字母后加上 '.'，然后移动到字符串的最后
ETF_df['同花顺代码'] = ETF_df['代码'].apply(lambda x: x + '.SZ' if x.startswith('1') else (x + '.SH' if x.startswith('5') else x))
# 提取 'ETF简称' 为空的行，并获取 '同花顺代码' 列的值
empty_etf_short_name_rows = ETF_df[pd.isna(ETF_df['ETF简称'])]

# 如果 empty_etf_short_name_rows 为空，则跳过后边的步骤
# 获取当前时间
now = datetime.now()
# 检查当前时间是否在下午3点到5点之间
if now.hour >= 15 and now.hour < 17:
    # 如果 empty_etf_short_name_rows 为空，则跳过后边的步骤
    if not empty_etf_short_name_rows.empty:
        # 同花顺http接口数据访问地址
        thsUrl = 'https://quantapi.51ifind.com/api/v1/basic_data_service'
        
        codes_to_fetch = ','.join(empty_etf_short_name_rows['同花顺代码'].tolist())
        # 构造请求参数
        para = {
            "codes": codes_to_fetch,
            "indipara": [
                {"indicator": "ths_fund_short_name_fund"},
                {"indicator": "ths_name_of_tracking_index_fund"},
                {"indicator": "ths_invest_type_first_classi_fund"},
                {"indicator": "ths_invest_type_second_classi_fund"}
            ]
        }
        thsResponse = requests.post(url=thsUrl, json=para, headers=thsHeaders)
        parsed_data = json.loads(thsResponse.content)
        # 将返回的数据赋值给 ETF_df 中对应行的四个空列
        for i, code in enumerate(codes_to_fetch.split(',')):
            product_data = parsed_data["tables"][i]['table']
            ETF_df.loc[ETF_df['同花顺代码'] == code, 'ETF简称'] = product_data['ths_fund_short_name_fund'][0]
            ETF_df.loc[ETF_df['同花顺代码'] == code, '跟踪指数'] = product_data['ths_name_of_tracking_index_fund'][0]
            ETF_df.loc[ETF_df['同花顺代码'] == code, '基金一级分类'] = product_data['ths_invest_type_first_classi_fund'][0]
            ETF_df.loc[ETF_df['同花顺代码'] == code, '基金二级分类'] = product_data['ths_invest_type_second_classi_fund'][0]
        logging.info(codes_to_fetch + '更新ETF基础信息')
    ETF_df.to_sql(name="ETF基础信息表", con=engine, if_exists='replace', index=False)
    logging.info('ETF基础信息表成功保存')
else:
    pass

def remove_duplicate_last_row(engine, etf_df):
    for index, row in etf_df.iterrows():
        etf_short_name = row['ETF简称']
        table_name = f"{etf_short_name}"
        
        # 检查表是否存在
        try:
            # 读取表的最后两行
            query = f"SELECT * FROM `{table_name}` ORDER BY 日期 DESC LIMIT 2"
            last_two_rows = pd.read_sql_query(text(query), con=engine)
            
            # 检查是否有至少两行数据
            if len(last_two_rows) < 2:
                print(f"{etf_short_name} 数据不足两行，无需检查")
                continue
            
            # 获取最后两行的“基金规模”值
            last_scale = last_two_rows.iloc[0]['基金规模']
            second_last_scale = last_two_rows.iloc[1]['基金规模']
            
            # 比较两个值是否相等
            if pd.isna(last_scale) or pd.isna(second_last_scale):
                print(f"{etf_short_name} 最后两行基金规模数据有缺失值，跳过")
                continue
            
            if last_scale == second_last_scale:
                # 删除最后一行
                delete_query = f"DELETE FROM `{table_name}` ORDER BY 日期 DESC LIMIT 1"
                with engine.connect() as connection:
                    connection.execute(text(delete_query))
                    connection.commit()  # 提交事务
                print(f"删除了 {etf_short_name} 的最后一行数据")
            else:
                pass
        
        except NoSuchTableError:
            print(f"表 {table_name} 不存在，跳过")
        except Exception as e:
            print(f"处理 {etf_short_name} 时发生错误: {e}")
remove_duplicate_last_row(engine, ETF_df)

# 同花顺http接口数据访问地址
thsUrl = 'https://quantapi.51ifind.com/api/v1/date_sequence'
# 遍历 ETF_df 中的每一行
for index, row in ETF_df.iterrows():
    # 获取 ETF 简称
    etf_short_name = row['ETF简称']
    ifind_etf_code = row['同花顺代码']
    # 获取代码的后六位编码
    symbol = row['代码'][-6:]

    # 检查数据库中是否存在同名表格
    table_name = f"{etf_short_name}"
    try:
        # 读取表格的最后一行
        last_row = pd.read_sql("SELECT * FROM `{}` ORDER BY 日期 DESC LIMIT 1".format(table_name), con=engine)
        last_date = last_row['日期'].iloc[0]
        if last_date >= time_this_Friday_time:
            print(f"{etf_short_name} 数据库已是最新日期数据")
            continue
        # 将日期转换为 datetime 对象
        last_date = last_date.to_pydatetime()
        # 计算下一日的日期
        start_date = (last_date + timedelta(days=1)).strftime('%Y%m%d')
        fund_etf_hist_em_df = ak.fund_etf_hist_em(symbol=symbol, period="daily", start_date=start_date, adjust="hfq")
    except:
        fund_etf_hist_em_df = ak.fund_etf_hist_em(symbol=symbol, period="daily", adjust="hfq")

    # 获取历史数据
    
    # 判断 fund_etf_hist_em_df 是否为空
    if fund_etf_hist_em_df.empty:
        print(f"{etf_short_name} 获取的历史数据为空，跳过此次循环")
        continue
    
    # 判断当前时间是否早于晚上8点
    current_time = datetime.now()
    if current_time.hour < 20:  # 20 表示晚上8点
        # 将 '日期' 列转换为 datetime 类型
        fund_etf_hist_em_df['日期'] = pd.to_datetime(fund_etf_hist_em_df['日期'])
        # 截取到今天之前的日期
        today = current_time.date()
        fund_etf_hist_em_df = fund_etf_hist_em_df[fund_etf_hist_em_df['日期'].dt.date < today]

    # 判断 fund_etf_hist_em_df 是否为空
    if fund_etf_hist_em_df.empty:
        print(f"{etf_short_name} 昨日的历史数据为空，跳过此次循环")
        continue

    # 获取 '日期' 列的起止日期
    start_date_str = pd.to_datetime(fund_etf_hist_em_df['日期'].min()).strftime('%Y%m%d')
    end_date_str = pd.to_datetime(fund_etf_hist_em_df['日期'].max()).strftime('%Y%m%d')
    
    # 构造请求参数
    para = {
        "codes": ifind_etf_code,
        "indipara": [
            {"indicator": "ths_fund_shares_fund", "indiparams": [""]},
            {"indicator": "ths_fund_scale_fund", "indiparams": [""]}
        ],
        "startdate": start_date_str,
        "enddate": end_date_str
    }
    
    # 发送请求
    thsResponse = requests.post(url=thsUrl, json=para, headers=thsHeaders)
    parsed_data = json.loads(thsResponse.content)
    
    # 获取返回的数据
    product_date = parsed_data["tables"][0]['time']
    product_data = parsed_data["tables"][0]['table']
    
    # 将返回的数据转换为 DataFrame
    product_df = pd.DataFrame({
        '日期': product_date,
        '基金份额': product_data['ths_fund_shares_fund'],
        '基金规模': product_data['ths_fund_scale_fund']
    })
    
    # 将 '日期' 列转换为 datetime 对象
    product_df['日期'] = pd.to_datetime(product_df['日期'])
    # 将 fund_etf_hist_em_df 的 '日期' 列转换为 datetime 对象
    fund_etf_hist_em_df['日期'] = pd.to_datetime(fund_etf_hist_em_df['日期']) 
    # 按照 '日期' 列进行合并
    merged_df = fund_etf_hist_em_df.merge(product_df, on='日期', how='left')   
    # 将合并后的数据写入数据库
    merged_df.to_sql(etf_short_name, con=engine, if_exists='append', index=False)

logging.info("所有ETF最新份额信息全部更新")


