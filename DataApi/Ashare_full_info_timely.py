import akshare as ak
from sqlalchemy import create_engine
import pandas as pd
import os
import mysql.connector
from datetime import datetime, timedelta
from chinese_calendar import is_workday
import warnings
import logging

# 从环境变量读取数据库秘钥信息
MYSQL_HOST = os.environ.get('MYSQL_HOST')   
MYSQL_USERNAME = os.environ.get('MYSQL_USERNAME')  
MYSQL_PASSWORD = os.environ.get('MYSQL_PASSWORD')
MYSQL_PORT = os.environ.get('MYSQL_PORT')

warnings.filterwarnings('ignore')

# 设置日志
logging.basicConfig(filename='/home/ubuntu/market/dc-66-python-2.0/execution.log', 
                    level=logging.INFO, 
                    format='%(asctime)s - %(levelname)s - %(message)s', 
                    filemode='a')  # 设置为追加模式

def connect_to_database():
    # 连接到 MySQL 数据库
    connection = mysql.connector.connect(
        user=MYSQL_USERNAME,
        password=MYSQL_PASSWORD,
        host=MYSQL_HOST,
        port=MYSQL_PORT,
        database='股票行情数据库'
    )
    # 创建 SQLAlchemy 引擎
    engine = create_engine(f'mysql+mysqlconnector://{MYSQL_USERNAME}:{MYSQL_PASSWORD}@{MYSQL_HOST}:{MYSQL_PORT}/股票行情数据库')
    return connection, engine

def is_trading_time(now):
    trading_start = datetime.strptime('09:30', '%H:%M').time()
    trading_end = datetime.strptime('15:02', '%H:%M').time()
    lunch_break_start = datetime.strptime('11:32', '%H:%M').time()
    lunch_break_end = datetime.strptime('13:00', '%H:%M').time()
    
    if trading_start <= now.time() <= lunch_break_start:
        return True
    elif lunch_break_end <= now.time() <= trading_end:
        return True
    else:
        return False

def get_week_start_end(date):
    # 获取本周的开始和结束日期
    start_of_week = date - timedelta(days=date.weekday())
    start_of_week = start_of_week.replace(hour=0, minute=0, second=0, microsecond=0)
    end_of_week = start_of_week + timedelta(days=6)
    return start_of_week, end_of_week

def main():
    now = datetime.now()
    if is_workday(now) and is_trading_time(now):
        connection, engine = connect_to_database()
        stock_zh_a_spot_em_df = ak.stock_zh_a_spot_em()
        
        # 添加时间戳列
        stock_zh_a_spot_em_df['更新时间'] = now
        
        # 将原始数据写入数据库
        stock_zh_a_spot_em_df.to_sql(name='A股盘中实时股票行情表', con=engine, if_exists='replace', index=False)
        
        # 如果现在是当天15:00之后，则同时保存到'A股日度行情时序表'
        if now.hour >= 15:
            stock_zh_a_spot_em_df.to_sql(name='A股日度行情时序表', con=engine, if_exists='append', index=False)

            # 获取本周的开始和结束日期
            start_of_week, end_of_week = get_week_start_end(now)

            # 从'A股日度行情时序表'中提取本周的数据
            query = f"SELECT * FROM `A股日度行情时序表` WHERE `更新时间` BETWEEN '{start_of_week}' AND '{end_of_week}'"
            selected_df = pd.read_sql_query(query, engine)
            # 剔除北交所股票
            selected_df = selected_df[~selected_df['代码'].astype(str).str.startswith('8') & ~selected_df['代码'].astype(str).str.startswith('9')]

            # 生成'A股每周股票行情统计表'
            selected_df = selected_df.groupby('代码').agg({
                '最新价': 'last',
                '昨收': 'first',
                '成交额': 'mean',
                '总市值': 'mean',
                '更新时间': 'max'
            })

            selected_df['涨跌幅'] = (selected_df['最新价'] / selected_df['昨收'])*100 - 100

            # 删除包含 NaN 的行
            selected_df = selected_df.dropna()
            
            # 定义涨跌幅的分组
            bins = [-float('inf'), -10, -7, -5, -3, -1, 0, 1, 3, 5, 7, 10, float('inf')]
            labels = ['<=-10', '-10~-7', '-7~-5', '-5~-3', '-3~-1', '-1~0', '0~1', '1~3', '3~5', '5~7', '7~10', '>=10']
            selected_df['涨跌幅分组'] = pd.cut(selected_df['涨跌幅'], bins=bins, labels=labels, right=False)
            
            # 定义总市值的分组
            bins_market_cap = [0, 2000000000, 5000000000, 10000000000, 50000000000, 100000000000, 200000000000, float('inf')]
            labels_market_cap = ['<20亿', '20~50亿', '50~100亿', '100~500亿', '500~1000亿', '1000~2000亿', '>=2000亿']
            selected_df['总市值分组'] = pd.cut(selected_df['总市值'], bins=bins_market_cap, labels=labels_market_cap, right=False)
            
            # 统计每个涨跌幅分组的数量
            count_by_change_group = selected_df['涨跌幅分组'].value_counts().reset_index()
            count_by_change_group.columns = ['涨跌幅分组', '数量']
            
            # 按照涨跌幅分组的标签顺序排序
            count_by_change_group = count_by_change_group.sort_values(by='涨跌幅分组').reset_index(drop=True)
            
            # 统计每个市值分组下对应的涨跌幅中位数
            median_by_market_cap_group = selected_df.groupby('总市值分组')['涨跌幅'].median().reset_index()
            median_by_market_cap_group.columns = ['总市值分组', '涨跌幅中位数']
            
            # 按照总市值分组的标签顺序排序
            median_by_market_cap_group = median_by_market_cap_group.sort_values(by='总市值分组').reset_index(drop=True)
            
            # 计算全部股票的涨跌幅中位数
            overall_median_change = selected_df['涨跌幅'].median()

            # 创建一个新的标签和统计数据
            overall_median_change_stats = pd.DataFrame({
                '总市值分组': ['全部股票'],
                '涨跌幅中位数': [overall_median_change]
            })

            # 将全部股票的涨跌幅中位数统计数据添加到结果数据框中
            median_by_market_cap_group = pd.concat([median_by_market_cap_group, overall_median_change_stats], ignore_index=True)

            # 计算总成交额
            total_turnover = selected_df['成交额'].sum()
            
            # 将涨跌幅分组和总市值分组的统计结果合并成一列
            result_df = pd.concat([count_by_change_group, median_by_market_cap_group], axis=1)
            
            # 将总成交额和时间列添加到合并后的 result_df 中
            result_df['总成交额'] = total_turnover
            result_df['更新时间'] = now

            # 将结果写入数据库
            result_df.to_sql(name='A股每周股票行情统计表', con=engine, if_exists='replace', index=False)

            logging.info('A股日度行情数据保存完毕')

        stock_zh_a_spot_em_df = stock_zh_a_spot_em_df[~(stock_zh_a_spot_em_df['代码'].astype(str).str.startswith('8') | stock_zh_a_spot_em_df['代码'].astype(str).str.startswith('9'))]
        
        # 选取需要的列
        selected_df = stock_zh_a_spot_em_df[['涨跌幅', '成交额', '总市值', '更新时间']]
        
        # 删除包含 NaN 的行
        selected_df = selected_df.dropna()
        
        # 定义涨跌幅的分组
        bins = [-float('inf'), -10, -7, -5, -3, -1, 0, 1, 3, 5, 7, 10, float('inf')]
        labels = ['<=-10', '-10~-7', '-7~-5', '-5~-3', '-3~-1', '-1~0', '0~1', '1~3', '3~5', '5~7', '7~10', '>=10']
        selected_df['涨跌幅分组'] = pd.cut(selected_df['涨跌幅'], bins=bins, labels=labels, right=False)
        
        # 定义总市值的分组
        bins_market_cap = [0, 2000000000, 5000000000, 10000000000, 50000000000, 100000000000, 200000000000, float('inf')]
        labels_market_cap = ['<20亿', '20~50亿', '50~100亿', '100~500亿', '500~1000亿', '1000~2000亿', '>=2000亿']
        selected_df['总市值分组'] = pd.cut(selected_df['总市值'], bins=bins_market_cap, labels=labels_market_cap, right=False)
        
        # 统计每个涨跌幅分组的数量
        count_by_change_group = selected_df['涨跌幅分组'].value_counts().reset_index()
        count_by_change_group.columns = ['涨跌幅分组', '数量']
        
        # 按照涨跌幅分组的标签顺序排序
        count_by_change_group = count_by_change_group.sort_values(by='涨跌幅分组').reset_index(drop=True)
        
        # 统计每个市值分组下对应的涨跌幅中位数
        median_by_market_cap_group = selected_df.groupby('总市值分组')['涨跌幅'].median().reset_index()
        median_by_market_cap_group.columns = ['总市值分组', '涨跌幅中位数']
        
        # 按照总市值分组的标签顺序排序
        median_by_market_cap_group = median_by_market_cap_group.sort_values(by='总市值分组').reset_index(drop=True)
        
        # 计算全部股票的涨跌幅中位数
        overall_median_change = selected_df['涨跌幅'].median()

        # 创建一个新的标签和统计数据
        overall_median_change_stats = pd.DataFrame({
            '总市值分组': ['全部股票'],
            '涨跌幅中位数': [overall_median_change]
        })

        # 将全部股票的涨跌幅中位数统计数据添加到结果数据框中
        median_by_market_cap_group = pd.concat([median_by_market_cap_group, overall_median_change_stats], ignore_index=True)

        # 计算总成交额
        total_turnover = selected_df['成交额'].sum()
        
        # 将涨跌幅分组和总市值分组的统计结果合并成一列
        result_df = pd.concat([count_by_change_group, median_by_market_cap_group], axis=1)
        
        # 将总成交额和时间列添加到合并后的 result_df 中
        result_df['总成交额'] = total_turnover
        result_df['更新时间'] = now
        
        # 将结果写入数据库
        result_df.to_sql(name='A股盘中实时股票行情统计表', con=engine, if_exists='replace', index=False)

        # 申万一级行业实时行情
        index_realtime_sw_df = ak.index_realtime_sw(symbol="一级行业")
        index_realtime_sw_df['涨跌幅'] = index_realtime_sw_df['最新价'] / index_realtime_sw_df['昨收盘'] - 1
        index_realtime_sw_df['振幅'] = (index_realtime_sw_df['最高价'] - index_realtime_sw_df['最低价']) / index_realtime_sw_df['昨收盘']
        
        # 添加时间戳列
        index_realtime_sw_df['更新时间'] = now

        index_realtime_sw_df.to_sql(name='申万一级行业实时行情统计表', con=engine, if_exists='replace', index=False)

        # 如果现在是当天15:00之后，则同时保存到'A股日度行情时序表'
        if now.hour >= 15:
            index_realtime_sw_df.to_sql(name='申万一级行业日度行情时序表', con=engine, if_exists='append', index=False)
            logging.info('申万一级行业日度行情数据保存完毕')

            # 获取本周的开始和结束日期
            start_of_week, end_of_week = get_week_start_end(now)
            # 从'申万一级行业日度行情时序表'中提取本周的数据
            query = f"SELECT * FROM `申万一级行业日度行情时序表` WHERE `更新时间` BETWEEN '{start_of_week}' AND '{end_of_week}'"
            week_df = pd.read_sql_query(query, engine)
            # 统计本周以来的数据
            weekly_stats_df = week_df.groupby('指数代码').agg({
                '指数代码': 'first',  # 保留最早日期的指数代码
                '指数名称': 'first',  # 保留最早日期的指数名称
                '昨收盘': 'first',  # 保留最早日期的昨收盘
                '今开盘': 'last',   # 保留最晚日期的今开盘
                '最新价': 'last',   # 保留最晚日期的最新价
                '成交额': 'mean',   # 保留平均值
                '成交量': 'mean',   # 保留平均值
                '最高价': 'max',    # 保留最大值
                '最低价': 'min',    # 保留最小值
                '涨跌幅': 'mean',    # 保留平均值
                '振幅': 'max',    # 保留平均值
                '更新时间': 'max'   # 保留最近的更新时间
            })
            # 计算涨跌幅和振幅
            weekly_stats_df['涨跌幅'] = (weekly_stats_df['最新价'] / weekly_stats_df['昨收盘']) - 1
            weekly_stats_df['振幅'] = (weekly_stats_df['最高价'] - weekly_stats_df['最低价']) / weekly_stats_df['昨收盘']
            # 将结果写入数据库
            weekly_stats_df.to_sql(name='申万一级行业每周行情统计表', con=engine, if_exists='replace', index=False)

        connection.close()

        logging.info('A股盘中实时行情数据读取解析完毕')
    else:
        pass

if __name__ == "__main__":
    main()