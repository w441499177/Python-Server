import sys
sys.path.append('/home/ubuntu/market/dc-66-python-2.0/SelfModule')

from tradedate import *  # 自定义封装函数
from selfdefinfunc import check_a_share_trading_day
import akshare as ak
import numpy as np
import pandas as pd
from datetime import datetime, date, timedelta
import warnings
import mysql.connector
from selenium.webdriver.chrome.options import Options
from sqlalchemy import create_engine
import requests
import json
import time
import os
import sys
import logging

warnings.filterwarnings('ignore')

# 设置日志
logging.basicConfig(filename='/home/ubuntu/market/dc-66-python-2.0/execution.log', 
                    level=logging.INFO, 
                    format='%(asctime)s - %(levelname)s - %(message)s', 
                    filemode='a')  # 设置为追加模式

# 检查是否非交易日
check_a_share_trading_day()

# 环境变量读取数据库秘钥信息
host = os.environ.get('MYSQL_HOST')   
user = os.environ.get('MYSQL_USERNAME')  
password = os.environ.get('MYSQL_PASSWORD')
port = os.environ.get('MYSQL_PORT')
refreshToken = os.environ.get('iFind_refreshToken')

# 数据库存储操作
cnx = mysql.connector.connect(user=user, password=password,
                              host=host,  port=port, database='指数行情数据库')
engine = create_engine(f'mysql+mysqlconnector://{user}:{password}@{host}:{port}/指数行情数据库')
cursor = cnx.cursor()

# 暂停'深证成指': '399001'
index_dict = {
    '上证指数': '000001', '沪深300': '000300', '创业板指': '399006', '上证50': '000016', '中证500': '000905', '中小100': '399005', '深证红利': '399324', '中证100': '000903', '中证A500': '000510',
    '基金指数': '000011', '国债指数': '000012', '中证转债': '000832',
    '上证红利': '000015', '中证全指': '000985',  '科创50': '000688', '中证1000': '000852', '中证800': '000906', '创业板50': '399673', '深证100': '399330', '国债指数': '000012', '北证50': '899050',
    '国证2000': '399303', '中证2000': '932000', '偏股基金': '930950', '纯债基金': '930609', '非纯债基': '930897', '华证微盘': '999004.SSI', '南华商品指数': 'NHCI.SL'}  # '南华金属指数': 'NHMI', '南华农产品指数': 'NHAI', '南华工业品指数': 'NHII', '南华能化指数': 'NHECI', '南华股指指数': 'IF', '南华贵金属指数': 'NHPMI'

index_pbpe = ["上证50", "沪深300", "创业板50", "中证500", "深证红利", "深证100", "中证1000", "上证红利", "中证800"]
index_csindex_data_source = ['偏股基金', '纯债基金', '非纯债基']
index_ifind_source = ['华证微盘', '南华商品指数'] # 
index_futures_source = []  # '南华金属指数', '南华农产品指数', '南华工业品指数', '南华能化指数', '南华股指指数', '南华贵金属指数'

# 获取access_token
getAccessTokenUrl = 'https://quantapi.51ifind.com/api/v1/get_access_token'
getAccessTokenHeader = {"ContentType":"application/json","refresh_token":refreshToken}
getAccessTokenResponse = requests.post(url=getAccessTokenUrl,headers=getAccessTokenHeader)
try:
    accessToken = json.loads(getAccessTokenResponse.content)['data']['access_token']
    thsHeaders = {"Content-Type":"application/json","access_token": accessToken}
except:
    logging.error('iFindToken需更新，跳过相关指数，请注意！！！！！！')
    index_ifind_source = []
# 同花顺http接口数据访问地址
thsUrl = 'https://quantapi.51ifind.com/api/v1/cmd_history_quotation'

# 截止日期计算
# 获取当前时间
now = datetime.now()
# 如果当前时间大于下午3点10分，则减去一天
if now.hour > 15 or (now.hour == 15 and now.minute > 10):
    today = now 
else:
    today = now - timedelta(days=1)
# 将日期格式化为指定的字符串格式
end_date = today.strftime('%Y%m%d')
n = 0

for name, index in index_dict.items():
    # 判断是否为新加入数据
    cursor.execute("SHOW TABLES LIKE %s", (name,))
    chanpin_judge = cursor.fetchone() == None
    # 数据库中没有指数时
    if chanpin_judge:
        if name in index_csindex_data_source:
            index_df = ak.stock_zh_index_hist_csindex(symbol=index, start_date="19700101", end_date=end_date)
            # 重命名列
            index_df = index_df.rename(columns={'成交金额': '成交额'})
            # 将'成交额'列中的所有数乘以100000000
            index_df['成交额'] = index_df['成交额'].apply(lambda x: x * 100000000)
            index_df = index_df.drop(['指数代码', '指数中文全称', '指数中文简称', '指数英文全称', '指数英文简称'], axis=1)
        elif name in index_ifind_source:
            para = {"codes": index,"indicators": "open,high,low,close,volume,amount","startdate":"19700101","enddate":end_date}
            thsResponse = requests.post(url=thsUrl, json=para, headers=thsHeaders)
            parsed_data = json.loads(thsResponse.content)
            product_date = parsed_data["tables"][0]['time']
            product_data = parsed_data["tables"][0]['table']
            # 将数据转换为DataFrame
            index_df = pd.DataFrame({
                '日期': product_date,
                '开盘': product_data['open'],
                '收盘': product_data['close'],
                '最高': product_data['high'],
                '最低': product_data['low'],
                '成交量': product_data['volume'],
                '成交额': product_data['amount']
            })
        elif name in index_futures_source:
            product_data = ak.futures_return_index_nh(symbol=index)
            # 将数据转换为DataFrame
            index_df = pd.DataFrame({
                '日期': product_data['date'],
                '收盘': product_data['value'],
                '成交额': np.nan
            })
        else:
            index_df = ak.index_zh_a_hist(symbol=index, period="daily", start_date="19900101", end_date=end_date)

        index_df['日期'] = pd.to_datetime(index_df['日期'])
        # 删除'日期'列的值为'1970-01-01'的这一行
        index_df = index_df[index_df['日期'] != '1970-01-01'].reset_index(drop=True)
        
        if name in index_pbpe:
            index_pb_df = ak.stock_index_pb_lg(symbol=name)
            index_pb_df['日期'] = pd.to_datetime(index_pb_df['日期'])
            index_pe_df = ak.stock_index_pe_lg(symbol=name)
            index_pe_df['日期'] = pd.to_datetime(index_pe_df['日期'])
            index_df = pd.merge(index_df, index_pb_df.drop(['指数'], axis=1), on='日期', how='left')
            index_df = pd.merge(index_df, index_pe_df.drop(['指数'], axis=1), on='日期', how='left')
    
    # 数据库需要更新时
    else:
        index_load_df = pd.read_sql_query("SELECT * FROM %s" % name, engine)
        jiezhi_date = index_load_df['日期'].iloc[-1].date()
        if jiezhi_date == today.date():
            n = n + 1
            print(f'{n}/{len(index_dict)} {name} 无需更新')
            continue
        jiezhi_date = (jiezhi_date+timedelta(days=1)).strftime('%Y%m%d')
        
        if name in index_csindex_data_source:
            try:
                index_df = ak.stock_zh_index_hist_csindex(symbol=index, start_date=jiezhi_date, end_date=end_date)
                index_df = index_df.drop(['指数代码', '指数中文全称', '指数中文简称', '指数英文全称', '指数英文简称'], axis=1)
                # 重命名列
                index_df = index_df.rename(columns={'成交金额': '成交额'})
                # 将'成交额'列中的所有数乘以100000000
                index_df['成交额'] = index_df['成交额'].apply(lambda x: x * 100000000)
            except:
                n = n + 1
                print(f'{n}/{len(index_dict)} {name} 数据源未更新')
                continue
        elif name in index_ifind_source:
            para = {"codes": index,"indicators": "open,high,low,close,volume,amount","startdate":jiezhi_date,"enddate":end_date}
            thsResponse = requests.post(url=thsUrl, json=para, headers=thsHeaders)
            parsed_data = json.loads(thsResponse.content)
            product_date = parsed_data["tables"][0]['time']
            product_data = parsed_data["tables"][0]['table']
            # 将数据转换为DataFrame
            index_df = pd.DataFrame({
                '日期': product_date,
                '开盘': product_data['open'],
                '收盘': product_data['close'],
                '最高': product_data['high'],
                '最低': product_data['low'],
                '成交量': product_data['volume'],
                '成交额': product_data['amount']
            })
        elif name in index_futures_source:
            try:
                product_data = ak.futures_return_index_nh(symbol=index)
            except:
                logging.error(f'{index} 数据源错误')
                continue
            # 将数据转换为DataFrame
            index_df = pd.DataFrame({
                '日期': product_data['date'],
                '收盘': product_data['value'],
                '成交额': np.nan
            })
            index_df = index_df[index_df['日期'] > index_load_df['日期'].iloc[-1].date()]
        else:
            index_df = ak.index_zh_a_hist(symbol=index, period="daily", start_date=jiezhi_date, end_date=end_date)
        
        index_df['日期'] = pd.to_datetime(index_df['日期'])
        index_df = pd.concat([index_load_df, index_df], ignore_index=True).drop_duplicates(subset='日期')
        if name in index_pbpe:
            index_pb_df = ak.stock_index_pb_lg(symbol=name)
            index_pb_df['日期'] = pd.to_datetime(index_pb_df['日期'])
            index_pe_df = ak.stock_index_pe_lg(symbol=name)
            index_pe_df['日期'] = pd.to_datetime(index_pe_df['日期'])
            index_df = pd.merge(index_df.loc[:, '日期':'换手率'], index_pb_df.drop(['指数'], axis=1), on='日期', how='left')
            index_df = pd.merge(index_df, index_pe_df.drop(['指数'], axis=1), on='日期', how='left')
            if np.isnan(index_pb_df['市净率']).any():
                # 立即停止脚本的执行
                logging.error("市净率列中有nan值，脚本停止执行。")
                sys.exit()
    
    # 增加数据列 #
    index_df['近四周收益率'] = np.nan
    index_df['今年以来收益率'] = np.nan
    index_df['近三月收益率'] = np.nan
    index_df['近六月收益率'] = np.nan
    index_df['近一年收益率'] = np.nan
    index_df['近两年收益率'] = np.nan
    index_df['近三年收益率'] = np.nan
    index_df['成立以来收益率'] = np.nan
    
    netvalue_target_chanpin = index_df.loc[0, '收盘']
    index_df['成立以来收益率'] = index_df.loc[:, '收盘']/netvalue_target_chanpin - 1
    
    for start_date, columns_name in zip([time_time_last_Friday_lag4_time, time_last_year_time, time_lag3_month_time, time_lag6_month_time, time_lag_year_time, time_lag2_year_time, time_lag3_year_time],
            ['近四周收益率', '今年以来收益率', '近三月收益率', '近六月收益率', '近一年收益率', '近两年收益率', '近三年收益率']):
        # 计算收益序列 #
        mask = index_df['日期'] >= start_date
        try:
            netvalue_last_year_chanpin = float((index_df.loc[index_df['日期'] == start_date, ['收盘']]).values[0][0])
        except:
            continue
        index_df[columns_name] = index_df.loc[mask].loc[:, '收盘']/netvalue_last_year_chanpin - 1     
        nav_series = index_df.loc[mask, columns_name]
        
    n = n + 1
    index_df.to_sql(name=name, con=engine, if_exists='replace', index=False)
    print(f'{n}/{len(index_dict)} {name} 存储完成')


# 查询数据库中'日期'列的最后截止日期
query = "SELECT MAX(日期) FROM 中美国债收益率数据库"
last_date = pd.read_sql_query(query, con=engine)['MAX(日期)'].iloc[0]

# 计算下一个日期（截止日期后的一天）作为新的start_date
if last_date:
    next_date = (last_date + pd.DateOffset(1)).date()
else:
    # 如果数据库中没有数据，则从指定的日期开始
    next_date = pd.to_datetime("1999-12-31").date()

# 从指定日期开始获取中美国债收益率数据
bond_zh_us_rate_df = ak.bond_zh_us_rate(start_date=next_date.strftime('%Y%m%d'))
bond_zh_us_rate_df.rename(columns={'中国国债收益率10年-2年': '中国国债收益率10年减2年', '美国国债收益率10年-2年': '美国国债收益率10年减2年'}, inplace=True)
bond_zh_us_rate_df['日期'] = pd.to_datetime(bond_zh_us_rate_df['日期'], format='%Y-%m-%d')

try:
    # 检查 '美国国债收益率2年' 列是否有数据
    if not pd.isnull(bond_zh_us_rate_df['美国国债收益率2年'].iloc[-1]):
        # 将新数据插入数据库
        bond_zh_us_rate_df.to_sql(name='中美国债收益率数据库', con=engine, if_exists='append', index=False)
        print('中美国债收益率数据库存储完成')
except:
    pass

# 定义字段名称映射
field_mapping = {
    '中国国债收益率2年': '中国2年期国债',
    '中国国债收益率5年': '中国5年期国债',
    '中国国债收益率10年': '中国10年期国债',
    '中国国债收益率30年': '中国30年期国债'
}

# 定义需要处理的国债字段
bond_fields = ['中国2年期国债', '中国5年期国债', '中国10年期国债', '中国30年期国债']

for target_field in bond_fields:
    # 获取原始字段名称
    source_field = next((k for k, v in field_mapping.items() if v == target_field), None)
    if source_field is None:
        continue
    # 检查数据库中是否存在该表
    cursor.execute(f"SHOW TABLES LIKE '{target_field}'")
    table_exists = cursor.fetchone()
    if not table_exists:
        bond_zh_us_rate_df = pd.read_sql_query("SELECT * FROM %s" % '中美国债收益率数据库', con=engine)
        # 过滤掉'收盘'列中存在空值的行
        filtered_df = bond_zh_us_rate_df[~bond_zh_us_rate_df[source_field].isnull()]
        # 如果表不存在，则创建新表
        new_df = pd.DataFrame({
            '日期': filtered_df['日期'],
            '开盘': np.nan,
            '收盘': filtered_df[source_field],
            '最高': np.nan,
            '最低': np.nan,
            '成交量': np.nan,
            '成交额': np.nan
        })
        new_df.to_sql(name=target_field, con=engine, if_exists='replace', index=False)
    else:
        # 过滤掉'收盘'列中存在空值的行
        filtered_df = bond_zh_us_rate_df[~bond_zh_us_rate_df[source_field].isnull()]
        # 如果表存在，则更新数据
        existing_df = pd.read_sql_query(f"SELECT * FROM {target_field}", con=engine)
        last_date = existing_df['日期'].iloc[-1]
        update_df = filtered_df[filtered_df['日期'] > last_date]
        if not update_df.empty:
            update_df = pd.DataFrame({
                '日期': update_df['日期'],
                '开盘': np.nan,
                '收盘': update_df[source_field],
                '最高': np.nan,
                '最低': np.nan,
                '成交量': np.nan,
                '成交额': np.nan
            })
            update_df.to_sql(name=target_field, con=engine, if_exists='append', index=False)
    print(f'{target_field} 数据库存储完成')

logging.info('每日指数行情数据存储完成')
# # 外盘期货代码获取
# # ak.futures_hq_subscribe_exchange_symbol()
# ZSD_futures_foreign_hist_df = ak.futures_foreign_hist(symbol="ZSD")
# ZSD_futures_foreign_hist_df.to_sql(name='纽约原油数据库', con=engine, if_exists='replace', index=False)
# logging.info('纽约原油数据库存储完成')

# 关闭游标
cursor.close()
# 关闭连接
cnx.close()