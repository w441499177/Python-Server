# -*- coding: utf-8 -*-
import sys
sys.path.append('/home/ubuntu/market/dc-66-python-2.0/SelfModule')

import matplotlib.pyplot as plt
import akshare as ak
import pandas as pd
import numpy as np
from datetime import time, datetime, date, timedelta
from selfdefinfunc import check_a_share_trading_day  # 自定义封装函数
import time as dtime
import matplotlib.dates as mdates
import os
from lxml import etree
from chinese_calendar import is_workday
from openpyxl import load_workbook, Workbook
import matplotlib.dates as mdates
import matplotlib.ticker as mtick
import mysql.connector
from sqlalchemy import create_engine
import sys
import logging

# 检查是否非交易日
check_a_share_trading_day()

# 设置日志
logging.basicConfig(filename='/home/ubuntu/market/dc-66-python-2.0/execution.log', 
                    level=logging.INFO, 
                    format='%(asctime)s - %(levelname)s - %(message)s', 
                    filemode='a')  # 设置为追加模式

# 环境变量读取数据库秘钥信息
host = os.environ.get('MYSQL_HOST')   
user = os.environ.get('MYSQL_USERNAME')  
password = os.environ.get('MYSQL_PASSWORD')
port = os.environ.get('MYSQL_PORT')

# 数据库存储操作
engine = create_engine(f'mysql+mysqlconnector://{user}:{password}@{host}:{port}/期货行情数据库')

# 今日数据更新
str_date = datetime.today().strftime('%Y%m%d')
# str_date = '20240711'
get_futures_daily_df = ak.get_futures_daily(start_date=str_date, end_date=str_date, market="CFFEX")
contract_name = {'上证50当月':'IH', '上证50下月':'IH', '上证50当季':'IH', '上证50下季':'IH',
                 '沪深300当月':'IF', '沪深300下月':'IF', '沪深300当季':'IF', '沪深300下季':'IF',
                 '中证500当月':'IC', '中证500下月':'IC', '中证500当季':'IC', '中证500下季':'IC',
                 '中证1000当月':'IM', '中证1000下月':'IM', '中证1000当季':'IM', '中证1000下季':'IM',
                 }

for i, m in zip(contract_name.values(), contract_name):
    # 验证是否重复日期数据
    query = "SELECT datetime FROM {} ORDER BY ROW_NUMBER() OVER() DESC LIMIT 1".format(m+'分钟')
    # 使用 SQL 查询从数据库中读取倒数第一行数据
    get_futures_date_db = pd.read_sql_query(query, engine)
    date_latest = (get_futures_date_db.iloc[0,0] + timedelta(1)).replace(hour=0, minute=0, second=0)
    if datetime.strptime(str_date, '%Y%m%d') < date_latest:
        logging.info(m + '分钟数据库最新数据无需更新')
        continue
    mask = get_futures_daily_df['variety'] == i 
    temp = get_futures_daily_df.loc[mask]
    temp.reset_index(drop=True, inplace=True)
    # 获取对应股指期货及指数日内分钟数据
    if m[-2:] == '当月':
        mask = temp.index % 4 == 0
        temp = temp.loc[mask]
        future_Sec_symbol = temp.iloc[-1,0]
        futures_Sec_min_df = ak.futures_zh_minute_sina(symbol=future_Sec_symbol, period="1")
        futures_Sec_min_df['datetime'] = pd.to_datetime(futures_Sec_min_df['datetime'], format='%Y-%m-%d %H:%M:%S')

        # 获取对应指数分钟级数据
        if m[:-2] == '上证50':
            index_zh_a_hist_min_em_df = ak.index_zh_a_hist_min_em(symbol="000016", period="1", start_date=str_date, end_date=str_date)
        elif m[:-2] == '沪深300':
            index_zh_a_hist_min_em_df = ak.index_zh_a_hist_min_em(symbol="000300", period="1", start_date=str_date, end_date=str_date)
        elif m[:-2] == '中证500':
            index_zh_a_hist_min_em_df = ak.index_zh_a_hist_min_em(symbol="000905", period="1", start_date=str_date, end_date=str_date)
        elif m[:-2] == '中证1000':
            index_zh_a_hist_min_em_df = ak.index_zh_a_hist_min_em(symbol="000852", period="1", start_date=str_date, end_date=str_date)
    if m[-2:] == '下月':
        mask = temp.index % 4 == 1
        temp = temp.loc[mask]
        future_Sec_symbol = temp.iloc[-1,0]
        futures_Sec_min_df = ak.futures_zh_minute_sina(symbol=future_Sec_symbol, period="1")
        futures_Sec_min_df['datetime'] = pd.to_datetime(futures_Sec_min_df['datetime'], format='%Y-%m-%d %H:%M:%S')
        # 获取对应指数分钟级数据
        if m[:-2] == '上证50':
            index_zh_a_hist_min_em_df = ak.index_zh_a_hist_min_em(symbol="000016", period="1", start_date=str_date, end_date=str_date)
        elif m[:-2] == '沪深300':
            index_zh_a_hist_min_em_df = ak.index_zh_a_hist_min_em(symbol="000300", period="1", start_date=str_date, end_date=str_date)
        elif m[:-2] == '中证500':
            index_zh_a_hist_min_em_df = ak.index_zh_a_hist_min_em(symbol="000905", period="1", start_date=str_date, end_date=str_date)
        elif m[:-2] == '中证1000':
            index_zh_a_hist_min_em_df = ak.index_zh_a_hist_min_em(symbol="000852", period="1", start_date=str_date, end_date=str_date)
    if m[-2:] == '当季':
        mask = temp.index % 4 == 2
        temp = temp.loc[mask]
        future_Sec_symbol = temp.iloc[-1,0]
        futures_Sec_min_df = ak.futures_zh_minute_sina(symbol=future_Sec_symbol, period="1")
        futures_Sec_min_df['datetime'] = pd.to_datetime(futures_Sec_min_df['datetime'], format='%Y-%m-%d %H:%M:%S')
        # 获取对应指数分钟级数据
        if m[:-2] == '上证50':
            index_zh_a_hist_min_em_df = ak.index_zh_a_hist_min_em(symbol="000016", period="1", start_date=str_date, end_date=str_date)
        elif m[:-2] == '沪深300':
            index_zh_a_hist_min_em_df = ak.index_zh_a_hist_min_em(symbol="000300", period="1", start_date=str_date, end_date=str_date)
        elif m[:-2] == '中证500':
            index_zh_a_hist_min_em_df = ak.index_zh_a_hist_min_em(symbol="000905", period="1", start_date=str_date, end_date=str_date)
        elif m[:-2] == '中证1000':
            index_zh_a_hist_min_em_df = ak.index_zh_a_hist_min_em(symbol="000852", period="1", start_date=str_date, end_date=str_date)
    if m[-2:] == '下季':
        mask = temp.index % 4 == 3
        temp = temp.loc[mask]
        future_Sec_symbol = temp.iloc[-1,0]
        futures_Sec_min_df = ak.futures_zh_minute_sina(symbol=future_Sec_symbol, period="1")
        futures_Sec_min_df['datetime'] = pd.to_datetime(futures_Sec_min_df['datetime'], format='%Y-%m-%d %H:%M:%S')
        # 获取对应指数分钟级数据
        if m[:-2] == '上证50':
            index_zh_a_hist_min_em_df = ak.index_zh_a_hist_min_em(symbol="000016", period="1", start_date=str_date, end_date=str_date)
        elif m[:-2] == '沪深300':
            index_zh_a_hist_min_em_df = ak.index_zh_a_hist_min_em(symbol="000300", period="1", start_date=str_date, end_date=str_date)
        elif m[:-2] == '中证500':
            index_zh_a_hist_min_em_df = ak.index_zh_a_hist_min_em(symbol="000905", period="1", start_date=str_date, end_date=str_date)
        elif m[:-2] == '中证1000':
            index_zh_a_hist_min_em_df = ak.index_zh_a_hist_min_em(symbol="000852", period="1", start_date=str_date, end_date=str_date)
    
    index_zh_a_hist_min_em_df.rename(columns={'时间':'datetime'}, inplace=True)
    index_zh_a_hist_min_em_df['datetime'] = pd.to_datetime(index_zh_a_hist_min_em_df['datetime'], format='%Y-%m-%d %H:%M:%S')
    mask = futures_Sec_min_df['datetime'].apply(lambda x: datetime.strftime(x,'%Y%m%d')) == str_date
    temp_df = futures_Sec_min_df.loc[mask]
    temp_df = temp_df.reset_index(drop=True)
    # 填充无交易分钟数据
    if len(temp_df) != 240:
        try:
            for k in range(239):
                if k == 0 and temp_df.iloc[k, 0].time() != pd.to_datetime('9:31:00').time():
                    idxs = np.split(temp_df.index, [k, len(temp_df)])
                    temp_df.set_index(idxs[0].union(idxs[1] + 1), inplace=True)
                    temp_df.loc[k] = temp_df.loc[k + 1]
                    temp_df.sort_index(inplace=True)
                    temp_df.iloc[k, 0] = temp_df.iloc[k + 1, 0].replace(hour=9, minute=31, second=0)
                    temp_df.iloc[k, 5] = 0
                elif (temp_df.iloc[k + 1, 0] - temp_df.iloc[k, 0]) == timedelta(seconds=60):
                    continue
                elif (temp_df.iloc[k + 1, 0] - temp_df.iloc[k, 0]) == timedelta(seconds=5460):
                    continue
                else:                       
                    idxs = np.split(temp_df.index, [k + 1, len(temp_df)])
                    temp_df.set_index(idxs[0].union(idxs[1] + 1), inplace=True)
                    temp_df.loc[k + 1] = temp_df.loc[k]
                    temp_df.sort_index(inplace=True)
                    temp_df.iloc[k + 1, 0] = temp_df.iloc[k, 0] + timedelta(seconds=60) 
                    temp_df.iloc[k + 1, 5] = 0
        except:
            pass
    # 将指数分钟数据并入
    temp_df = temp_df.merge(index_zh_a_hist_min_em_df[['datetime', '收盘']].iloc[1:, ], on='datetime', how='left')
    temp_df.rename(columns={'收盘':'index'}, inplace=True)
    temp_df['basis'] = temp_df['close'] - temp_df['index']
    temp_df['basis_rate'] = temp_df['basis']/temp_df['index']
    # 合约到期日计算
    date_exp_first = date(year=2000+int(future_Sec_symbol[2:4]),month=int(future_Sec_symbol[4:6]),day=1)
    if date.weekday(date_exp_first) < 4:
        date_exp =  date_exp_first + timedelta(days=14 + 4 - date.weekday(date_exp_first))
    elif date.weekday(date_exp_first) == 7:
        date_exp =  date_exp_first + timedelta(days=14 + 4)
    else:
        date_exp =  date_exp_first + timedelta(days=19 + 7 - date.weekday(date_exp_first))
    # 交易日验证
    if date_exp.year == date.today().year:
        for tt in range(10):
            if is_workday(date_exp) == True:
                temp['expiring_date'] = date_exp
                break
            else:
                date_exp = date_exp + timedelta(days=tt+1)
    temp_df['expiring_date'] = date_exp
    due_to_expiring = (date_exp - datetime.strptime(str_date, '%Y%m%d').date()).days
    temp_df['due_to_expiring'] = due_to_expiring
    if due_to_expiring != 0:
        temp_df['annual_basis'] = temp_df['basis_rate']/(temp_df['due_to_expiring']/365) 
    else:
        temp_df['annual_basis'] = 0                     
    temp_df = temp_df.drop_duplicates(subset=['datetime'], keep='first', inplace=False)
    temp_df = temp_df.sort_values(by='datetime', axis=0)
    temp_df.to_sql(name=m+'分钟', con=engine, if_exists='append', index=False)
    print(m+'分钟数据更新完毕')
logging.info('每日股指期货分钟级数据更新完毕')