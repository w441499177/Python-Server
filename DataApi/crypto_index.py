import os
import mysql.connector
from sqlalchemy import create_engine
import pandas as pd
import requests
import logging

# 设置日志
logging.basicConfig(filename='/home/ubuntu/market/dc-66-python-2.0/execution.log', 
                    level=logging.INFO, 
                    format='%(asctime)s - %(levelname)s - %(message)s', 
                    filemode='a')  # 设置为追加模式

# 从环境变量读取数据库秘钥信息
MYSQL_HOST = os.environ.get('MYSQL_HOST')   
MYSQL_USERNAME = os.environ.get('MYSQL_USERNAME')  
MYSQL_PASSWORD = os.environ.get('MYSQL_PASSWORD')
MYSQL_PORT = os.environ.get('MYSQL_PORT')

def connect_to_database():
    return mysql.connector.connect(
        user=MYSQL_USERNAME,
        password=MYSQL_PASSWORD,
        host=MYSQL_HOST,
        port=MYSQL_PORT,
        database='指数行情数据库'
    )

def fetch_initial_data(symbol, end_date, limit):
    url = f"https://min-api.cryptocompare.com/data/v2/histoday?fsym={symbol}&tsym=USD&toTs={end_date}&limit={limit}"
    if end_date == '':
        url = f"https://min-api.cryptocompare.com/data/v2/histoday?fsym={symbol}&tsym=USD&limit={limit}"
    response = requests.get(url)
    data = response.json()['Data']['Data']
    df = pd.DataFrame(data)
    df['日期'] = pd.to_datetime(df['time'], unit='s')  # 将time列改为日期列，并命名为'日期'
    df.drop(columns=['time'], inplace=True)  # 删除time列
    df.rename(columns={'high': '最高', 'low': '最低', 'open': '开盘', 'close': '收盘', 'volumefrom': '成交量', 'volumeto': '成交额'}, inplace=True)
    # 更改列名为中文
    df = df[df['收盘'] != 0]  # 删除收盘为0的行
    return df[['日期', '最高', '最低', '开盘', '收盘', '成交量', '成交额']]

def create_table_if_not_exists(symbol):
    cnx = connect_to_database()
    cursor = cnx.cursor()
    table_name = f"{symbol}"
    cursor.execute(f"SHOW TABLES LIKE '{table_name}'")
    result = cursor.fetchone()

    if not result:
        # 如果表不存在，创建表
        cursor.execute(f"""
            CREATE TABLE {table_name} (
                日期 DATETIME NOT NULL,
                最高 FLOAT NOT NULL,
                最低 FLOAT NOT NULL,
                开盘 FLOAT NOT NULL,
                收盘 FLOAT NOT NULL,
                成交量 FLOAT NOT NULL,
                成交额 FLOAT NOT NULL,
                UNIQUE (日期)
            )
        """)
        print(f"Created table {table_name}")
    cursor.close()
    cnx.close()

def save_to_database(symbol, df):
    cnx = connect_to_database()
    table_name = f"{symbol}"
    df.to_sql(table_name, con=engine, if_exists='append', index=False, chunksize=1000)
    cnx.close()

def save_initial_data(symbol):
    limit = 1000  # 每次获取数据的限制
    num_years = 10  # 总共获取数据的年数
    today = pd.Timestamp.now().normalize()  # 当前日期
    all_data = pd.DataFrame()  # 用于存储所有数据
    for i in range(num_years):
        start_date = today - pd.DateOffset(years=(i + 1))
        end_date = int(start_date.timestamp() + limit - 1)  # 从起始日期开始的限定天数
        df = fetch_initial_data(symbol, end_date, limit)
        all_data = pd.concat([all_data, df], ignore_index=True)
    # 删除日期重复的行
    all_data.drop_duplicates(subset=['日期'], inplace=True)
    # 直接按照日期升序排序，然后保存到数据库
    all_data = all_data.sort_values(by='日期')
    save_to_database(symbol, all_data)
    print(f"Saved initial historical data for {symbol}")

def update_table(symbol):
    cnx = connect_to_database()
    table_name = f"{symbol}"

    cursor = cnx.cursor()
    cursor.execute(f"SELECT MAX(日期) FROM {table_name}")
    result = cursor.fetchone()
    last_date = result[0] if result[0] else pd.Timestamp.min
    cursor.close()

    today = pd.Timestamp.now().normalize()
    last_date = pd.Timestamp(last_date)
    limit = (today - last_date).days

    df = fetch_initial_data(symbol, '', limit)
    df = df[df['日期'] > last_date]
    df = df.iloc[:-1]
    try:
        if not df.empty:
            save_to_database(symbol, df)
            logging.info(f"Updated historical data for {symbol}")
        else:
            logging.info(f"{symbol} no need to update")
    except:
        logging.info(f"{symbol} something wrong")
        pass

    cnx.close()

def table_exists(symbol):
    cnx = connect_to_database()
    cursor = cnx.cursor()

    table_name = f"{symbol}"

    cursor.execute(f"SHOW TABLES LIKE '{table_name}'")
    result = cursor.fetchone()

    cursor.close()
    cnx.close()

    return result is not None

def main():
    symbols = ['BTC', 'ETH', 'BNB', 'SOL', 'XRP', 'DOGE']

    for symbol in symbols:
        if not table_exists(symbol):
            create_table_if_not_exists(symbol)
            save_initial_data(symbol)
        else:
            update_table(symbol)
    
if __name__ == "__main__":
    engine = create_engine(f'mysql+mysqlconnector://{MYSQL_USERNAME}:{MYSQL_PASSWORD}@{MYSQL_HOST}:{MYSQL_PORT}/指数行情数据库')
    main()