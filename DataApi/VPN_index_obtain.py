import os
import mysql.connector
from sqlalchemy import create_engine
import pandas as pd
import yfinance as yf
import logging

# 设置日志
logging.basicConfig(filename='/home/ubuntu/market/dc-66-python-2.0/execution.log', 
                    level=logging.INFO, 
                    format='%(asctime)s - %(levelname)s - %(message)s', 
                    filemode='a')  # 设置为追加模式

# 中文名称与Yahoo Finance的符号的映射关系
symbol_map = {
    '标普500': '^GSPC',
    '道琼斯工业平均指数': '^DJI',
    '纳斯达克综合指数': '^IXIC',
    '罗素2000': '^RUT',
    '国际黄金': 'GC=F',
    '国际原油': 'CL=F',
    '日经225': '^N225',
    '印度NIFTY50': '^NSEI',
    '英国富时100': '^FTSE',
    '美国10年期国债': '^TNX',
    '恒生指数': '^HSI',
    'CBOE波动率指数': '^VIX',
    '美元兑人民币汇率': 'CNY=X',
}

# 从环境变量读取数据库秘钥信息
MYSQL_HOST = os.environ.get('MYSQL_HOST')
MYSQL_USERNAME = os.environ.get('MYSQL_USERNAME')
MYSQL_PASSWORD = os.environ.get('MYSQL_PASSWORD')
MYSQL_PORT = os.environ.get('MYSQL_PORT')

def connect_to_database():
    return mysql.connector.connect(
        user=MYSQL_USERNAME,
        password=MYSQL_PASSWORD,
        host=MYSQL_HOST,
        port=MYSQL_PORT,
        database='指数行情数据库'  # 数据库名称改为英文
    )

def create_table_if_not_exists(symbol):
    cnx = connect_to_database()
    cursor = cnx.cursor()
    cursor.execute(f"SHOW TABLES LIKE '{symbol}'")
    result = cursor.fetchone()

    if not result:
        # 如果表不存在，创建表
        cursor.execute(f"""
            CREATE TABLE `{symbol}` (
                `日期` DATETIME NOT NULL,
                `开盘` FLOAT NOT NULL,
                `最高` FLOAT NOT NULL,
                `最低` FLOAT NOT NULL,
                `收盘` FLOAT NOT NULL,
                `成交量` FLOAT NOT NULL,
                `成交额` FLOAT NOT NULL,
                UNIQUE (`日期`)
            )
        """)
        print(f"Created table {symbol}")
    cursor.close()
    cnx.close()

def fetch_initial_data(symbol, start_date, end_date):
    ticker = yf.Ticker(symbol_map[symbol])
    df = ticker.history(start=start_date, end=end_date)
    df.reset_index(inplace=True)
    df.rename(columns={
        'Date': '日期',
        'Open': '开盘',
        'High': '最高',
        'Low': '最低',
        'Close': '收盘',
        'Volume': '成交量'
    }, inplace=True)
    df['成交额'] = df['成交量']  # 计算成交额

    # 将日期时间列转换为北京时间 'Asia/Shanghai'，并去掉时区信息，转为 datetime 类型
    if symbol == '日经225':
        df['日期'] = df['日期'].dt.tz_convert('Asia/Shanghai').dt.date + pd.Timedelta(hours=24)
    else:
        df['日期'] = df['日期'].dt.tz_convert('Asia/Shanghai').dt.date

    df = df[['日期', '开盘', '最高', '最低', '收盘', '成交量', '成交额']]
    return df

def save_to_database(symbol, df):
    engine = create_engine(f'mysql+mysqlconnector://{MYSQL_USERNAME}:{MYSQL_PASSWORD}@{MYSQL_HOST}:{MYSQL_PORT}/指数行情数据库')
    try:
        df.to_sql(symbol, con=engine, if_exists='append', index=False, chunksize=1000)
        print(f"Saved data to database for {symbol}")
    except Exception as e:
        logging.error(f"Error occurred while saving data for {symbol}: {str(e)}")
        print(f"{symbol}无需更新")
    finally:
        engine.dispose()

def save_initial_data(symbol):
    end_date = pd.Timestamp.now().normalize()
    start_date = end_date - pd.DateOffset(years=30)
    df = fetch_initial_data(symbol, start_date, end_date)
    save_to_database(symbol, df)
    print(f"Saved initial historical data for {symbol}")

def update_table(symbol):
    cnx = connect_to_database()
    cursor = cnx.cursor()
    cursor.execute(f"SELECT MAX(`日期`) FROM `{symbol}`")
    result = cursor.fetchone()
    last_date = result[0] if result[0] else pd.Timestamp.min
    cursor.close()
    cnx.close()

    today = pd.Timestamp.now().normalize()

    df = fetch_initial_data(symbol, last_date, today)
    # 将数据库中的最新日期转换为 datetime 类型
    last_date_db = pd.Timestamp(last_date)
    # 去除重复日期数据
    df = df[df['日期'] > last_date_db.date()]
    if not df.empty:
        save_to_database(symbol, df)
        print(f"Updated historical data for {symbol}")
    else:
        print(f"No update needed for {symbol}")

def table_exists(symbol):
    cnx = connect_to_database()
    cursor = cnx.cursor()

    cursor.execute(f"SHOW TABLES LIKE '{symbol}'")
    result = cursor.fetchone()

    cursor.close()
    cnx.close()

    return result is not None

def main():
    symbols = [
        '标普500', '道琼斯工业平均指数', '纳斯达克综合指数', '罗素2000',
        '国际黄金', '国际原油', '日经225', '印度NIFTY50', '英国富时100',
        '美国10年期国债', '恒生指数', 'CBOE波动率指数', '美元兑人民币汇率'
    ]

    for symbol in symbols:
        if not table_exists(symbol):
            create_table_if_not_exists(symbol)
            save_initial_data(symbol)
        else:
            update_table(symbol)

if __name__ == "__main__":
    main()
    logging.info('全球大类资产行情数据更新')
