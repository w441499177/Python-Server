import sys
sys.path.append('/home/ubuntu/market/dc-66-python-2.0/SelfModule')

from selenium import webdriver
import warnings
import mysql.connector
from selenium.webdriver.chrome.options import Options
from sqlalchemy import create_engine
import akshare as ak
import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np
import pandas as pd
from datetime import datetime, date, timedelta
import matplotlib.ticker as mtick
import matplotlib.dates as mdates
from matplotlib.pyplot import MultipleLocator
from tradedate import *  # 自定义封装函数
from downloadindex import data_CSI300, data_CSI500, data_CSI1000, data_CSIA500
from selfdefinfunc import Search_NavDate, check_a_share_trading_day
import time
import os
import re
from openpyxl import load_workbook, Workbook
import logging

# 设置日志
logging.basicConfig(filename='/home/ubuntu/market/dc-66-python-2.0/execution.log', 
                    level=logging.INFO, 
                    format='%(asctime)s - %(levelname)s - %(message)s', 
                    filemode='a')  # 设置为追加模式

plt.rcParams['font.sans-serif'] = ['SimHei']  # 用来正常显示中文标签
plt.rcParams['axes.unicode_minus'] = False  # 用来正常显示负号
warnings.filterwarnings('ignore')

# 检查是否非交易日
check_a_share_trading_day()

# 环境变量读取数据库秘钥信息
host = os.environ.get('MYSQL_HOST')   
user = os.environ.get('MYSQL_USERNAME')  
password = os.environ.get('MYSQL_PASSWORD')
port = os.environ.get('MYSQL_PORT')

# 数据库存储操作
engine_stragegy = create_engine('mysql+mysqlconnector://market_viewer:09LW82k3Fg@172.16.10.26:3306/strategy')
engine_performance = create_engine('mysql+mysqlconnector://market_viewer:09LW82k3Fg@172.16.10.26:3306/history')
engine = create_engine(f'mysql+mysqlconnector://{user}:{password}@{host}:{port}/衍盛策略绩效数据库')

# 构建 SQL 查询，选择本地server_risk表的倒数第一行数据
query = "SELECT * FROM {} ORDER BY ROW_NUMBER() OVER() DESC LIMIT 1".format('server_risk')
datebase_newest = pd.read_sql_query(query, engine) # 使用 SQL 查询从数据库中读取倒数第一行数据
query_str = "SELECT * FROM %s WHERE date > '%s' AND CONVERT(date USING utf8mb4) > '%s'" % ('server_risk', datebase_newest['date'].iloc[-1], datebase_newest['date'].iloc[-1])
list_all_pnl = pd.read_sql_query(query_str, engine_performance)

# 构建 SQL 查询，选择本地alpha_history表的倒数第一行数据
query = "SELECT * FROM {} ORDER BY ROW_NUMBER() OVER() DESC LIMIT 1".format('alpha_history')
datebase_newest = pd.read_sql_query(query, engine) # 使用 SQL 查询从数据库中读取倒数第一行数据
query_str = "SELECT * FROM %s WHERE date > '%s' AND end_alpha > 0" % ('alpha_history', datebase_newest['date'].iloc[-1]) # 获取未更新过的list_all_alpha
list_all_alpha = pd.read_sql_query(query_str, engine_performance)
list_all_stragegy = pd.read_sql_query("SELECT * FROM %s" % 'strategy_grouping', engine_stragegy) # 使用 .format() 方法构建查询语句

# 保存本地数据库
list_all_stragegy.to_sql(name='strategy_grouping', con=engine, if_exists='replace', index=False)
list_all_alpha.to_sql(name='alpha_history', con=engine, if_exists='append', index=False)
list_all_pnl.to_sql(name='server_risk', con=engine, if_exists='append', index=False)

# 读取本地库
list_all_stragegy = pd.read_sql_query("SELECT * FROM %s" % 'strategy_grouping', engine)
list_all_alpha = pd.read_sql_query("SELECT * FROM %s" % 'alpha_history', engine)
list_all_pnl = pd.read_sql_query("SELECT * FROM %s" % 'server_risk', engine)

# 维护在数据库中的策略基础信息
strategy_total_list = pd.read_sql_query("SELECT * FROM %s" % 'strategy_total_list', engine).replace({None: np.nan, '': np.nan})

# 筛选出所有实盘strategy
strategy_total_list_prod = strategy_total_list.loc[strategy_total_list['strategy_type']=='Prod'].reset_index(drop=True)
# 筛选出所有模拟盘strategy
strategy_total_list_pilot = strategy_total_list.loc[strategy_total_list['strategy_type']=='Pilot'].reset_index(drop=True)
# 获取今天的日期
today_date = datetime.now().date()


# 比较今天的日期和给定日期
if today_date < time_this_Friday_time.date():
    time_this_Friday_time = datetime.combine(today_date, time_this_Friday_time.time())

CSI300_weekly_ret = data_CSI300.loc[data_CSI300['date'] == time_this_Friday_time, 'close'].values[0]/data_CSI300.loc[data_CSI300['date'] == time_last_Friday_time, 'close'].values[0] - 1 
CSI500_weekly_ret = data_CSI500.loc[data_CSI500['date'] == time_this_Friday_time, 'close'].values[0]/data_CSI500.loc[data_CSI500['date'] == time_last_Friday_time, 'close'].values[0] - 1 
CSI1000_weekly_ret = data_CSI1000.loc[data_CSI1000['date'] == time_this_Friday_time, 'close'].values[0]/data_CSI1000.loc[data_CSI1000['date'] == time_last_Friday_time, 'close'].values[0] - 1
CSIA500_weekly_ret = data_CSIA500.loc[data_CSIA500['date'] == time_this_Friday_time, 'close'].values[0]/data_CSIA500.loc[data_CSIA500['date'] == time_last_Friday_time, 'close'].values[0] - 1

list_all_alpha_weekly = list_all_alpha[pd.to_datetime(list_all_alpha['date']) > time_last_Friday_time].reset_index(drop=True)
for strategy_name in strategy_total_list_prod['strategy_name']:
    row_index = strategy_total_list.index[strategy_total_list['strategy_name'] == strategy_name][0]
    strategy_name_target = strategy_name + '_T'
    list_strategy = list_all_alpha_weekly.loc[list_all_alpha_weekly['strategy_name'] == strategy_name]
    list_strategy['daily_alpha'] = (list_strategy['end_alpha']/100)
    list_strategy_target = list_all_alpha_weekly.loc[list_all_alpha_weekly['strategy_name'] == strategy_name_target]
    list_strategy_target['daily_alpha'] = (list_strategy_target['end_alpha']/100)
    
    weekly_prod_ret = np.prod(list_strategy['daily_alpha']) - 1
    strategy_total_list.loc[row_index, 'prod_ret'] = weekly_prod_ret
    weekly_target_ret = np.prod(list_strategy_target['daily_alpha']) - 1
    strategy_total_list.loc[row_index, 'target_ret'] = weekly_target_ret
    if strategy_total_list.loc[row_index, 'target_index'] == 'CSI500':
        strategy_total_list.loc[row_index, 'target_index_ret'] = CSI500_weekly_ret
    elif strategy_total_list.loc[row_index, 'target_index'] == 'CSI300':
        strategy_total_list.loc[row_index, 'target_index_ret'] = CSI300_weekly_ret
    elif strategy_total_list.loc[row_index, 'target_index'] == 'CSI1000':
        strategy_total_list.loc[row_index, 'target_index_ret'] = CSI1000_weekly_ret
    elif strategy_total_list.loc[row_index, 'target_index'] == 'CSIA500':
        strategy_total_list.loc[row_index, 'target_index_ret'] = CSIA500_weekly_ret
    else:
        strategy_total_list.loc[row_index, 'target_index_ret'] = 0

# list_strategy_pnl_total = pd.DataFrame(columns=['date', 'strategy_name', 'Total_PL', 'Trading_PL', 'Total_Stocks_Value', 'Strategy_Daily_Ret'])
# [pd.to_datetime(list_all_pnl['date']) > time_last_Friday_time]
list_strategy_pnl_total = pd.read_sql_query("SELECT * FROM %s" % '子策略实盘绩效表', engine)

# 子策略实盘绩效表
list_all_pnl_weekly = list_all_pnl.sort_values(by='date', ascending=True).reset_index(drop=True)
list_all_pnl_weekly['date'] = pd.to_datetime(list_all_pnl_weekly['date'])
for strategy_name in strategy_total_list_prod['strategy_name']:
    list_strategy_pnl_weekly = list_all_pnl_weekly[list_all_pnl_weekly['strategy_name'].str.contains(strategy_name.replace('MF_', ''))]
    list_strategy_pnl_weekly_total = list_strategy_pnl_weekly.groupby('date')[['Total_PL', 'Trading_PL', 'Total_Stocks_Value']].sum().reset_index()
    benchmark_index = globals()['data_' + strategy_total_list.loc[strategy_total_list['strategy_name'] == strategy_name, 'target_index'].values[0]].copy()
    benchmark_index['daily_index_ret'] = benchmark_index['close'].pct_change()
    list_strategy_pnl_weekly_total = pd.merge(list_strategy_pnl_weekly_total, benchmark_index[['date', 'daily_index_ret']], on='date', how='inner')
    list_strategy_pnl_weekly_total['Strategy_Daily_Ret'] = list_strategy_pnl_weekly_total['Total_PL'] / (list_strategy_pnl_weekly_total['Total_Stocks_Value'] - list_strategy_pnl_weekly_total['Total_PL'])
    list_strategy_pnl_weekly_total.loc[list_strategy_pnl_weekly_total['Total_Stocks_Value'] == 0, 'Strategy_Daily_Ret'] = 0
    list_strategy_pnl_weekly_total['Strategy_Daily_Alpha_Ret'] = (list_strategy_pnl_weekly_total['Strategy_Daily_Ret'] + 1) / (list_strategy_pnl_weekly_total['daily_index_ret'] + 1) - 1 
    
    
    list_strategy_pnl_weekly_total.insert(1, 'strategy_name', strategy_name) 
    # 
    list_strategy_pnl_total = pd.concat([list_strategy_pnl_total, list_strategy_pnl_weekly_total], ignore_index=True)

list_strategy_pnl_total.drop_duplicates(subset=['date', 'strategy_name'], keep='last', inplace=True)
list_strategy_pnl_total.replace(np.inf, 0, inplace=True)
list_strategy_pnl_total.to_sql(name='子策略实盘绩效表', con=engine, if_exists='replace', index=False)

# 子策略实盘净值入库
for strategy_name in strategy_total_list_prod['strategy_name']:
    list_chanpin_nav = list_strategy_pnl_total[list_strategy_pnl_total['strategy_name'] == strategy_name].reset_index(drop=True)
    list_chanpin_nav['strategy_nav'] = (list_chanpin_nav['Strategy_Daily_Ret'] + 1).cumprod()
    # 找到策略开始前的一个交易日
    date_strategy_start = data_CSI500.loc[data_CSI500.loc[data_CSI500['date'] == list_chanpin_nav.loc[0, 'date']].index.values[0] - 1, 'date']
    # 将nav起点锚定为1
    new_row = pd.DataFrame({'date': date_strategy_start,  
                       'strategy_nav': 1},
                      index=[0])
    # 将新行插入到原始DataFrame的第一行  
    list_chanpin_nav = pd.concat([new_row, list_chanpin_nav])
    # 首先更新策略持仓绩效
    list_chanpin_nav = list_chanpin_nav[['date', 'strategy_nav']].rename(columns={'date': '净值日期', 'strategy_nav': '复权净值'})
    try:
        # 构建 SQL 查询，选择表的倒数第一行数据
        query = "SELECT 净值日期, 复权净值 FROM {} ORDER BY ROW_NUMBER() OVER() DESC LIMIT 1".format(strategy_name + '_Prod')
        # 使用 SQL 查询从数据库中读取倒数第一行数据
        list_chanpin_newest = pd.read_sql_query(query, engine)
        list_chanpin_nav = list_chanpin_nav.loc[list_chanpin_nav['净值日期'] > list_chanpin_newest.iloc[0, 0]]
    except:
        pass
    list_chanpin_nav.to_sql(name=strategy_name + '_Prod', con=engine, if_exists='append', index=False)
    

# time_this_Friday_time = time_this_Friday_time - timedelta(days=6)
# time_last_Friday_time = time_last_Friday_time - timedelta(days=7)
# 计算子策略周收益率柱状图
# 对标指数周收益率计算
list_all_pnl_weekly_total = list_strategy_pnl_total[pd.to_datetime(list_strategy_pnl_total['date']) > time_last_Friday_time].reset_index(drop=True)
for strategy_name in strategy_total_list_prod['strategy_name']:
    row_index = strategy_total_list.index[strategy_total_list['strategy_name'] == strategy_name][0]
    list_strategy = list_all_pnl_weekly_total.loc[list_all_pnl_weekly_total['strategy_name'] == strategy_name]
    if strategy_name == 'MF_JakiroWeekly':
        strategy_total_list.loc[row_index, 'pl_ret'] = np.nan
        strategy_total_list.loc[row_index, 'alpha_ret'] = np.nan
    else:
        strategy_total_list.loc[row_index, 'pl_ret'] = np.prod(list_strategy['Strategy_Daily_Ret'] + 1) - 1
        strategy_total_list.loc[row_index, 'alpha_ret'] = strategy_total_list.loc[row_index, 'pl_ret'] - strategy_total_list.loc[row_index, 'target_index_ret']

strategy_total_list.to_sql(name='子策略实盘周度绩效表', con=engine, if_exists='replace', index=False)


# 计算子策略日收益率柱状图
# 获取当前时间
today_datetime = datetime.strptime(datetime.today().date().strftime('%Y-%m-%d'), '%Y-%m-%d')
if list_strategy_pnl_total['date'].iloc[-1] == today_datetime:
    list_all_alpha_daily = list_all_alpha[pd.to_datetime(list_all_alpha['date']) == today_datetime].reset_index(drop=True)
    for strategy_name in strategy_total_list_prod['strategy_name']:
        row_index = strategy_total_list.index[strategy_total_list['strategy_name'] == strategy_name][0]
        strategy_name_target = strategy_name + '_T'
        list_strategy = list_all_alpha_daily.loc[list_all_alpha_daily['strategy_name'] == strategy_name]
        list_strategy['daily_alpha'] = (list_strategy['end_alpha']/100)
        list_strategy_target = list_all_alpha_daily.loc[list_all_alpha_daily['strategy_name'] == strategy_name_target]
        list_strategy_target['daily_alpha'] = (list_strategy_target['end_alpha']/100)
        
        daily_prod_ret = np.prod(list_strategy['daily_alpha']) - 1
        strategy_total_list.loc[row_index, 'prod_ret'] = daily_prod_ret
        daily_target_ret = np.prod(list_strategy_target['daily_alpha']) - 1
        strategy_total_list.loc[row_index, 'target_ret'] = daily_target_ret
        if strategy_total_list.loc[row_index, 'target_index'] == 'CSI500':
            strategy_total_list.loc[row_index, 'target_index_ret'] = data_CSI500['close'].iloc[-1]/data_CSI500['close'].iloc[-2]-1
        elif strategy_total_list.loc[row_index, 'target_index'] == 'CSI300':
            strategy_total_list.loc[row_index, 'target_index_ret'] = data_CSI300['close'].iloc[-1]/data_CSI300['close'].iloc[-2]-1
        elif strategy_total_list.loc[row_index, 'target_index'] == 'CSI1000':
            strategy_total_list.loc[row_index, 'target_index_ret'] = data_CSI1000['close'].iloc[-1]/data_CSI1000['close'].iloc[-2]-1
        elif strategy_total_list.loc[row_index, 'target_index'] == 'CSIA500':
            strategy_total_list.loc[row_index, 'target_index_ret'] = data_CSIA500['close'].iloc[-1]/data_CSIA500['close'].iloc[-2]-1
        else:
            strategy_total_list.loc[row_index, 'target_index_ret'] = 0
    # 对标指数日收益率计算
    list_all_pnl_daily_total = list_strategy_pnl_total[pd.to_datetime(list_strategy_pnl_total['date']) == today_datetime].reset_index(drop=True)
    for strategy_name in strategy_total_list_prod['strategy_name']:
        row_index = strategy_total_list.index[strategy_total_list['strategy_name'] == strategy_name][0]
        list_strategy = list_all_pnl_daily_total.loc[list_all_pnl_daily_total['strategy_name'] == strategy_name]
        if strategy_name == 'MF_JakiroWeekly':
            strategy_total_list.loc[row_index, 'pl_ret'] = np.nan
            strategy_total_list.loc[row_index, 'alpha_ret'] = np.nan
        else:
            strategy_total_list.loc[row_index, 'pl_ret'] = np.prod(list_strategy['Strategy_Daily_Ret'] + 1) - 1
            strategy_total_list.loc[row_index, 'alpha_ret'] = strategy_total_list.loc[row_index, 'pl_ret'] - strategy_total_list.loc[row_index, 'target_index_ret']
    
    strategy_total_list.to_sql(name='子策略实盘日度绩效表', con=engine, if_exists='replace', index=False)


# 单个子策略历史收益曲线（实盘）

# 遍历每个strategy_name
for idx, strategy_name in enumerate(strategy_total_list_prod['strategy_name']):
    if 'Option' in strategy_name:
        continue
    list_strategy = list_all_alpha.loc[list_all_alpha['strategy_name'] == strategy_name].reset_index(drop=True)
    list_strategy['date'] = pd.to_datetime(list_strategy['date'])
    try:
        benchmark_index = globals()['data_' + strategy_total_list.loc[strategy_total_list['strategy_name'] == strategy_name, 'target_index'].values[0]].copy()
        benchmark_index['daily_index_ret'] = benchmark_index['close'].pct_change()
        list_strategy = pd.merge(list_strategy, benchmark_index[['date', 'daily_index_ret']], on='date', how='inner')
        list_strategy['daily_alpha_ret'] = (list_strategy['end_alpha'] - 100) / 100 - list_strategy['daily_index_ret']
        list_strategy['strategy_alpha_nav'] = (list_strategy['daily_alpha_ret'] + 1).cumprod()
    except:
        list_strategy['daily_index_ret'] = np.nan
        list_strategy['daily_alpha_ret'] = np.nan
        list_strategy['strategy_alpha_nav'] = np.nan

    list_strategy['daily_ret'] = (list_strategy['end_alpha'] - 100) / 100
    list_strategy['strategy_nav'] = (list_strategy['daily_ret'] + 1).cumprod()
    
    # 找到策略开始前的一个交易日
    date_strategy_start = data_CSI500.loc[data_CSI500.loc[data_CSI500['date'] == list_strategy.loc[0, 'date']].index.values[0] - 1, 'date']
    # 将nav起点锚定为1
    new_row = pd.DataFrame({'date': date_strategy_start,  
                       'strategy_nav': 1,  
                       'strategy_alpha_nav': 1},  
                      index=[0])
    # 将新行插入到原始DataFrame的第一行  
    list_strategy = pd.concat([new_row, list_strategy])
    # 首先更新策略持仓绩效
    list_chanpin_nav = list_strategy[['date', 'strategy_nav']].rename(columns={'date': '净值日期', 'strategy_nav': '复权净值'})
    try:
        # 构建 SQL 查询，选择表的倒数第一行数据
        query = "SELECT 净值日期, 复权净值 FROM {} ORDER BY ROW_NUMBER() OVER() DESC LIMIT 1".format(strategy_name + '_Pilot')
        # 使用 SQL 查询从数据库中读取倒数第一行数据
        list_chanpin_newest = pd.read_sql_query(query, engine)
        list_chanpin_nav = list_chanpin_nav.loc[list_chanpin_nav['净值日期'] > list_chanpin_newest.iloc[0, 0]]
    except:
        pass
    list_chanpin_nav.to_sql(name=strategy_name + '_Pilot', con=engine, if_exists='append', index=False)

# 单个子策略历史收益曲线（模拟盘）
# 遍历每个strategy_name
for idx, strategy_name in enumerate(strategy_total_list_pilot['strategy_name']):
    if 'Option' in strategy_name:
        continue
    list_strategy = list_all_alpha.loc[list_all_alpha['strategy_name'] == strategy_name].reset_index(drop=True)
    list_strategy['date'] = pd.to_datetime(list_strategy['date'])

    try:
        benchmark_index = globals()['data_' + strategy_total_list.loc[strategy_total_list['strategy_name'] == strategy_name, 'target_index'].values[0]].copy()
        benchmark_index['daily_index_ret'] = benchmark_index['close'].pct_change()
        list_strategy = pd.merge(list_strategy, benchmark_index[['date', 'daily_index_ret']], on='date', how='inner')
        list_strategy['daily_alpha_ret'] = (list_strategy['end_alpha'] - 100) / 100 - list_strategy['daily_index_ret']
        list_strategy['strategy_alpha_nav'] = (list_strategy['daily_alpha_ret'] + 1).cumprod()
    except:
        list_strategy['daily_index_ret'] = np.nan
        list_strategy['daily_alpha_ret'] = np.nan
        list_strategy['strategy_alpha_nav'] = np.nan

    list_strategy['daily_ret'] = (list_strategy['end_alpha'] - 100) / 100
    list_strategy['strategy_nav'] = (list_strategy['daily_ret'] + 1).cumprod()
    
    # 找到策略开始前的一个交易日
    date_strategy_start = data_CSI500.loc[data_CSI500.loc[data_CSI500['date'] == list_strategy.loc[0, 'date']].index.values[0] - 1, 'date']
    # 将nav起点锚定为1
    new_row = pd.DataFrame({'date': date_strategy_start,  
                       'strategy_nav': 1,  
                       'strategy_alpha_nav': 1},  
                      index=[0])
    # 将新行插入到原始DataFrame的第一行  
    list_strategy = pd.concat([new_row, list_strategy])
    # 首先更新策略持仓绩效
    list_chanpin_nav = list_strategy[['date', 'strategy_nav']].rename(columns={'date': '净值日期', 'strategy_nav': '复权净值'})
    try:
        # 构建 SQL 查询，选择表的倒数第一行数据
        query = "SELECT 净值日期, 复权净值 FROM {} ORDER BY ROW_NUMBER() OVER() DESC LIMIT 1".format(strategy_name + '_Pilot')
        # 使用 SQL 查询从数据库中读取倒数第一行数据
        list_chanpin_newest = pd.read_sql_query(query, engine)
        list_chanpin_nav = list_chanpin_nav.loc[list_chanpin_nav['净值日期'] > list_chanpin_newest.iloc[0, 0]]
    except:
        pass
    list_chanpin_nav.to_sql(name=strategy_name + '_Pilot', con=engine, if_exists='append', index=False)



# 周度日期序列 #
weekly_date = data_CSI1000.drop('close', axis=1)
weekly_date = weekly_date.rename(columns = {'date':'净值日期'})
weekly_date['是否周末净值'] = ''
weekly_date.iloc[-1, 1] = '是'
for i in range(len(weekly_date) - 1):
    if weekly_date.iloc[i+1, 0] - weekly_date.iloc[i, 0] != timedelta(days=1):
        weekly_date.iloc[i, 1] = '是'
    else:
        weekly_date.iloc[i, 1] = '否'
weekly_date = weekly_date[weekly_date['是否周末净值'] == '是'].drop('是否周末净值', axis=1).reset_index().drop('index', axis=1)
weekly_date = pd.to_datetime(weekly_date['净值日期'])

# 计算各个策略的净值时序指标
list = pd.read_sql_query("SELECT * FROM %s" % 'strategy_total_list', engine)
# 选择满足条件的行
selected_rows = list.loc[list['strategy_type'] == 'Prod']
# 复制这些行
copied_rows = selected_rows.copy()
# 将复制的行中的'strategy_type'列的值改为'Pilot'
copied_rows['strategy_type'] = 'Pilot'
# 使用concat函数将原始的DataFrame和复制的行连接起来
list = pd.concat([list, copied_rows]).reset_index(drop=True)

for n in range(len(list)):
    strategy_sql_name = list.loc[n, 'strategy_name'] + '_' + list.loc[n, 'strategy_type']
    if 'Option' in strategy_sql_name:
        continue
    list_chanpin_info = pd.read_sql_query("SELECT * FROM %s" % strategy_sql_name, engine)
    
    # 识别策略类型 #
    if list.loc[n, 'target_index'] == 'CSI500':
        type_strategy = '500指增'
    elif list.loc[n, 'target_index'] == 'CSI300':
        type_strategy = '300指增'
    elif list.loc[n, 'target_index'] == 'CSI1000':
        type_strategy = '1000指增'
    elif list.loc[n, 'target_index'] == 'CSIA500':
        type_strategy = 'A500指增'
    else:
        type_strategy = '中性'
    
    type_strategy_judge = '指增' in type_strategy
    list.loc[n, '成立日期'] = list_chanpin_info.iloc[0, 0]
    # 产品成立日期格式转datetime #
    date_found_chanpin = list.loc[n, '成立日期'].to_pydatetime()
    # 删除坏点净值 #
    list_chanpin_info = list_chanpin_info[list_chanpin_info['复权净值'] != '-']
    # 修改净值格式 #
    list_chanpin_info['复权净值'] = list_chanpin_info['复权净值'].astype(float)
    overlap_dates = list_chanpin_info['净值日期'].isin(data_CSI1000['date'])
    # 只保留五列数据
    list_chanpin_info = list_chanpin_info[overlap_dates][['净值日期', '复权净值']]
    list_chanpin_info['净值日期'] = pd.to_datetime(list_chanpin_info['净值日期'])
    
    # 对标指数调整 #
    if type_strategy == '500指增':
        list_chanpin_info = pd.merge(list_chanpin_info, data_CSI500.rename(columns={'date':'净值日期'}))
        list_chanpin_info = list_chanpin_info.rename(columns = {'close':'对标指数'})
    elif type_strategy.find('300指增') != -1:
        list_chanpin_info = pd.merge(list_chanpin_info, data_CSI300.rename(columns={'date':'净值日期'}))
        list_chanpin_info = list_chanpin_info.rename(columns = {'close':'对标指数'})
    elif type_strategy.find('1000指增') != -1:
        list_chanpin_info = pd.merge(list_chanpin_info, data_CSI1000.rename(columns={'date':'净值日期'}))
        list_chanpin_info = list_chanpin_info.rename(columns = {'close':'对标指数'})
    elif type_strategy.find('A500指增') != -1:
        list_chanpin_info = pd.merge(list_chanpin_info, data_CSIA500.rename(columns={'date':'净值日期'}))
        list_chanpin_info = list_chanpin_info.rename(columns = {'close':'对标指数'})
    elif type_strategy.find('空气指增') != -1:
        list_chanpin_info = pd.merge(list_chanpin_info, data_GZ2000.rename(columns={'date':'净值日期'}))
        list_chanpin_info = list_chanpin_info.rename(columns = {'close':'对标指数'})
    else:
        list_chanpin_info['对标指数'] = np.nan
    
    # 计算绝对收益 #
    if date_found_chanpin <= time_last_year_time:
        temp = list_chanpin_info.loc[(list_chanpin_info['净值日期'] <= time_last_year_time)]
        temp1 = list_chanpin_info.loc[(list_chanpin_info['净值日期'] <= time_last_Friday_time)]
        ytd = temp1.values[-1][2]/temp.values[-1][2] - 1

    # 增加数据列 #
    columns_to_add = [
        "近四周收益率", "对标指数近四周收益率", "近四周超额（除法）",
        "今年以来收益率", "对标指数今年以来收益率", "今年以来超额（除法）", "今年以来最大回撤", "今年以来最大回撤（超额）",
        "近三月收益率", "对标指数近三月收益率", "近三月超额（除法）", "近三月最大回撤", "近三月最大回撤（超额）",
        "近六月收益率", "对标指数近六月收益率", "近六月超额（除法）", "近六月最大回撤", "近六月最大回撤（超额）",
        "近一年收益率", "对标指数近一年收益率", "近一年超额（除法）", "近一年最大回撤", "近一年最大回撤（超额）",
        "近两年收益率", "对标指数近两年收益率", "近两年超额（除法）", "近两年最大回撤", "近两年最大回撤（超额）",
        "近三年收益率", "对标指数近三年收益率", "近三年超额（除法）", "近三年最大回撤", "近三年最大回撤（超额）",
        "成立以来收益率", "对标指数成立以来收益率", "成立以来超额（除法）", "成立以来最大回撤", "成立以来最大回撤（超额）"
    ]
    
    # 使用循环初始化数据列为NaN
    for column in columns_to_add:
        list_chanpin_info[column] = np.nan 
    
# =============================================================================
#     # 移除该列并存储在变量中
#     column_move = list_chanpin_info.pop('成立以来累计超额最大回撤')
#     # 将该列插入到新位置
#     list_chanpin_info.insert(11, '成立以来累计超额最大回撤', column_move)
# =============================================================================

# 计算成立以来数据 #      
    # 计算收益序列 #
    netvalue_target_chanpin = list_chanpin_info.loc[0, '复权净值']
    list_chanpin_info['成立以来收益率'] = list_chanpin_info.loc[:, '复权净值']/netvalue_target_chanpin - 1
    # 计算对标指数收益率/超额（除法）序列 # 
    if type_strategy_judge:       
        index_target_chanpin = list_chanpin_info.loc[0, '对标指数']
        list_chanpin_info['对标指数成立以来收益率'] = list_chanpin_info.loc[:, '对标指数']/index_target_chanpin - 1
        list_chanpin_info['成立以来超额（除法）'] = list_chanpin_info.apply(lambda x: (x['成立以来收益率']+1)/(x['对标指数成立以来收益率']+1)-1, axis=1)
    # 计算最大回撤序列 #
    nav_series = list_chanpin_info.loc[:, '成立以来收益率']
    list_chanpin_info.loc[0, '成立以来最大回撤'] = 0
    for d in range(1, len(nav_series)):
        list_chanpin_info.loc[nav_series.index[d], '成立以来最大回撤'] = (nav_series.iloc[d] + 1)/(max(nav_series.iloc[0:d+1]) + 1) - 1
        if list_chanpin_info.loc[nav_series.index[d], '成立以来最大回撤'] > 0:
            list_chanpin_info.loc[nav_series.index[d], '成立以来最大回撤'] = 0
    nav_series = list_chanpin_info.loc[:, '成立以来收益率']
    # 计算超额最大回撤序列 #
    if type_strategy_judge:
        nav_series = list_chanpin_info.loc[:, '成立以来超额（除法）']
        list_chanpin_info.loc[0, '成立以来最大回撤（超额）'] = 0
        for d in range(1, len(nav_series)):
            list_chanpin_info.loc[nav_series.index[d], '成立以来最大回撤（超额）'] = (nav_series.iloc[d] + 1)/(max(nav_series.iloc[0:d+1]) + 1) - 1
            if list_chanpin_info.loc[nav_series.index[d], '成立以来最大回撤（超额）'] > 0:
                list_chanpin_info.loc[nav_series.index[d], '成立以来最大回撤（超额）'] = 0    

    # 定义时间间隔和相应变量
    time_intervals = [
        ("今年以来", time_last_year_time),
        ("近三月", time_lag3_month_time),
        ("近六月", time_lag6_month_time),
        ("近一年", time_lag_year_time),
        ("近两年", time_lag2_year_time),
        ("近三年", time_lag3_year_time)
    ]
    
    for interval, start_time in time_intervals:
        if date_found_chanpin <= start_time:
            time_chanpin = Search_NavDate(list_chanpin_info, start_time, date_found_chanpin, time_time_last_Friday_lag4_time)
            date_start = time_chanpin
    
            mask = list_chanpin_info['净值日期'] >= date_start
            netvalue_chanpin = float((list_chanpin_info.loc[list_chanpin_info['净值日期'] == date_start, ['复权净值']]).values[0][0])
            list_chanpin_info[f'{interval}收益率'] = list_chanpin_info.loc[mask].loc[:, '复权净值'] / netvalue_chanpin - 1
    
            if type_strategy_judge:
                index_netvalue_chanpin = float((list_chanpin_info.loc[list_chanpin_info['净值日期'] == date_start, ['对标指数']]).values[0][0])
                list_chanpin_info[f'对标指数{interval}收益率'] = list_chanpin_info.loc[mask].loc[:, '对标指数'] / index_netvalue_chanpin - 1
                list_chanpin_info[f'{interval}超额（除法）'] = list_chanpin_info.apply(lambda x: (x[f'{interval}收益率'] + 1) / (x[f'对标指数{interval}收益率'] + 1) - 1, axis=1)
    
            # 计算最大回撤
            nav_series = list_chanpin_info.loc[mask, f'{interval}收益率']
            list_chanpin_info.loc[nav_series.index[0], f'{interval}最大回撤'] = 0
            for d in range(1, len(nav_series)):
                list_chanpin_info.loc[nav_series.index[d], f'{interval}最大回撤'] = (nav_series.iloc[d] + 1) / (
                        max(nav_series.iloc[0:d + 1]) + 1) - 1
                if list_chanpin_info.loc[nav_series.index[d], f'{interval}最大回撤'] > 0:
                    list_chanpin_info.loc[nav_series.index[d], f'{interval}最大回撤'] = 0
    
            if type_strategy_judge:
                # 计算超额收益的最大回撤
                nav_series = list_chanpin_info.loc[mask, f'{interval}超额（除法）']
                list_chanpin_info.loc[nav_series.index[0], f'{interval}最大回撤（超额）'] = 0
                for d in range(1, len(nav_series)):
                    list_chanpin_info.loc[nav_series.index[d], f'{interval}最大回撤（超额）'] = (nav_series.iloc[d] + 1) / (
                            max(nav_series.iloc[0:d + 1]) + 1) - 1
                    if list_chanpin_info.loc[nav_series.index[d], f'{interval}最大回撤（超额）'] > 0:
                        list_chanpin_info.loc[nav_series.index[d], f'{interval}最大回撤（超额）'] = 0

# 计算近四周数据 #
    # 时间定义 #
    date_start = Search_NavDate(list_chanpin_info, time_time_last_Friday_lag4_time, date_found_chanpin, time_time_last_Friday_lag4_time)        
    # 是否符合成立区间判定 #
    if date_found_chanpin <= time_time_last_Friday_lag4_time:    
        # 计算收益序列 #
        mask = ((list_chanpin_info['净值日期'] >= date_start) & (list_chanpin_info['净值日期'] <= time_last_Friday_time))
        netvalue_lag4_week_chanpin = float((list_chanpin_info.loc[list_chanpin_info['净值日期'] == date_start, ['复权净值']]).values[0][0])
        list_chanpin_info['近四周收益率'] = list_chanpin_info.loc[mask].loc[:, '复权净值']/netvalue_lag4_week_chanpin - 1
        
        # 计算对标指数收益率/超额（除法）序列 #
        if type_strategy_judge:
            index_netvalue_lag4_week_chanpin = float((list_chanpin_info.loc[list_chanpin_info['净值日期'] == date_start, ['对标指数']]).values[0][0])
            list_chanpin_info['对标指数近四周收益率'] = list_chanpin_info.loc[mask].loc[:, '对标指数']/index_netvalue_lag4_week_chanpin - 1
            list_chanpin_info['近四周超额（除法）'] = list_chanpin_info.apply(lambda x: (x['近四周收益率']+1)/(x['对标指数近四周收益率']+1)-1, axis=1)       
    
    list_chanpin_info.loc[:, '近四周收益率':'成立以来最大回撤（超额）'] = list_chanpin_info.loc[:, '近四周收益率':'成立以来最大回撤（超额）'].round(4)
    list_chanpin_info.to_sql(name=list.iloc[n, 0], con=engine, if_exists='replace', index=False)
    
    # 波动率相关性计算
    # 表结构及列名修改
    list_chanpin_info_weekly = pd.merge(list_chanpin_info, weekly_date, on='净值日期', how='inner')

    list_chanpin_info_weekly = list_chanpin_info_weekly.drop(labels = ['成立以来最大回撤', '对标指数今年以来收益率', '今年以来最大回撤', '近一年最大回撤', '对标指数近一年收益率',
                                            '近三月最大回撤', '对标指数近三月收益率', '近三月最大回撤（超额）', '近六月最大回撤', '对标指数近六月收益率', '近六月最大回撤（超额）',
                                            '近两年最大回撤', '近三年最大回撤', '成立以来最大回撤（超额）', '今年以来最大回撤（超额）', '对标指数近两年收益率',
                                            '近一年最大回撤（超额）', '近两年最大回撤（超额）', '近三年最大回撤（超额）', '对标指数近三年收益率', '近四周收益率',
                                            '对标指数近四周收益率', '近四周超额（除法）', '成立以来超额（除法）'], axis=1)
    list_chanpin_info_weekly.columns = ['净值日期' , '复权净值', '对标指数',  '今年以来波动率', '今年以来超额波动率', 
                                        '近三月波动率', '近三月超额波动率', '近六月波动率', '近六月超额波动率', 
                                        '近一年波动率', '近一年超额波动率', '近两年波动率', '近两年超额波动率', '近三年波动率', 
                                        '近三年超额波动率', '成立以来波动率', '成立以来超额波动率']
    
    # 绝对收益波动率
    for key_name in ['今年以来波动率', '近三月波动率', '近六月波动率', '近一年波动率', '近两年波动率', '近三年波动率', '成立以来波动率']:
        for d in range(len(list_chanpin_info_weekly)):
            if (np.isnan(list_chanpin_info_weekly.loc[d, key_name]) == False) and (d>=1):
                list_chanpin_info_weekly.loc[d, key_name] = list_chanpin_info_weekly.loc[d, '复权净值']/list_chanpin_info_weekly.loc[d-1, '复权净值'] - 1
            else:
                list_chanpin_info_weekly.loc[d, key_name] = np.nan
        if type_strategy_judge:
            volatility = np.std(list_chanpin_info_weekly[key_name])
    
    # 超额收益波动率
    if type_strategy_judge:
        for key_name in ['今年以来超额波动率', '近三月超额波动率', '近六月超额波动率', '近一年超额波动率', '近两年超额波动率', '近三年超额波动率', '成立以来超额波动率']:
            for d in range(len(list_chanpin_info_weekly)):
                if (np.isnan(list_chanpin_info_weekly.loc[d, key_name]) == False) and (d>=1):
                    list_chanpin_info_weekly.loc[d, key_name] = list_chanpin_info_weekly.loc[d, '复权净值']/list_chanpin_info_weekly.loc[d-1, '复权净值'] - list_chanpin_info_weekly.loc[d, '对标指数']/list_chanpin_info_weekly.loc[d-1, '对标指数']
                else:
                    list_chanpin_info_weekly.loc[d, key_name] = np.nan    
            volatility = np.std(list_chanpin_info_weekly[key_name])
    list_chanpin_info_weekly.loc[:, '今年以来波动率':'成立以来超额波动率'] = list_chanpin_info_weekly.loc[:, '今年以来波动率':'成立以来超额波动率'].round(4)
    list_chanpin_info_weekly.to_sql(name=strategy_sql_name+'(波动率)', con=engine, if_exists='replace', index=False)
    list_chanpin_info.to_sql(name=strategy_sql_name, con=engine, if_exists='replace', index=False)

logging.info('衍盛策略绩效净值数据清洗完毕')