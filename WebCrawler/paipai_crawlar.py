import sys
sys.path.append('/home/ubuntu/market/dc-66-python-2.0/SelfModule')

from selfdefinfunc import Calculate_Reinvested_Nav_Process # 自定义封装函数
from tradedate import time_last_Friday_time  # 自定义封装函数

from selenium import webdriver
import warnings
import mysql.connector
from selenium.webdriver.chrome.options import Options
from sqlalchemy import create_engine
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.action_chains import ActionChains
from datetime import datetime
import pandas as pd
from itertools import combinations
import time
import os
import re
import base64
from PIL import Image, ImageEnhance
import io
import random
import pytesseract
import logging

# 设置日志
logging.basicConfig(filename='/home/ubuntu/market/dc-66-python-2.0/execution.log', 
                    level=logging.INFO, 
                    format='%(asctime)s - %(levelname)s - %(message)s', 
                    filemode='a')  # 设置为追加模式

warnings.filterwarnings('ignore')

# 环境变量读取数据库秘钥信息
host = os.environ.get('MYSQL_HOST')   
user = os.environ.get('MYSQL_USERNAME')  
password = os.environ.get('MYSQL_PASSWORD')
port = os.environ.get('MYSQL_PORT')

# 数据库存储操作
cnx = mysql.connector.connect(user=user, password=password,
                              host=host,  port=port, database='同业产品数据库')
engine = create_engine(f'mysql+mysqlconnector://{user}:{password}@{host}:{port}/同业产品数据库')
cursor = cnx.cursor()

# 读取数据库表
list_plat = pd.read_sql_query("SELECT * FROM %s" % '所有产品爬虫信息表', engine)
list_self = pd.read_sql_query("SELECT * FROM %s" % '用户产品净值收集汇总表', engine)

list_paipai = list_plat[list_plat['产品网址'].str.contains('simuwang')].reset_index(drop=True)
list_self_paipai = list_self[list_self['产品网址'].str.contains('simuwang')].reset_index(drop=True)

def calculate_change_percentage(prev_value, current_value):
    if prev_value == 0:
        return 0
    return ((current_value - prev_value) / prev_value) * 100

def fix_recognition_errors(parsed_data, precision_factor, product):
    skip_current_row = False  # 添加标志变量，用于指示是否需要跳过当前循环

    # 从第一个日期开始向后处理，直到倒数第一行停止
    for i in range(len(parsed_data) - 1, 0, -1):
        prev_data = parsed_data[i]
        current_data = parsed_data[i - 1]
        
        # 复权净值的检查与修正类似单位净值
        if '复权净值' in prev_data and '复权净值' in current_data:
            prev_adj_value = float(prev_data['复权净值'])
            current_adj_value = float(current_data['复权净值'])
            calculated_change = calculate_change_percentage(prev_adj_value, current_adj_value)
            
            parsed_change = current_data.get('涨跌幅')
            if parsed_change:
                parsed_change = float(re.sub(r'[^\d.-]', '', parsed_change))
                
                if abs(calculated_change - parsed_change) >= 0.01* precision_factor:
                    # 尝试修正复权净值的识别错误
                    for r in range(1, len(current_data['复权净值']) + 1):  # 遍历所有可能的替换次数
                        for combo in combinations(['3', '6'], r):  # 生成所有可能的替换组合
                            temp_adj_value = list(current_data['复权净值'])
                            replaced_count = 0
                            for char in combo:
                                if char == '3':
                                    replace_char = '2'
                                elif char == '6':
                                    replace_char = '8'
                                
                                for pos in range(len(temp_adj_value)):
                                    if temp_adj_value[pos] == replace_char:
                                        temp_adj_value[pos] = char
                                        replaced_count += 1
                                        if replaced_count == r:
                                            break
                                if replaced_count == r:
                                    break
                            
                            if replaced_count == r:
                                temp_adj_value = ''.join(temp_adj_value)
                                temp_adj_value_float = float(temp_adj_value)
                                temp_change = calculate_change_percentage(prev_adj_value, temp_adj_value_float)
                                if abs(temp_change - parsed_change) < 0.01* precision_factor:
                                    logging.info(product + current_data['净值日期'] + f"复权净值识别错误修正: 原值 {current_data['复权净值']}, 修正值 {temp_adj_value}")
                                    current_data['复权净值'] = temp_adj_value
                                    break
                        if replaced_count == r:
                            # 如果所有替换组合都无法修正，设置标志变量
                            skip_current_row = True
                            break
        
        # 计算单位净值的涨跌幅
        if '单位净值' in prev_data and '单位净值' in current_data:
            prev_net_value = float(prev_data['单位净值'])
            current_net_value = float(current_data['单位净值'])
            calculated_change = calculate_change_percentage(prev_net_value, current_net_value)
        
            # 获取解析出的涨跌幅并去掉百分号
            parsed_change = current_data.get('涨跌幅')
            if parsed_change:
                parsed_change = float(re.sub(r'[^\d.-]', '', parsed_change))
                
                # 如果计算出的涨跌幅与解析出的涨跌幅不一致，尝试修正识别错误
                if abs(calculated_change - parsed_change) >= 0.01* precision_factor:  # 允许一定的误差
                    logging.info(f"单位净值识别错误检测: 计算涨跌幅 {calculated_change}, 解析涨跌幅 {parsed_change}")
                    
                    # 尝试修正单位净值的识别错误
                    for r in range(1, len(current_data['单位净值']) + 1):  # 遍历所有可能的替换次数
                        for combo in combinations(['3', '6'], r):  # 生成所有可能的替换组合
                            temp_net_value = list(current_data['单位净值'])
                            replaced_count = 0
                            for char in combo:
                                if char == '3':
                                    replace_char = '2'
                                elif char == '6':
                                    replace_char = '8'
                                
                                for pos in range(len(temp_net_value)):
                                    if temp_net_value[pos] == replace_char:
                                        temp_net_value[pos] = char
                                        replaced_count += 1
                                        if replaced_count == r:
                                            break
                                if replaced_count == r:
                                    break
                            
                            if replaced_count == r:
                                temp_net_value = ''.join(temp_net_value)
                                temp_net_value_float = float(temp_net_value)
                                temp_change = calculate_change_percentage(prev_net_value, temp_net_value_float)
                                if abs(temp_change - parsed_change) < 0.01* precision_factor:
                                    logging.info(product + current_data['净值日期'] + f"单位净值识别错误修正: 原值 {current_data['单位净值']}, 修正值 {temp_net_value}")
                                    current_data['单位净值'] = temp_net_value
                                    break
                        if replaced_count == r:
                            # 如果所有替换组合都无法修正，设置标志变量
                            skip_current_row = True
                            break

        # 检查累计净值
        if '累计净值' in prev_data and '累计净值' in current_data:
            prev_net_value = float(prev_data['单位净值'])
            current_net_value = float(current_data['单位净值'])
            
            prev_accum_value = float(prev_data['累计净值'])
            current_accum_value = float(current_data['累计净值'])
            prev_diff = prev_accum_value - prev_net_value
            current_diff = current_accum_value - current_net_value
            
            if abs(current_diff - prev_diff) > 0.00009* precision_factor:
            # 检查是否是分红日
            
                # 检查是否是下一个累计净值出现问题
                for r in range(1, len(current_data['累计净值']) + 1):  # 遍历所有可能的替换次数
                    for combo in combinations(['3', '6'], r):  # 生成所有可能的替换组合
                        temp_accum_value = list(current_data['累计净值'])
                        replaced_count = 0
                        for char in combo:
                            if char == '3':
                                replace_char = '2'
                            elif char == '6':
                                replace_char = '8'
                            
                            for pos in range(len(temp_accum_value)):
                                if temp_accum_value[pos] == replace_char:
                                    temp_accum_value[pos] = char
                                    replaced_count += 1
                                    if replaced_count == r:
                                        break
                            if replaced_count == r:
                                break
                        
                        if replaced_count == r:
                            temp_accum_value = ''.join(temp_accum_value)
                            temp_accum_value_float = float(temp_accum_value)
                            temp_diff = temp_accum_value_float - current_net_value
                            if abs(temp_diff - prev_diff) <= 0.00009* precision_factor:
                                logging.info(product + current_data['净值日期'] + f"累计净值识别错误修正: 原值 {current_data['累计净值']}, 修正值 {temp_accum_value}")
                                current_data['累计净值'] = temp_accum_value
                                break
                    if replaced_count == r:
                        # 如果所有替换组合都无法修正，设置标志变量
                        skip_current_row = True
                        break
    return skip_current_row  # 返回标志变量

# 创建 ChromeOptions 对象
chrome_options = Options()
chrome_options.add_argument('--headless')
chrome_options.add_argument('--disable-gpu')
chrome_options.add_experimental_option('excludeSwitches', ['enable-automation'])
chrome_options.add_experimental_option('useAutomationExtension', False)
# 创建 Chrome WebDriver，并传入 ChromeOptions
driver = webdriver.Chrome(options=chrome_options) 
time.sleep(3)

# 谷歌浏览器 79和79版本后防止被检测
driver.execute_cdp_cmd("Page.addScriptToEvaluateOnNewDocument", {
"source": """
Object.defineProperty(navigator, 'webdriver', {
  get: () => false
});
"""
})

# 排排网爬虫
driver.get('https://dc.simuwang.com/')
time.sleep(5)

# 勾选我已阅读并同意
driver.find_element(By.XPATH, '//label[@class="el-checkbox agree-checkbox items-start! pt-2! w-20"]').click() # 账号密码登录
# 密码登录
driver.find_element(By.XPATH, '//button[@class="nav-btn ml-24"]').click()   # 点击合格投资者确认

# 使用 By 类来定位元素
elements = driver.find_elements(By.XPATH, '//input[@placeholder="请输入手机号"]')
# 检查是否存在第二个元素
if len(elements) > 1:
    # 第二个元素的索引是1
    elements[1].send_keys('13121023419')
else:
    logging.info("没有找到第二个匹配的元素。")

# 输入密码
driver.find_element(By.XPATH,'//input[@placeholder="请输入密码"]').send_keys('6988wwwcn')
# 点击登录
driver.find_element(By.XPATH, '//button[@class="el-button login-btn"]').click()
time.sleep(random.randint(3, 6))

for list_all, list_paipai in [(list_plat, list_paipai), (list_self, list_self_paipai)]:
    # 开始循环网址
    for n, adr in enumerate(list_paipai['产品网址']):
        if_skip = False
        # 查询产品净值数据库表
        # 构建 SQL 查询，选择表的倒数第一行数据
        query = "SELECT 净值日期, 单位净值, 累计净值, 复权净值 FROM {} ORDER BY ROW_NUMBER() OVER() DESC LIMIT 1".format(list_paipai.loc[n, '产品名称'])
        # 使用 SQL 查询从数据库中读取倒数第一行数据
        list_chanpin_info = pd.read_sql_query(query, engine)
        # 查看是否已经更新
        if list_chanpin_info['净值日期'].iloc[0] >= time_last_Friday_time:
            list_all.loc[list_all['产品名称'] == list_paipai.loc[n, '产品名称'], '最新净值日期'] = list_chanpin_info['净值日期'].iloc[0]
            print(str(n) + list_paipai.loc[n, '产品名称'] + '排排网净值已是最新')
            continue
        
        driver.get(adr)
        
        time.sleep(random.randint(2, 4))
        # 找到要点击的元素
        driver.find_elements(By.XPATH, '//h2[@class="xp-nav-item IQom3tCwg"]')[1].click()
        
        time.sleep(random.randint(3, 4))
         # 使用你提供的 XPath 表达式锁定第一个 div
        try:
            first_table = driver.find_element(By.XPATH, '''
            //table[contains(@class, "el-table__header")]
            /thead/tr/th[
                contains(., '净值日期') or
                contains(., '单位净值') or
                contains(., '累计净值(分红不投资)') or
                contains(., '累计净值(分红再投资)') or
                contains(., '净值变动')
            ]
            ''')
        except:
            logging.info('!' + str(n) + list_paipai.loc[n, '产品名称'] + '排排网网址停止更新，请检查')
            continue
        
        # 找到包含第一个 table 的 div 标签
        first_table_div = first_table.find_element(By.XPATH, './ancestor::div[1]')
        
        # 找到与该 div 标签并列的下一个 div 标签
        next_div = first_table_div.find_element(By.XPATH, './following-sibling::div[1]')
        
        # 从下一个 div 标签中找到 table
        next_table = next_div.find_element(By.XPATH, './/table')
    
        # 找出 table 里面所有的 tr 标签
        divs = next_table.find_elements(By.XPATH, './/tr')
    
        # 用于存储解析出的数据
        parsed_data = []
        
        first_iteration = True
        for div in divs:
            # 找到<div>内的所有<span>元素
            spans = div.find_elements(By.TAG_NAME, 'span')
            # 保存第一个<span>的文本内容为'净值日期'
            net_value_date = div.text.split('\n')[0]
            
            # 如果当前净值日期小于等于数据库中最新净值日期，则停止解析
            if pd.to_datetime(net_value_date, format='%Y-%m-%d') <= list_chanpin_info['净值日期'].iloc[0]:
                if first_iteration:
                    if_skip = True
                break
            
            # 标记第一次循环已完成
            first_iteration = False
            parsed_data.append({'净值日期': net_value_date})
            
            # 保存第5个<span>的文本内容为涨跌幅
            parsed_data[-1]['涨跌幅'] = div.text.split('\n')[1]
            
            # 从第二个<span>开始解析，保存为单位净值、复权净值、累计净值
            for i, span in enumerate(spans[0:3]):
                # 找到<span>内的所有<img>元素
                images = span.find_elements(By.TAG_NAME, 'img')
                
                # 用于存储当前<span>内的数字
                current_number = []
                
                img_first_iteration = True  # 添加标志变量
                for img in images:
                    # 模拟鼠标经过图像
                    ActionChains(driver).move_to_element(img).perform()
                    
                    # 获取图像的渲染宽度
                    rendered_width = img.size['width']
                    
                    # 如果渲染宽度不为0，则处理图像
                    if rendered_width == 0 or rendered_width == 1:
                        continue
                    
                    # 单独的小数点处理
                    if rendered_width == 4:
                        current_number.append('.')
                        continue
                    
                    # 获取图片的base64数据
                    src = img.get_attribute('src')
                    base64_data = src.split(',')[1]
                    # 解码base64数据
                    image_data = base64.b64decode(base64_data)   
                    # 使用PIL打开图像
                    image = Image.open(io.BytesIO(image_data)).convert("RGBA")        
                    # 创建一个纯白色的背景图像
                    background = Image.new("RGBA", image.size, "WHITE")
                    # 将图像与背景图像进行alpha合成
                    image = Image.alpha_composite(background, image)
                    
                    recognized_content = ''
                    # 判断图像中包含的字符数量
                    for scale_factor in range(1, 5):  # 尝试不同的scale_factor，从1到4
                        # 提高图像分辨率
                        new_size = (image.width * scale_factor, image.height * scale_factor)
                        resized_image = image.resize(new_size, Image.Resampling.LANCZOS)
                        enhancer = ImageEnhance.Contrast(resized_image)
                        resized_image = enhancer.enhance(2)
                        
                        width, height = resized_image.size
                        
                        if width < height:  # 假设一个字符的图像宽度是两个字符的一倍
                            # 图像包含一个字符
                            char_image = resized_image
                            recognized_content = pytesseract.image_to_string(char_image, config=r'--psm 6 -c tessedit_char_whitelist=0123456789.').strip()
                            if recognized_content:
                                break
                        elif width == height:  # 假设图像包含两个字符，其中一个字符一定是 '.'
                            char_width = width // 3
                            # 考虑第一次有可能把小数点识别到左边的情况
                            if not img_first_iteration:
                                
                                left_image = resized_image.crop((0, 0, char_width, height))
                                right_image = resized_image.crop((char_width, 0, width, height))
                                # 识别左半部分的字符
                                left_content = pytesseract.image_to_string(left_image, config=r'--psm 6 -c tessedit_char_whitelist=0123456789').strip()
                                right_content = pytesseract.image_to_string(right_image, config=r'--psm 6 -c tessedit_char_whitelist=0123456789').strip()
                                
                                # 如果左半部分识别结果不为空，则右半部分一定是 '.'
                                if left_content:
                                    recognized_content = left_content + '.'
                                    break
                                if right_content:
                                    recognized_content = '.' + right_content
                                    break
                            
                            # 尝试2/3比例分割
                            left_image = resized_image.crop((0, 0, char_width*2, height))
                            right_image = resized_image.crop((char_width*2, 0, width, height))
                            # 识别左半部分的字符
                            left_content = pytesseract.image_to_string(left_image, config=r'--psm 6 -c tessedit_char_whitelist=0123456789').strip()
                            right_content = pytesseract.image_to_string(right_image, config=r'--psm 6 -c tessedit_char_whitelist=0123456789').strip()
                            
                            # 如果左半部分识别结果不为空，则右半部分一定是 '.'
                            if left_content:
                                recognized_content = left_content + '.'
                                break
                            if right_content:
                                recognized_content = '.' + right_content
                                break
                        else:
                            # 图像包含两个字符
                            char_width = width // 2
                            left_image = resized_image.crop((0, 0, char_width, height))
                            right_image = resized_image.crop((char_width, 0, width, height))
                            left_content = pytesseract.image_to_string(left_image, config=r'--psm 6 -c tessedit_char_whitelist=0123456789').strip()
                            right_content = pytesseract.image_to_string(right_image, config=r'--psm 6 -c tessedit_char_whitelist=0123456789').strip()
                            if left_content and right_content:
                                recognized_content = left_content + right_content
                                break
                    img_first_iteration = False  # 更新标志变量
                    current_number.append(recognized_content)
                
                # 将识别出的数字拼接成一个完整的数字
                full_number = ''.join(current_number)
                
                # 保存解析出的数字
                if i == 0:
                    parsed_data[-1]['单位净值'] = full_number
                elif i == 2:
                    parsed_data[-1]['复权净值'] = full_number
                elif i == 1:
                    parsed_data[-1]['累计净值'] = full_number
            
        # 如果净值未更新，直接跳过
        if if_skip == True:
            print('#' + str(n) + list_paipai.loc[n, '产品名称'] + '排排网净值未更新')
            continue
        
        latest_net_value_date = list_chanpin_info['净值日期'].iloc[0].strftime('%Y-%m-%d')
        # 将数据库中的最新净值数据添加到解析数据中
        if list_chanpin_info.shape[0] > 0:
            parsed_data.append({
                '净值日期': latest_net_value_date,
                '单位净值': str(list_chanpin_info['单位净值'].iloc[0]),
                '复权净值': str(list_chanpin_info['复权净值'].iloc[0]),
                '累计净值': str(list_chanpin_info['累计净值'].iloc[0])
            })
        
        # 净值检查环节
        # 获取净值精度因子
        if list_paipai.loc[n, '净值小数点分位'] == 3:
            precision_factor = 10
        else:
            precision_factor = 1

        # 调用 fix_recognition_errors 函数
        skip_current_row = fix_recognition_errors(parsed_data, precision_factor, list_paipai.loc[n, '产品名称'])
        # 如果需要跳过当前循环，则跳过
        if skip_current_row:
            # 回填总表数据
            list_all.loc[list_all['产品名称'] == list_paipai.loc[n, '产品名称'], '更新净值日期'] = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
            logging.info('@' + str(n) + list_paipai.loc[n, '产品名称'] + '排排网净值无法识别纠正，请检查')
            continue

        try:
            # 将解析数据调整为DataFrame格式
            parsed_df = pd.DataFrame(parsed_data).drop(columns=['涨跌幅']).sort_values(by='净值日期', ascending=True)
        except:
            logging.info('!' + str(n) + list_paipai.loc[n, '产品名称'] + '排排网网址停止更新，请检查')
            continue

        # 将除了“净值日期”以外的所有列转换为 float 类型
        for column in parsed_df.columns:
            if column != '净值日期':
                parsed_df[column] = parsed_df[column].astype(float)
        # 重新计算复权净值
        parsed_df['复权净值'] = Calculate_Reinvested_Nav_Process(parsed_df, '单位净值', '累计净值', '复权净值')
        # 把重复数据删除
        parsed_df = parsed_df[parsed_df['净值日期'] != latest_net_value_date]
        
        # 将解析数据追加到数据库表中
        parsed_df.to_sql(list_paipai.loc[n, '产品名称'], engine, if_exists='append', index=False)
        # 输出结果
        logging.info('*' + str(n) + list_paipai.loc[n, '产品名称'] + '排排网净值已更新')
        # 回填总表数据
        list_all.loc[list_all['产品名称'] == list_paipai.loc[n, '产品名称'], '更新净值日期'] = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        list_all.loc[list_all['产品名称'] == list_paipai.loc[n, '产品名称'], '最新净值日期'] = parsed_df.iloc[-1]['净值日期']

list_plat.to_sql(name='所有产品爬虫信息表', con=engine, if_exists='replace', index=False)
list_self.to_sql(name='用户产品净值收集汇总表', con=engine, if_exists='replace', index=False)

driver.quit()

# 关闭游标
cursor.close()
# 关闭连接
cnx.close()

# =============================================================================
# base64_data = 'iVBORw0KGgoAAAANSUhEUgAAABgAAAAkBAMAAABh4ecdAAAAG1BMVEUAAAAfHx8mJiYMDAwGBgYTExMZGRksLCwzMzP4q2PnAAAAAXRSTlMAQObYZgAAAAlwSFlzAAAOxAAADsQBlSsOGwAAAJ9JREFUGJVjYMADKjogoI0Ap9UFDBzAnGZkA6jMYU6qKEqBclolQA5IgHDawa5phLq6PdnFsKPDAMIRAJIWHQVgThOIZO0AGxoaAiJZoJogQKMdiZPRgcSJ6HDAwbHAKQPmMIcGIExj62gAcyRA9jBD3MEAcUEH2G2MEDEJsKszIK726GgPdg7qgJjMDAn3VogNRiB2uwHUPkP19kKQZQD2Ykhws6SrSQAAAABJRU5ErkJggg=='
# # 解码base64数据
# image_data = base64.b64decode(base64_data)
# 
# # 使用PIL打开图像
# image = Image.open(io.BytesIO(image_data)).convert("RGBA")
# 
# # 创建一个纯白色的背景图像
# background = Image.new("RGBA", image.size, "WHITE")
# 
# # 将图像与背景图像进行alpha合成
# image = Image.alpha_composite(background, image)
# recognized_content = ''
# 
# # 判断图像中包含的字符数量
# for scale_factor in range(1, 5):  # 尝试不同的scale_factor，从1到4
#     # 提高图像分辨率
#     new_size = (image.width * scale_factor, image.height * scale_factor)
#     resized_image = image.resize(new_size, Image.Resampling.LANCZOS)
#     enhancer = ImageEnhance.Contrast(resized_image)
#     resized_image = enhancer.enhance(2)
#     
#     width, height = resized_image.size
#     
#     if width < height:  # 假设一个字符的图像宽度是两个字符的一倍
#         # 图像包含一个字符
#         char_image = resized_image
#         recognized_content = pytesseract.image_to_string(char_image, config=r'--psm 6 -c tessedit_char_whitelist=0123456789.').strip()
#         if recognized_content:
#             break
#     elif width == height:  # 假设图像包含两个字符，其中一个字符一定是 '.'
#         char_width = width // 3
#         left_image = resized_image.crop((0, 0, char_width, height))
#         right_image = resized_image.crop((char_width, 0, width, height))
#         # 识别左半部分的字符
#         left_content = pytesseract.image_to_string(left_image, config=r'--psm 6 -c tessedit_char_whitelist=0123456789.').strip()
#         right_content = pytesseract.image_to_string(right_image, config=r'--psm 6 -c tessedit_char_whitelist=0123456789.').strip()
#         
#         # 如果左半部分识别结果不为空，则右半部分一定是 '.'
#         if left_content:
#             recognized_content = left_content + '.'
#             break
#         if right_content:
#             recognized_content = '.' + right_content
#             break
#         if recognized_content == '':
#             # 尝试2/3比例分割 
#             left_image = resized_image.crop((0, 0, char_width*2, height))
#             right_image = resized_image.crop((char_width*2, 0, width, height))
#             # 识别左半部分的字符
#             left_content = pytesseract.image_to_string(left_image, config=r'--psm 6 -c tessedit_char_whitelist=0123456789').strip()
#             right_content = pytesseract.image_to_string(right_image, config=r'--psm 6 -c tessedit_char_whitelist=0123456789').strip()
#             
#             # 如果左半部分识别结果不为空，则右半部分一定是 '.'
#             if left_content:
#                 recognized_content = left_content + '.'
#                 break
#             if right_content:
#                 recognized_content = '.' + right_content
#                 break
#         
#     else:
#         # 图像包含两个字符
#         char_width = width // 2
#         left_image = resized_image.crop((0, 0, char_width, height))
#         right_image = resized_image.crop((char_width, 0, width, height))
#         left_content = pytesseract.image_to_string(left_image, config=r'--psm 6 -c tessedit_char_whitelist=0123456789.').strip()
#         right_content = pytesseract.image_to_string(right_image, config=r'--psm 6 -c tessedit_char_whitelist=0123456789.').strip()
#         if left_content and right_content:
#             recognized_content = left_content + right_content
#             break
# 
# =============================================================================
