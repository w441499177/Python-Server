import sys
sys.path.append('/home/ubuntu/market/dc-66-python-2.0/SelfModule')

from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from urllib.parse import unquote
from lxml import etree
import mysql.connector
from sqlalchemy import create_engine
import codecs
import warnings
import akshare as ak
import matplotlib.pyplot as plt
from selenium.webdriver.common.by import By
import numpy as np
import pandas as pd
from datetime import datetime, date, timedelta
from downloadindex import data_CSI300 # 自定义封装函数
import time
import os
import requests
from io import BytesIO
from PIL import Image
import pytesseract

plt.rcParams['font.sans-serif'] = ['SimHei']  # 用来正常显示中文标签
plt.rcParams['axes.unicode_minus'] = False  # 用来正常显示负号
warnings.filterwarnings('ignore')

def Recent_TradeDay(date):
    for i in range(10):
        if (is_workday(date) == False) or (date.weekday() == 5) or (date.weekday() == 6):
            date = date + timedelta(days=-1)
        else:
            break
    return date

# 环境变量读取数据库秘钥信息
host = os.environ.get('MYSQL_HOST')   
user = os.environ.get('MYSQL_USERNAME')  
password = os.environ.get('MYSQL_PASSWORD')
port = os.environ.get('MYSQL_PORT')

# 数据库存储操作
cnx = mysql.connector.connect(user=user, password=password,
                              host=host,  port=port, database='鸣熙产品净值数据库')
engine = create_engine(f'mysql+mysqlconnector://{user}:{password}@{host}:{port}/鸣熙产品净值数据库')
cursor = cnx.cursor()

# 创建 ChromeOptions 对象
chrome_options = Options()
chrome_options.add_argument('--headless')
chrome_options.add_argument('--disable-gpu')
# 创建 Chrome WebDriver，并传入 ChromeOptions
driver = webdriver.Chrome(options=chrome_options) 
driver.get('http://zscrm.stonescloud.com/accounts/sign_in') 
time.sleep(2) # 等待让页面完全加载

a = 0 
while a==0:
    # 验证码处理
        # 定位验证码元素
    captcha_element = driver.find_element(By.XPATH,'//div[@id="proof"]/a/img')
        # 获取验证码图片的src属性
    captcha_src = captcha_element.get_attribute('src')
        # 下载验证码图片
    response = requests.get(captcha_src)
    image = Image.open(BytesIO(response.content))

        # 使用Tesseract识别验证码
    captcha_text = pytesseract.image_to_string(image).replace(" ", "").replace("\n", "")

    # 自动填充账号和密码并点击登录
    driver.find_element(By.XPATH,'//input[@placeholder="邮箱"]').send_keys('wanghuistb@foxmail.com')      # 输入账号
    driver.find_element(By.XPATH,'//input[@placeholder="密码"]').send_keys('tfkW5oF7')        # 输入密码
    driver.find_element(By.XPATH,'//input[@placeholder="请输入右侧验证码"]').send_keys(captcha_text)        # 输入验证码
    driver.find_element(By.XPATH,'//div/a[@class="login_de"]').click()     # 点击登录
    time.sleep(2)
    try:
        sucess_judge = driver.find_elements(By.XPATH,'//span[@class="ver_mid tip_title_sizie12"]')[0].text
    except:
        sucess_judge = '无'
    # 判断此时的页面
    if sucess_judge == '验证码不匹配':
        driver.get('http://zscrm.stonescloud.com/accounts/sign_in')
        time.sleep(2)
        try:
            driver.find_element(By.XPATH,'//div/a[@class="btn fott_qud01"]').click()      # 点击提示框
        except:
            time.sleep(2)
    else:
        a = 1
time.sleep(5) # 等待让页面完全加载

# 【所有产品明细表】
list_chanpin_info = pd.read_sql_query("SELECT * FROM %s" % '所有产品明细表', engine)
# 处理特殊产品
list_chanpin_info = list_chanpin_info[list_chanpin_info['最新份额'] != '-']

# 读取所有产品份额表和所有产品规模表
all_chanpin_fene = pd.read_sql_query("SELECT * FROM %s" % '所有产品份额表', engine)
all_chanpin_aum = pd.read_sql_query("SELECT * FROM %s" % '所有产品规模表', engine)

num_need_update = str(len(list_chanpin_info))

first_iteration = True  # 设置一个标志，用于判断是否是第一次循环
# 遍历每个产品净值表
k = 0
for url, full_name, chanpin_aum in zip(list_chanpin_info['产品净值链接'], list_chanpin_info['基金全称'], list_chanpin_info['资产规模']):
    
    url = url.replace('nets', 'shares')
    # 判断产品是否最新净值、是否新加入产品、是否已清盘
    driver.get(url)
    if first_iteration:  # 检查是否是第一次循环
        time.sleep(8)  # 等待15秒
        first_iteration = False  # 重置标志
    # 设置等待的最长时间，例如60秒
    time.sleep(6)

    a = 0
    if a == 0:
        # 构建 SQL 查询，检查表中是否存在指定的列名
        query_check_column = """
        SELECT COLUMN_NAME
        FROM INFORMATION_SCHEMA.COLUMNS
        WHERE TABLE_NAME = '所有产品规模表' AND COLUMN_NAME = '{}'
        """.format(full_name)
        
        # 使用 pandas 读取数据库
        result_check_column = pd.read_sql_query(query_check_column, engine)
        
        if not result_check_column.empty:
            # 列名存在，构建 SQL 查询，取该列最后一行的净值日期数据
            query_get_last_row = """
            SELECT 净值日期
            FROM 所有产品规模表
            ORDER BY 净值日期 DESC
            LIMIT 1
            """
            
            # 使用 pandas 读取数据库
            result_last_row = pd.read_sql_query(query_get_last_row, engine)
            
            update_date = result_last_row['净值日期'][0]  # 载入最后的更新日期
            
            nav_date = []
            nav_fene = []
            nav_aum = []
            
            # 源代码字符串解析
            encoded_string = driver.execute_cdp_cmd('Page.captureSnapshot', {})['data']
            # 将字符串转换为字节对象
            encoded_bytes = bytes(encoded_string, 'utf-8')
            # 解码字符串
            decoded_bytes = codecs.decode(encoded_bytes, 'quoted-printable')
            # 将解码后的字节对象转换为字符串
            decoded_string = decoded_bytes.decode('utf-8')
            root = etree.HTML(decoded_string)
            # 使用 XPath 定位产品信息
            elements = root.xpath('//div[@style="width: 1048px;"]/div/div/table[@id="toolbar"]/tbody/tr[@tabindex="-1"]')
            a = 0
            iteration_count = 0  # Initialize the iteration counter
            
            while a == 0 and iteration_count < 5:
                # 遍历第一页的元素
                for element in elements:
                    updating_date = datetime.strptime(element.xpath('.//td[@aria-describedby="toolbar_日期"]')[0].text, '%Y-%m-%d')
                    if updating_date <= update_date:
                        a = 1
                        if element == elements[0]:
                            print(full_name + '无需更新规模')
                        break
                    # 判断是否为未更新净值
                    if updating_date > update_date:
                        fene_name = element.xpath('.//td[@aria-describedby="toolbar_基金名称"]')[0].text.lower()
                        # 排除非主份额情况
                        if fene_name != full_name:
                            continue
                        # 净值日期
                        nav_date_value = element.xpath('.//td[@aria-describedby="toolbar_日期"]')[0].text
                        # 最新份额
                        nav_fene_value = element.xpath('.//td[@aria-describedby="toolbar_最新份额"]')[0].text
                        # 最新规模
                        nav_aum_value = element.xpath('.//td[@aria-describedby="toolbar_最新规模"]')[0].text
                        # 如果值为'-'，跳过当前循环
                        if nav_date_value == '-' or nav_fene_value == '-' or nav_aum_value == '-':
                            continue
                        # 此处执行你的其他操作，将有效的值添加到相应的列表中
                        nav_date.append(nav_date_value)
                        nav_fene.append(nav_fene_value)
                        nav_aum.append(nav_aum_value)
                # 判断是否需要进入下一页
                    # 如果最后更新日期仍然没有接上截止日期
                if updating_date > update_date:
                    try:
                        iframe = driver.find_elements(By.CSS_SELECTOR, 'iframe[id="J_iframe"]')[1]
                        driver.switch_to.frame(iframe)
                        page_next = driver.find_element(By.XPATH,'//td[@title="下一页"]').click()
                    except:
                        # 源代码字符串解析
                        encoded_string = driver.execute_cdp_cmd('Page.captureSnapshot', {})['data']
                        # 将字符串转换为字节对象
                        encoded_bytes = bytes(encoded_string, 'utf-8')
                        # 解码字符串
                        decoded_bytes = codecs.decode(encoded_bytes, 'quoted-printable')
                        # 将解码后的字节对象转换为字符串
                        decoded_string = decoded_bytes.decode('utf-8')
                        root = etree.HTML(decoded_string)
                        # 使用 XPath 定位产品信息
                        elements = root.xpath('//table[@id="toolbar"]/tbody/tr[@tabindex="-1"]')
                else:
                    a = 1
                # Increment the iteration counter
                iteration_count += 1
            if iteration_count == 5:
                print(full_name + "超时未抓取最新净值")
            
        else:
            nav_date = []
            nav_fene = []
            nav_aum = []
            # 判断是否最后一页净值
            a = 0
            num_judge = 0
            while a == 0:
                # 先判断是否需要进入翻页循环
                if num_judge == 0:
                    iframe = driver.find_elements(By.CSS_SELECTOR, 'iframe[id="J_iframe"]')[1]
                    driver.switch_to.frame(iframe)
                page_next = driver.find_element(By.XPATH,'//td[@title="下一页"]')
    
                time.sleep(2)
                page_judge = page_next.get_attribute('class')
                if page_judge == 'ui-pg-button ui-corner-all ui-state-disabled':
                    a = 1
                else:
                    num_judge = 1
                
                # 源代码字符串解析
                encoded_string = driver.execute_cdp_cmd('Page.captureSnapshot', {})['data']
                # 将字符串转换为字节对象
                encoded_bytes = bytes(encoded_string, 'utf-8')
                # 解码字符串
                decoded_bytes = codecs.decode(encoded_bytes, 'quoted-printable')
                # 将解码后的字节对象转换为字符串
                decoded_string = decoded_bytes.decode('utf-8')
                root = etree.HTML(decoded_string)
                # 使用 XPath 定位产品信息
                elements = root.xpath('//div[@style="width: 1048px;"]/div/div/table[@id="toolbar"]/tbody/tr[@tabindex="-1"]')
                for element in elements:
                    fene_name = element.xpath('.//td[@aria-describedby="toolbar_基金名称"]')[0].text.lower()
                    # 排除非主份额情况
                    if fene_name != full_name:
                        continue
                    # 净值日期
                    nav_date_value = element.xpath('.//td[@aria-describedby="toolbar_日期"]')[0].text
                    # 最新份额
                    nav_fene_value = element.xpath('.//td[@aria-describedby="toolbar_最新份额"]')[0].text
                    # 最新规模
                    nav_aum_value = element.xpath('.//td[@aria-describedby="toolbar_最新规模"]')[0].text
                    # 如果值为'-'，跳过当前循环
                    if nav_date_value == '-' or nav_fene_value == '-' or nav_aum_value == '-':
                        continue
                    # 此处执行你的其他操作，将有效的值添加到相应的列表中
                    nav_date.append(nav_date_value)
                    nav_fene.append(nav_fene_value)
                    nav_aum.append(nav_aum_value)
                # 最后点击翻页
                page_next.click()
        
        list_chanpin_nav = pd.DataFrame({'净值日期':nav_date, '最新份额':nav_fene, '最新规模':nav_aum})
        list_chanpin_nav['净值日期'] = pd.to_datetime(list_chanpin_nav['净值日期'])
        list_chanpin_nav = list_chanpin_nav.replace('-', np.nan)
        list_chanpin_nav = list_chanpin_nav.dropna()
        list_chanpin_nav = list_chanpin_nav.sort_values(by='净值日期', ascending=True).reset_index().drop('index', axis=1)
        
        # 剔除非交易日净值
        list_chanpin_nav = list_chanpin_nav[list_chanpin_nav['净值日期'].isin(data_CSI300['date'])]
        list_chanpin_nav = list_chanpin_nav.dropna(how='any')
        list_chanpin_nav = list_chanpin_nav.drop_duplicates().reset_index(drop=True)
         
        # 合并最新份额数据到所有产品份额表
        if full_name in all_chanpin_fene.columns:
            # 如果列存在，追加数据
            all_chanpin_fene = all_chanpin_fene.merge(list_chanpin_nav[['净值日期', '最新份额']], on='净值日期', how='outer')
            all_chanpin_fene[full_name] = all_chanpin_fene[full_name].combine_first(all_chanpin_fene['最新份额'])
            all_chanpin_fene = all_chanpin_fene.drop(columns=['最新份额'])
        else:
            # 如果列不存在，创建新列
            all_chanpin_fene = all_chanpin_fene.merge(list_chanpin_nav[['净值日期', '最新份额']], on='净值日期', how='outer')
            all_chanpin_fene = all_chanpin_fene.rename(columns={'最新份额': full_name})

        # 合并最新规模数据到所有产品规模表
        if full_name in all_chanpin_aum.columns:
            # 如果列存在，追加数据
            all_chanpin_aum = all_chanpin_aum.merge(list_chanpin_nav[['净值日期', '最新规模']], on='净值日期', how='outer')
            all_chanpin_aum[full_name] = all_chanpin_aum[full_name].combine_first(all_chanpin_aum['最新规模'])
            all_chanpin_aum = all_chanpin_aum.drop(columns=['最新规模'])
        else:
            # 如果列不存在，创建新列
            all_chanpin_aum = all_chanpin_aum.merge(list_chanpin_nav[['净值日期', '最新规模']], on='净值日期', how='outer')
            all_chanpin_aum = all_chanpin_aum.rename(columns={'最新规模': full_name})
         
    # 更新进展数
    k = k + 1
    print(full_name + '更新规模' + str(k) + '/' + num_need_update)

driver.quit()

# 首先对数据进行排序
all_chanpin_fene = all_chanpin_fene.sort_values(by='净值日期', ascending=True).reset_index(drop=True)
all_chanpin_aum = all_chanpin_aum.sort_values(by='净值日期', ascending=True).reset_index(drop=True)

# 关闭连接
engine.dispose()
cnx.close()

time.sleep(1)

engine = create_engine(f'mysql+mysqlconnector://{user}:{password}@{host}:{port}/鸣熙产品净值数据库')

# 使用上下文管理器来确保事务的正确管理
with engine.begin() as connection:
    try:
        # 将更新后的数据写回数据库
        all_chanpin_fene.to_sql('所有产品份额表', connection, if_exists='replace', index=False)
    except Exception as e:
        # 如果发生异常，回滚事务
        connection.rollback()
        raise e

# 使用上下文管理器来确保事务的正确管理
with engine.begin() as connection:
    try:
        # 将更新后的数据写回数据库
        all_chanpin_aum.to_sql('所有产品规模表', connection, if_exists='replace', index=False)
    except Exception as e:
        # 如果发生异常，回滚事务
        connection.rollback()
        raise e

# 关闭连接
engine.dispose()
cnx.close()
