import sys
sys.path.append('/home/ubuntu/market/dc-66-python-2.0/SelfModule')

from selenium import webdriver
import warnings
import mysql.connector
from selenium.webdriver.chrome.options import Options
from sqlalchemy import create_engine
import matplotlib.pyplot as plt
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
import numpy as np
import pandas as pd
from datetime import datetime, timedelta
from tradedate import *  # 自定义封装函数
from downloadindex import BenchmarkIndex_mapping # 自定义封装函数
from selfdefinfunc import BenchmarkIndex, Calculate_Reinvested_Nav, Process_Nav_Dataframe, check_a_share_trading_day
import time
import os
import re
import requests
import json
import imaplib
import email
from email.header import decode_header
from typing import List
from openpyxl import load_workbook, Workbook
import random
import tempfile
import logging
import subprocess

# 设置日志
logging.basicConfig(filename='/home/ubuntu/market/dc-66-python-2.0/execution.log', 
                    level=logging.INFO, 
                    format='%(asctime)s - %(levelname)s - %(message)s', 
                    filemode='a')  # 设置为追加模式

plt.rcParams['font.sans-serif'] = ['SimHei']  # 用来正常显示中文标签
plt.rcParams['axes.unicode_minus'] = False  # 用来正常显示负号
warnings.filterwarnings('ignore')

# 检查是否非交易日
check_a_share_trading_day()

# 环境变量读取数据库秘钥信息
host = os.environ.get('MYSQL_HOST')   
user = os.environ.get('MYSQL_USERNAME')  
password = os.environ.get('MYSQL_PASSWORD')
port = os.environ.get('MYSQL_PORT')
refreshToken = os.environ.get('iFind_refreshToken')

# 数据库存储操作
cnx = mysql.connector.connect(user=user, password=password,
                              host=host,  port=port, database='同业产品数据库')
engine = create_engine(f'mysql+mysqlconnector://{user}:{password}@{host}:{port}/同业产品数据库')
cursor = cnx.cursor()


# 获取当前脚本的绝对路径
current_script_path = os.path.abspath(__file__)
# 获取当前脚本所在目录的绝对路径
current_dir = os.path.dirname(current_script_path)

# 先计算清洗一遍目前存量的数据，然后再进行后续动作
target_script_path = os.path.join(current_dir, '..', 'DataProcess', 'caculate_nav_allinone.py')
# 确保目标脚本路径是绝对路径
target_script_path = os.path.abspath(target_script_path)
# 使用 subprocess 模块执行目标脚本
try:
    result = subprocess.run(['python', target_script_path], check=True, capture_output=True, text=True)
    print("脚本输出:" + result.stdout)
except subprocess.CalledProcessError as e:
    print("脚本执行出错:" + e.stderr)


# 分别对应所有爬虫数据来源
list_plat = pd.read_sql_query("SELECT * FROM %s" % '所有产品爬虫信息表', engine)
list_self = pd.read_sql_query("SELECT * FROM %s" % '用户产品净值收集汇总表', engine)

# 添加平台产品的不同数据源
list_jinfuzi = list_plat[list_plat['产品网址'].str.contains('jfz')].reset_index(drop=True)
list_fof99 = list_plat[list_plat['产品网址'].str.contains('fof99')].reset_index(drop=True)
list_neibu = list_plat[list_plat['产品网址'].str.contains('内部')].reset_index(drop=True)
list_ifind = list_plat[list_plat['产品网址'].str.contains('同花顺')].reset_index(drop=True)
list_email = list_plat[list_plat['产品网址'].str.contains('邮箱')].reset_index(drop=True)

# 筛选 list_self 中的数据
list_self_fof99 = list_self[list_self['产品网址'].str.contains('fof99')].reset_index(drop=True)
list_self_ifind = list_self[list_self['产品网址'].str.contains('同花顺')].reset_index(drop=True)
list_self_email = list_self[list_self['产品网址'].str.contains('邮箱')].reset_index(drop=True)

# 3. 控制请求频率，添加随机延迟
def random_delay():
    delay = random.uniform(3, 6)
    time.sleep(delay)

# 创建 ChromeOptions 对象
chrome_options = Options()
chrome_options.add_argument('--headless')  
chrome_options.add_argument('--disable-gpu')  
chrome_options.add_argument('--no-sandbox')  
chrome_options.add_argument('--disable-infobars')  
chrome_options.add_argument('--incognito')  
chrome_options.add_argument('--hide-scrollbars')  
chrome_options.add_argument('--window-size=1280x1024')  
chrome_options.add_argument('--user-agent="Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36"')  # 设置请求头的 user-agent
# 创建 Chrome WebDriver，并传入 ChromeOptions
driver = webdriver.Chrome(options=chrome_options)
# 谷歌浏览器 79和79版本后防止被检测
driver.execute_cdp_cmd("Page.addScriptToEvaluateOnNewDocument", {
    "source": """
    Object.defineProperty(navigator, 'webdriver', {
        get: () => false
    });
    """
})

time.sleep(3)

# 金斧子爬虫
list = list_jinfuzi
driver.get('https://www.jfz.com/user/login') 
driver.find_element(By.XPATH,'//input[@type="text"]').send_keys('17521589405')      # 输入账号
driver.find_element(By.XPATH,'//input[@type="password"]').send_keys('930911Wwwcn')        # 输入密码
driver.find_element(By.XPATH,'//span[@class="el-checkbox__inner"]').click() # 合格投资者点击框
driver.find_element(By.XPATH,'//button[@class="el-button btn btn-primary el-button--default"]').click()   # 点击登录
# 确认网页跳转后再执行
random_delay()

# 遍历爬取净值数据 #
k = 0
logging.info('金斧子爬虫开始运行')
for list_all, list in [(list_plat, list_jinfuzi)]:
    for n, adr in enumerate(list['产品网址']):
        try:
            # 查询产品净值数据库表
            # 构建 SQL 查询，选择表的倒数第一行数据
            query = "SELECT 净值日期, 单位净值, 累计净值, 复权净值 FROM {} ORDER BY ROW_NUMBER() OVER() DESC LIMIT 1".format(list.loc[n, '产品名称'])
            # 使用 SQL 查询从数据库中读取倒数第一行数据
            list_chanpin_info = pd.read_sql_query(query, engine)

            if isinstance(list.loc[n, '最新净值日期'], str):
                list.loc[n, '最新净值日期'] = pd.to_datetime(list.loc[n, '最新净值日期'])
            if list_chanpin_info['净值日期'].iloc[0] >= time_last_Friday_time:
                list_all.loc[list_all['产品名称'] == list.loc[n, '产品名称'], '最新净值日期'] = list_chanpin_info['净值日期'].iloc[0]
                print('金斧子无需更新:' + '{:.2%}'.format((n+1)/len(list)))
                k = k + 1
                continue
        except:
            logging.info('新产品没有最新净值日期')

        driver.get(adr)
        time.sleep(2)
        # 数据库查询是否存在该表格
        cursor.execute("SHOW TABLES LIKE %s", (list.iloc[n, 0],))
        chanpin_judge = cursor.fetchone() == None
        # 判断是否为新加入产品
        if chanpin_judge:
            js = 'document.getElementsByClassName("left")[1].scrollTop=100000'
            driver.execute_script(js)
            time.sleep(2)
            ddate = driver.find_elements(By.XPATH,'//div[@class="table-networth-outer flex-shrink"]/div/div/table[@class="el-table__body"]/tbody/tr')
            while_count = 0  # 添加计数器
            while ddate == []:
                ddate = driver.find_elements(By.XPATH,'//div[@class="table-networth-outer flex-shrink"]/div/div/table[@class="el-table__body"]/tbody/tr')
                while_count += 1
                if while_count > 50:
                    logging.info(f"跳出循环：{list.iloc[n, 0]} 的 while 循环超过10次，检查是否网址失效")
                    break
            if while_count > 50:  # 检查计数器是否超过10次
                continue
            found_date = driver.find_elements(By.XPATH,"//span[contains(text(),'成立时间:')]/../span/span")[0].text
            list_chanpin_date = []
            list_chanpin_netvalue = []
            list_chanpin_netvalue_leiji = []
            list_chanpin_netvalue_cum = []
            # 检验所需净值序列的源代码是否全部载入
            s = 0
            while ddate[-1].text.split()[0] != found_date:
                ddate = driver.find_elements(By.XPATH,'//div[@class="table-networth-outer flex-shrink"]/div/div/table[@class="el-table__body"]/tbody/tr')
                s = s + 1
                if s > 50:
                    break
            update_date = datetime.strptime(found_date, '%Y-%m-%d') + timedelta(-1)
        else:
            ddate = driver.find_elements(By.XPATH,'//div[@class="table-networth-outer flex-shrink"]/div/div/table[@class="el-table__body"]/tbody/tr')
            while_count = 0  # 添加计数器
            while ddate == []:
                ddate = driver.find_elements(By.XPATH,'//div[@class="table-networth-outer flex-shrink"]/div/div/table[@class="el-table__body"]/tbody/tr')
                while_count += 1
                if while_count > 10:
                    logging.info(f"跳出循环：{list.iloc[n, 0]} 的 while 循环超过10次，检查是否网址失效")
                    break
            if while_count > 10:  # 检查计数器是否超过10次
                n += 1
                continue
            found_date = driver.find_elements(By.XPATH,"//span[contains(text(),'成立时间:')]/../span/span")[0].text

            list_chanpin_date = list_chanpin_info['净值日期'].tolist()
            list_chanpin_netvalue = list_chanpin_info['单位净值'].tolist()
            list_chanpin_netvalue_leiji = list_chanpin_info['累计净值'].tolist()
            list_chanpin_netvalue_cum = list_chanpin_info['复权净值'].tolist()
            update_date = list_chanpin_date[-1]
            # 检验所需净值序列的源代码是否全部载入
            while (datetime.strptime(ddate[-1].text.split()[0], '%Y-%m-%d') > update_date) == True:
                logging.info("检验载入源代码")
                ddate = driver.find_elements(By.XPATH,'//div[@class="table-networth-outer flex-shrink"]/div/div/table[@class="el-table__body"]/tbody/tr')
            if datetime.strptime(ddate[0].text.split()[0], '%Y-%m-%d') <= update_date:
                list_all.loc[list_all['产品名称'] == list.loc[n, '产品名称'], '最新净值日期'] = datetime.strptime(ddate[0].text.split()[0], '%Y-%m-%d')
                print(list.iloc[n, 0] + '：目标网址未更新净值')
                continue

        # 解析soup内净值数据 #
        for chanpin_info in ddate:
            chanpin_date = datetime.strptime(chanpin_info.text.split()[0], '%Y-%m-%d')
            if chanpin_date > update_date:        
                list_chanpin_date.append(chanpin_date)
                chanpin_netvalue = chanpin_info.text.split()[1]
                list_chanpin_netvalue.append(chanpin_netvalue)
                chanpin_netvalue_cum = chanpin_info.text.split()[2]
                list_chanpin_netvalue_cum.append(chanpin_netvalue_cum)
                # 金斧子没有累计净值
                list_chanpin_netvalue_leiji.append(np.nan)
        if chanpin_judge:
            try:            
                list_chanpin_date = [datetime.strptime(d, '%Y-%m-%d') for d in list_chanpin_date]
            except:
                list_chanpin_date = [d for d in list_chanpin_date]
        
        list_chanpin_info = pd.DataFrame({'净值日期':list_chanpin_date, '单位净值':list_chanpin_netvalue, '累计净值':list_chanpin_netvalue_leiji, '复权净值':list_chanpin_netvalue_cum})
        list_chanpin_info = list_chanpin_info.sort_values(by='净值日期', ascending=True)
        list_chanpin_info.drop_duplicates()
        # 回填爬虫总表数据
        list_all.loc[list_all['产品名称'] == list.loc[n, '产品名称'], '最新净值日期'] = list_chanpin_info.iloc[-1]['净值日期']
        list_all.loc[list_all['产品名称'] == list.loc[n, '产品名称'], '更新净值日期'] = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        # 删除list_chanpin_info的第一行数据
        list_chanpin_info = list_chanpin_info.iloc[1:]

    # 文件保存
        # 将数据插入到数据库表格的最后
        list_chanpin_info.to_sql(name=list.iloc[n, 0], con=engine, if_exists='append', index=False)
        try:
            if list_chanpin_info.iloc[-1, 0] >= time_last_Friday_time:
                k = k + 1
        except:
            continue
        print('金斧子爬虫进度' + '{:.2%}'.format((n+1)/len(list)))
        logging.info('金斧子:' + list.iloc[n, 0] + ':' + '{:.2%}'.format(k/(n+1)))

list_plat.to_sql(name='所有产品爬虫信息表', con=engine, if_exists='replace', index=False)


# 火富牛爬虫
list = list_fof99
driver.get('https://mp.fof99.com/user/login') 

time.sleep(2) # 等待3秒让页面完全加载

# 自动填充账号和密码并点击登录
driver.find_element(By.XPATH,'//input[@placeholder="请输入手机号"]').send_keys('13810753614')      # 输入账号
driver.find_element(By.XPATH,'//input[@type="password"]').send_keys('Lida1234')        # 输入密码
driver.find_element(By.XPATH,'//button[@class="ant-btn ant-btn-primary ant-btn-lg ant-btn-block login-btn"]').click()      # 点击登录

time.sleep(2) # 等待2秒让登陆完成
# 遍历爬取净值数据 #

logging.info('火富牛爬虫开始运行')
for list_all, list in [(list_plat, list_fof99), (list_self, list_self_fof99)]:
    k = 0
    for n, adr in enumerate(list['产品网址']):
        try:
            # 查询产品净值数据库表
            # 构建 SQL 查询，选择表的倒数第一行数据
            query = "SELECT 净值日期, 单位净值, 累计净值, 复权净值 FROM {} ORDER BY ROW_NUMBER() OVER() DESC LIMIT 1".format(list.loc[n, '产品名称'])
            # 使用 SQL 查询从数据库中读取倒数第一行数据
            list_chanpin_info = pd.read_sql_query(query, engine)

            if isinstance(list.loc[n, '最新净值日期'], str):
                list.loc[n, '最新净值日期'] = pd.to_datetime(list.loc[n, '最新净值日期'])
            if list_chanpin_info['净值日期'].iloc[0] >= time_last_Friday_time:
                list_all.loc[list_all['产品名称'] == list.loc[n, '产品名称'], '最新净值日期'] = list_chanpin_info['净值日期'].iloc[0]
                print('火富牛无需更新:' + '{:.2%}'.format((n+1)/len(list)))
                k = k + 1
                continue
        except:
            logging.info(list.loc[n, '产品名称'] + '新产品没有最新净值日期')

        driver.execute_cdp_cmd("Page.addScriptToEvaluateOnNewDocument", {  
            "source": """
            Object.defineProperty(navigator, 'webdriver', {
                get: () => undefined
            })
            """
        })
        driver.get(adr)
        # 数据库查询是否存在该表格
        cursor.execute("SHOW TABLES LIKE %s", (list.iloc[n, 0],))
        chanpin_judge = cursor.fetchone() == None
        time.sleep(4)
        # 判断是否为新加入产品
        if chanpin_judge:     # 新加入产品情形
            list_chanpin_date = []               # 净值日期
            list_chanpin_netvalue = []            # 单位净值
            list_chanpin_netvalue_leiji = []       # 累计净值
            list_chanpin_netvalue_cum = []         # 复权净值
    # =============================================================================
    #             num_js = driver.execute_script("return document.getElementsByClassName('virtual-content')[0].style.height;")//800 + 1    # 计算下拉滚动条循环次数    
    # =============================================================================
            virtual_content_element = driver.find_element(By.XPATH, '//div[@class="virtual-content"]/div')
            scroll_element = driver.find_element(By.XPATH, '//div[@class="virtual-content"]')
            original_style = virtual_content_element.get_attribute('style')
            # 使用正则表达式从style属性值中提取height属性值
            height_value_match = re.search(r'height:\s*([\d.]+)px', original_style)
            height_value = int(height_value_match.group(1)) if height_value_match else None
            num_js = height_value//1176 + 1    # 计算下拉滚动条循环次数    
            
            # 遍历并检索每次下拉后含净值信息的html结构，解析后填入对应list
            for i in range(int(num_js)):
                div_js = driver.find_elements(By.XPATH,'//div[@class="virtual-content"]/div/div/div[@class="flex flex-nowrap border-b-ebeef5 justify-around"]')
                for chanpin_info in div_js:
                    list_chanpin_date.append(chanpin_info.find_elements(By.TAG_NAME, 'div')[0].text)  
                    list_chanpin_netvalue.append(chanpin_info.find_elements(By.TAG_NAME, 'div')[1].find_elements(By.TAG_NAME, 'span')[0].text)
                    list_chanpin_netvalue_leiji.append(chanpin_info.find_elements(By.TAG_NAME, 'div')[2].find_elements(By.TAG_NAME, 'span')[0].text)
                    list_chanpin_netvalue_cum.append(chanpin_info.find_elements(By.TAG_NAME, 'div')[3].find_elements(By.TAG_NAME, 'span')[0].text)
    # =============================================================================
    #                 js = 'document.getElementsByClassName("virtual-scroller scroller")[0].scrollBy(0, 800)'
    #                 driver.execute_script(js)   # 执行下拉800单位的js拖动条操作
    # =============================================================================
                driver.execute_script("arguments[0].scrollBy(0, 1176);", scroll_element)
                time.sleep(0.2)
            list_chanpin_date = [datetime.strptime(d, '%Y-%m-%d') for d in list_chanpin_date]   # 修改日期格式
            judge_if_update = 1

        else:             # 老产品情形                     
            list_chanpin_date = list_chanpin_info['净值日期'].tolist()
            list_chanpin_netvalue = list_chanpin_info['单位净值'].tolist()
            list_chanpin_netvalue_leiji = list_chanpin_info['累计净值'].tolist()
            list_chanpin_netvalue_cum = list_chanpin_info['复权净值'].tolist()   # 载入原有净值信息
            update_date = datetime.strftime(list_chanpin_date[-1], '%Y-%m-%d')  # 载入最后的更新日期
            div_js = driver.find_elements(By.XPATH,'//div[@class="virtual-content"]/div/div/div[@class="flex flex-nowrap border-b-ebeef5 justify-around"]')
            judge_if_update = 0
            for chanpin_info in div_js:
                if chanpin_info.find_elements(By.TAG_NAME, 'div')[0].text > update_date:  # 判断是否未更新的净值日期
                    judge_if_update = judge_if_update + 1
                    list_chanpin_date.append(datetime.strptime(chanpin_info.find_elements(By.TAG_NAME, 'div')[0].text, '%Y-%m-%d'))  
                    list_chanpin_netvalue.append(chanpin_info.find_elements(By.TAG_NAME, 'div')[1].find_elements(By.TAG_NAME, 'span')[0].text)
                    list_chanpin_netvalue_leiji.append(chanpin_info.find_elements(By.TAG_NAME, 'div')[2].find_elements(By.TAG_NAME, 'span')[0].text)
                    list_chanpin_netvalue_cum.append(chanpin_info.find_elements(By.TAG_NAME, 'div')[3].find_elements(By.TAG_NAME, 'span')[0].text)
        list_chanpin_info = pd.DataFrame({'净值日期':list_chanpin_date, '单位净值':list_chanpin_netvalue, '累计净值':list_chanpin_netvalue_leiji, '复权净值':list_chanpin_netvalue_cum})
        list_chanpin_info = list_chanpin_info.drop_duplicates()
        list_chanpin_info = list_chanpin_info.sort_values(by='净值日期', ascending=True)
        # 回填总表数据
        list_all.loc[list_all['产品名称'] == list.loc[n, '产品名称'], '最新净值日期'] = list_chanpin_info.iloc[-1]['净值日期']
        list_all.loc[list_all['产品名称'] == list.loc[n, '产品名称'], '更新净值日期'] = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        if judge_if_update == 0:
            continue
        # 匹配业绩基准数据
        list_chanpin_info = BenchmarkIndex(list_chanpin_info, time_last_Friday_time, BenchmarkIndex_mapping[list.loc[n, '策略']])
        # 删除list_chanpin_info的第一行数据
        list_chanpin_info = list_chanpin_info.iloc[1:]
        
    # 文件保存 #
        # 将数据插入到数据库表格的最后
        list_chanpin_info.to_sql(name=list.iloc[n, 0], con=engine, if_exists='append', index=False)
        try:
            if list_chanpin_info.iloc[-1, 0] >= time_last_Friday_time:
                k = k + 1
        except:
            continue
        print('火富牛爬虫进度' + '{:.2%}'.format((n+1)/len(list)))
        logging.info('火富牛:' + list.iloc[n, 0] + ':' + '{:.2%}'.format(k/(n+1)))

driver.quit()

# 内部产品净值更新
# 连接到MySQL数据库
conn = mysql.connector.connect(
    user=user, password=password, host=host, port=port,
    database='衍盛产品净值数据库'
) 

list = list_neibu
n = 0
k = 0
for adr in list['产品网址']:
    Yansheng_name = list.loc[n, '产品名称']
    if Yansheng_name == '衍盛量化cta乌锦春1号':
        Yansheng_name = '衍盛量化cta乌锦春1号私募证券投资基金'
    if Yansheng_name == '衍盛深启空气指增':
        Yansheng_name = '衍盛深启空气指增私募证券投资基金'
    if Yansheng_name == '衍盛量化精选一期':
        Yansheng_name = '衍盛量化精选一期私募投资基金'
    if Yansheng_name == '铭量中证500增强1号':
        Yansheng_name = '衍盛铭量中证500增强1号私募证券投资基金'
    if Yansheng_name == '铭量对冲1号':
        Yansheng_name = '衍盛铭量对冲1号私募证券投资基金'
    if Yansheng_name == '衍盛指数增强1号':
        Yansheng_name = '衍盛指数增强1号私募投资基金'
    if Yansheng_name == '衍盛1000指数增强1号':
        Yansheng_name = '衍盛1000指数增强1号私募证券投资基金'
    if Yansheng_name == '衍盛铭量中证1000增强1号':
        Yansheng_name = '衍盛铭量中证1000增强1号私募证券投资基金'
    if Yansheng_name == '衍盛铭量智选1号':
        Yansheng_name = '衍盛铭量智选1号私募证券投资基金'
    if Yansheng_name == '衍盛铭量沪深300指数增强1号':
        Yansheng_name = '衍盛铭量沪深300指数增强1号证券私募投资基金'
    if Yansheng_name == '衍盛铭量铭泰1号':
        Yansheng_name = '衍盛铭量铭泰1号私募证券投资基金'

    cursor.execute("SHOW TABLES LIKE %s", (list.iloc[n, 0],))
    chanpin_judge = cursor.fetchone() == None
    
    # 判断是否为新加入产品
    if chanpin_judge: 
        list_chanpin_info = pd.read_sql("SELECT * FROM {}".format(Yansheng_name), conn)
        list_chanpin_info = list_chanpin_info[['净值日期', '单位净值', '累计净值', '复权净值']]
    else:
        # 构建 SQL 查询，选择表的倒数第一行数据
        query = "SELECT 净值日期, 单位净值, 累计净值, 复权净值 FROM {} ORDER BY ROW_NUMBER() OVER() DESC LIMIT 1".format(list.iloc[n, 0])
        # 使用 SQL 查询从数据库中读取倒数第一行数据
        list_chanpin_info = pd.read_sql_query(query, engine)
        list_chanpin_temp = pd.read_sql("SELECT * FROM {}".format(Yansheng_name), conn)[['净值日期', '单位净值', '累计净值', '复权净值']]
        list_chanpin_info = list_chanpin_temp.loc[list_chanpin_temp['净值日期'] > list_chanpin_info.iloc[0]['净值日期']]
    
    if list_chanpin_info.empty:
        n = n + 1
        continue
    # 匹配业绩基准数据
    list_chanpin_info = BenchmarkIndex(list_chanpin_info, time_last_Friday_time, BenchmarkIndex_mapping[list.loc[n, '策略']])
    
    # 回填总表数据
    list_plat.loc[list_plat['产品名称'] == list.loc[n, '产品名称'], '最新净值日期'] = list_chanpin_info.iloc[-1]['净值日期']
    list_plat.loc[list_plat['产品名称'] == list.loc[n, '产品名称'], '更新净值日期'] = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
    list_chanpin_info.to_sql(name=list.loc[n, '产品名称'], con=engine, if_exists='append', index=False)
    n = n + 1
cursor = cnx.cursor()


# 同花顺产品净值更新
# 获取access_token
getAccessTokenUrl = 'https://quantapi.51ifind.com/api/v1/get_access_token'
getAccessTokenHeader = {"Content-Type":"application/json","refresh_token":refreshToken}
getAccessTokenResponse = requests.post(url=getAccessTokenUrl, headers=getAccessTokenHeader)
accessToken = json.loads(getAccessTokenResponse.content)['data']['access_token']
thsHeaders = {"Content-Type":"application/json","access_token": accessToken}

for list_all, list_adr in [(list_plat, list_ifind), (list_self, list_self_ifind)]:
    for product_name, adr, type_strategy in zip(list_adr['产品名称'], list_adr['产品网址'], list_adr['策略']):
        product_ifind_num = adr.split(':')[1]
        try:
            # 构建 SQL 查询，选择表的倒数第一行数据
            query = "SELECT 净值日期, 单位净值, 累计净值, 复权净值 FROM {} ORDER BY ROW_NUMBER() OVER() DESC LIMIT 1".format(product_name)
            # 使用 SQL 查询从数据库中读取倒数第一行数据
            list_chanpin_info = pd.read_sql_query(query, engine)
            try:
                product_update_date = list_chanpin_info.iloc[-1]['净值日期']
                if product_update_date < time_last_Friday_time:
                    formatted_end_date = time_last_Friday_time.strftime("%Y%m%d")
                    formatted_start_date = (product_update_date+timedelta(1)).strftime("%Y%m%d")
                    thsUrl = 'https://quantapi.51ifind.com/api/v1/date_sequence'
                    # 处理资管计划产品
                    if product_ifind_num.find("SM") == -1:
                        # 查询最新净值日期
                        thsUrlDate = 'https://quantapi.51ifind.com/api/v1/basic_data_service'
                        para = {"codes":product_ifind_num,"indipara":[{"indicator":"ths_latestnetvaluedate_bcm"}]}
                        thsResponse = requests.post(url=thsUrlDate, json=para, headers=thsHeaders)
                        parsed_data = json.loads(thsResponse.content)
                        newest_date = datetime.strptime(parsed_data["tables"][0]['table']['ths_latestnetvaluedate_bcm'][0], '%Y%m%d')
                        if time_last_Friday_time > newest_date:
                            continue
                        para = {"codes":product_ifind_num,"startdate":formatted_start_date,"enddate":formatted_end_date,"functionpara":{"Fill":"Blank","Interval":"W"},"indipara":[{"indicator":"ths_latestnetvaluedate_bcm"},{"indicator":"ths_unit_nv_bcm","indiparams":[""]},{"indicator":"ths_accum_unit_nv_bcm","indiparams":[""]},{"indicator":"ths_adjustment_nv_bcm","indiparams":[""]}]}
                        thsResponse = requests.post(url=thsUrl, json=para, headers=thsHeaders)
                        parsed_data = json.loads(thsResponse.content)
                        product_date = parsed_data["tables"][0]['time']
                        ths_unit_nv_bcm = parsed_data["tables"][0]['table']["ths_unit_nv_bcm"]
                        ths_accum_unit_nv_bcm = parsed_data["tables"][0]['table']["ths_accum_unit_nv_bcm"]
                        ths_adjustment_nv_bcm = parsed_data["tables"][0]['table']["ths_adjustment_nv_bcm"]
                    # 处理私募基金产品
                    else:
                        # 查询最新净值日期
                        thsUrlDate = 'https://quantapi.51ifind.com/api/v1/basic_data_service'
                        para = {"codes":product_ifind_num,"indipara":[{"indicator":"ths_latest_nv_date_sp"}]}
                        thsResponse = requests.post(url=thsUrlDate, json=para, headers=thsHeaders)
                        parsed_data = json.loads(thsResponse.content)
                        newest_date = datetime.strptime(parsed_data["tables"][0]['table']['ths_latest_nv_date_sp'][0], '%Y%m%d')
                        if time_last_Friday_time > newest_date:
                            continue
                        para = {"codes":product_ifind_num,"startdate":formatted_start_date,"enddate":formatted_end_date,"functionpara":{"Interval":"W"},"indipara":[{"indicator":"ths_latest_nv_date_sp"},{"indicator":"ths_unit_nv_sp","indiparams":[""]},{"indicator":"ths_accum_unit_nv_sp","indiparams":[""]},{"indicator":"ths_adjustment_nv_sp","indiparams":[""]}]}
                        thsResponse = requests.post(url=thsUrl, json=para, headers=thsHeaders)
                        parsed_data = json.loads(thsResponse.content)
                        product_date = parsed_data["tables"][0]['time']
                        ths_unit_nv_bcm = parsed_data["tables"][0]['table']["ths_unit_nv_sp"]
                        ths_accum_unit_nv_bcm = parsed_data["tables"][0]['table']["ths_accum_unit_nv_sp"]
                        ths_adjustment_nv_bcm = parsed_data["tables"][0]['table']["ths_adjustment_nv_sp"]
                    
                    # 将数据转换为DataFrame
                    product_df = pd.DataFrame({
                        '净值日期': pd.to_datetime(product_date),
                        '单位净值': ths_unit_nv_bcm,
                        '累计净值': ths_accum_unit_nv_bcm,
                        '复权净值': ths_adjustment_nv_bcm
                    })
                    list_chanpin_info = pd.concat([list_chanpin_info[['净值日期', '单位净值', '累计净值', '复权净值']], product_df], ignore_index=True)
                    list_all.loc[list_all['产品名称'] == product_name, '更新净值日期'] = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
                    list_all.loc[list_all['产品名称'] == product_name, '最新净值日期'] = list_chanpin_info.iloc[-1]['净值日期']
                else:
                    continue
            except:
                logging.error(product_name+'同花顺净值更新出现bug')
        # 处理新产品
        except:
            try:
                formatted_end_date = time_last_Friday_time.strftime("%Y%m%d")
                thsUrl = 'https://quantapi.51ifind.com/api/v1/date_sequence'
                # 处理资管计划产品
                if product_ifind_num.find("SM") == -1:      
                    para = {"codes":product_ifind_num,"indipara":[{"indicator":"ths_established_date_bcm"}]}
                    thsResponse = requests.post(url='https://quantapi.51ifind.com/api/v1/basic_data_service', json=para, headers=thsHeaders)
                    parsed_data = json.loads(thsResponse.content)
                    formatted_start_date = parsed_data["tables"][0]['table']["ths_established_date_bcm"][0]
                    para = {"codes":product_ifind_num,"startdate":formatted_start_date,"enddate":formatted_end_date,"functionpara":{"Fill":"Blank","Interval":"W"},"indipara":[{"indicator":"ths_latestnetvaluedate_bcm"},{"indicator":"ths_unit_nv_bcm","indiparams":[""]},{"indicator":"ths_accum_unit_nv_bcm","indiparams":[""]},{"indicator":"ths_adjustment_nv_bcm","indiparams":[""]}]}
                    thsResponse = requests.post(url=thsUrl, json=para, headers=thsHeaders)
                    parsed_data = json.loads(thsResponse.content)
                    product_date = parsed_data["tables"][0]['time']
                    ths_unit_nv_bcm = parsed_data["tables"][0]['table']["ths_unit_nv_bcm"]
                    ths_accum_unit_nv_bcm = parsed_data["tables"][0]['table']["ths_accum_unit_nv_bcm"]
                    ths_adjustment_nv_bcm = parsed_data["tables"][0]['table']["ths_adjustment_nv_bcm"]
                # 处理私募基金产品
                else:
                    para = {"codes":product_ifind_num,"indipara":[{"indicator":"ths_established_date_sp"}]}
                    thsResponse = requests.post(url='https://quantapi.51ifind.com/api/v1/basic_data_service', json=para, headers=thsHeaders)
                    parsed_data = json.loads(thsResponse.content)
                    formatted_start_date = parsed_data["tables"][0]['table']["ths_established_date_sp"][0]
                    para = {"codes":product_ifind_num,"startdate":formatted_start_date,"enddate":formatted_end_date,"functionpara":{"Interval":"W"},"indipara":[{"indicator":"ths_latest_nv_date_sp"},{"indicator":"ths_unit_nv_sp","indiparams":[""]},{"indicator":"ths_accum_unit_nv_sp","indiparams":[""]},{"indicator":"ths_adjustment_nv_sp","indiparams":[""]}]}
                    thsResponse = requests.post(url=thsUrl, json=para, headers=thsHeaders)
                    parsed_data = json.loads(thsResponse.content)
                    product_date = parsed_data["tables"][0]['time']
                    ths_unit_nv_bcm = parsed_data["tables"][0]['table']["ths_unit_nv_sp"]
                    ths_accum_unit_nv_bcm = parsed_data["tables"][0]['table']["ths_accum_unit_nv_sp"]
                    ths_adjustment_nv_bcm = parsed_data["tables"][0]['table']["ths_adjustment_nv_sp"]
                
                # 将数据转换为DataFrame
                list_chanpin_info = pd.DataFrame({
                    '净值日期': pd.to_datetime(product_date),
                    '单位净值': ths_unit_nv_bcm,
                    '累计净值': ths_accum_unit_nv_bcm,
                    '复权净值': ths_adjustment_nv_bcm
                })
                list_all.loc[list_all['产品名称'] == product_name, '更新净值日期'] = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
                list_all.loc[list_all['产品名称'] == product_name, '最新净值日期'] = list_chanpin_info.iloc[-1]['净值日期']
            except:
                logging.error(product_name+'同花顺新产品净值更新出现bug')
        
        # 匹配业绩基准数据
        list_chanpin_info = BenchmarkIndex(list_chanpin_info, time_last_Friday_time, BenchmarkIndex_mapping[type_strategy])
        # 删除list_chanpin_info的第一行数据
        list_chanpin_info = list_chanpin_info.iloc[1:]    
        
        list_chanpin_info.to_sql(name=product_name, con=engine, if_exists='append', index=False)


def decode_mime_words(s):
    """Decode MIME encoded words to a human readable string."""
    if s is None:
        return ""
    decoded_string = ""
    for word, encoding in decode_header(s):
        if isinstance(word, bytes):
            decoded_string += word.decode(encoding or "utf-8")
        else:
            decoded_string += word
    return decoded_string

def sanitize_filename(filename):
    """Sanitize the filename by removing special characters."""
    return re.sub(r'[^A-Za-z0-9_.-]', '_', filename)

def login_to_email(host: str, username: str, password: str) -> imaplib.IMAP4_SSL:
    connection = imaplib.IMAP4_SSL(host)
    try:
        connection.login(username, password)
        # 解决网易邮箱报错：Unsafe Login. Please contact kefu@188.com for help
        imaplib.Commands["ID"] = ('AUTH',)
        args = ("name", host, "contact", host, "version", "1.0.0", "vendor", "myclient")
        connection._simple_command("ID", str(args).replace(",", "").replace("'", "\""))
        logging.info("Login successful")
    except imaplib.IMAP4.error as e:
        logging.error(f"Login failed: {e}")
        return None
    return connection

def list_folders(connection: imaplib.IMAP4_SSL):
    status, folders = connection.list()
    if status == 'OK':
        pass
    else:
        logging.error("Failed to list folders")

def list_emails(connection: imaplib.IMAP4_SSL, folder: str, unseen_only: bool = True) -> list:
    criteria = 'UNSEEN' if unseen_only else 'ALL'
    try:
        # 选择邮件文件夹，imaplib会自动处理非ASCII字符
        status, response = connection.select(mailbox=folder)
        
        if status != 'OK':
            logging.error(f"Failed to select folder: {folder}, {response}")
            return []
        else:
            logging.info(f"Successfully selected folder: {folder}")

        # 搜索邮件，这里搜索所有邮件
        status, response = connection.search(None, criteria)
        if status == 'OK':
            return response[0].split()
    except imaplib.IMAP4.error as e:
        logging.error(f"Select or search failed: {e}")
    return []

def fetch_email_info(connection: imaplib.IMAP4_SSL, mail_id: str, mark_as_read: bool = False) -> dict:
    status, data = connection.fetch(mail_id, '(BODY.PEEK[])')
    if status == 'OK':
        raw_email = data[0][1]  # 取第一个邮件的内容
        msg = email.message_from_bytes(raw_email)  # 解析邮件内容
        
        email_info = {
            'Message-ID': msg.get('Message-ID'),
            'From': decode_mime_words(msg.get('From')),
            'To': decode_mime_words(msg.get('To')),
            'Subject': decode_mime_words(msg.get('Subject')),
            'Date': msg.get('Date'),
            'Body': '',
            'Attachments': []
        }
        
        if msg.is_multipart():
            for part in msg.walk():
                content_disposition = part.get('Content-Disposition', None)
                if content_disposition and 'attachment' in content_disposition:
                    filename = part.get_filename()
                    if filename:
                        decoded_filename = decode_mime_words(filename)
                        sanitized_filename = sanitize_filename(decoded_filename)
                        filepath = os.path.join(tempfile.gettempdir(), sanitized_filename)
                        with open(filepath, 'wb') as f:
                            f.write(part.get_payload(decode=True))
                        email_info['Attachments'].append(filepath)
                elif part.get_content_type() == 'text/plain':
                    email_info['Body'] += part.get_payload(decode=True).decode(part.get_content_charset() or 'utf-8')
                elif part.get_content_type() == 'text/html':
                    email_info['Body'] += part.get_payload(decode=True).decode(part.get_content_charset() or 'utf-8')
        else:
            email_info['Body'] += msg.get_payload(decode=True).decode(msg.get_content_charset() or 'utf-8')
        
        # 如果需要将邮件标记为已读，则执行以下操作
        if mark_as_read:
            connection.store(mail_id, '+FLAGS', '\\Seen')
        
        return email_info
    return None

if __name__ == '__main__':
    imap_host = 'imap.163.com'
    username = 'xwab_fund_nav@163.com'
    password = 'FUHHXADODOSSSFIC'  # 替换为你的实际密码或授权码
    
    connection = login_to_email(imap_host, username, password)
    if connection:
        list_folders(connection)  # 列出所有文件夹
        folder = 'INBOX'  # 尝试使用大写的'INBOX'
        mail_ids = list_emails(connection, folder)
        email_list = []  # 用于存储所有邮件信息的列表
        
        if mail_ids:  # 只有在成功选择文件夹后才处理邮件
            for mail_id in mail_ids:
                email_info = fetch_email_info(connection, mail_id)
                if email_info:
                    email_list.append(email_info)
        
        # 将所有邮件信息转换为 DataFrame
        df_unread_emails = pd.DataFrame(email_list)
        df_unread_emails['mail_id'] = mail_ids
        
        if not df_unread_emails.empty:
            for list_all, list_email in [(list_plat, list_email), (list_self, list_self_email)]:
                for n, chanpin_name in enumerate(list_email['产品名称']):
                    if isinstance(list_email.loc[n, '最新净值日期'], str):
                        list_email.loc[n, '最新净值日期'] = pd.to_datetime(list_email.loc[n, '最新净值日期'])
                    if list_email.loc[n, '最新净值日期'] >= pd.to_datetime(time_last_Friday_time):
                        print(chanpin_name + '邮件无需更新：' + '{:.2%}'.format((n+1)/len(list_email)))
                        continue
                    
                    # 构建 SQL 查询，选择表的倒数第一行数据
                    query = f"""
                    SELECT 净值日期, 单位净值, 累计净值, 复权净值
                    FROM {chanpin_name}
                    ORDER BY ROW_NUMBER() OVER() DESC
                    LIMIT 1
                    """
                    # 使用 SQL 查询从数据库中读取倒数第一行数据
                    list_chanpin_info = pd.read_sql_query(query, engine)
                    list_chanpin_info = list_chanpin_info.loc[list_chanpin_info['净值日期'] > list_chanpin_info.iloc[0]['净值日期']]
                    list_chanpin_date = []
                    list_chanpin_netvalue = []
                    list_chanpin_netvalue_leiji = []
                    # 检查收件箱中包含 chanpin_name 的邮件
                    for idx, email_info in df_unread_emails.iterrows():
                        if chanpin_name in email_info['Subject']:
                            mail_id = email_info['mail_id']
                            from_address = email_info['From']
                            body = email_info['Body']
                            # 招商托管
                            if 'yywbfa@cmschina.com.cn' in from_address:
                                # 提取邮件正文中的表格
                                tables = pd.read_html(body)
                                found = False  # 标志变量，用于控制是否找到包含“净值”的列或行                      
                                for table in tables:
                                    # 将列名转换为字符串类型
                                    str_columns = [str(col) for col in table.columns]
                                    if any('净值' in col for col in str_columns):
                                        found = True
                                        break  # 跳出当前表格的循环
                                    else:
                                        # 检查正文中的每一行
                                        for row in table.itertuples(index=False):
                                            if any('净值' in str(value) for value in row):
                                                found = True
                                                break  # 跳出当前行的循环
                                        if found:
                                            break  # 如果找到包含“净值”的行，跳出整个大循环
                                df_excel = table
                                df_excel['日期'] = pd.to_datetime(df_excel['日期'], format='%Y年%m月%d日')
                                # 查找包含特定字段的列并追加到 list_chanpin_info 中
                                for column in df_excel.columns:
                                    if '日期' in column:
                                        if pd.to_datetime(df_excel[column][0]) <= list_email.loc[n, '最新净值日期']:
                                            break
                                        list_chanpin_date.append(df_excel[column][0])
                                    elif '单位净值' == column or '资产份额净值' in column or '（NAV/Share）' in column:
                                        list_chanpin_netvalue.append(df_excel[column][0])
                                    elif '累计' in column:
                                        list_chanpin_netvalue_leiji.append(df_excel[column][0])
                                        break
                            # 海通托管
                            elif 'jjtgb_tzjd2@haitong.com' in from_address:
                                # 提取邮件正文中的表格
                                tables = pd.read_html(body)
                                # 检查表格中是否包含所需信息
                                table = tables[0]
                                if '净值日期' in table.columns and '单位净值' in table.columns and '累计净值' in table.columns:
                                    # 提取信息
                                    for index, row in table.iterrows():
                                        date = datetime.strptime(row['净值日期'], '%Y-%m-%d')
                                        if date <= list_email.loc[n, '最新净值日期']:
                                            break
                                        unit_nav = row['单位净值']
                                        cumulative_unit_nav = row['累计净值']
                                        # 追加到列表中
                                        list_chanpin_date.append(date)
                                        list_chanpin_netvalue.append(unit_nav)
                                        list_chanpin_netvalue_leiji.append(cumulative_unit_nav)

                            else:
                                for attachment in email_info['Attachments']:
                                    if attachment.endswith('.xls') or attachment.endswith('.xlsx'):
                                        df_excel = pd.read_excel(attachment)
                                        # 查找包含特定字段的列并追加到 list_chanpin_info 中
                                        for column in df_excel.columns:
                                            if '日期' in column:
                                                if pd.to_datetime(df_excel[column][0]) <= list_email.loc[n, '最新净值日期']:
                                                    break
                                                list_chanpin_date.append(df_excel[column][0])
                                            elif '单位净值' == column or '资产份额净值' in column or '（NAV/Share）' in column:
                                                list_chanpin_netvalue.append(df_excel[column][0])
                                            elif '累计' in column:
                                                list_chanpin_netvalue_leiji.append(df_excel[column][0])
                                                break
                            # 将邮件标记为已读
                            fetch_email_info(connection, mail_id, mark_as_read=True)
                    if list_chanpin_date == []:
                        print(chanpin_name + '净值未更新')
                        continue
                    list_chanpin_info = pd.DataFrame({'净值日期': list_chanpin_date, '单位净值': list_chanpin_netvalue, '累计净值': list_chanpin_netvalue_leiji})
                    list_chanpin_info = list_chanpin_info.sort_values(by='净值日期', ascending=True)
                    list_chanpin_info.drop_duplicates()
                    # 回填爬虫总表数据
                    list_all.loc[list_all['产品名称'] == list_email.loc[n, '产品名称'], '最新净值日期'] = list_chanpin_info.iloc[-1]['净值日期']
                    # 文件保存
                        # 将数据插入到数据库表格的最后
                    list_chanpin_info.to_sql(name=list_email.iloc[n, 0], con=engine, if_exists='append', index=False)
                    # 统一计算复权净值
                    list_chanpin_info = pd.read_sql_query("SELECT * FROM %s" % chanpin_name, engine)
                    list_chanpin_info['复权净值'] = Calculate_Reinvested_Nav(list_chanpin_info, '单位净值', '累计净值')
                    list_chanpin_info.to_sql(name=chanpin_name, con=engine, if_exists='replace', index=False)
                    list_all.loc[list_all['产品名称'] == chanpin_name, '更新净值日期'] = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
                    list_all.loc[list_all['产品名称'] == chanpin_name, '最新净值日期'] = list_chanpin_info.iloc[-1]['净值日期']
                    
                    logging.info(chanpin_name + '产品净值更新')
        
        # 关闭连接前确保已经选择了文件夹
        if connection.state == 'SELECTED':
            connection.close()
        connection.logout()
    else:
        logging.error("Unable to log in to the email, please check your username and password.")

list_plat.to_sql(name='所有产品爬虫信息表', con=engine, if_exists='replace', index=False)
list_self.to_sql(name='用户产品净值收集汇总表', con=engine, if_exists='replace', index=False)


# 关闭游标
cursor.close()
# 关闭连接
cnx.close()

# # 排排网爬虫
# target_script_path = os.path.join(current_dir, '..', 'WebCrawler', 'paipai_crawlar.py')
# # 确保目标脚本路径是绝对路径
# target_script_path = os.path.abspath(target_script_path)
# # 使用 subprocess 模块执行目标脚本
# try:
#     result = subprocess.run(['python', target_script_path], check=True, capture_output=True, text=True)
#     print("排排网脚本输出:" + result.stdout)
# except subprocess.CalledProcessError as e:
#     print("脚本执行出错:" + e.stderr)

# 计算基金收益率脚本
target_script_path = os.path.join(current_dir, '..', 'DataProcess', 'caculate_nav_allinone.py')
# 确保目标脚本路径是绝对路径
target_script_path = os.path.abspath(target_script_path)
# 使用 subprocess 模块执行目标脚本
try:
    result = subprocess.run(['python', target_script_path], check=True, capture_output=True, text=True)
    print("基金收益计算脚本输出:" + result.stdout)
except subprocess.CalledProcessError as e:
    print("脚本执行出错:" + e.stderr)

# 计算策略指数脚本
target_script_path = os.path.join(current_dir, '..', 'DataProcess', 'caculate_chanpin.py')
# 确保目标脚本路径是绝对路径
target_script_path = os.path.abspath(target_script_path)
# 使用 subprocess 模块执行目标脚本
try:
    result = subprocess.run(['python', target_script_path], check=True, capture_output=True, text=True)
    print("策略指数计算脚本输出:" + result.stdout)
except subprocess.CalledProcessError as e:
    print("脚本执行出错:" + e.stderr)

# 生成基金周报脚本
target_script_path = os.path.join(current_dir, '..', 'FileGeneration', 'Tongye_weeklyReport.py')
# 确保目标脚本路径是绝对路径
target_script_path = os.path.abspath(target_script_path)
# 使用 subprocess 模块执行目标脚本
try:
    result = subprocess.run(['python', target_script_path], check=True, capture_output=True, text=True)
    print("产品周报脚本输出:" + result.stdout)
except subprocess.CalledProcessError as e:
    print("脚本执行出错:" + e.stderr)

logging.info('运算脚本执行完成')






# if_skip_geshang = True
# # 是否跳过格上爬虫
# if if_backup_adr == False and if_list_geshang_all_update == False and if_skip_geshang == False:
#     # 格上财富爬虫
#     list = list_geshang
#     driver.get('https://www.licai.com/')
#     time.sleep(2)
    
#     try:
#         driver.find_element(By.XPATH, '//input[@id="checkbox3" and @type="checkbox"]').click()   # 点击合格投资者
#         driver.find_element(By.XPATH, '//button[@class="botton-big"]').click()   # 点击合格投资者确认
#     except:
#         pass
#     time.sleep(2)

#     try:
#         wait = WebDriverWait(driver, 10)
#         element = wait.until(EC.element_to_be_clickable((By.XPATH, '//a[contains(text(), "登录 注册")]')))
#         element.click()
#     except:
#         pass
#     time.sleep(2)

#     driver.find_element(By.XPATH,'//span[contains(text(), "账号密码登录")]').click() # 账号密码登录
#     driver.find_element(By.XPATH,'//input[@placeholder="请输入6-20位字符密码"]').send_keys('930911Wwwcn')        # 输入密码
#     time.sleep(3)
    
#     # 假设 driver 是您已经初始化的 WebDriver 实例
#     # 使用 By 类来定位元素
#     elements = driver.find_elements(By.XPATH, '//input[@placeholder="请输入您的手机号"]')
    
#     # 检查是否存在第二个元素
#     if len(elements) > 1:
#         # 第二个元素的索引是1
#         elements[1].send_keys('13121023419')
#     else:
#         logging.info("没有找到第二个匹配的元素。")
#     driver.find_element(By.XPATH, '//div[@class="loginBtn" and @data-v-3d295074=""]').click()   # 点击登录
#     # 确认网页跳转后再执行
#     random_delay()
    
#     logging.info('格上爬虫开始运行')
#     # 遍历爬取净值数据 #
#     n = 0
#     k = 0
#     for adr in list['产品网址']:
#         try:
#             if isinstance(list.loc[n, '最新净值日期'], str):
#                 list.loc[n, '最新净值日期'] = pd.to_datetime(list.loc[n, '最新净值日期'])
#             if list.loc[n, '最新净值日期'] >= time_last_Friday_time:
#                 print('格上无需更新:' + '{:.2%}'.format((n+1)/len(list)))
#                 k = k + 1
#                 n = n + 1
#                 continue
#         except:
#             logging.info('新产品没有最新净值日期')

#         driver.get(adr)
#         random_delay()
#         try:
#             driver.find_element(By.XPATH, '//h2[contains(text(), "历史净值")]').click()
#         except:
#             logging.info(list.iloc[n, 0] + '净值链接已失效')
#             n = n + 1
#             continue
#         time.sleep(3)
#         # 数据库查询是否存在该表格
#         cursor.execute("SHOW TABLES LIKE %s", (list.iloc[n, 0],))
#         chanpin_judge = cursor.fetchone() == None
#         # 判断是否为新加入产品
#         if chanpin_judge:
#             # 设置滚动的初始位置
#             found_date = driver.find_elements(By.XPATH, '//div[@class="table-msg-title" and text()="成立日期："]/following-sibling::div//span')[0].text
#             start_time = time.time()  # 记录开始时间
#             while True:
#                 # 使用JavaScript滚动到当前位置的下一部分
#                 js = 'document.getElementsByClassName("tbody-wrapper")[0].scrollBy(0, 1000000000);'
#                 driver.execute_script(js)
#                 # 模拟等待页面加载
#                 time.sleep(0.1)
#                 ddate = driver.find_elements(By.XPATH, '//div[@class="detail-block"][1]/div/div/div/div[@class="table-core-container"]/div/div/table[@class="table table-fixed-height"]/tbody/tr')
#                 while ddate == []:
#                     ddate = driver.find_elements(By.XPATH, '//div[@class="detail-block"][1]/div/div/div/div[@class="table-core-container"]/div/div/table[@class="table table-fixed-height"]/tbody/tr')
#                 if found_date == ddate[-1].text.split()[0]:
#                     break
#                 current_time = time.time()
#                 elapsed_time = current_time - start_time
#                 if elapsed_time > 90:  # 如果运行时间超过60秒
#                     logging.info((list.iloc[n, 0] + "已经运行超过90秒"))
#                     break
#             time.sleep(1)
#             list_chanpin_date = []
#             list_chanpin_netvalue = []
#             list_chanpin_netvalue_leiji = []
#             list_chanpin_netvalue_cum = []
#             update_date = datetime.strptime(found_date, '%Y-%m-%d') + timedelta(-1)
#         else:
#             # 构建 SQL 查询，选择表的倒数第一行数据
#             query = "SELECT 净值日期, 单位净值, 累计净值, 复权净值 FROM {} ORDER BY ROW_NUMBER() OVER() DESC LIMIT 1".format(list.iloc[n, 0])
#             # 使用 SQL 查询从数据库中读取倒数第一行数据
#             list_chanpin_info = pd.read_sql_query(query, engine)
#             list_chanpin_date = list_chanpin_info['净值日期'].tolist()
#             list_chanpin_netvalue = list_chanpin_info['单位净值'].tolist()
#             list_chanpin_netvalue_leiji = list_chanpin_info['累计净值'].tolist()
#             list_chanpin_netvalue_cum = list_chanpin_info['复权净值'].tolist()
#             update_date = list_chanpin_date[-1]
            
#             ddate = driver.find_elements(By.XPATH, '//div[@class="detail-block"][1]/div/div/div/div[@class="table-core-container"]/div/div/table[@class="table table-fixed-height"]/tbody/tr')
#             # 检验所需净值序列的源代码是否全部载入
#             while (datetime.strptime(ddate[-1].text.split()[0], '%Y-%m-%d') > update_date) == True:
#                 logging.info(list.iloc[n, 0] + "检验载入源代码")
#                 ddate = driver.find_elements(By.XPATH, '//div[@class="detail-block"][1]/div/div/div/div[@class="table-core-container"]/div/div/table[@class="table table-fixed-height"]/tbody/tr')
#             if datetime.strptime(ddate[0].text.split()[0], '%Y-%m-%d') <= update_date:
#                 list_all.loc[list_all['产品名称'] == list.loc[n, '产品名称'], '最新净值日期'] = datetime.strptime(ddate[0].text.split()[0], '%Y-%m-%d')
#                 print(list.iloc[n, 0] + '：目标网址未更新净值')
#                 n = n + 1
#                 continue
    
#         # 解析soup内净值数据 #
#         for chanpin_info in ddate:
#             chanpin_date = datetime.strptime(chanpin_info.text.split()[0], '%Y-%m-%d')
#             if chanpin_date > update_date:        
#                 list_chanpin_date.append(chanpin_date)
#                 chanpin_netvalue = chanpin_info.text.split()[1]
#                 list_chanpin_netvalue.append(chanpin_netvalue)
#                 chanpin_netvalue_cum = chanpin_info.text.split()[2]
#                 list_chanpin_netvalue_cum.append(chanpin_netvalue_cum)    
#                 chanpin_netvalue_leiji = chanpin_info.text.split()[3]
#                 list_chanpin_netvalue_leiji.append(chanpin_netvalue_leiji) 
#         if chanpin_judge:
#             try:            
#                 list_chanpin_date = [datetime.strptime(d, '%Y-%m-%d') for d in list_chanpin_date]
#             except:
#                 list_chanpin_date = [d for d in list_chanpin_date]
        
#         list_chanpin_info = pd.DataFrame({'净值日期':list_chanpin_date, '单位净值':list_chanpin_netvalue, '累计净值':list_chanpin_netvalue_leiji, '复权净值':list_chanpin_netvalue_cum})
#         list_chanpin_info = list_chanpin_info.sort_values(by='净值日期', ascending=True)
#         list_chanpin_info.drop_duplicates()
#         # 回填爬虫总表数据
#         list_all.loc[list_all['产品名称'] == list.loc[n, '产品名称'], '最新净值日期'] = list_chanpin_info.iloc[-1]['净值日期']
#         # 匹配业绩基准数据
#         list_chanpin_info = BenchmarkIndex(list_chanpin_info, time_last_Friday_time, BenchmarkIndex_mapping[list.loc[n, '策略']])
#         # 删除list_chanpin_info的第一行数据
#         list_chanpin_info = list_chanpin_info.iloc[1:]    
    
#     # 文件保存
#         # 将数据插入到数据库表格的最后
#         list_chanpin_info.to_sql(name=list.iloc[n, 0], con=engine, if_exists='append', index=False)
#         list_all.loc[list_all['产品网址'] == adr, '是否刷新净值'] = '是'
#         try:
#             if list_chanpin_info.iloc[-1, 0] >= time_last_Friday_time:
#                 k = k + 1
#         except:
#             n = n + 1
#             continue
#         print('格上爬虫进度' + '{:.2%}'.format((n+1)/len(list)))
#         logging.info('格上:' + list.iloc[n, 0] + ':' + '{:.2%}'.format(k/(n+1)))
#         n = n + 1
    
#     list_all.to_sql(name='所有产品爬虫信息表', con=engine, if_exists='replace', index=False)
