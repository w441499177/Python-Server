import sys
sys.path.append('/home/ubuntu/market/dc-66-python-2.0/SelfModule')

from selenium import webdriver
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.chrome.options import Options
import mysql.connector
from tradedate import *  # 自定义封装函数
from sqlalchemy import create_engine
from lxml import etree
import warnings
import akshare as ak
import matplotlib.pyplot as plt
from bs4 import BeautifulSoup
from selenium.webdriver.common.by import By
import numpy as np
import pandas as pd
from datetime import datetime, date, timedelta
import matplotlib.ticker as mtick
import matplotlib.dates as mdates
from matplotlib.pyplot import MultipleLocator
from tradedate import *  # 自定义封装函数
from selfdefinfunc import Locator_Len, Search_NavDate # 自定义封装函数
from chinese_calendar import is_workday
import time
import os
import re
from openpyxl import load_workbook, Workbook
import logging

plt.rcParams['font.sans-serif'] = ['SimHei']  # 用来正常显示中文标签
plt.rcParams['axes.unicode_minus'] = False  # 用来正常显示负号
warnings.filterwarnings('ignore')

# 设置日志
logging.basicConfig(filename='/home/ubuntu/market/dc-66-python-2.0/execution.log', 
                    level=logging.INFO, 
                    format='%(asctime)s - %(levelname)s - %(message)s', 
                    filemode='a')  # 设置为追加模式

# 环境变量读取数据库秘钥信息
host = os.environ.get('MYSQL_HOST')   
user = os.environ.get('MYSQL_USERNAME')  
password = os.environ.get('MYSQL_PASSWORD')
port = os.environ.get('MYSQL_PORT')

# 数据库存储操作
engine = create_engine(f'mysql+mysqlconnector://{user}:{password}@{host}:{port}/衍盛客户持仓数据库')
list_shenshu_info = pd.read_sql_query("SELECT * FROM %s" % '历史申赎汇总表', engine)

# 创建 ChromeOptions 对象
chrome_options = Options()
chrome_options.add_argument('--headless')
chrome_options.add_argument('--disable-gpu')
# 创建 Chrome WebDriver，并传入 ChromeOptions
driver = webdriver.Chrome(options=chrome_options) 
driver.get('http://172.16.12.99/#/login') 
time.sleep(2) # 等待让页面完全加载

# 自动填充账号和密码并点击登录
driver.find_element(By.XPATH,'//input[@id="name"]').send_keys('wanghui')      # 输入账号
driver.find_element(By.XPATH,'//input[@type="password"]').send_keys('wanghui')        # 输入密码
driver.find_element(By.XPATH,'//button[@class="el-button el-button--primary"]').click()      # 点击登录
time.sleep(5) # 等待让页面完全加载

# 导航到产品基础信息页面
    # 获取产品名称对照表
driver.find_element(By.XPATH,"//div[@class='el-submenu__title']//span[text()='基金管理']").click()
time.sleep(1) # 等待让页面完全加载
    # 定位目标元素
element = driver.find_element(By.XPATH,"//div[@class='submenu']//span[text()='基金信息管理']")  
time.sleep(1) # 等待让页面完全加载
    # 创建ActionChains对象
actions = ActionChains(driver)
    # 将鼠标悬停在目标元素上
actions.move_to_element(element).perform()
driver.find_element(By.XPATH,"//li[contains(text(), '基金申赎记录')]").click()

# =============================================================================
# # 获取产品名称对照表
# shenshu_date = []     # 申购日期
# chanpin_code = []     # 备案号
# chanpin_name = []     # 申赎份额名称
# investor_name = []     # 投资人
# shenshu_type = []     # 申购类型
# chanpin_nav = []     # 基金净值
# chanpin_jine = []     # 确认金额 
# chanpin_fene = []     # 确认份额
# =============================================================================
a = 0
page_number = 2  # 初始化页数为1
while a == 0:
    time.sleep(5)
    elements = driver.find_elements(By.XPATH,'//div/table/tbody/tr[@class="el-table__row"]')
# =============================================================================
#     for element in elements:
#         # 申购日期
#         shenshu_date.append(element.find_elements(By.XPATH, './/td[contains(@class, "el-table")]')[0].text)
#         # 备案号
#         chanpin_code.append(element.find_elements(By.XPATH, './/td[contains(@class, "el-table")]')[1].text)
#         # 申赎份额名称
#         chanpin_name.append(element.find_elements(By.XPATH, './/td[contains(@class, "el-table")]')[2].text)
#         # 投资人
#         investor_name.append(element.find_elements(By.XPATH, './/td[contains(@class, "el-table")]')[3].text)
#         # 申购类型
#         shenshu_type.append(element.find_elements(By.XPATH, './/td[contains(@class, "el-table")]')[4].text)
#         # 基金净值
#         chanpin_nav.append(element.find_elements(By.XPATH, './/td[contains(@class, "el-table")]')[6].text)
#         # 确认金额
#         chanpin_jine.append(element.find_elements(By.XPATH, './/td[contains(@class, "el-table")]')[8].text)
#         # 确认份额
#         chanpin_fene.append(element.find_elements(By.XPATH, './/td[contains(@class, "el-table")]')[9].text)
# =============================================================================
    for element in elements:
        # 获取当前行的8个字段值
        row_data = {
            '申购日期': element.find_elements(By.XPATH, './/td[contains(@class, "el-table")]')[0].text,
            '备案号': element.find_elements(By.XPATH, './/td[contains(@class, "el-table")]')[1].text,
            '申赎份额名称': element.find_elements(By.XPATH, './/td[contains(@class, "el-table")]')[2].text,
            '投资人': element.find_elements(By.XPATH, './/td[contains(@class, "el-table")]')[3].text,
            '申购类型': element.find_elements(By.XPATH, './/td[contains(@class, "el-table")]')[4].text,
            '基金净值': element.find_elements(By.XPATH, './/td[contains(@class, "el-table")]')[6].text,
            '确认金额': element.find_elements(By.XPATH, './/td[contains(@class, "el-table")]')[8].text,
            '确认份额': element.find_elements(By.XPATH, './/td[contains(@class, "el-table")]')[9].text
        }
        new_data = pd.DataFrame(row_data, index=[0])     # 将字典转换为DataFrame
        # 检查是否与已插入数据重复
        duplicate = False
        for index, existing_row in list_shenshu_info.iterrows():
            if existing_row.equals(new_data.loc[0]):
                duplicate = True
                logging.info('已更新到最新申赎数据')
                b = 1
                a = 1
                break
        if duplicate:
            # If it's a duplicate, break the loop
            break
        else:
            # If not a duplicate, add it to the DataFrame
            list_shenshu_info = pd.concat([list_shenshu_info, new_data], ignore_index=True)
    b = 0
    while b == 0:
        try:
            page_next = driver.find_element(By.XPATH,'//button[@class="btn-next"]')
            page_judge = page_next.get_attribute('disabled')
            b = 1
        except:
            page_next = driver.find_element(By.XPATH,'//button[@class="btn-next"]')
            page_judge = page_next.get_attribute('disabled')
    if page_judge == 'true':
        a = 1
    else:
        if a == 1:
            continue
        time.sleep(1)
        driver.find_element(By.XPATH,'//button[@class="btn-next"]').click()
        logging.info(f'翻至第{page_number}页')
        page_number += 1  # 递增页数
    time.sleep(1)

# =============================================================================
# list_shenshu_info = pd.DataFrame({'申购日期':shenshu_date, '备案号':chanpin_code, '申赎份额名称':chanpin_name, '投资人':investor_name,
#                                   '申购类型':shenshu_type, '基金净值':chanpin_nav, '确认金额':chanpin_jine, '确认份额':chanpin_fene})
# =============================================================================
# 按照申购日期降序排列
list_shenshu_info = list_shenshu_info.sort_values(by='申购日期', ascending=False)
# 重置索引
list_shenshu_info = list_shenshu_info.reset_index(drop=True)
list_shenshu_info.to_sql(name = '历史申赎汇总表', con=engine, if_exists='replace', index=False)

driver.quit()