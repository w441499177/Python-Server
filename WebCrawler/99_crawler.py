import sys
sys.path.append('/home/ubuntu/market/dc-66-python-2.0/SelfModule')

from selenium import webdriver
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.chrome.options import Options
import mysql.connector
from tradedate import *  # 自定义封装函数
from sqlalchemy import create_engine
from lxml import etree
import warnings
import akshare as ak
import matplotlib.pyplot as plt
from bs4 import BeautifulSoup
from selenium.webdriver.common.by import By
import numpy as np
import pandas as pd
from datetime import datetime, date, timedelta
import matplotlib.ticker as mtick
import matplotlib.dates as mdates
from matplotlib.pyplot import MultipleLocator
from selfdefinfunc import Locator_Len, Search_NavDate, check_a_share_trading_day # 自定义封装函数
from downloadindex import data_CSI300, data_CSI500, data_CSI1000, data_CSIA500, data_GZ2000  # 自定义封装函数
from chinese_calendar import is_workday
import time
import os
import re
from openpyxl import load_workbook, Workbook
import requests
import logging

# 设置日志
logging.basicConfig(filename='/home/ubuntu/market/dc-66-python-2.0/execution.log', 
                    level=logging.INFO, 
                    format='%(asctime)s - %(levelname)s - %(message)s', 
                    filemode='a')  # 设置为追加模式

plt.rcParams['font.sans-serif'] = ['SimHei']  # 用来正常显示中文标签
plt.rcParams['axes.unicode_minus'] = False  # 用来正常显示负号
warnings.filterwarnings('ignore')

# 检查是否非交易日
check_a_share_trading_day()

# 环境变量读取数据库秘钥信息
host = os.environ.get('MYSQL_HOST')   
user = os.environ.get('MYSQL_USERNAME')  
password = os.environ.get('MYSQL_PASSWORD')
port = os.environ.get('MYSQL_PORT')

# 数据库存储操作
cnx = mysql.connector.connect(user=user, password=password,
                              host=host,  port=port, database='衍盛产品净值数据库')
engine = create_engine(f'mysql+mysqlconnector://{user}:{password}@{host}:{port}/衍盛产品净值数据库')
cursor = cnx.cursor()

# 获取当前日期时间
current_datetime = datetime.now()
# if current_datetime.weekday() == 5 or current_datetime.weekday() == 6:
#     print("今天是周末，退出脚本")
#     exit()

# 运行参数设置
    # 是否更新产品列表
update_chanpin_judge = False #True or False

# 创建 ChromeOptions 对象
chrome_options = Options()
chrome_options.add_argument('--headless')
chrome_options.add_argument('--disable-gpu')
# 创建 Chrome WebDriver，并传入 ChromeOptions
driver = webdriver.Chrome(options=chrome_options)
driver.get('http://172.16.12.99/#/login')
time.sleep(2) # 等待让页面完全加载

# 自动填充账号和密码并点击登录
driver.find_element(By.XPATH,'//input[@id="name"]').send_keys('wanghui')      # 输入账号
driver.find_element(By.XPATH,'//input[@type="password"]').send_keys('wanghui')        # 输入密码
driver.find_element(By.XPATH,'//button[@class="el-button el-button--primary"]').click()      # 点击登录
time.sleep(3) # 等待让页面完全加载

# =============================================================================
# # 执行查询
# query = f"SELECT UPDATE_TIME FROM information_schema.tables WHERE TABLE_NAME = '99产品明细表'"
# cursor.execute(query)
# # 获取查询结果
# update_time = cursor.fetchone()[0]
# # 将字符串时间戳转换为datetime对象
# update_time = datetime.strptime(update_time_str, "%Y-%m-%d %H:%M:%S")
# # 获取今天的日期
# today = date.today()
# # 检查是否是今天的时间
# is_today_update = update_time.date() == today
# =============================================================================

# 判断是否更新产品列表
if update_chanpin_judge == True:
    # 导航到产品基础信息页面
        # 获取产品名称对照表
    driver.find_element(By.XPATH,"//div[@class='el-submenu__title']//span[text()='基金管理']").click()
    time.sleep(2) # 等待让页面完全加载
        # 定位目标元素
    element = driver.find_element(By.XPATH,"//div[@class='submenu']//span[text()='基金信息管理']")  
    time.sleep(2) # 等待让页面完全加载
        # 创建ActionChains对象
    actions = ActionChains(driver)
        # 将鼠标悬停在目标元素上
    actions.move_to_element(element).perform()
    driver.find_element(By.XPATH,"//li[contains(text(), '基金基础信息')]").click()
    time.sleep(1)
    
    # 获取产品名称对照表
    chanpin_name = list()
    chanpin_simname = list()
    chanpin_code = list()
    found_date = list()
    
    # 初始化标志变量
    first_time = True
    
    a = 0
    while a == 0:
        if first_time:
            driver.refresh()
            time.sleep(5)  # 刷新后等待5秒，确保页面加载完成
            first_time = False  # 将标志变量设置为False，避免后续循环再次等待5秒
        
        time.sleep(3)
        elements = driver.find_elements(By.XPATH,'//div/table/tbody/tr[@class="el-table__row"]')
        # 只获取前十个元素
        elements = elements[:10]
        
        for element in elements:
            # 基金名称
            chanpin_name.append(element.find_elements(By.XPATH, './/td[contains(@class, "el-table")]')[5].text)
            # 基金简称
            chanpin_simname_str = element.find_elements(By.XPATH, './/td[contains(@class, "el-table")]')[3].text
            chanpin_simname.append(chanpin_simname_str)
            # 产品备案号
            chanpin_code.append(element.find_elements(By.XPATH, './/td[contains(@class, "el-table")]')[1].text)
            # 产品成立日
            found_date.append(element.find_elements(By.XPATH, './/td[contains(@class, "el-table")]')[8].text)
        b = 0
        while b == 0:
            try:
                page_next = driver.find_element(By.XPATH,'//button[@class="btn-next"]')
                page_judge = page_next.get_attribute('disabled')
                b = 1
            except:
                page_next = driver.find_element(By.XPATH,'//button[@class="btn-next"]')
                page_judge = page_next.get_attribute('disabled')
        # 处理备案号缺失情况
        if page_judge == 'true':
            a = 1
        else:
            time.sleep(3)
            driver.find_element(By.XPATH,'//button[@class="btn-next"]').click()
        time.sleep(3)
    
    
    list_chanpin_info = pd.DataFrame({'基金简称':chanpin_name, '基金简写':chanpin_simname, '成立日期':found_date, '基金代码':chanpin_code})
 
    # # 处理备案号缺失情况
    # index_num  = list_chanpin_info.index[list_chanpin_info['基金简写'] == 'QS11'].tolist()[0]
    # list_chanpin_info.loc[index_num, '基金代码'] = 'SXN456'
    if list_chanpin_info.empty:
        print("list_chanpin_info is empty. Script terminated.")
        exit()
        
    # 删除基金简称为空的行
    list_chanpin_info = list_chanpin_info[list_chanpin_info['基金简称'].str.strip() != '']
    
    # 保存
    list_chanpin_info.to_sql(name='99产品明细表', con=engine, if_exists='replace', index=False)


query = "SELECT * FROM %s" % '产品策略对照表'
# 或者使用 .format() 方法构建查询语句
strategy_list = pd.read_sql_query(query, engine)

# 预估净值抓取计算
time.sleep(4)
driver.get('http://172.16.12.99/#/risk_summary')
time.sleep(2)
driver.find_element(By.XPATH,'//span[@class="el-radio__input"]/span[@class="el-radio__inner"]').click()
time.sleep(5)
# 最新数据时间戳
datetime_profit = driver.find_elements(By.XPATH,'//div/div/p/h3/span')[1].text
while datetime_profit == '':
    time.sleep(1)
    datetime_profit = driver.find_elements(By.XPATH,'//div/div/p/h3/span')[1].text

# 获取当前日期和时间  
current_time = datetime.now()  
  
# 格式化为月、日、时、分  
current_time = current_time.strftime('%m月%d日 %H时%M分') 

index_spot_df = ak.stock_zh_index_spot_sina()
zz300_change = index_spot_df.loc[index_spot_df['名称'] == '沪深300', '涨跌幅'].values[0]/100
zz500_change = index_spot_df.loc[index_spot_df['名称'] == '中证500', '涨跌幅'].values[0]/100
zz1000_change =  index_spot_df.loc[index_spot_df['名称'] == '中证1000', '涨跌幅'].values[0]/100
zzA500_change =  index_spot_df.loc[index_spot_df['名称'] == '中证A500', '涨跌幅'].values[0]/100
gz2000_change =  index_spot_df.loc[index_spot_df['名称'] == '国证2000', '涨跌幅'].values[0]/100

# 重新定义新的指数序列变量
data_CSI300_copy = data_CSI300.copy()
data_CSI500_copy = data_CSI500.copy()
data_CSI1000_copy = data_CSI1000.copy()
data_CSIA500_copy = data_CSIA500.copy()
data_GZ2000_copy = data_GZ2000.copy()


# 接下来，在每个DataFrame的最后一行添加一行空数据
data_CSI300_copy.loc[len(data_CSI300_copy.index)] = [None]*len(data_CSI300_copy.columns)
data_CSI500_copy.loc[len(data_CSI500_copy.index)] = [None]*len(data_CSI500_copy.columns)
data_CSI1000_copy.loc[len(data_CSI1000_copy.index)] = [None]*len(data_CSI1000_copy.columns)
data_CSIA500_copy.loc[len(data_CSIA500_copy.index)] = [None]*len(data_CSIA500_copy.columns)
data_GZ2000_copy.loc[len(data_GZ2000_copy.index)] = [None]*len(data_GZ2000_copy.columns)

# 更新指数序列到最新
data_CSI300_copy.iloc[-1, 1] = index_spot_df.loc[index_spot_df['名称'] == '沪深300', '最新价'].values[0]
data_CSI500_copy.iloc[-1, 1] = index_spot_df.loc[index_spot_df['名称'] == '中证500', '最新价'].values[0]
data_CSI1000_copy.iloc[-1, 1] = index_spot_df.loc[index_spot_df['名称'] == '中证1000', '最新价'].values[0]
data_CSIA500_copy.iloc[-1, 1] = index_spot_df.loc[index_spot_df['名称'] == '中证A500', '最新价'].values[0]
data_GZ2000_copy.iloc[-1, 1] = index_spot_df.loc[index_spot_df['名称'] == '国证2000', '最新价'].values[0]

# 更新指数序列日期到最新
data_CSI300_copy.iloc[-1, 0] = datetime.today().date().strftime('%Y-%m-%d %H:%M:%S')
data_CSI500_copy.iloc[-1, 0] = datetime.today().date().strftime('%Y-%m-%d %H:%M:%S')
data_CSI1000_copy.iloc[-1, 0] = datetime.today().date().strftime('%Y-%m-%d %H:%M:%S')
data_CSIA500_copy.iloc[-1, 0] = datetime.today().date().strftime('%Y-%m-%d %H:%M:%S')
data_GZ2000_copy.iloc[-1, 0] = datetime.today().date().strftime('%Y-%m-%d %H:%M:%S')

chanpin_name = list()
chanpin_simname = list()
chanpin_strategy = list()
chanpin_ab_performence = list()
chanpin_re_performence = list()
time.sleep(5)

elements = driver.find_elements(By.XPATH,'//table/tbody/tr[@class="el-table__row"]')
n = 0
for n in range(len(elements)-2):
    root = etree.HTML(elements[n].get_attribute("outerHTML"))
    element = root.xpath('//td/div[@class="cell"]')
    # 基金简写
    chanpin_simname_str = element[1].text
    chanpin_simname.append(chanpin_simname_str)
    # 基金简称
    chanpin_name.append(strategy_list.loc[strategy_list['基金简写'] == chanpin_simname_str, '基金简称'].values[0])
    # 策略类型
    strategy_type_str = strategy_list.loc[strategy_list['基金简写'] == chanpin_simname_str, '策略类型'].values[0]
    chanpin_strategy.append(strategy_type_str)
    # 预估收益率
    chanpin_ab_performence_str = element[9].text
    chanpin_ab_performence.append(chanpin_ab_performence_str)
    # 预估超额
    chanpin_ab_performence_int = float(chanpin_ab_performence_str.strip('%'))/100
    if "指增" in strategy_type_str:
        if "A500" in strategy_type_str:
            chanpin_re_performence.append('{:.2%}'.format(chanpin_ab_performence_int - zzA500_change))
        elif "500" in strategy_type_str:
            chanpin_re_performence.append('{:.2%}'.format(chanpin_ab_performence_int - zz500_change))
        elif "300" in strategy_type_str:
            chanpin_re_performence.append('{:.2%}'.format(chanpin_ab_performence_int - zz300_change))
        elif "1000" in strategy_type_str:
            chanpin_re_performence.append('{:.2%}'.format(chanpin_ab_performence_int - zz1000_change))
        elif "空气" in strategy_type_str:
            chanpin_re_performence.append('{:.2%}'.format(chanpin_ab_performence_int - gz2000_change))
    else:
        chanpin_re_performence.append('不适用')
    n = n + 1
list_chanpin_estimate = pd.DataFrame({'基金简称':chanpin_name, '基金简写':chanpin_simname, '策略类型':chanpin_strategy, '预估收益率':chanpin_ab_performence, '预估超额':chanpin_re_performence})
list_chanpin_estimate['更新时间'] = current_time
list_chanpin_estimate.to_sql(name='当日预估净值表', con=engine, if_exists='replace', index=False)

driver.quit()

# 把当日预估净值数据导入到历史中
# 把当日预估净值表导入数据库
list_yugu = pd.read_sql_query("SELECT * FROM %s" % '99预估净值历史数据', engine)
list_chanpin_estimate_trans = list_chanpin_estimate[['基金简称', '预估收益率']].transpose()
list_chanpin_estimate_trans.columns = list_chanpin_estimate_trans.iloc[0]
list_chanpin_estimate_trans = list_chanpin_estimate_trans[1:].reset_index(drop=True)

# 日期格式化
formatted_datetime = pd.to_datetime(current_datetime.strftime('%Y-%m-%d 00:00:00'), format='%Y-%m-%d %H:%M:%S')

# 验证是否包含特定值
contains_value = formatted_datetime in list_yugu['净值日期'].values
if contains_value:
    list_yugu = list_yugu.drop(list_yugu.index[-1])
else:
    # 检查每个列是否在DataFrame中存在
    for column in list_chanpin_estimate_trans.columns:
        if column not in list_yugu.columns:
            # 在DataFrame中新增列并增加一行
            list_yugu[column] = None

# 合并最新预估数值列  
list_yugu = list_yugu._append(list_chanpin_estimate_trans, ignore_index=True)
list_yugu['净值日期'].iloc[-1] = formatted_datetime


# 需要跳过的产品名称
skip_production_list = ['衍盛量化CTA二号']

# 计算当周预估净值数据
# 修改列名
list_chanpin_estimate_weekly = list_chanpin_estimate.rename(columns={"预估收益率": "周预估收益", "预估超额": "周预估超额"})
for column in list_yugu.columns[1:]:
    if column in skip_production_list:
        continue
    # 提取表名以及策略名称
    chanpin_fullname = strategy_list.loc[strategy_list['基金简称']==column, '基金全称'].values[0]
    chanpin_strategy = strategy_list.loc[strategy_list['基金简称']==column, '策略类型'].values[0]
    # 根据策略名称匹配对标指数
    index_mapping = {
    '500指增': '中证500',
    '1000指增': '中证1000',
    '300指增': '沪深300',
    'A500指增': '中证A500',
    '空气指增': '国证2000'
    }
    # 根据策略名称匹配对标指数数据表
    index_data_mapping = {
    '300指增': data_CSI300_copy,
    '500指增': data_CSI500_copy,
    '1000指增': data_CSI1000_copy,
    'A500指增': data_CSIA500_copy,
    '空气指增': data_GZ2000_copy
    }
    # 对标指数变量赋值
    if re.search('指增', chanpin_strategy):
        index_duibiao = index_mapping[chanpin_strategy]
    # 查询对应产品的净值表
    chanpin_yugu_list = pd.read_sql_query("SELECT * FROM %s" % chanpin_fullname, engine)
    # 筛选上周五之后的净值序列
    mask = chanpin_yugu_list['净值日期'] >= time_last_Friday_time
    chanpin_yugu_list = chanpin_yugu_list.loc[mask].reset_index(drop=True)
    # 判断是否需要净值日期是否小于预估日期
    if chanpin_yugu_list.empty:
        continue
    else:
        # 日期格式转化
        list_yugu['净值日期'] = pd.to_datetime(list_yugu['净值日期'])
        # 需要添加的预估日期和收益单独成列
        yugu_date_list = list_yugu.loc[list_yugu['净值日期'] > chanpin_yugu_list['净值日期'].iloc[-1], '净值日期']
        yugu_yield_list = list_yugu.loc[list_yugu['净值日期'] > chanpin_yugu_list['净值日期'].iloc[-1], column]
        # 按行添加数据
        for yugu_date, yugu_yield in zip(yugu_date_list, yugu_yield_list):
            try:
                # 对指增和非指增进行分类
                if re.search('指增', chanpin_strategy):
                    chanpin_yugu_list = chanpin_yugu_list[['净值日期', '复权净值', '对标指数']]
                    # 查询当日对标指数数据
                    index_yugu_list = index_data_mapping[chanpin_strategy]
                    index_yugu = index_yugu_list.loc[index_yugu_list['date'] == yugu_date, 'close'].values[0]
                    # 创建要添加的新行的数据
                    new_row = pd.Series({'净值日期': yugu_date, '复权净值': chanpin_yugu_list['复权净值'].iloc[-1]*(1+float(yugu_yield.replace('%', ''))/100), '对标指数': index_yugu})
                else:
                    chanpin_yugu_list = chanpin_yugu_list[['净值日期', '复权净值']]
                    # 创建要添加的新行的数据
                    new_row = pd.Series({'净值日期': yugu_date, '复权净值': chanpin_yugu_list['复权净值'].iloc[-1]*(1+float(yugu_yield.replace('%', ''))/100)})
            except:
                continue
            # 使用loc方法将新行添加到DataFrame中
            chanpin_yugu_list = chanpin_yugu_list._append(new_row, ignore_index=True)
    # 将结果录入总表
    yield_rate = (chanpin_yugu_list['复权净值'].iloc[-1] / chanpin_yugu_list['复权净值'].iloc[0] - 1) * 100
    formatted_yield_rate = "{:.2f}%".format(yield_rate)
    list_chanpin_estimate_weekly.loc[list_chanpin_estimate_weekly['基金简称'] == column, '周预估收益'] = formatted_yield_rate
    if re.search('指增', chanpin_strategy):
        index_duibiao_rate = yield_rate - (chanpin_yugu_list['对标指数'].iloc[-1] / chanpin_yugu_list['对标指数'].iloc[0] - 1) * 100
        formatted_index_duibiao_rate = "{:.2f}%".format(index_duibiao_rate)
        list_chanpin_estimate_weekly.loc[list_chanpin_estimate_weekly['基金简称'] == column, '周预估超额'] = formatted_index_duibiao_rate
    else:
        list_chanpin_estimate_weekly.loc[list_chanpin_estimate_weekly['基金简称'] == column, '周预估超额'] = '不适用'

# 保存预估净值历史数据
list_yugu.to_sql(name='99预估净值历史数据', con=engine, if_exists='replace', index=False)
list_chanpin_estimate_weekly['更新时间'] = current_time
list_chanpin_estimate_weekly.to_sql(name='99预估净值周超额', con=engine, if_exists='replace', index=False)

logging.info(current_time + '99预估数据已更新')

# 关闭游标
cursor.close()
# 关闭连接
cnx.close()