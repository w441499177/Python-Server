import sys
sys.path.append('/home/ubuntu/market/dc-66-python-2.0/SelfModule')

from selenium import webdriver
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.chrome.options import Options
from urllib.parse import unquote
from lxml import etree
import mysql.connector
from sqlalchemy import create_engine
import codecs
import warnings
import matplotlib.pyplot as plt
from selenium.webdriver.common.by import By
import numpy as np
import pandas as pd
from datetime import datetime, date, timedelta
from tradedate import *  # 自定义封装函数
from downloadindex import BenchmarkIndex_mapping, data_CSI1000  # 自定义封装函数
from selfdefinfunc import Search_NavDate, BenchmarkIndex, check_a_share_trading_day
from chinese_calendar import is_workday
import time
import os
import re
import requests
from io import BytesIO
from PIL import Image
import pytesseract
import logging

plt.rcParams['font.sans-serif'] = ['SimHei']  # 用来正常显示中文标签
plt.rcParams['axes.unicode_minus'] = False  # 用来正常显示负号
warnings.filterwarnings('ignore')

# 设置日志
logging.basicConfig(filename='/home/ubuntu/market/dc-66-python-2.0/execution.log', 
                    level=logging.INFO, 
                    format='%(asctime)s - %(levelname)s - %(message)s', 
                    filemode='a')  # 设置为追加模式

# 检查是否非交易日
check_a_share_trading_day()

def Recent_TradeDay(date):
    for i in range(10):
        if (is_workday(date) == False) or (date.weekday() == 5) or (date.weekday() == 6):
            date = date + timedelta(days=-1)
        else:
            break
    return date

# 环境变量读取数据库秘钥信息
host = os.environ.get('MYSQL_HOST')   
user = os.environ.get('MYSQL_USERNAME')  
password = os.environ.get('MYSQL_PASSWORD')
port = os.environ.get('MYSQL_PORT')

# 数据库存储操作
cnx = mysql.connector.connect(user=user, password=password,
                              host=host,  port=port, database='衍盛产品净值数据库')
engine = create_engine(f'mysql+mysqlconnector://{user}:{password}@{host}:{port}/衍盛产品净值数据库')
cursor = cnx.cursor()

# 创建 ChromeOptions 对象
chrome_options = Options()
chrome_options.add_argument('--headless')
chrome_options.add_argument('--disable-gpu')
# 创建 Chrome WebDriver，并传入 ChromeOptions
driver = webdriver.Chrome(options=chrome_options) 
driver.get('http://zscrm.stonescloud.com/accounts/sign_in') 
time.sleep(2) # 等待让页面完全加载

a = 0 
while a==0:
    # 验证码处理
        # 定位验证码元素
    captcha_element = driver.find_element(By.XPATH,'//div[@id="proof"]/a/img')
        # 获取验证码图片的src属性
    captcha_src = captcha_element.get_attribute('src')
        # 下载验证码图片
    response = requests.get(captcha_src)
    image = Image.open(BytesIO(response.content))

        # 使用Tesseract识别验证码
    captcha_text = pytesseract.image_to_string(image).replace(" ", "").replace("\n", "")

    # 自动填充账号和密码并点击登录
    driver.find_element(By.XPATH,'//input[@placeholder="邮箱"]').send_keys('wanghui@derivatives-china.com')      # 输入账号
    driver.find_element(By.XPATH,'//input[@placeholder="密码"]').send_keys('Yansheng19930516')        # 输入密码
    driver.find_element(By.XPATH,'//input[@placeholder="请输入右侧验证码"]').send_keys(captcha_text)        # 输入验证码
    driver.find_element(By.XPATH,'//div/a[@class="login_de"]').click()     # 点击登录
    time.sleep(2)
    try:
        sucess_judge = driver.find_elements(By.XPATH,'//span[@class="ver_mid tip_title_sizie12"]')[0].text
    except:
        sucess_judge = '无'
    # 判断此时的页面
    if sucess_judge == '验证码不匹配':
        driver.get('http://zscrm.stonescloud.com/accounts/sign_in')
        time.sleep(2)
        try:
            driver.find_element(By.XPATH,'//div/a[@class="btn fott_qud01"]').click()      # 点击提示框
        except:
            time.sleep(2)
    else:
        a = 1
time.sleep(5) # 等待让页面完全加载

# 搜索每个产品的信息及链接
driver.get('http://zscrm.stonescloud.com/products') 
time.sleep(5) # 等待让页面完全加载
encoded_string = driver.execute_cdp_cmd('Page.captureSnapshot', {})['data']
# 将字符串转换为字节对象
encoded_bytes = bytes(encoded_string, 'utf-8')
# 解码字符串
decoded_bytes = codecs.decode(encoded_bytes, 'quoted-printable')
# 将解码后的字节对象转换为字符串
decoded_string = decoded_bytes.decode('utf-8')
root = etree.HTML(decoded_string)
# 使用 XPath 定位产品信息
elements = root.xpath('//table[@id="toolbar"]/tbody/tr[@tabindex="-1"]')

if not elements:  
    logging.error('Data is empty, stopped running!')  
    exit()

chanpin_fullname = list()
chanpin_name = list()
chanpin_url = list()
chanpin_code = list()
nav_date = list()
nav_updated = list()
found_date = list()
product_scale = list()
product_fene = list()
ifneed_update = list()

# 遍历elements并形成产品信息表
for element in elements:
    if element.xpath('.//td[@aria-describedby="toolbar_最新净值日期"]')[0].text == '-':
        continue
    # 最新净值
    nav_updated.append(element.xpath('.//td[@aria-describedby="toolbar_最新净值"]')[0].text)
    # 最新净值日期
    try:
        date_updated = datetime.strptime(element.xpath('.//td[@aria-describedby="toolbar_最新净值日期"]')[0].text, '%Y-%m-%d')
        nav_date.append(date_updated)
    except:
        nav_date.append(element.xpath('.//td[@aria-describedby="toolbar_最新净值日期"]')[0].text)
    # 基金简称
    chanpin_name.append(element.xpath('.//td[@aria-describedby="toolbar_基金简称"]')[0].text)
    # 资产规模
    product_scale.append(element.xpath('.//td[@aria-describedby="toolbar_product_scale"]')[0].text)
    # 最新份额
    try:
        product_fene.append(element.xpath('.//td[@aria-describedby="toolbar_最新份额"]//span')[0].text)
    except:
        product_fene.append(element.xpath('.//td[@aria-describedby="toolbar_最新份额"]')[0].text)
    # 成立日期
    found_date.append(element.xpath('.//td[@aria-describedby="toolbar_成立日期"]')[0].text)
    # 基金全称
    full_name = element.xpath('.//td[@aria-describedby="toolbar_基金全称"]/a')[0].text.lower()
    chanpin_fullname.append(full_name)
    # 基金代码
    chanpin_code.append(element.xpath('.//td[@aria-describedby="toolbar_基金代码"]')[0].text)
    # 产品净值链接
        # 确定链接所在字符串
    variable = etree.tostring(element, encoding='unicode')
    start_index = variable.find('id="') + 4
    end_index = variable.find('"', start_index)
        # 提取出 id 值
    id_value = variable[start_index:end_index]
    url = 'http://zscrm.stonescloud.com/products/' + id_value + '/nets'
    chanpin_url.append(url)
    
    # 针对非交易日净值修正为实际截止的交易日
    while is_workday(date_updated) == False:  
        date_updated = date_updated + timedelta(-1)  

    try:
        # 构建查询语句，只查询最后一行数据
        query = "SELECT * FROM {} ORDER BY 净值日期 DESC LIMIT 1".format(full_name)
        # 使用pandas读取SQL查询结果
        list_chanpin_nav = pd.read_sql_query(query, engine)
        # 假设Recent_TradeDay函数正确定义，并且可以处理一个日期字符串
        # 获取最后一行的净值日期
        lastest_date = list_chanpin_nav['净值日期'].iloc[0]
        date_updated = Recent_TradeDay(date_updated)
        if lastest_date < date_updated:
            ifneed_update.append('是')
        else:
            ifneed_update.append('否')
    except:
        if nav_date[-1] == '-':
            ifneed_update.append('否')
        else:
            ifneed_update.append('是')

product_scale = [float(x.replace(",", "")) for x in product_scale]
list_chanpin_info = pd.DataFrame({'基金全称':chanpin_fullname, '基金简称':chanpin_name, '成立日期':found_date, 
                                  '资产规模':product_scale, '最新份额':product_fene, '最新净值日期':nav_date, '最新净值':nav_updated, 
                                  '最新净值日期':nav_date, '基金代码':chanpin_code, '产品净值链接':chanpin_url, '是否需更新':ifneed_update})
list_chanpin_info.loc[list_chanpin_info['基金全称'] == '中融-【京港伟业】单一证券投资资金信托', '是否需更新'] = '否'    # 处理【京港伟业】避免每次都更新净值

# 首先筛选出'成立日期'字段值为'-'的行  
mask = list_chanpin_info['成立日期'] == '-'  
# 然后删除这些行  
list_chanpin_info = list_chanpin_info.drop(list_chanpin_info[mask].index)
# 保存所有产品明细表
list_chanpin_info.to_sql(name='所有产品明细表', con=engine, if_exists='replace', index=False)

# 更新【产品策略对照表】
list_chanpin_info = pd.read_sql_query("SELECT * FROM %s" % '所有产品明细表', engine)
list_chanpin_strategy = pd.read_sql_query("SELECT * FROM %s" % '产品策略对照表', engine)
list_chanpin_strategy['基金全称'] = list_chanpin_strategy['基金全称'].str.lower()

# 检查原DataFrame长度
if len(list_chanpin_strategy) < len(list_chanpin_info):
    # 计算需要添加的行数
    num_rows_to_add = len(list_chanpin_info) - len(list_chanpin_strategy)
    # 添加新行
    new_rows = pd.DataFrame(index=range(num_rows_to_add), columns=list_chanpin_strategy.columns)
    list_chanpin_strategy = list_chanpin_strategy._append(new_rows)

list_chanpin_strategy = pd.merge(list_chanpin_info[['基金全称', '基金简称', '基金代码', '资产规模', '最新份额', '最新净值日期', '成立日期']], list_chanpin_strategy[['基金全称', '策略类型', '策略调整时点', '策略简介', '固定开放日', '是否多份额', '是否刷新净值', '是否更新周报', '是否资管产品']], on='基金全称', how='left')

# 将所有未更新净值产品筛选出来
list_chanpin_info = list_chanpin_info[list_chanpin_info['是否需更新']=='是']
# 处理特殊产品
list_chanpin_info = list_chanpin_info[list_chanpin_info['基金简称'] != '-']

num_need_update = str(len(list_chanpin_info))

first_iteration = True  # 设置一个标志，用于判断是否是第一次循环
# 遍历每个产品净值表
k = 0
for url, full_name, nav_updated, chanpin_aum in zip(list_chanpin_info['产品净值链接'], list_chanpin_info['基金全称'], list_chanpin_info['最新净值'], list_chanpin_info['资产规模']):
    # 判断产品是否最新净值、是否新加入产品、是否已清盘
    driver.get(url)
    if first_iteration:  # 检查是否是第一次循环
        time.sleep(10)  # 等待15秒
        first_iteration = False  # 重置标志
    # 设置等待的最长时间，例如60秒
    time.sleep(8)

    a = 0
    if a == 0 and nav_updated != '-':
        # 数据库已有产品情形
        try:
            # 构建 SQL 查询，选择表的倒数第一行数据
            query = "SELECT 净值日期, 单位净值, 累计净值, 复权净值 FROM {} ORDER BY ROW_NUMBER() OVER() DESC LIMIT 1".format(full_name)
            # 使用 SQL 查询从数据库中读取倒数第一行数据
            list_chanpin_nav = pd.read_sql_query(query, engine)
            update_date = list_chanpin_nav.iloc[0, 0].to_pydatetime()  # 载入最后的更新日期
            
            nav_date = []
            nav_danwei = []
            nav_leiji = []
            nav_fuquan = []  
            
            # 源代码字符串解析
            encoded_string = driver.execute_cdp_cmd('Page.captureSnapshot', {})['data']
            # 将字符串转换为字节对象
            encoded_bytes = bytes(encoded_string, 'utf-8')
            # 解码字符串
            decoded_bytes = codecs.decode(encoded_bytes, 'quoted-printable')
            # 将解码后的字节对象转换为字符串
            decoded_string = decoded_bytes.decode('utf-8')
            root = etree.HTML(decoded_string)
            # 使用 XPath 定位产品信息
            elements = root.xpath('//table[@id="toolbar"]/tbody/tr[@tabindex="-1"]')
            a = 0
            iteration_count = 0  # Initialize the iteration counter
            
            while a == 0 and iteration_count < 5:
                # 遍历第一页的元素
                for element in elements:
                    updating_date = datetime.strptime(element.xpath('.//td[@aria-describedby="toolbar_净值日期"]')[0].text, '%y-%m-%d')
                    if updating_date == update_date:
                        a = 1
                        break
                    # 判断是否为未更新净值
                    if updating_date > update_date:
                        fene_name = element.xpath('.//td[@aria-describedby="toolbar_基金名称"]')[0].text.lower()
                        # 排除非主份额情况
                        if fene_name != full_name:
                            continue
                        # 净值日期
                        nav_date_value = element.xpath('.//td[@aria-describedby="toolbar_净值日期"]')[0].text
                        # 单位净值
                        nav_danwei_value = element.xpath('.//td[@aria-describedby="toolbar_单位净值"]')[0].text
                        # 累计净值
                        nav_leiji_value = element.xpath('.//td[@aria-describedby="toolbar_累计净值"]')[0].text
                        # 复权净值
                        nav_fuquan_value = element.xpath('.//td[@aria-describedby="toolbar_复权净值"]')[0].text
                        # 如果值为'-'，跳过当前循环
                        if nav_date_value == '-' or nav_danwei_value == '-' or nav_leiji_value == '-' or nav_fuquan_value == '-':
                            continue
                        # 此处执行你的其他操作，将有效的值添加到相应的列表中
                        nav_date.append(nav_date_value)
                        nav_danwei.append(nav_danwei_value)
                        nav_leiji.append(nav_leiji_value)
                        nav_fuquan.append(nav_fuquan_value)
                # 判断是否需要进入下一页
                    # 如果最后更新日期仍然没有接上截止日期
                if updating_date > update_date:
                    try:
                        iframe = driver.find_elements(By.CSS_SELECTOR, 'iframe[id="J_iframe"]')[1]
                        driver.switch_to.frame(iframe)
                        page_next = driver.find_element(By.XPATH,'//td[@title="下一页"]').click()
                    except:
                        # 源代码字符串解析
                        encoded_string = driver.execute_cdp_cmd('Page.captureSnapshot', {})['data']
                        # 将字符串转换为字节对象
                        encoded_bytes = bytes(encoded_string, 'utf-8')
                        # 解码字符串
                        decoded_bytes = codecs.decode(encoded_bytes, 'quoted-printable')
                        # 将解码后的字节对象转换为字符串
                        decoded_string = decoded_bytes.decode('utf-8')
                        root = etree.HTML(decoded_string)
                        # 使用 XPath 定位产品信息
                        elements = root.xpath('//table[@id="toolbar"]/tbody/tr[@tabindex="-1"]')
                else:
                    a = 1
                # Increment the iteration counter
                iteration_count += 1
            if iteration_count == 5:
                logging.info(full_name + "超时未抓取最新净值")
        
        # 数据库没有产品情形
        except:
            # 确认是否存续产品误入
            # 数据库查询是否存在该表格
            cursor.execute("SHOW TABLES LIKE %s", (full_name,))
            chanpin_judge = cursor.fetchone() == None
            if chanpin_judge == False:
                cursor.execute("SHOW TABLES LIKE %s", (full_name,))
                chanpin_judge = cursor.fetchone() == None
                if chanpin_judge == False:
                    logging.info(full_name + '：产品误读跳过')
                    continue
            nav_date = list()
            nav_danwei = list()
            nav_leiji = list()
            nav_fuquan = list()
            # 判断是否最后一页净值
            a = 0
            num_judge = 0
            while a == 0:
                # 先判断是否需要进入翻页循环
                if num_judge == 0:
                    iframe = driver.find_elements(By.CSS_SELECTOR, 'iframe[id="J_iframe"]')[1]
                    driver.switch_to.frame(iframe)
                page_next = driver.find_element(By.XPATH,'//td[@title="下一页"]')
    
                time.sleep(2)
                page_judge = page_next.get_attribute('class')
                if page_judge == 'ui-pg-button ui-corner-all ui-state-disabled':
                    a = 1
                else:
                    num_judge = 1
                
                # 源代码字符串解析
                encoded_string = driver.execute_cdp_cmd('Page.captureSnapshot', {})['data']
                # 将字符串转换为字节对象
                encoded_bytes = bytes(encoded_string, 'utf-8')
                # 解码字符串
                decoded_bytes = codecs.decode(encoded_bytes, 'quoted-printable')
                # 将解码后的字节对象转换为字符串
                decoded_string = decoded_bytes.decode('utf-8')
                root = etree.HTML(decoded_string)
                # 使用 XPath 定位产品信息
                elements = root.xpath('//table[@id="toolbar"]/tbody/tr[@tabindex="-1"]')
                for element in elements:
                    fene_name = element.xpath('.//td[@aria-describedby="toolbar_基金名称"]')[0].text.lower()
                    # 排除非主份额情况
                    if fene_name != full_name:
                        continue
                    # 净值日期
                    nav_date_value = element.xpath('.//td[@aria-describedby="toolbar_净值日期"]')[0].text
                    # 单位净值
                    nav_danwei_value = element.xpath('.//td[@aria-describedby="toolbar_单位净值"]')[0].text
                    # 累计净值
                    nav_leiji_value = element.xpath('.//td[@aria-describedby="toolbar_累计净值"]')[0].text
                    # 复权净值
                    nav_fuquan_value = element.xpath('.//td[@aria-describedby="toolbar_复权净值"]')[0].text
                    # 如果值为'-'，跳过当前循环
                    if nav_date_value == '-' or nav_danwei_value == '-' or nav_leiji_value == '-' or nav_fuquan_value == '-':
                        continue
                    # 此处执行你的其他操作，将有效的值添加到相应的列表中
                    nav_date.append(nav_date_value)
                    nav_danwei.append(nav_danwei_value)
                    nav_leiji.append(nav_leiji_value)
                    nav_fuquan.append(nav_fuquan_value)
                # 最后点击翻页
                page_next.click()
    else:
        k = k + 1
        logging.info('净值数据更新进展(无净值)' + str(k) + '/' + num_need_update)
        continue
    
    try:
        nav_danwei = [float(num) for num in nav_danwei]
        nav_leiji = [float(num) for num in nav_leiji]
        nav_fuquan = [float(num) for num in nav_fuquan]
    except:
        logging.info('净值格式问题产品：' + full_name)
    list_chanpin_nav = pd.DataFrame({'净值日期':nav_date, '单位净值':nav_danwei, '累计净值':nav_leiji, '复权净值':nav_fuquan})
    list_chanpin_nav['净值日期'] = pd.to_datetime(list_chanpin_nav['净值日期'], format='%y-%m-%d')
    list_chanpin_nav = list_chanpin_nav.replace('-', np.nan)
    list_chanpin_nav = list_chanpin_nav.dropna()
    list_chanpin_nav = list_chanpin_nav.sort_values(by='净值日期', ascending=True).reset_index().drop('index', axis=1)
    
    # 剔除非交易日净值
    list_chanpin_nav = list_chanpin_nav.dropna(how='any')
    list_chanpin_nav = list_chanpin_nav.drop_duplicates()
    # 产品策略查询
    type_strategy = list_chanpin_strategy.loc[list_chanpin_strategy['基金全称'] == full_name, '策略类型'].iloc[-1]
        
    list_chanpin_strategy.loc[list_chanpin_strategy['基金全称'] == full_name, '是否刷新净值'] = '是'
    list_chanpin_nav.to_sql(name=full_name, con=engine, if_exists='append', index=False)                    
    
    # 更新进展数
    k = k + 1
    print(full_name + '更新' + str(k) + '/' + num_need_update)

list_chanpin_strategy['成立日期'] = pd.to_datetime(list_chanpin_strategy['成立日期'])
list_chanpin_99 = pd.read_sql_query("SELECT * FROM %s" % '99产品明细表', engine)
list_chanpin_strategy = pd.merge(list_chanpin_strategy, list_chanpin_99[['基金简写', '基金代码']], on='基金代码', how='left').drop_duplicates()
list_chanpin_strategy = list_chanpin_strategy.reset_index(drop=True)
list_chanpin_strategy.to_sql(name='产品策略对照表', con=engine, if_exists='replace', index=False)

driver.quit()

# 周度日期序列 #
weekly_date = data_CSI1000.drop('close', axis=1)
weekly_date = weekly_date.rename(columns = {'date':'净值日期'})
weekly_date['是否周末净值'] = ''
weekly_date.iloc[-1, 1] = '是'
for i in range(len(weekly_date) - 1):
    if weekly_date.iloc[i+1, 0] - weekly_date.iloc[i, 0] != timedelta(days=1):
        weekly_date.iloc[i, 1] = '是'
    else:
        weekly_date.iloc[i, 1] = '否'
weekly_date = weekly_date[weekly_date['是否周末净值'] == '是'].drop('是否周末净值', axis=1).reset_index().drop('index', axis=1)
weekly_date = pd.to_datetime(weekly_date['净值日期'])

# 计算各个产品的净值时序指标
n = 0
for n in range(len(list_chanpin_strategy)):
    if list_chanpin_strategy.loc[n, '是否刷新净值'] != '是':
        n = n + 1
        continue
    try:
        list_chanpin_info = pd.read_sql_query("SELECT * FROM %s" % list_chanpin_strategy.loc[n, '基金全称'], engine)
    except:
        n = n + 1
        continue
    
    # 识别策略类型 # 
    type_strategy = list_chanpin_strategy.loc[n, '策略类型']
    type_strategy_judge = '指增' in type_strategy
    list_chanpin_strategy.loc[n, '成立日期'] = list_chanpin_info.iloc[0, 0]
    # 产品成立日期格式转datetime #
    date_found_chanpin = list_chanpin_strategy.loc[n, '成立日期'].to_pydatetime()
    # 删除坏点净值 #
    list_chanpin_info = list_chanpin_info[list_chanpin_info['复权净值'] != '-']
    # 修改净值格式 #
    list_chanpin_info[['单位净值', '累计净值', '复权净值',]] = list_chanpin_info[['单位净值', '累计净值', '复权净值']].astype(float)
    overlap_dates = list_chanpin_info['净值日期'].isin(data_CSI1000['date'])
    # 只保留五列数据
    list_chanpin_info = list_chanpin_info[overlap_dates][['净值日期', '单位净值', '累计净值', '复权净值']]
    list_chanpin_info['净值日期'] = pd.to_datetime(list_chanpin_info['净值日期'])
    
    # 匹配对标指数
    if not isinstance(BenchmarkIndex_mapping[type_strategy], str):
        list_chanpin_info = BenchmarkIndex(list_chanpin_info[['净值日期', '单位净值', '累计净值', '复权净值']], time_last_Friday_time, BenchmarkIndex_mapping[type_strategy])
    
    # 计算绝对收益 #
    if date_found_chanpin <= time_last_year_time:
        temp = list_chanpin_info.loc[(list_chanpin_info['净值日期'] <= time_last_year_time)]
        temp1 = list_chanpin_info.loc[(list_chanpin_info['净值日期'] <= time_last_Friday_time)]
        ytd = temp1.values[-1][2]/temp.values[-1][2] - 1

    # 增加数据列 #
    columns_to_add = [
        "近四周收益率", "对标指数近四周收益率", "近四周超额（除法）",
        "今年以来收益率", "对标指数今年以来收益率", "今年以来超额（除法）", "今年以来最大回撤", "今年以来最大回撤（超额）",
        "近三月收益率", "对标指数近三月收益率", "近三月超额（除法）", "近三月最大回撤", "近三月最大回撤（超额）",
        "近六月收益率", "对标指数近六月收益率", "近六月超额（除法）", "近六月最大回撤", "近六月最大回撤（超额）",
        "近一年收益率", "对标指数近一年收益率", "近一年超额（除法）", "近一年最大回撤", "近一年最大回撤（超额）",
        "近两年收益率", "对标指数近两年收益率", "近两年超额（除法）", "近两年最大回撤", "近两年最大回撤（超额）",
        "近三年收益率", "对标指数近三年收益率", "近三年超额（除法）", "近三年最大回撤", "近三年最大回撤（超额）",
        "成立以来收益率", "对标指数成立以来收益率", "成立以来超额（除法）", "成立以来最大回撤", "成立以来最大回撤（超额）"
    ]
    
    # 使用循环初始化数据列为NaN
    for column in columns_to_add:
        list_chanpin_info[column] = np.nan 
    
# =============================================================================
#     # 移除该列并存储在变量中
#     column_move = list_chanpin_info.pop('成立以来累计超额最大回撤')
#     # 将该列插入到新位置
#     list_chanpin_info.insert(11, '成立以来累计超额最大回撤', column_move)
# =============================================================================

# 计算成立以来数据 #      
    # 计算收益序列 #
    netvalue_target_chanpin = list_chanpin_info.loc[0, '复权净值']
    list_chanpin_info['成立以来收益率'] = list_chanpin_info.loc[:, '复权净值']/netvalue_target_chanpin - 1
    # 计算对标指数收益率/超额（除法）序列 # 
    if type_strategy_judge:       
        index_target_chanpin = list_chanpin_info.loc[0, '对标指数']
        list_chanpin_info['对标指数成立以来收益率'] = list_chanpin_info.loc[:, '对标指数']/index_target_chanpin - 1
        list_chanpin_info['成立以来超额（除法）'] = list_chanpin_info.apply(lambda x: (x['成立以来收益率']+1)/(x['对标指数成立以来收益率']+1)-1, axis=1)
    # 计算最大回撤序列 #
    nav_series = list_chanpin_info.loc[:, '成立以来收益率']
    list_chanpin_info.loc[0, '成立以来最大回撤'] = 0
    for d in range(1, len(nav_series)):
        list_chanpin_info.loc[nav_series.index[d], '成立以来最大回撤'] = (nav_series.iloc[d] + 1)/(max(nav_series.iloc[0:d+1]) + 1) - 1
        if list_chanpin_info.loc[nav_series.index[d], '成立以来最大回撤'] > 0:
            list_chanpin_info.loc[nav_series.index[d], '成立以来最大回撤'] = 0
    nav_series = list_chanpin_info.loc[:, '成立以来收益率']
    # 计算超额最大回撤序列 #
    if type_strategy_judge:
        nav_series = list_chanpin_info.loc[:, '成立以来超额（除法）']
        list_chanpin_info.loc[0, '成立以来最大回撤（超额）'] = 0
        for d in range(1, len(nav_series)):
            list_chanpin_info.loc[nav_series.index[d], '成立以来最大回撤（超额）'] = (nav_series.iloc[d] + 1)/(max(nav_series.iloc[0:d+1]) + 1) - 1
            if list_chanpin_info.loc[nav_series.index[d], '成立以来最大回撤（超额）'] > 0:
                list_chanpin_info.loc[nav_series.index[d], '成立以来最大回撤（超额）'] = 0    

    # 定义时间间隔和相应变量
    time_intervals = [
        ("今年以来", time_last_year_time),
        ("近三月", time_lag3_month_time),
        ("近六月", time_lag6_month_time),
        ("近一年", time_lag_year_time),
        ("近两年", time_lag2_year_time),
        ("近三年", time_lag3_year_time)
    ]
    
    for interval, start_time in time_intervals:
        if date_found_chanpin <= start_time:
            time_chanpin = Search_NavDate(list_chanpin_info, start_time, date_found_chanpin, time_time_last_Friday_lag4_time)
            date_start = time_chanpin
    
            mask = list_chanpin_info['净值日期'] >= date_start
            netvalue_chanpin = float((list_chanpin_info.loc[list_chanpin_info['净值日期'] == date_start, ['复权净值']]).values[0][0])
            list_chanpin_info[f'{interval}收益率'] = list_chanpin_info.loc[mask].loc[:, '复权净值'] / netvalue_chanpin - 1
    
            if type_strategy_judge:
                index_netvalue_chanpin = float((list_chanpin_info.loc[list_chanpin_info['净值日期'] == date_start, ['对标指数']]).values[0][0])
                list_chanpin_info[f'对标指数{interval}收益率'] = list_chanpin_info.loc[mask].loc[:, '对标指数'] / index_netvalue_chanpin - 1
                list_chanpin_info[f'{interval}超额（除法）'] = list_chanpin_info.apply(lambda x: (x[f'{interval}收益率'] + 1) / (x[f'对标指数{interval}收益率'] + 1) - 1, axis=1)
    
            # 计算最大回撤
            nav_series = list_chanpin_info.loc[mask, f'{interval}收益率']
            list_chanpin_info.loc[nav_series.index[0], f'{interval}最大回撤'] = 0
            for d in range(1, len(nav_series)):
                list_chanpin_info.loc[nav_series.index[d], f'{interval}最大回撤'] = (nav_series.iloc[d] + 1) / (
                        max(nav_series.iloc[0:d + 1]) + 1) - 1
                if list_chanpin_info.loc[nav_series.index[d], f'{interval}最大回撤'] > 0:
                    list_chanpin_info.loc[nav_series.index[d], f'{interval}最大回撤'] = 0
    
            if type_strategy_judge:
                # 计算超额收益的最大回撤
                nav_series = list_chanpin_info.loc[mask, f'{interval}超额（除法）']
                list_chanpin_info.loc[nav_series.index[0], f'{interval}最大回撤（超额）'] = 0
                for d in range(1, len(nav_series)):
                    list_chanpin_info.loc[nav_series.index[d], f'{interval}最大回撤（超额）'] = (nav_series.iloc[d] + 1) / (
                            max(nav_series.iloc[0:d + 1]) + 1) - 1
                    if list_chanpin_info.loc[nav_series.index[d], f'{interval}最大回撤（超额）'] > 0:
                        list_chanpin_info.loc[nav_series.index[d], f'{interval}最大回撤（超额）'] = 0

# 计算近四周数据 #
    # 时间定义 #
    date_start = Search_NavDate(list_chanpin_info, time_time_last_Friday_lag4_time, date_found_chanpin, time_time_last_Friday_lag4_time)        
    # 是否符合成立区间判定 #
    if date_found_chanpin <= time_time_last_Friday_lag4_time:    
        # 计算收益序列 #
        mask = ((list_chanpin_info['净值日期'] >= date_start) & (list_chanpin_info['净值日期'] <= time_last_Friday_time))
        netvalue_lag4_week_chanpin = float((list_chanpin_info.loc[list_chanpin_info['净值日期'] == date_start, ['复权净值']]).values[0][0])
        list_chanpin_info['近四周收益率'] = list_chanpin_info.loc[mask].loc[:, '复权净值']/netvalue_lag4_week_chanpin - 1
        
        # 计算对标指数收益率/超额（除法）序列 #
        if type_strategy_judge:
            index_netvalue_lag4_week_chanpin = float((list_chanpin_info.loc[list_chanpin_info['净值日期'] == date_start, ['对标指数']]).values[0][0])
            list_chanpin_info['对标指数近四周收益率'] = list_chanpin_info.loc[mask].loc[:, '对标指数']/index_netvalue_lag4_week_chanpin - 1
            list_chanpin_info['近四周超额（除法）'] = list_chanpin_info.apply(lambda x: (x['近四周收益率']+1)/(x['对标指数近四周收益率']+1)-1, axis=1)       
    
    list_chanpin_info.loc[:, '近四周收益率':'成立以来最大回撤（超额）'] = list_chanpin_info.loc[:, '近四周收益率':'成立以来最大回撤（超额）'].round(4)
    list_chanpin_info.to_sql(name=list_chanpin_strategy.iloc[n, 0], con=engine, if_exists='replace', index=False)
    
    # 波动率相关性计算
    # 表结构及列名修改
    list_chanpin_info_weekly = pd.merge(list_chanpin_info, weekly_date, on='净值日期', how='inner')

    list_chanpin_info_weekly = list_chanpin_info_weekly.drop(labels = ['累计净值', '成立以来最大回撤', '对标指数今年以来收益率', '今年以来最大回撤', '近一年最大回撤', '对标指数近一年收益率',
                                            '近三月最大回撤', '对标指数近三月收益率', '近三月最大回撤（超额）', '近六月最大回撤', '对标指数近六月收益率', '近六月最大回撤（超额）',
                                            '近两年最大回撤', '近三年最大回撤', '成立以来最大回撤（超额）', '今年以来最大回撤（超额）', '对标指数近两年收益率',
                                            '近一年最大回撤（超额）', '近两年最大回撤（超额）', '近三年最大回撤（超额）', '对标指数近三年收益率', '近四周收益率',
                                            '对标指数近四周收益率', '近四周超额（除法）', '成立以来超额（除法）'], axis=1)
    list_chanpin_info_weekly.columns = ['净值日期' ,'单位净值', '复权净值', '对标指数',  '今年以来波动率', '今年以来超额波动率', 
                                        '近三月波动率', '近三月超额波动率', '近六月波动率', '近六月超额波动率', 
                                        '近一年波动率', '近一年超额波动率', '近两年波动率', '近两年超额波动率', '近三年波动率', 
                                        '近三年超额波动率', '成立以来波动率', '成立以来超额波动率']
    
    # 绝对收益波动率
    for key_name in ['今年以来波动率', '近三月波动率', '近六月波动率', '近一年波动率', '近两年波动率', '近三年波动率', '成立以来波动率']:
        for d in range(len(list_chanpin_info_weekly)):
            if (np.isnan(list_chanpin_info_weekly.loc[d, key_name]) == False) and (d>=1):
                list_chanpin_info_weekly.loc[d, key_name] = list_chanpin_info_weekly.loc[d, '复权净值']/list_chanpin_info_weekly.loc[d-1, '复权净值'] - 1
            else:
                list_chanpin_info_weekly.loc[d, key_name] = np.nan
        if type_strategy_judge:
            volatility = np.std(list_chanpin_info_weekly[key_name])
    
    # 超额收益波动率
    if type_strategy_judge:
        for key_name in ['今年以来超额波动率', '近三月超额波动率', '近六月超额波动率', '近一年超额波动率', '近两年超额波动率', '近三年超额波动率', '成立以来超额波动率']:
            for d in range(len(list_chanpin_info_weekly)):
                if (np.isnan(list_chanpin_info_weekly.loc[d, key_name]) == False) and (d>=1):
                    list_chanpin_info_weekly.loc[d, key_name] = list_chanpin_info_weekly.loc[d, '复权净值']/list_chanpin_info_weekly.loc[d-1, '复权净值'] - list_chanpin_info_weekly.loc[d, '对标指数']/list_chanpin_info_weekly.loc[d-1, '对标指数']
                else:
                    list_chanpin_info_weekly.loc[d, key_name] = np.nan    
            volatility = np.std(list_chanpin_info_weekly[key_name])
    list_chanpin_info_weekly.loc[:, '今年以来波动率':'成立以来超额波动率'] = list_chanpin_info_weekly.loc[:, '今年以来波动率':'成立以来超额波动率'].round(4)
    list_chanpin_info_weekly.to_sql(name=list_chanpin_strategy.iloc[n, 0]+'(波动率)', con=engine, if_exists='replace', index=False)
    list_chanpin_strategy.loc[n, '是否刷新净值'] = '否'
    list_chanpin_info.to_sql(name=list_chanpin_strategy.iloc[n, 0], con=engine, if_exists='replace', index=False)
    n = n + 1

logging.info('衍盛产品净值数据清洗进度全部完成')
list_chanpin_strategy = list_chanpin_strategy.sort_values(by='成立日期', ascending=True).reset_index(drop=True)

# 两次尝试避免出现丢失连接的情况
try:
    list_chanpin_strategy.to_sql(name='产品策略对照表', con=engine, if_exists='replace', index=False)
except:
    engine = create_engine(f'mysql+mysqlconnector://{user}:{password}@{host}:{port}/衍盛产品净值数据库')
    list_chanpin_strategy.to_sql(name='产品策略对照表', con=engine, if_exists='replace', index=False)


def fetch_and_save_data():
    keywords = ['指增', 'CTA', '多策略', '中性', '套利', '期权', '雪球', '通道', '专户']
    
    # 创建一个新的列表，用于存储经过策略类型匹配处理后的数据
    new_list_chanpin_strategy = []
    
    # 遍历 list_chanpin_strategy 的策略类型
    for i in range(len(list_chanpin_strategy)):
        strategy_type = list_chanpin_strategy['策略类型'][i]
        
        # 初始化一个标志来表示是否找到匹配的关键词
        found_match = False
        
        # 遍历关键词
        for keyword in keywords:
            # 特殊处理 '指增' 类型
            if keyword == '指增' and '指増' in strategy_type:
                strategy_type = '指增'
                found_match = True
                break
            elif keyword in strategy_type:
                strategy_type = keyword
                found_match = True
                break
        
        # 如果没有找到匹配的关键词，不将该行数据添加到新的列表中，实现删除该行的效果
        if found_match:
            # 将策略类型替换为匹配到的关键词
            row = list_chanpin_strategy.iloc[i].copy()
            row['策略类型'] = strategy_type
            new_list_chanpin_strategy.append(row)
    
    new_list_chanpin_strategy = pd.DataFrame(new_list_chanpin_strategy)
    
    # 使用 groupby 函数按照 '策略类型' 进行分类汇总  
    grouped = new_list_chanpin_strategy.groupby('策略类型')['资产规模'].sum()
    
    # 将结果转换为字典格式  
    api_data = grouped.astype(int).to_dict()
    
    # 确保所有关键词都有值，缺失的用0填充
    for keyword in keywords:
        if keyword not in api_data:
            api_data[keyword] = 0
    
    # 在这里对获取到的数据进行处理和保存
    save_data_to_database(api_data)

def save_data_to_database(api_data):
    # 检查表格是否存在
    table_name = '策略历史规模表'
    check_table_query = f"SHOW TABLES LIKE '{table_name}'"
    cursor.execute(check_table_query)
    cursor.fetchone()
    # 表格已存在，直接插入或更新数据
    today = date.today()
    insert_query = f"INSERT INTO {table_name} (净值日期, 指增, CTA, 多策略, 中性, 套利, 期权, 雪球, 通道, 专户) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s) " \
                   f"ON DUPLICATE KEY UPDATE 指增=VALUES(指增), CTA=VALUES(CTA), 多策略=VALUES(多策略), 中性=VALUES(中性), " \
                   f"套利=VALUES(套利), 期权=VALUES(期权), 雪球=VALUES(雪球), 通道=VALUES(通道), 专户=VALUES(专户)"
    data_values = [today] + [api_data[key] for key in ['指增', 'CTA', '多策略', '中性', '套利', '期权', '雪球', '通道', '专户']]
    cursor.execute(insert_query, data_values)
    # SQL查询以删除重复行，只保留每个日期的最后一行
    try:
        remove_duplicates_query = f"DELETE t1 FROM {table_name} t1 " \
                                  f"JOIN {table_name} t2 " \
                                  f"ON t1.净值日期 = t2.净值日期 AND t1.净值日期 < t2.净值日期"
        cursor.execute(remove_duplicates_query)
        print("衍盛产品规模Duplicate rows removed.")
    except:
        pass
    cnx.commit()
    logging.info(f"Data inserted/updated for date: {today}")

# 调用函数获取数据并保存
fetch_and_save_data()

# 关闭连接
engine.dispose()
cnx.close()
