import sys
sys.path.append('/home/ubuntu/market/dc-66-python-2.0/SelfModule')

from selenium import webdriver
import warnings
import mysql.connector
from selenium.webdriver.chrome.options import Options
from sqlalchemy import create_engine
import matplotlib.pyplot as plt
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import pandas as pd
from datetime import datetime, timedelta
from tradedate import *  # 自定义封装函数
from downloadindex import BenchmarkIndex_mapping # 自定义封装函数
from selfdefinfunc import BenchmarkIndex
import time
import os
import random
import logging

# 设置日志
logging.basicConfig(filename='/home/ubuntu/market/dc-66-python-2.0/execution.log', 
                    level=logging.INFO, 
                    format='%(asctime)s - %(levelname)s - %(message)s', 
                    filemode='a')  # 设置为追加模式

plt.rcParams['font.sans-serif'] = ['SimHei']  # 用来正常显示中文标签
plt.rcParams['axes.unicode_minus'] = False  # 用来正常显示负号
warnings.filterwarnings('ignore')

# 环境变量读取数据库秘钥信息
host = os.environ.get('MYSQL_HOST')   
user = os.environ.get('MYSQL_USERNAME')  
password = os.environ.get('MYSQL_PASSWORD')
port = os.environ.get('MYSQL_PORT')

# 数据库存储操作
cnx = mysql.connector.connect(user=user, password=password,
                              host=host,  port=port, database='同业产品数据库')
engine = create_engine(f'mysql+mysqlconnector://{user}:{password}@{host}:{port}/同业产品数据库')
cursor = cnx.cursor()

# 设置是否跳过格上使用备用网址
if_backup_adr = False
# 网址字段定义
if if_backup_adr == True:
    key_adr_word = '备用产品网址'
else:
    key_adr_word = '产品网址'

# 分别对应所有爬虫数据来源
list_all = pd.read_sql_query("SELECT * FROM %s" % '所有产品爬虫信息表', engine)
list_geshang = list_all[list_all['产品网址'].str.contains('licai')].reset_index(drop=True)

# 3. 控制请求频率，添加随机延迟
def random_delay():
    delay = random.uniform(3, 6)
    time.sleep(delay)

# 创建 ChromeOptions 对象
chrome_options = Options()
# chrome_options.add_argument('--headless')
# chrome_options.add_argument('--disable-gpu')
# chrome_options.add_experimental_option('excludeSwitches', ['enable-automation'])
# chrome_options.add_experimental_option('useAutomationExtension', False)

# 创建 Chrome WebDriver，并传入 ChromeOptions
driver = webdriver.Chrome(options=chrome_options) 
time.sleep(2)

# 谷歌浏览器 79和79版本后防止被检测
driver.execute_cdp_cmd("Page.addScriptToEvaluateOnNewDocument", {
    "source": """
    Object.defineProperty(navigator, 'webdriver', {
      get: () => undefined
    })
  """
})
    
# 格上财富爬虫
list = list_geshang
driver.get('https://www.licai.com/')
time.sleep(5)

try:
    driver.find_element(By.XPATH, '//input[@id="checkbox3" and @type="checkbox"]').click()   # 点击合格投资者
    driver.find_element(By.XPATH, '//button[@class="botton-big"]').click()   # 点击合格投资者确认
except:
    pass
time.sleep(3)

try:
    wait = WebDriverWait(driver, 10)
    element = wait.until(EC.element_to_be_clickable((By.XPATH, '//a[contains(text(), "登录 注册")]')))
    element.click()
except:
    pass
time.sleep(1)
driver.refresh()
time.sleep(4)

driver.find_element(By.XPATH,'//span[contains(text(), "账号密码登录")]').click() # 账号密码登录
driver.find_element(By.XPATH,'//input[@placeholder="请输入6-20位字符密码"]').send_keys('930911Wwwcn')        # 输入密码
time.sleep(5)
# 假设 driver 是您已经初始化的 WebDriver 实例
# 使用 By 类来定位元素
elements = driver.find_elements(By.XPATH, '//input[@placeholder="请输入您的手机号"]')

# 检查是否存在第二个元素
if len(elements) > 1:
    # 第二个元素的索引是1
    elements[1].send_keys('13121023419')
else:
    logging.info("没有找到第二个匹配的元素。")
driver.find_element(By.XPATH, '//div[@class="loginBtn" and @data-v-3d295074=""]').click()   # 点击登录
# 确认网页跳转后再执行
random_delay()

# 遍历爬取净值数据 #
n = 0
k = 0
for adr in list['产品网址']:
    try:
        if isinstance(list.loc[n, '最新净值日期'], str):
            list.loc[n, '最新净值日期'] = pd.to_datetime(list.loc[n, '最新净值日期'])
        if list.loc[n, '最新净值日期'] >= time_last_Friday_time:
            logging.info('格上无需更新:' + '{:.2%}'.format((n+1)/len(list)))
            k = k + 1
            n = n + 1
            continue
    except:
        logging.info('新产品没有最新净值日期')

    driver.get(adr)
    time.sleep(3)
    driver.find_element(By.XPATH, '//h2[contains(text(), "历史净值")]').click()
    time.sleep(3)
    # 数据库查询是否存在该表格
    cursor.execute("SHOW TABLES LIKE %s", (list.iloc[n, 0],))
    chanpin_judge = cursor.fetchone() == None
    # 判断是否为新加入产品
    if chanpin_judge:
        # 设置滚动的初始位置
        found_date = driver.find_elements(By.XPATH, '//div[@class="table-msg-title" and text()="成立日期："]/following-sibling::div//span')[0].text
        start_time = time.time()  # 记录开始时间
        while True:
            # 使用JavaScript滚动到当前位置的下一部分
            js = 'document.getElementsByClassName("tbody-wrapper")[0].scrollBy(0, 1000000000);'
            driver.execute_script(js)
            # 模拟等待页面加载
            time.sleep(0.1)
            ddate = driver.find_elements(By.XPATH, '//div[@class="detail-block"][1]/div/div/div/div[@class="table-core-container"]/div/div/table[@class="table table-fixed-height"]/tbody/tr')
            while ddate == []:
                ddate = driver.find_elements(By.XPATH, '//div[@class="detail-block"][1]/div/div/div/div[@class="table-core-container"]/div/div/table[@class="table table-fixed-height"]/tbody/tr')
            if found_date == ddate[-1].text.split()[0]:
                break
            current_time = time.time()
            elapsed_time = current_time - start_time
            if elapsed_time > 90:  # 如果运行时间超过60秒
                logging.info((list.iloc[n, 0] + "已经运行超过90秒"))
                break
        time.sleep(1)
        list_chanpin_date = []
        list_chanpin_netvalue = []
        list_chanpin_netvalue_leiji = []
        list_chanpin_netvalue_cum = []
        update_date = datetime.strptime(found_date, '%Y-%m-%d') + timedelta(-1)
    else:
        # 构建 SQL 查询，选择表的倒数第一行数据
        query = "SELECT 净值日期, 单位净值, 累计净值, 复权净值 FROM {} ORDER BY ROW_NUMBER() OVER() DESC LIMIT 1".format(list.iloc[n, 0])
        # 使用 SQL 查询从数据库中读取倒数第一行数据
        list_chanpin_info = pd.read_sql_query(query, engine)
        list_chanpin_date = list_chanpin_info['净值日期'].tolist()
        list_chanpin_netvalue = list_chanpin_info['单位净值'].tolist()
        list_chanpin_netvalue_leiji = list_chanpin_info['累计净值'].tolist()
        list_chanpin_netvalue_cum = list_chanpin_info['复权净值'].tolist()
        update_date = list_chanpin_date[-1]
        
        ddate = driver.find_elements(By.XPATH, '//div[@class="detail-block"][1]/div/div/div/div[@class="table-core-container"]/div/div/table[@class="table table-fixed-height"]/tbody/tr')
        # 检验所需净值序列的源代码是否全部载入
        while (datetime.strptime(ddate[-1].text.split()[0], '%Y-%m-%d') > update_date) == True:
            logging.info("检验载入源代码")
            ddate = driver.find_elements(By.XPATH, '//div[@class="detail-block"][1]/div/div/div/div[@class="table-core-container"]/div/div/table[@class="table table-fixed-height"]/tbody/tr')
        if datetime.strptime(ddate[0].text.split()[0], '%Y-%m-%d') <= update_date:
            list_all.loc[list_all['产品名称'] == list.loc[n, '产品名称'], '最新净值日期'] = datetime.strptime(ddate[0].text.split()[0], '%Y-%m-%d')
            logging.info(list.iloc[n, 0] + '：目标网址未更新净值')
            n = n + 1
            continue

    # 解析soup内净值数据 #
    for chanpin_info in ddate:
        chanpin_date = datetime.strptime(chanpin_info.text.split()[0], '%Y-%m-%d')
        if chanpin_date > update_date:        
            list_chanpin_date.append(chanpin_date)
            chanpin_netvalue = chanpin_info.text.split()[1]
            list_chanpin_netvalue.append(chanpin_netvalue)
            chanpin_netvalue_cum = chanpin_info.text.split()[2]
            list_chanpin_netvalue_cum.append(chanpin_netvalue_cum)    
            chanpin_netvalue_leiji = chanpin_info.text.split()[3]
            list_chanpin_netvalue_leiji.append(chanpin_netvalue_leiji) 
    if chanpin_judge:
        try:            
            list_chanpin_date = [datetime.strptime(d, '%Y-%m-%d') for d in list_chanpin_date]
        except:
            list_chanpin_date = [d for d in list_chanpin_date]
    
    list_chanpin_info = pd.DataFrame({'净值日期':list_chanpin_date, '单位净值':list_chanpin_netvalue, '累计净值':list_chanpin_netvalue_leiji, '复权净值':list_chanpin_netvalue_cum})
    list_chanpin_info = list_chanpin_info.sort_values(by='净值日期', ascending=True)
    list_chanpin_info.drop_duplicates()
    # 回填爬虫总表数据
    list_all.loc[list_all['产品名称'] == list.loc[n, '产品名称'], '最新净值日期'] = list_chanpin_info.iloc[-1]['净值日期']
    # 匹配业绩基准数据
    list_chanpin_info = BenchmarkIndex(list_chanpin_info, time_last_Friday_time, BenchmarkIndex_mapping[list.loc[n, '策略']])
    # 删除list_chanpin_info的第一行数据
    list_chanpin_info = list_chanpin_info.iloc[1:]    

# 文件保存
    # 将数据插入到数据库表格的最后
    list_chanpin_info.to_sql(name=list.iloc[n, 0], con=engine, if_exists='append', index=False)
    list_all.loc[list_all['产品网址'] == adr, '是否刷新净值'] = '是'
    try:
        if list_chanpin_info.iloc[-1, 0] >= time_last_Friday_time:
            k = k + 1
    except:
        n = n + 1
        continue
    logging.info('格上爬虫进度' + '{:.2%}'.format((n+1)/len(list)))
    logging.info(list.iloc[n, 0] + ':' + '{:.2%}'.format(k/(n+1)))
    n = n + 1

list_all.to_sql(name='所有产品爬虫信息表', con=engine, if_exists='replace', index=False)
