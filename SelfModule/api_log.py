# -*- coding=utf-8
from qcloud_cos import CosConfig
from qcloud_cos import CosS3Client
import sys
import os
import logging

# 设置日志
logging.basicConfig(filename='/home/ubuntu/market/dc-66-python-2.0/execution.log', 
                    level=logging.INFO, 
                    format='%(asctime)s - %(levelname)s - %(message)s', 
                    filemode='a')  # 设置为追加模式

def create_cos_client(secret_id, secret_key, region, token=None, scheme='https'):
    """
    创建并返回一个腾讯云 COS 客户端对象

    :param secret_id: 用户的 SecretId
    :param secret_key: 用户的 SecretKey
    :param region: 桶归属的 region
    :param token: 临时密钥的 token，默认为 None
    :param scheme: 访问 COS 的协议，默认为 'https'
    :return: CosS3Client 对象
    """
    config = CosConfig(Region=region, SecretId=secret_id, SecretKey=secret_key, Token=token, Scheme=scheme)
    client = CosS3Client(config)
    return client
