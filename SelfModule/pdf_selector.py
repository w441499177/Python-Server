import tkinter as tk
from tkinter import filedialog, ttk
from pdf2image import convert_from_path
from PIL import Image, ImageTk

# 全局变量
pdf_image = None 
orig_w, orig_h = 0, 0
scale = 1.0

# 选择文件
def select_file():
    global pdf_filename
    pdf_filename = filedialog.askopenfilename(filetypes=[("PDF 文件", ".pdf")])
    # 选择文件后立即显示图像
    display_pdf()

# 显示PDF
def display_pdf():
    global pdf_image, orig_w, orig_h, image
    pdf_image = None
    # 打开PDF
    images = convert_from_path(pdf_filename)
    
    # 第一页
    image = images[0] 
    
    # 原始尺寸
    w, h = image.size
    if not orig_w:
       orig_w, orig_h = w, h

    # 缩放        
    image = image.resize((int(w * scale), int(h * scale)))
    if pdf_image is None:
        pdf_image = ImageTk.PhotoImage(image)
    else:
        pdf_image.paste(image)
    
    # 设置canvas尺寸
    canvas.config(width=w, height=h) 

    # 显示图像
    canvas.create_image(0, 0, anchor=tk.NW, image=pdf_image)

    # 设置滚动区域 
    canvas.config(scrollregion=(0, 0, w, h))

    # 缓存引用
    canvas.photo = pdf_image

# 缩放事件  
def zoom_in(e):
    global scale
    scale *= 1.1
    # 在缩放事件中重新显示图像
    display_pdf()  
    
def zoom_out(e):
    global scale
    scale *= 0.9
    # 在缩放事件中重新显示图像
    display_pdf()  

def reset_zoom(e):
    global scale
    scale = 1.0
    # 在重置缩放事件中重新显示图像
    display_pdf()  
    
# 滚轮滚动  
def scroll(e):
    canvas.yview_scroll(-1 * int(e.delta / 120), "units")
    # 在滚动事件中重新显示图像
    display_pdf()  

# 获取坐标
def get_coord(e):
    x, y = e.x, e.y
    y_offset = canvas.yview()[0]  # 获取当前垂直滚动偏移
    y += int(y_offset * orig_h * scale)  # 根据滚动偏移调整y坐标
    coord_label["text"] = f"坐标：({x}, {y})"

# 主GUI函数
def main():
    global pdf_filename
    global canvas
    global coord_label
    global image
    pdf_filename = None

    root = tk.Tk()
    canvas = tk.Canvas(root, width=600, height=500)
    canvas.pack(side='left')

    scrollbar = ttk.Scrollbar(root, orient='vertical', command=canvas.yview)
    scrollbar.pack(side='right', fill='y')

    canvas.config(yscrollcommand=scrollbar.set)

    # 显示坐标的Label
    coord_label = tk.Label(root, text="")
    coord_label.pack()

    select_btn = tk.Button(root, text="选择PDF", command=select_file)
    select_btn.pack()

    exit_btn = tk.Button(root, text="退出", command=lambda: [root.quit(), root.destroy()])
    exit_btn.pack()

    # 事件绑定
    canvas.bind("<Button-1>", get_coord)
    root.bind("<Key-Up>", zoom_in)
    root.bind("<Key-Down>", zoom_out)
    root.bind("<Key-r>", reset_zoom)
    canvas.bind_all("<MouseWheel>", scroll)
    
    # 注册窗口关闭时的清理函数
    def cleanup():
        global pdf_image  # 引入全局变量
        if pdf_image:
            pdf_image.__del__()  # 手动释放资源
        root.quit()

    root.protocol("WM_DELETE_WINDOW", cleanup)

    root.mainloop()

if __name__ == "__main__":
    main()