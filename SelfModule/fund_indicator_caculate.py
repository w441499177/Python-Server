import pandas as pd
import numpy as np
from datetime import timedelta

def calculate_fund_metrics(nav_dates, nav_values, periods_dict, benchmark_values=None):
    """
    计算基金的各项指标，包括收益率、最大回撤和波动率。
    
    参数：
        nav_dates (pd.Series): 净值日期序列
        nav_values (pd.Series): 复权净值序列
        periods_dict (dict): 包含各个时间段的起始和结束日期的字典
        benchmark_values (pd.Series, optional): 对标指数序列，默认为 None
    
    返回：
        pd.DataFrame: 包含各项指标的 DataFrame
    """
    def calculate_return(series):
        return series.iloc[-1] / series.iloc[0] - 1
    
    def calculate_max_drawdown(series):
        if len(series) <= 1:
            return 0
        
        max_drawdown = 0
        cummax = series.iloc[0]
        
        for value in series:
            if value > cummax:
                cummax = value
            else:
                drawdown = (value - cummax) / cummax
                if drawdown < max_drawdown:
                    max_drawdown = drawdown
        return max_drawdown
    
    def calculate_volatility(series, trading_days_per_year=252):
        if len(series) <= 1:
            return 0
        
        # 计算实际的日期间距
        actual_days = (nav_dates.iloc[-1] - nav_dates.iloc[0]).days
        if actual_days == 0:
            return 0
        
        returns = series.pct_change()
        volatility = returns.std() * np.sqrt(trading_days_per_year * (actual_days / len(series)))
        return volatility
    
    def calculate_metrics(nav, benchmark=None, calculate_vol_draw=True):
        metrics = {}
        
        metrics['收益率'] = calculate_return(nav)
        
        if calculate_vol_draw:
            metrics['最大回撤'] = calculate_max_drawdown(nav)
            metrics['波动率'] = calculate_volatility(nav)
        
        if benchmark is not None:
            metrics[f"对标指数{period_name}收益率"] = calculate_return(benchmark)
            metrics['超额（除法）'] = calculate_return(nav / benchmark)
            
            if calculate_vol_draw:
                metrics['最大回撤（超额）'] = calculate_max_drawdown(nav / benchmark)
                metrics['超额波动率'] = calculate_volatility(nav / benchmark)
        
        return metrics
    
    metrics_dict = {}
    
    for period_name, (start_date, end_date) in periods_dict.items():
        mask = (nav_dates >= start_date) & (nav_dates <= end_date)
        nav_period = nav_values[mask]
        
        calculate_vol_draw = True
        if period_name in ['近一周', '近四周']:
            calculate_vol_draw = False
        
        if benchmark_values is not None:
            benchmark_period = benchmark_values[mask]
        else:
            benchmark_period = None
        
        metrics = calculate_metrics(nav_period, benchmark_period, calculate_vol_draw)
        
        if nav_dates[0] > start_date:
            for metric_name, value in metrics.items():
                if '对标指数' not in metric_name:
                    metrics_dict[f"{period_name}{metric_name}"] = np.nan
                else:
                    metrics_dict[metric_name] = np.nan
            continue
        
        for metric_name, value in metrics.items():
            if '对标指数' not in metric_name:
                metrics_dict[f"{period_name}{metric_name}"] = value
            else:
                metrics_dict[metric_name] = value
    
    return pd.DataFrame(metrics_dict, index=[0])



def Calculate_Fund_Metrics_With_Series(nav_dates, nav_values, periods_dict, benchmark_values=None):
    """
    返回所有计算结果序列的封装函数。
    
    参数：
        nav_dates (pd.Series): 净值日期序列
        nav_values (pd.Series): 复权净值序列
        periods_dict (dict): 包含各个时间段的起始和结束日期的字典
        benchmark_values (pd.Series, optional): 对标指数序列，默认为 None
    
    返回：
        pd.DataFrame: 包含所有计算结果指标列的 DataFrame
    """
    def calculate_return_series(series):
        returns = series.pct_change().fillna(0)
        cumulative_returns = (1 + returns).cumprod() - 1
        return cumulative_returns
    
    def calculate_max_drawdown_series(series):
        if len(series) <= 1:
            return pd.Series([0] * len(series), index=series.index)
        
        max_drawdown = []
        cummax = series.iloc[0]
        
        for value in series:
            if value > cummax:
                cummax = value
            drawdown = (value - cummax) / cummax
            max_drawdown.append(drawdown)
        
        return pd.Series(max_drawdown, index=series.index)
    
    def calculate_metrics_series(nav, benchmark=None, calculate_vol_draw=True):
        metrics = {}
        
        metrics['收益率'] = calculate_return_series(nav)
        
        if calculate_vol_draw:
            metrics['最大回撤'] = calculate_max_drawdown_series(nav)
        
        if benchmark is not None and not benchmark.isnull().any():
            metrics[f"对标指数{period_name}收益率"] = calculate_return_series(benchmark)
            metrics['超额（除法）'] = calculate_return_series(nav / benchmark)
            
            if calculate_vol_draw:
                metrics['最大回撤（超额）'] = calculate_max_drawdown_series(nav / benchmark)
        else:
            metrics[f"对标指数{period_name}收益率"] = np.nan
            metrics['超额（除法）'] = np.nan
            metrics['最大回撤（超额）'] = np.nan
        
        return metrics
    
    metrics_series_dict = {}
    
    for period_name, (start_date, end_date) in periods_dict.items():
        mask = (nav_dates >= start_date) & (nav_dates <= end_date)
        nav_period = nav_values[mask]
        
        calculate_vol_draw = True
        if period_name in ['近一周', '近四周']:
            calculate_vol_draw = False
        
        if benchmark_values is not None:
            benchmark_period = benchmark_values[mask]
        else:
            benchmark_period = None
        
        # 检查 nav_dates 中最晚的日期是否晚于 start_date
        if nav_dates.max() <= start_date:
            metric_names = ['收益率']
            metric_names.extend([f"对标指数{period_name}收益率", '超额（除法）'])
            if calculate_vol_draw:
                metric_names.extend(['最大回撤'])
                metric_names.extend(['最大回撤（超额）'])
            
            for metric_name in metric_names:
                if metric_name != f"对标指数{period_name}收益率":
                    metrics_series_dict[f"{period_name}{metric_name}"] = pd.Series([np.nan] * len(nav_period), index=nav_period.index)
                else:
                    metrics_series_dict[f"{metric_name}"] = pd.Series([np.nan] * len(nav_period), index=nav_period.index)
            continue
        
        metrics_series = calculate_metrics_series(nav_period, benchmark_period, calculate_vol_draw)
        
        if nav_dates[0] > start_date:
            for metric_name, value in metrics_series.items():
                if '对标指数' not in metric_name:
                    metrics_series_dict[f"{period_name}{metric_name}"] = pd.Series([np.nan] * len(nav_period), index=nav_period.index)
                else:
                    metrics_series_dict[metric_name] = pd.Series([np.nan] * len(nav_period), index=nav_period.index)
            continue
        
        for metric_name, value in metrics_series.items():
            if '对标指数' not in metric_name:
                metrics_series_dict[f"{period_name}{metric_name}"] = value
            else:
                metrics_series_dict[metric_name] = value
    
    # 将所有序列合并成一个 DataFrame
    metrics_df = pd.DataFrame(metrics_series_dict)
    
    return metrics_df


def Calculate_Weekly_Indicator_Metrics(list_chanpin_info, date_found_chanpin, weekly_date):
    """
    计算剩余的时序指标。
    
    参数：
        list_chanpin_info (pd.DataFrame): 包含净值和各列数据指标的 DataFrame
        date_found_chanpin (datetime): 产品成立日期
        weekly_date (pd.DataFrame): 包含完整周末净值和月末净值的 DataFrame
    
    返回：
        pd.DataFrame: 包含计算结果的 DataFrame
    """
    # 计算年化收益率
    list_chanpin_info['净值日期'] = pd.to_datetime(list_chanpin_info['净值日期'])
    list_chanpin_info.loc[list_chanpin_info['净值日期'] - date_found_chanpin < timedelta(365), '累计年化收益率'] = list_chanpin_info.loc[list_chanpin_info['净值日期'] - date_found_chanpin < timedelta(365), '成立以来收益率']
    list_chanpin_info.loc[list_chanpin_info['净值日期'] - date_found_chanpin >= timedelta(365), '累计年化收益率'] = \
        (list_chanpin_info.loc[list_chanpin_info['净值日期'] - date_found_chanpin >= timedelta(365), '成立以来收益率'] + 1) \
        **(timedelta(365)/(list_chanpin_info.loc[list_chanpin_info['净值日期'] - date_found_chanpin >= timedelta(365), '净值日期'] - date_found_chanpin)) - 1
    
    # 检查是否有对标指数数据
    has_benchmark = not list_chanpin_info['对标指数'].isnull().all()
    
    if has_benchmark:
        list_chanpin_info.loc[list_chanpin_info['净值日期'] - date_found_chanpin < timedelta(365), '累计年化超额收益率'] = list_chanpin_info.loc[list_chanpin_info['净值日期'] - date_found_chanpin < timedelta(365), '成立以来超额（除法）']
        list_chanpin_info.loc[list_chanpin_info['净值日期'] - date_found_chanpin >= timedelta(365), '累计年化超额收益率'] = \
            (list_chanpin_info.loc[list_chanpin_info['净值日期'] - date_found_chanpin >= timedelta(365), '成立以来超额（除法）'] + 1) \
            **(timedelta(365)/(list_chanpin_info.loc[list_chanpin_info['净值日期'] - date_found_chanpin >= timedelta(365), '净值日期'] - date_found_chanpin)) - 1
    else:
        list_chanpin_info.loc[:, '累计年化超额收益率'] = np.nan
    
    # 筛选 weekly_date 中 '是否周末净值' 为 '是' 的行
    weekly_date_filtered = weekly_date[weekly_date['是否周末净值'] == '是']
    
    # 合并 weekly_date 和 list_chanpin_info，保留所有列
    merged_data_weekly = pd.merge(weekly_date_filtered, list_chanpin_info, on='净值日期', how='left')
    merged_data = pd.merge(weekly_date, list_chanpin_info, on='净值日期', how='left')
    
    # 计算本周收益率
    merged_data_weekly['本周收益率'] = np.where(
        merged_data_weekly['是否周末净值_x'] == '是',  # 使用 _x 后缀的列
        merged_data_weekly['复权净值'] / merged_data_weekly['复权净值'].shift(1) - 1,
        np.nan
    )
    
    # 计算本月收益率
    merged_data['本月收益率'] = np.where(
        merged_data['是否月末净值_x'] == '是',  # 使用 _x 后缀的列
        merged_data['复权净值'] / merged_data['复权净值'].shift(1) - 1,
        np.nan
    )
    
    # 计算本年收益率
    merged_data['本年收益率'] = np.where(
        merged_data['是否年末净值_x'] == '是',  # 使用 _x 后缀的列
        merged_data['复权净值'] / merged_data['复权净值'].shift(1) - 1,
        np.nan
    )
    
    if has_benchmark:
        # 计算本周超额
        merged_data_weekly['本周超额'] = np.where(
            merged_data_weekly['是否周末净值_x'] == '是',  # 使用 _x 后缀的列
            merged_data_weekly['本周收益率'] - (merged_data_weekly['对标指数'] / merged_data_weekly['对标指数'].shift(1) - 1),
            np.nan
        )
        
        # 计算本月超额
        merged_data['本月超额'] = np.where(
            merged_data['是否月末净值_x'] == '是',  # 使用 _x 后缀的列
            merged_data['本月收益率'] - (merged_data['对标指数'] / merged_data['对标指数'].shift(1) - 1),
            np.nan
        )
        
        # 计算本年超额
        merged_data['本年超额'] = np.where(
            merged_data['是否年末净值_x'] == '是',  # 使用 _x 后缀的列
            merged_data['本年收益率'] - (merged_data['对标指数'] / merged_data['对标指数'].shift(1) - 1),
            np.nan
        )
    else:
        merged_data_weekly['本周超额'] = np.nan
        merged_data['本月超额'] = np.nan
        merged_data['本年超额'] = np.nan
    
    # 将计算结果返回赋值给 list_chanpin_info
    for index, row in merged_data_weekly.iterrows():
        if row['净值日期'] in list_chanpin_info['净值日期'].values:
            list_chanpin_info.loc[list_chanpin_info['净值日期'] == row['净值日期'], '本周收益率'] = row['本周收益率']
            list_chanpin_info.loc[list_chanpin_info['净值日期'] == row['净值日期'], '本周超额'] = row['本周超额']
    
    # 将计算结果返回赋值给 list_chanpin_info
    for index, row in merged_data.iterrows():
        if row['净值日期'] in list_chanpin_info['净值日期'].values:
            list_chanpin_info.loc[list_chanpin_info['净值日期'] == row['净值日期'], '本月收益率'] = row['本月收益率']
            list_chanpin_info.loc[list_chanpin_info['净值日期'] == row['净值日期'], '本年收益率'] = row['本年收益率']
            list_chanpin_info.loc[list_chanpin_info['净值日期'] == row['净值日期'], '本月超额'] = row['本月超额']
            list_chanpin_info.loc[list_chanpin_info['净值日期'] == row['净值日期'], '本年超额'] = row['本年超额']
    
    # 计算夏普比率、卡玛比率、索提诺比率
    for d in range(len(list_chanpin_info)):
        try:
            weekly_returns = list_chanpin_info.loc[list_chanpin_info['是否周末净值'] == '是', '本周收益率']
            volatility_chanpin = np.std(weekly_returns) * np.sqrt(52)
            volatility_down_chanpin = np.std(weekly_returns[weekly_returns < 0]) * np.sqrt(52)
            list_chanpin_info.loc[d, '夏普比率'] = round(list_chanpin_info.loc[d, '累计年化收益率'] / volatility_chanpin, 2)
            list_chanpin_info.loc[d, '索提诺比率'] =  round(list_chanpin_info.loc[d, '累计年化收益率'] / volatility_down_chanpin, 2)
            list_chanpin_info.loc[d, '卡玛比率'] = round(list_chanpin_info.loc[d, '累计年化收益率'] / - list_chanpin_info.loc[0:d, '成立以来最大回撤'].min(skipna=True), 2)
            
            if has_benchmark:
                weekly_excess_returns = list_chanpin_info.loc[list_chanpin_info['是否周末净值'] == '是', '本周超额']
                volatility_excess_chanpin = np.std(weekly_excess_returns) * np.sqrt(52)
                volatility_excess_down_chanpin = np.std(weekly_excess_returns[weekly_excess_returns < 0]) * np.sqrt(52)
                list_chanpin_info.loc[d, '超额夏普比率'] = round(list_chanpin_info.loc[d, '累计年化超额收益率'] / volatility_excess_chanpin, 2)
                list_chanpin_info.loc[d, '超额索提诺比率'] = round(list_chanpin_info.loc[d, '累计年化超额收益率'] / volatility_excess_down_chanpin, 2)
                list_chanpin_info.loc[d, '超额卡玛比率'] = round(list_chanpin_info.loc[d, '累计年化超额收益率'] / - list_chanpin_info.loc[0:d, '成立以来最大回撤（超额）'].min(skipna=True), 2)
            else:
                list_chanpin_info.loc[:, '超额夏普比率'] = np.nan
                list_chanpin_info.loc[:, '超额索提诺比率'] = np.nan
                list_chanpin_info.loc[:, '超额卡玛比率'] = np.nan
        except:
            list_chanpin_info.loc[d, '夏普比率'] = np.nan
            list_chanpin_info.loc[d, '卡玛比率'] = np.nan
            list_chanpin_info.loc[d, '索提诺比率'] = np.nan
            list_chanpin_info.loc[d, '超额夏普比率'] = np.nan
            list_chanpin_info.loc[d, '超额卡玛比率'] = np.nan
            list_chanpin_info.loc[d, '超额索提诺比率'] = np.nan
    
    # 计算周胜率、月胜率、周超额胜率、月超额胜率
    list_chanpin_info['周胜率'] = (list_chanpin_info['本周收益率'] > 0).rolling(window=len(list_chanpin_info['本周收益率'].dropna()), min_periods=1).sum() / (list_chanpin_info['本周收益率'].notna()).rolling(window=len(list_chanpin_info['本周收益率'].dropna()), min_periods=1).sum()
    try:
        list_chanpin_info['月胜率'] = (list_chanpin_info['本月收益率'] > 0).rolling(window=len(list_chanpin_info['本月收益率'].dropna()), min_periods=1).sum() / (list_chanpin_info['本月收益率'].notna()).rolling(window=len(list_chanpin_info['本月收益率'].dropna()), min_periods=1).sum()
    except:
        list_chanpin_info['月胜率'] = np.nan
    
    if has_benchmark:
        list_chanpin_info['超额周胜率'] = (list_chanpin_info['本周超额'] > 0).rolling(window=len(list_chanpin_info['本周超额'].dropna()), min_periods=1).sum() / (list_chanpin_info['本周超额'].notna()).rolling(window=len(list_chanpin_info['本周超额'].dropna()), min_periods=1).sum()
        try:
            list_chanpin_info['超额月胜率'] = (list_chanpin_info['本月超额'] > 0).rolling(window=len(list_chanpin_info['本月超额'].dropna()), min_periods=1).sum() / (list_chanpin_info['本月超额'].notna()).rolling(window=len(list_chanpin_info['本月超额'].dropna()), min_periods=1).sum()
        except:
            list_chanpin_info['超额月胜率'] = np.nan
    else:
        list_chanpin_info['超额周胜率'] = np.nan
        list_chanpin_info['超额月胜率'] = np.nan
    
    # 替换 -inf 为 NaN
    list_chanpin_info = list_chanpin_info.replace(to_replace=['', None, -np.inf, np.inf], value=float(np.nan), inplace=False).round(4)
    
    return list_chanpin_info