# -*- coding: utf-8 -*-
"""
Created on Sat Feb 18 22:49:04 2023

@author: wanghui
"""
import numpy as np
import pandas as pd
import calendar
from datetime import datetime, date, timedelta
from chinese_calendar import is_workday
from matplotlib.backends.backend_agg import FigureCanvasAgg
from PIL import Image
import logging

# 设置日志
logging.basicConfig(filename='/home/ubuntu/market/dc-66-python-2.0/execution.log', 
                    level=logging.INFO, 
                    format='%(asctime)s - %(levelname)s - %(message)s', 
                    filemode='a')  # 设置为追加模式

# 图表坐标轴间隔分档函数
def Locator_Len(gap):
    if gap/6 > 0.4:
        locator = 0.5 
    elif gap/6 > 0.3:
        locator = 0.5
    elif gap/6 > 0.15:
        locator = 0.4
    elif gap/6 > 0.1:
        locator = 0.2
    elif gap/6 > 0.05:
        locator = 0.1
    elif gap/6 > 0.03:
        locator = 0.05
    elif gap/6 > 0.01:
        locator = 0.02
    else:
        locator = 0.01
    return(locator)

# 目标产品对应净值日期搜索函数
def Search_NavDate(df, dt, date_found_chanpin, time_time_last_Friday_lag4_time):
    def Recent_TradeDay(date):
        for i in range(10):
            if (is_workday(date) == False) or (date.weekday() == 5) or (date.weekday() == 6):
                date = date + timedelta(days=-1)
            else:
                break
        return date

    # 产品目标净值日检索 #
    if date_found_chanpin <= dt:
        temp = df.loc[(df['净值日期'] <= dt)]
        m= -1 
        last_date = temp.values[m][0]
        while Recent_TradeDay(temp.values[m][0]) != temp.values[m][0]:
            m = m - 1
        # 确定去年最后一个净值日是否为交易日 #
        m = m - 1
        while is_workday(temp.values[m+1][0]) == True:
            if dt == time_time_last_Friday_lag4_time:
                time_chanpin = temp.values[m+1][0]
            else:
                time_chanpin = temp.values[m+1][0]
            if time_chanpin == Recent_TradeDay(time_chanpin):
                break
            else:            
                m = m - 1
    else:
        time_chanpin = ''
    return time_chanpin

def Render_Mpl_to_Pil(fig):
    """
    将matplotlib图像渲染为PIL图像。
    
    参数:
    - fig: matplotlib的Figure对象。
    
    返回:
    - PIL图像对象。
    """
    # 创建一个FigureCanvasAgg对象，用于渲染图形
    canvas = FigureCanvasAgg(fig)
    
    # 渲染图形到指定的区域
    canvas.draw()
    
    # 获取渲染后的图像数据，并将其转换为RGB模式
    buf = canvas.buffer_rgba()
    
    # 将渲染后的图像数据转换为PIL Image对象
    pil_image = Image.frombytes("RGBA", canvas.get_width_height(), buf, "raw", "RGBA")
    
    return pil_image

def TradeType(shenshu, lst_data, lst_moban):
    if shenshu == '分红' or shenshu == '现金分红':
        lst_moban['累计分红'] = lst_moban['累计分红'] + lst_data.get('确认金额')
        lst_moban['已实现收益'] = lst_moban['已实现收益'] + lst_data.get('确认金额')
    elif shenshu == '提取超额收益':
        lst_moban['已实现收益'] = lst_moban['已实现收益'] - lst_data.get('确认金额')
        lst_moban['累计提取业绩报酬'] = lst_moban['累计提取业绩报酬'] + lst_data.get('确认金额')
    elif shenshu == '红利再投':
        lst_moban['已实现收益'] = lst_moban['已实现收益'] - lst_data.get('确认金额')
        lst_moban['持有份额'] = lst_moban['持有份额'] + lst_data.get('确认份额')
    elif shenshu == '认购' or shenshu == '申购' or shenshu == '超额收益' or shenshu == '份额受让':
        lst_moban['投入本金'] = lst_moban['投入本金'] + lst_data.get('确认金额')
        lst_moban['持有份额'] = lst_moban['持有份额'] + lst_data.get('确认份额')
        lst_moban['累计申购'] = lst_moban['累计申购'] + lst_data.get('确认金额')
        lst_moban['单位成本'] = lst_moban['投入本金']/lst_moban['持有份额']
    elif shenshu == '赎回' or shenshu == '份额转让':
        lst_moban['累计赎回'] = lst_moban['累计赎回'] + lst_data.get('确认金额')
        lst_moban['单位成本'] = lst_moban['投入本金']/lst_moban['持有份额']
        lst_moban['持有份额'] = lst_moban['持有份额'] + lst_data.get('确认份额')
        shuhui_yingli = (lst_data['基金净值'] - lst_moban['单位成本'])*(-lst_data.get('确认份额'))
        lst_moban['已实现收益'] = lst_moban['已实现收益'] + shuhui_yingli
        lst_moban['投入本金'] = lst_moban['投入本金'] - (lst_data.get('确认金额') - shuhui_yingli)
        if lst_moban['持有份额'] < 10000:
            lst_moban['持有份额'] = 0
        if lst_moban['投入本金'] < 10000:
            lst_moban['投入本金'] = 0
    elif shenshu == '封闭期结束份额折算':
        lst_moban['持有份额'] = lst_moban['持有份额'] + lst_data.get('确认份额')
    
    lst_moban['持有总金额'] = lst_moban['持有份额']*lst_moban['单位净值']
    if lst_moban['持有份额'] != 0:
        lst_moban['单位成本'] = lst_moban['投入本金']/lst_moban['持有份额']
    else:
        lst_moban['单位成本'] = 0
    lst_moban['未实现收益'] = lst_moban['持有总金额'] - lst_moban['投入本金']
    return lst_moban


def BenchmarkIndex(list_chanpin_info, time_last_Friday_time, BenchmarkData):
    if isinstance(BenchmarkData, pd.DataFrame):
        index_last_Friday = float((BenchmarkData.loc[BenchmarkData['date'] == time_last_Friday_time, ['close']]).values[0][0])
        list_chanpin_info = pd.merge(list_chanpin_info, BenchmarkData.rename(columns={'date':'净值日期'}), on='净值日期')
        list_chanpin_info = list_chanpin_info.rename(columns={'close':'对标指数'})
    elif isinstance(BenchmarkData, float):
        list_chanpin_info['对标指数'] = np.nan
    elif isinstance(BenchmarkData, str):
        pass
    return list_chanpin_info

def Calculate_Reinvested_Nav(df: pd.DataFrame, unit_nav_col: str, cumulative_nav_col: str) -> pd.Series:
    """
    根据单位净值和累计净值计算分红再投资后的复权净值数据。

    参数:
    - df: 包含单位净值和累计净值的DataFrame。
    - unit_nav_col: 单位净值所在列的列名。
    - cumulative_nav_col: 累计净值所在列的列名。

    返回:
    - 包含计算得到的复权净值数据的Series。
    """
    reinvested_nav = pd.Series(index=df.index, dtype=float)
    reinvested_nav.iloc[0] = df[unit_nav_col].iloc[0]  # 初始复权净值等于初始单位净值

    for i in range(1, len(df)):
        cumulative_return = (df[unit_nav_col].iloc[i] + (df[cumulative_nav_col].iloc[i] - df[unit_nav_col].iloc[i] - (df[cumulative_nav_col].iloc[i-1] - df[unit_nav_col].iloc[i-1]))) / df[unit_nav_col].iloc[i - 1]
        reinvested_nav.iloc[i] = reinvested_nav.iloc[i - 1] * cumulative_return
    # 整体保留四位小数
    reinvested_nav = reinvested_nav.round(4)
    return reinvested_nav

def Calculate_Reinvested_Nav_Process(df: pd.DataFrame, unit_nav_col: str, cumulative_nav_col: str, adv_nav_col: str) -> pd.Series:
    """
    根据单位净值和累计净值计算分红再投资后的复权净值数据。

    参数:
    - df: 包含单位净值和累计净值的DataFrame。
    - unit_nav_col: 单位净值所在列的列名。
    - cumulative_nav_col: 累计净值所在列的列名。

    返回:
    - 包含计算得到的复权净值数据的Series。
    """
    reinvested_nav = pd.Series(index=df.index, dtype=float)
    reinvested_nav.iloc[0] = df[adv_nav_col].iloc[0]  # 初始复权净值等于初始单位净值

    for i in range(1, len(df)):
        cumulative_return = (df[unit_nav_col].iloc[i] + (df[cumulative_nav_col].iloc[i] - df[unit_nav_col].iloc[i] - (df[cumulative_nav_col].iloc[i-1] - df[unit_nav_col].iloc[i-1]))) / df[unit_nav_col].iloc[i - 1]
        reinvested_nav.iloc[i] = reinvested_nav.iloc[i - 1] * cumulative_return
    # 整体保留四位小数
    reinvested_nav = reinvested_nav.round(4)
    return reinvested_nav

def check_a_share_trading_day():
    # 获取当前日期
    today = date.today()
    
    # 检查是否为法定工作日
    if_workday = is_workday(today)
    
    # 检查是否为周一到周五
    if_weekday = today.weekday() < 5
    
    # 两个条件同时满足才是交易日
    if if_workday and if_weekday:
        pass
    else:
        logging.info("今天不是A股交易日")
        exit()  # 停止脚本运行

# 检查净值准确度
def get_precision_factor(dataframe, column_name, recent_count=10):
    # 获取指定列的最近几个值
    recent_values = dataframe[column_name].dropna().tail(recent_count).unique()
    
    # 初始化最大小数位数
    max_decimal_places = 0
    
    # 遍历最近几个值，找到最大小数位数
    for value in recent_values:
        decimal_places = len(str(value).split('.')[1]) if '.' in str(value) else 0
        if decimal_places > max_decimal_places:
            max_decimal_places = decimal_places
    
    # 返回对应的精度因子
    return max_decimal_places

# 处理清洗净值序列
def Process_Nav_Dataframe(list_chanpin_info):
    # 剔除重复净值日期的行，保留序号靠后的数据所在行
    list_chanpin_info = list_chanpin_info.drop_duplicates(subset='净值日期', keep='last')

    # 对净值日期进行升序排列
    list_chanpin_info = list_chanpin_info.sort_values('净值日期').reset_index(drop=True)

    # 获取净值日期范围
    start_date = list_chanpin_info['净值日期'].min()
    end_date = list_chanpin_info['净值日期'].max()
    all_dates = pd.date_range(start=start_date, end=end_date)

    # 过滤出中国工作日日期的数据（排除周末和非工作日）
    valid_dates = [date for date in all_dates if is_workday(date) and date.weekday() < 5]
    list_chanpin_info = list_chanpin_info[list_chanpin_info['净值日期'].isin(valid_dates)]

    # 将'单位净值'、'累计净值'和'复权净值'列转换为float格式
    for column in ['单位净值', '累计净值']:
        if column in list_chanpin_info.columns:
            list_chanpin_info[column] = list_chanpin_info[column].astype(float)

    # 使用try对'复权净值'进行调整
    try:
        list_chanpin_info['复权净值'] = list_chanpin_info['复权净值'].astype(float)
    except KeyError:
        pass  # 如果'复权净值'列不存在，则跳过

    return list_chanpin_info

# 检查净值准确度
def get_precision_factor(dataframe, column_name, recent_count=10):
    # 获取指定列的最近几个值
    recent_values = dataframe[column_name].dropna().tail(recent_count).unique()
    
    # 初始化最大小数位数
    max_decimal_places = 0
    
    # 遍历最近几个值，找到最大小数位数
    for value in recent_values:
        decimal_places = len(str(value).split('.')[1]) if '.' in str(value) else 0
        if decimal_places > max_decimal_places:
            max_decimal_places = decimal_places
    
    # 返回对应的精度因子
    return max_decimal_places