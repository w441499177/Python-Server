import numpy as np
import pandas as pd
import akshare as ak
import os
from datetime import datetime, date, timedelta
import mysql.connector
from sqlalchemy import create_engine
import sys

# 环境变量读取数据库秘钥信息
host = os.environ.get('MYSQL_HOST')   
user = os.environ.get('MYSQL_USERNAME')  
password = os.environ.get('MYSQL_PASSWORD')
port = os.environ.get('MYSQL_PORT')
engine = create_engine(f'mysql+mysqlconnector://{user}:{password}@{host}:{port}/指数行情数据库')

for var_name in ['data_CSI50', 'data_CSI300', 'data_CSI500', 'data_CSI1000', 'data_CSI800', 'data_GZ2000', "data_CSI2000", "data_CSI_total", "data_NHCI", "data_CSIA500"]:
    if var_name in globals():
        del globals()[var_name]

def DownloadIndex():
    # 清除原有变量名
    global data_CSI50, data_CSI300, data_CSI500, data_CSI1000, data_CSI800, data_GZ2000, data_CSI2000, data_CSI_total, data_NHCI, data_CSIA500

    # SQL 查询语句
    query = """
    SELECT 日期 AS date, 收盘 AS close
    FROM 南华商品指数
    """
    # 执行查询并将结果读取到 DataFrame
    data_NHCI = pd.read_sql(query, engine)
    # 获取今天日期并减去一天
    # 获取当前时间
    now = datetime.now()
    
    # 如果当前时间大于下午3点10分，则减去一天
    if now.hour > 15 or (now.hour == 15 and now.minute > 10):
        today = now 
    else:
        today = now - timedelta(days=1)
    # 将日期格式化为指定的字符串格式
    end_date = today.strftime('%Y%m%d')
    # 下载指数数据
    symbol_zhongzheng_index =  pd.DataFrame({
        "证券代码": ["000016", "000300", "000905", "000852", "000906", "399303", "932000", "000985", "000510"],
        "指数表名": ["data_CSI50", "data_CSI300", "data_CSI500", "data_CSI1000", "data_CSI800", "data_GZ2000", "data_CSI2000", "data_CSI_total", "data_CSIA500"],
        "证券名称": ["上证50", "沪深300", "中证500", "中证1000", "中证800", "国证2000", "中证2000", "中证全指", "中证A500"]
        })
    n = 0
    for i, index_name in zip(symbol_zhongzheng_index["证券代码"], symbol_zhongzheng_index["证券名称"]):
        zhongzheng_index_df = ak.index_zh_a_hist(symbol=i, period="daily", start_date="20091231", end_date=end_date)
        zhongzheng_index_df['日期'] = pd.to_datetime(zhongzheng_index_df['日期'])
        zhongzheng_index_df = zhongzheng_index_df.iloc[:, [0,2]].rename(columns={'日期': 'date', '收盘': 'close'})
        if i == "000016":
            data_CSI50 = zhongzheng_index_df.copy()
        elif i == "000300":
            data_CSI300 = zhongzheng_index_df.copy()
        elif i == "000905":
            data_CSI500 = zhongzheng_index_df.copy()
        elif i == "000852":
            data_CSI1000 = zhongzheng_index_df.copy()
        elif i == "000906":
            data_CSI800 = zhongzheng_index_df.copy()
        elif i == "399303":
            data_GZ2000 = zhongzheng_index_df.copy()
        elif i == "932000":
            data_CSI2000 = zhongzheng_index_df.copy()
        elif i == "000985":
            data_CSI_total = zhongzheng_index_df.copy()
        elif i == "000510":
            data_CSIA500 = zhongzheng_index_df.copy()

DownloadIndex()

BenchmarkIndex_mapping = {
    '300指增': data_CSI300,
    '500指增': data_CSI500,
    '1000指增': data_CSI1000,
    '国证2000指增': data_GZ2000,
    '空气指增': data_GZ2000,
    'A500指增': data_CSIA500,
    '2000指增': data_CSI2000,
    '中证全指指增': data_CSI_total,
    '杠杆指增': data_CSI_total,
    '南华商品指增': data_NHCI,
    '主观多头': data_CSI800,
    '100亿元以上': data_CSI800,
    '50-100亿元': data_CSI800,
    '50亿元以下': data_CSI800,
    'CTA': np.nan,
    '低频CTA': np.nan,
    '中频CTA': np.nan,
    '高频CTA': np.nan,
    '中长周期': np.nan,
    '中周期': np.nan,
    '中短周期': np.nan,
    '主观期货': np.nan,
    '债券': np.nan,
    '高收益债': np.nan,
    '利率债': np.nan,
    '混合债券': np.nan,
    '其他': np.nan,
    '宏观策略': np.nan,
    '多策略': np.nan,
    '套利': np.nan,
    '期权': np.nan,
    '其他衍生品': np.nan,
    '雪球': np.nan,
    'DMA': np.nan,
    '杠杆中性': np.nan,
    '中性': np.nan,
    '通道': np.nan,
    '专户': np.nan
}