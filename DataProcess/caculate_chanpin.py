import sys
sys.path.append('/home/ubuntu/market/dc-66-python-2.0/SelfModule')

import warnings
import mysql.connector
from sqlalchemy import create_engine
import numpy as np
import pandas as pd
from datetime import datetime, date, timedelta
from tradedate import *  # 自定义封装函数
from downloadindex import BenchmarkIndex_mapping, data_CSI1000  # 自定义封装函数
import os
import logging

# 设置日志
logging.basicConfig(filename='/home/ubuntu/market/dc-66-python-2.0/execution.log', 
                    level=logging.INFO, 
                    format='%(asctime)s - %(levelname)s - %(message)s', 
                    filemode='a')  # 设置为追加模式

warnings.filterwarnings('ignore')

# 环境变量读取数据库秘钥信息
host = os.environ.get('MYSQL_HOST')   
user = os.environ.get('MYSQL_USERNAME')  
password = os.environ.get('MYSQL_PASSWORD')
port = os.environ.get('MYSQL_PORT')

# 数据库存储操作
cnx = mysql.connector.connect(user=user, password=password,
                              host=host,  port=port, database='同业产品数据库')
engine = create_engine(f'mysql+mysqlconnector://{user}:{password}@{host}:{port}/同业产品数据库')
engine_index = create_engine(f'mysql+mysqlconnector://{user}:{password}@{host}:{port}/指数行情数据库')
engine_chanpin = create_engine(f'mysql+mysqlconnector://{user}:{password}@{host}:{port}/衍盛产品净值数据库')
cursor = cnx.cursor()

# 检查净值准确度
def get_precision_factor(dataframe, column_name, recent_count=10):
    # 获取指定列的最近几个值
    recent_values = dataframe[column_name].dropna().tail(recent_count).unique()
    
    # 初始化最大小数位数
    max_decimal_places = 0
    
    # 遍历最近几个值，找到最大小数位数
    for value in recent_values:
        decimal_places = len(str(value).split('.')[1]) if '.' in str(value) else 0
        if decimal_places > max_decimal_places:
            max_decimal_places = decimal_places
    
    # 返回对应的精度因子
    return max_decimal_places

# 日期参数
parameter = pd.DataFrame([['本周五', time_this_Friday],
     ['上周五', time_last_Friday],
     ['上上周五', time_last_Friday_lag1],
     ['上周五四周前', time_last_Friday_lag4],
     ['上月底', time_last_month],
     ['上上月底', time_last_month_lag1],
     ['上年底', time_last_year],                 
     ['近一年', time_lag_year],
     ['近两年', time_lag2_year],
     ['近三年', time_lag3_year]])

# 获取当前日期
current_date = datetime.now().date()

# 检查 time_last_Friday_time 是否在本周之内
start_of_week = current_date - timedelta(days=current_date.weekday())

if start_of_week <= time_last_Friday:
    logging.info("本周五已过，下周刷新策略数据，终止脚本运行")
    sys.exit()

list_all = pd.read_sql_query("SELECT * FROM %s" % '所有产品爬虫信息表', engine)
summary_df = pd.read_sql_query("SELECT * FROM %s" % '所有产品计算指标汇总表', engine)
list_all = pd.merge(list_all[['产品名称', '产品网址']], summary_df, on='产品名称', how='left')
celue_total_list = pd.read_sql_query("SELECT * FROM %s" % '策略指数汇总表', engine)

logging.info('策略指数计算开始')

# 指定要保留的产品名称
target_products = [
    '公募300指增策略指数',
    '公募500指增策略指数',
    '公募1000指增策略指数',
    '公募国证2000指增策略指数'
]
# 使用 loc 方法筛选出指定产品名称的行
celue_total_list = celue_total_list.loc[celue_total_list['产品名称'].isin(target_products)].reset_index(drop=True)

# 底层产品净值匹配
sheet_list = ['中性策略指数', '300指增策略指数', '500指增策略指数', '1000指增策略指数', '空气指增策略指数', '中短周期策略指数', '中周期策略指数', '中长周期策略指数', '100亿元以上策略指数', '50-100亿元策略指数', '50亿元以下策略指数']
yansheng_compare_strategy_list = ['中性策略指数', '500指增策略指数', '1000指增策略指数', '中长周期策略指数', '空气指增策略指数']
mingliang_compare_strategy_list = ['中性策略指数', '300指增策略指数', '500指增策略指数', '1000指增策略指数', '空气指增策略指数']
for celue_name in sheet_list:
    filtered_list = list_all.loc[list_all['策略'] == celue_name.strip('策略指数')]
    filtered_list = filtered_list.sort_values(by="成立日期",ascending=True).reset_index(drop=True)
    num_strategy = filtered_list['策略'].value_counts()[celue_name.strip('策略指数')]
    index_info = pd.read_sql_query(f"SELECT * FROM `{celue_name}`", engine)
    # 保留第一列内容 
    data = index_info.iloc[:,0]
    # 获取列名
    cols = index_info.columns
    # 将第一列内容和列名合并为一个DataFrame
    index_info = pd.DataFrame(data, columns=cols)
    # 定位'对标指数'列的索引
    drop_index = index_info.columns.get_loc('对标指数') + 1 
    # 删除定位索引之后的列
    index_info.drop(index_info.columns[drop_index:], axis=1, inplace=True)
    
    # 列名列表按照你的顺序
    new_columns = ['成立以来收益率', '对标指数成立以来收益率', '成立以来超额（除法）', '成立以来最大回撤', '成立以来最大回撤（超额）', '成立以来波动率', '成立以来超额波动率',
                    '近四周收益率', '对标指数近四周收益率', '近四周超额（除法）', '今年以来收益率', '对标指数今年以来收益率', '今年以来超额（除法）', '今年以来最大回撤', '今年以来最大回撤（超额）', '今年以来波动率', '今年以来超额波动率',
                    '近三月收益率', '对标指数近三月收益率', '近三月超额（除法）', '近三月最大回撤', '近三月最大回撤（超额）', '近三月波动率', '近三月超额波动率',
                    '近六月收益率', '对标指数近六月收益率', '近六月超额（除法）', '近六月最大回撤', '近六月最大回撤（超额）', '近六月波动率', '近六月超额波动率',
                    '近一年收益率', '对标指数近一年收益率', '近一年超额（除法）', '近一年最大回撤', '近一年最大回撤（超额）', '近一年波动率', '近一年超额波动率',
                    '近两年收益率', '对标指数近两年收益率', '近两年超额（除法）', '近两年最大回撤', '近两年最大回撤（超额）', '近两年波动率', '近两年超额波动率',
                    '近三年收益率', '对标指数近三年收益率', '近三年超额（除法）', '近三年最大回撤', '近三年最大回撤（超额）', '近三年波动率', '近三年超额波动率']
    
    # 添加新列名到 index_info
    index_info = index_info.reindex(columns=list(index_info.columns) + new_columns)
    
    # 更新到上周五最新日期
    if index_info.iloc[-1, 0].date() != time_last_Friday:
        index_info.loc[len(index_info), '净值日期'] = time_last_Friday_time
    
    strategy_name = celue_name.strip('策略指数')
    
    # 从字典中获取相应的数据
    data = BenchmarkIndex_mapping.get(strategy_name)
    
    if isinstance(data, pd.DataFrame):
        # 获取上周五的收盘价
        index_last_Friday = float((data.loc[data['date'] == time_last_Friday_time, ['close']]).values[0][0])
    
        # 合并数据
        index_nav = pd.merge(index_info['净值日期'], data.rename(columns={'date':'净值日期'}), on='净值日期')
        index_nav = index_nav.rename(columns = {'close':'对标指数'})
        index_info['对标指数'] = index_nav['对标指数']
    else:
        index_info['对标指数'] = np.nan
    
        # 净值序列匹配 #       
    for n in range(num_strategy):
        # 给产品拼接查询sql语句
        list_chanpin_info = pd.read_sql_query("SELECT * FROM %s" % filtered_list.loc[n, '产品名称'], engine)
        list_chanpin_info = list_chanpin_info.loc[list_chanpin_info['净值日期'] <= time_last_Friday_time]
        # 这里保证全部都是周频净值
        list_chanpin_info = list_chanpin_info.loc[list_chanpin_info['是否周末净值'] == '是'].reset_index().drop('index', axis=1)
        temp_nav = pd.merge(index_info['净值日期'], list_chanpin_info[['净值日期', '复权净值']])
        temp_nav = pd.merge(index_info['净值日期'], temp_nav, how='outer')
        index_info[filtered_list.iloc[n, 0]] = temp_nav['复权净值']
    
    # 指数编制计算和分析
    index_info.loc[:, '成立以来收益率':'近三年超额波动率'] = np.nan
    
    # 计算成立以来数据 #
    index_info.loc[0, '复权净值'] = 1
    index_info.loc[0, '样本数量'] = 0
    index_info.loc[0, '成立以来收益率'] = 0
    index_info.loc[0, '成立以来最大回撤'] = 0
    index_info.loc[0, '成立以来最大回撤（超额）'] = 0
    index_info[['复权净值', '对标指数', '成立以来收益率', '对标指数成立以来收益率', '成立以来超额（除法）', '成立以来最大回撤', '成立以来最大回撤（超额）']] = index_info[['复权净值', '对标指数', '成立以来收益率', '对标指数成立以来收益率', '成立以来超额（除法）', '成立以来最大回撤', '成立以来最大回撤（超额）']].astype(float)
    
    # 计算成立以来数据 #
    if '指增' in celue_name or '亿元' in celue_name:
        index_info.loc[0, '对标指数成立以来收益率'] = 0
        index_info.loc[0, '成立以来超额（除法）'] = 0

    for date_length in range(len(index_info)-1):
        n = 0
        cum_return = 0
        # 指数等权重编制计算
        for celue_length in range(num_strategy):            
            if np.isnan(index_info.iloc[date_length, 55 + celue_length]*index_info.iloc[date_length + 1, 55 + celue_length]) == False:
                cum_return = cum_return + index_info.iloc[date_length + 1, 55 + celue_length]/index_info.iloc[date_length, 55 + celue_length] - 1
                n = n + 1

        index_info.loc[date_length + 1, '样本数量'] = n
        try:
            index_info.loc[date_length + 1, '复权净值'] = float(index_info.loc[date_length, '复权净值']*(cum_return/n + 1))
        except:
            logging.info('!' + celue_name + ' ' + str(index_info.loc[date_length + 1, '净值日期']) + ' 指数成分为空，请检查！')
            sys.exit()

        index_info.loc[date_length + 1, '成立以来收益率'] = float(index_info.loc[date_length + 1, '复权净值'] - 1)
        # 最大回撤
        index_info.loc[date_length + 1, '成立以来最大回撤'] = (index_info.loc[date_length + 1,'复权净值'])/max(index_info.loc[0: (date_length + 1), '复权净值']) - 1
        if index_info.loc[date_length + 1, '成立以来最大回撤'] > 0:
            index_info.loc[date_length + 1, '成立以来最大回撤'] = 0
        # 绝对收益波动率
        index_info.loc[date_length + 1, '成立以来波动率'] = (index_info.loc[date_length + 1, '成立以来收益率'] + 1)/(index_info.loc[date_length, '成立以来收益率'] + 1) - 1
        
        # 相对收益指标计算
        if '指增' in celue_name or '亿元' in celue_name:
            # 基准指数以及超额计算
            index_info.loc[date_length + 1, '对标指数成立以来收益率'] = index_info.loc[date_length + 1, '对标指数']/index_info.loc[0, '对标指数'] - 1
            index_info.loc[date_length + 1, '成立以来超额（除法）'] = (index_info.loc[date_length + 1, '成立以来收益率'] + 1)/(index_info.loc[date_length + 1, '对标指数成立以来收益率'] + 1) - 1
            # 最大超额回撤
            index_info.loc[date_length + 1, '成立以来最大回撤（超额）'] = (index_info.loc[date_length + 1,'成立以来超额（除法）'] + 1)/max(index_info.loc[0: (date_length + 1), '成立以来超额（除法）'] + 1) - 1
            if index_info.loc[date_length + 1, '成立以来最大回撤（超额）'] > 0:
                index_info.loc[date_length + 1, '成立以来最大回撤（超额）'] = 0
            # 超额波动率
            index_info.loc[date_length + 1, '成立以来超额波动率'] = (index_info.loc[date_length + 1, '成立以来超额（除法）'] + 1)/(index_info.loc[date_length, '成立以来超额（除法）'] + 1) - 1
        
        # 其他区间字段计算
        for date_start, com_columns, spe_columns, draw_columns, bench_columns, chanpin_draw_columns, vol_columns, excess_vol_columns in zip(
            [time_time_last_Friday_lag4_time, time_last_year_time, time_lag3_month_time, time_lag6_month_time, time_lag_year_time, time_lag2_year_time, time_lag3_year_time],
            ['近四周收益率', '今年以来收益率', '近三月收益率', '近六月收益率', '近一年收益率', '近两年收益率', '近三年收益率'],
            ['近四周超额（除法）', '今年以来超额（除法）', '近三月超额（除法）', '近六月超额（除法）', '近一年超额（除法）', '近两年超额（除法）', '近三年超额（除法）'],
            ['近四周最大回撤（超额）', '今年以来最大回撤（超额）', '近三月最大回撤（超额）', '近六月最大回撤（超额）', '近一年最大回撤（超额）', '近两年最大回撤（超额）', '近三年最大回撤（超额）'],
            ['对标指数近四周收益率', '对标指数今年以来收益率', '对标指数近三月收益率', '对标指数近六月收益率', '对标指数近一年收益率', '对标指数近两年收益率', '对标指数近三年收益率'],
            ['近四周最大回撤', '今年以来最大回撤', '近三月最大回撤', '近六月最大回撤', '近一年最大回撤', '近两年最大回撤', '近三年最大回撤'],
            ['近四周波动率', '今年以来波动率', '近三月波动率', '近六月波动率', '近一年波动率', '近两年波动率', '近三年波动率'],
            ['近四周超额波动率', '今年以来超额波动率', '近三月超额波动率', '近六月超额波动率', '近一年超额波动率', '近两年超额波动率', '近三年超额波动率']
        ):
                                                                                    
            if index_info.iloc[date_length + 1, 0] >= date_start:
                # 绝对收益字段数据计算 
                first_index = index_info.loc[index_info['净值日期'] >= date_start].index[0]
                temp = index_info.loc[index_info['净值日期'] >= date_start].reset_index(drop=True)
                # 普通收益率字段计算
                index_info.loc[date_length + 1, com_columns] = index_info.loc[date_length + 1, '复权净值']/temp.loc[0, '复权净值']-1
                if date_start != time_time_last_Friday_lag4_time:
                    # 最大回撤
                    index_info.loc[date_length + 1, chanpin_draw_columns] = (index_info.loc[date_length + 1,'复权净值'])/max(index_info.loc[first_index:(date_length + 1), '复权净值']) - 1
                    if index_info.loc[date_length + 1, chanpin_draw_columns] > 0:
                        index_info.loc[date_length + 1, chanpin_draw_columns] = 0
                    # 波动率
                    if first_index != (date_length + 1):
                        index_info.loc[date_length + 1, vol_columns] = (index_info.loc[date_length + 1,'复权净值'])/(index_info.loc[date_length, '复权净值']) - 1
                
                # 相对收益字段数据计算
                if '指增' in celue_name or '亿元' in celue_name:
                # 相对收益
                    # 超额收益字段计算
                    index_info.loc[date_length + 1, spe_columns] = (index_info.loc[date_length + 1, com_columns] + 1)/(index_info.loc[date_length + 1, '对标指数']/temp.loc[0, '对标指数']) - 1
                    # 基准收益
                    index_info.loc[date_length + 1, bench_columns] = index_info.loc[date_length + 1, '对标指数']/index_info.loc[first_index, '对标指数'] - 1 
                    # 指增最大回撤及波动率字段计算
                    if date_start != time_time_last_Friday_lag4_time:
                        # 最大回撤
                        index_info.loc[date_length + 1, draw_columns] = (index_info.loc[date_length + 1,'成立以来超额（除法）'] + 1)/max(index_info.loc[first_index:(date_length + 1), '成立以来超额（除法）'] + 1) - 1
                        if index_info.loc[date_length + 1, draw_columns] > 0:
                            index_info.loc[date_length + 1, draw_columns] = 0
                        # 波动率
                        if first_index != (date_length + 1):
                            index_info.loc[date_length + 1, excess_vol_columns] = (index_info.loc[date_length + 1, '成立以来超额（除法）'] + 1)/(index_info.loc[date_length, '成立以来超额（除法）'] + 1) - 1
       
    # 填写总表数据 #
    celue_total_list = celue_total_list._append(pd.Series(np.nan, index=celue_total_list.columns), ignore_index=True)  # 添加空白行
    row_num = celue_total_list.index[-1]
    celue_total_list.loc[row_num, '产品名称'] = celue_name 
    cols = index_info.loc[:, '成立以来收益率':'近三年超额波动率'].columns
    celue_total_list.loc[row_num, '成立日期'] = index_info.loc[0, '净值日期']
    celue_total_list.loc[row_num, '最新净值日期'] = index_info.iloc[-1]['净值日期']
    # 专门计算目前实际在更新净值的策略产品数量
        # 过滤掉在 list_all 中产品网址为 '无' 的行
    list_celue_filtered = filtered_list[filtered_list['产品名称'].isin(list_all[list_all['产品网址'] != '无']['产品名称'])]
    num_strategy_available = list_celue_filtered['策略'].value_counts()[celue_name.strip('策略指数')]
    celue_total_list.loc[row_num, '净值更新率'] = "{:.0%}".format((n+1) / num_strategy_available)
    
    celue_total_list.loc[row_num, '近一周收益率'] = index_info.iloc[-1]['成立以来波动率']
    celue_total_list.loc[row_num, '近一周超额（除法）'] = index_info.iloc[-1]['成立以来超额波动率']
    celue_total_list.loc[row_num, '对标指数近一周收益率'] = (index_info.iloc[-1]['成立以来波动率']+1) / (index_info.iloc[-1]['成立以来超额波动率']+1) - 1 
    for col in cols:
        celue_total_list.loc[row_num, col] = index_info.iloc[-1][col]
    for col in ['成立以来波动率', '今年以来波动率', '近三月波动率', '近六月波动率', '近一年波动率', '近两年波动率', '近三年波动率']:
        celue_total_list.loc[row_num, col] = index_info[col].dropna().std() * np.sqrt(52) 
    for col in ['成立以来超额波动率', '今年以来超额波动率', '近三月超额波动率', '近六月超额波动率', '近一年超额波动率', '近两年超额波动率', '近三年超额波动率']:
        celue_total_list.loc[row_num, col] = index_info[col].dropna().std() * np.sqrt(52)
    for col in ['成立以来最大回撤', '今年以来最大回撤', '近三月最大回撤', '近六月最大回撤', '近一年最大回撤', '近两年最大回撤', '近三年最大回撤']:
        celue_total_list.loc[row_num, col] = index_info[col].dropna().min()
    for col in ['成立以来最大回撤（超额）', '今年以来最大回撤（超额）', '近三月最大回撤（超额）', '近六月最大回撤（超额）', '近一年最大回撤（超额）', '近两年最大回撤（超额）', '近三年最大回撤（超额）']:
        celue_total_list.loc[row_num, col] = index_info[col].dropna().min()
    
    # 计算衍盛产品策略分位
    if celue_name in yansheng_compare_strategy_list:
        # 读取对应策略汇总表
        celue_total_list = celue_total_list._append(pd.Series(np.nan, index=celue_total_list.columns), ignore_index=True)  # 添加空白行
        row_num = celue_total_list.index[-1]
        celue_total_list.loc[row_num, '产品名称'] = celue_name + '衍盛排名分位'
        # 计算近一周收益率并排序
        compare_row = (index_info.iloc[-1, drop_index+52:]/index_info.iloc[-2, drop_index+52:]-1).sort_values(ascending=False, na_position='last')
        # 计算衍盛所在索引
        # 解决三个产品可能先匹配到铭量产品的问题
        if celue_name == '1000指增策略指数':
            matching_index = compare_row.index[compare_row.index.str.contains("衍盛1000")]
        elif celue_name == '500指增策略指数':
            matching_index = compare_row.index[compare_row.index.str.contains("衍盛指数")]
        elif celue_name == '中性策略指数':
            matching_index = compare_row.index[compare_row.index.str.contains("衍盛量化")]
        elif celue_name == '中长周期策略指数':
            matching_index = compare_row.index[compare_row.index.str.contains("衍盛")]
        elif celue_name == '空气指增策略指数':
            matching_index = compare_row.index[compare_row.index.str.contains("衍盛深启")]
        # 计算近一周排名分位
        row_number_rank = (compare_row.index.get_loc(matching_index[0])+ 1)/compare_row.count()
        celue_total_list.loc[row_num, '近一周收益率'] = row_number_rank
        for date_column_name in ['近四周收益率', '今年以来收益率', '近三月收益率', '近六月收益率', '近一年收益率', '近两年收益率', '近三年收益率']:
            filtered_list[date_column_name] = pd.to_numeric(filtered_list[date_column_name], errors='coerce')
            compare_row = filtered_list[['产品名称', date_column_name]].sort_values(by=date_column_name, ascending=False, na_position='last')
            compare_row = compare_row[pd.to_numeric(compare_row[date_column_name], errors='coerce').notnull()].reset_index(drop=True)
            # 计算衍盛所在索引
            # 解决三个产品可能先匹配到铭量产品的问题
            if celue_name == '1000指增策略指数':
                matching_index = compare_row.index[compare_row['产品名称'].str.contains("衍盛1000")]
            elif celue_name == '500指增策略指数':
                matching_index = compare_row.index[compare_row['产品名称'].str.contains("衍盛指数")]
            elif celue_name == '中性策略指数':
                matching_index = compare_row.index[compare_row['产品名称'].str.contains("衍盛量化")]
            elif celue_name == '中长周期策略指数':
                matching_index = compare_row.index[compare_row['产品名称'].str.contains("衍盛")]
            elif celue_name == '空气指增策略指数':
                matching_index = compare_row.index[compare_row['产品名称'].str.contains("衍盛深启")]
            if not matching_index.empty:
                row_number_rank = (compare_row.index.get_loc(matching_index[0]) + 1)/compare_row.count()[1]
                celue_total_list.loc[row_num, date_column_name] = row_number_rank
            else:
                continue
    
    # 计算铭量产品策略分位
    if celue_name in mingliang_compare_strategy_list:
        # 读取对应策略汇总表
        celue_total_list = celue_total_list._append(pd.Series(np.nan, index=celue_total_list.columns), ignore_index=True)  # 添加空白行
        row_num = celue_total_list.index[-1]
        celue_total_list.loc[row_num, '产品名称'] = celue_name + '铭量排名分位'
        # 计算近一周收益率并排序
        compare_row = (index_info.iloc[-1, drop_index+52:]/index_info.iloc[-2, drop_index+52:]-1).sort_values(ascending=False, na_position='last')
        # 计算铭量所在索引
        matching_index = compare_row.index[compare_row.index.str.contains("铭量")]
        # 计算近一周排名分位
        row_number_rank = (compare_row.index.get_loc(matching_index[0])+ 1)/compare_row.count()
        celue_total_list.loc[row_num, '近一周收益率'] = row_number_rank
        for date_column_name in ['近四周收益率', '今年以来收益率', '近三月收益率', '近六月收益率', '近一年收益率', '近两年收益率', '近三年收益率']:
            filtered_list[date_column_name] = pd.to_numeric(filtered_list[date_column_name], errors='coerce')
            compare_row = filtered_list[['产品名称', date_column_name]].sort_values(by=date_column_name, ascending=False, na_position='last')
            compare_row = compare_row[pd.to_numeric(compare_row[date_column_name], errors='coerce').notnull()].reset_index(drop=True)
            # 计算铭量所在索引
            matching_index = compare_row.index[compare_row['产品名称'].str.contains("铭量")]
            if not matching_index.empty:
                row_number_rank = (compare_row.index.get_loc(matching_index[0]) + 1)/compare_row.count()[1]
                celue_total_list.loc[row_num, date_column_name] = row_number_rank
            else:
                continue
    index_info.to_sql(name=celue_name, con=engine, if_exists='replace', index=False)
    index_info.rename(columns={'净值日期': '日期', '复权净值': '收盘', '样本数量': '成交额'}, inplace=True)

    # 创建一个新的日期范围，包含index_info和data的日期范围
    new_index = pd.date_range(start=index_info['日期'].iloc[0], end=index_info['日期'].iloc[-1])
    # 创建一个新的DataFrame，包含new_index
    new_df = pd.DataFrame(new_index, columns=['日期'])
    # 使用merge函数将new_df和data['date']合并，保留两者交集中的行
    new_df = pd.merge(new_df, data_CSI1000, left_on='日期', right_on='date', how='inner')
    
    # 使用merge函数将new_df和index_info合并，以日期为键
    merged_df = pd.merge(new_df['日期'], index_info, left_on='日期', right_on='日期', how='left')
    # 将merged_df的'date'列设置为索引，并去掉多余的'日期'列
    merged_df.set_index('日期', inplace=True)
    merged_df.reset_index(inplace=True)
    merged_df.interpolate(method='linear', inplace=True)
    merged_df.to_sql(name=celue_name, con=engine_index, if_exists='replace', index=False)
    logging.info(celue_name + '清洗计算完毕')



# 把衍盛产品收益率计入策略汇总表
index_yansheng = pd.read_sql_query("SELECT * FROM %s" % '产品净值周报汇总表', engine_chanpin)
n = 0
for product_name in index_yansheng['产品名称']:
    celue_total_list = celue_total_list._append(pd.Series(np.nan, index=celue_total_list.columns), ignore_index=True)
    row_num = celue_total_list.index[-1]
    celue_total_list.loc[row_num, '产品名称'] = product_name
    celue_total_list.loc[row_num, '成立日期'] = index_yansheng.loc[n, '成立日期']
    celue_total_list.loc[row_num, '最新净值日期'] = index_yansheng.loc[n, '最新净值日期']
    celue_total_list.loc[row_num, '近一周收益率'] = index_yansheng.loc[n, '本周收益率']
    celue_total_list.loc[row_num, '近一周超额（除法）'] = index_yansheng.loc[n, '本周超额']
    celue_total_list.loc[row_num, '近四周收益率'] = index_yansheng.loc[n, '近四周收益率']
    celue_total_list.loc[row_num, '近四周超额（除法）'] = index_yansheng.loc[n, '近四周超额（除法）']
    celue_total_list.loc[row_num, '今年以来收益率'] = index_yansheng.loc[n, '今年以来收益率']
    celue_total_list.loc[row_num, '今年以来超额（除法）'] = index_yansheng.loc[n, '今年以来超额（除法）']
    celue_total_list.loc[row_num, '近三月收益率'] = index_yansheng.loc[n, '近三月收益率']
    celue_total_list.loc[row_num, '近三月超额（除法）'] = index_yansheng.loc[n, '近三月超额（除法）']
    celue_total_list.loc[row_num, '近六月收益率'] = index_yansheng.loc[n, '近六月收益率']
    celue_total_list.loc[row_num, '近六月超额（除法）'] = index_yansheng.loc[n, '近六月超额（除法）']
    celue_total_list.loc[row_num, '近一年收益率'] = index_yansheng.loc[n, '近一年收益率']
    celue_total_list.loc[row_num, '近一年超额（除法）'] = index_yansheng.loc[n, '近一年超额（除法）']
    celue_total_list.loc[row_num, '近两年收益率'] = index_yansheng.loc[n, '近两年收益率']
    celue_total_list.loc[row_num, '近两年超额（除法）'] = index_yansheng.loc[n, '近两年超额（除法）']
    celue_total_list.loc[row_num, '近三年收益率'] = index_yansheng.loc[n, '近三年收益率']
    celue_total_list.loc[row_num, '近三年超额（除法）'] = index_yansheng.loc[n, '近三年超额（除法）']
    n = n + 1

# =============================================================================
# # 把铭量产品收益率计入策略汇总表
# index_mingliang = pd.read_sql_query("SELECT * FROM %s" % '股票alpha产品汇总表', engine)
# index_mingliang = index_mingliang.loc[index_mingliang['管理人'] == '铭量'].reset_index(drop=True)
# n = 0
# for product_name in index_mingliang['产品名称']:
#     celue_total_list = celue_total_list._append(pd.Series(np.nan, index=celue_total_list.columns), ignore_index=True)
#     row_num = celue_total_list.index[-1]
#     celue_total_list.loc[row_num, '产品名称'] = product_name
#     celue_total_list.loc[row_num, '成立日期'] = index_mingliang.loc[n, '成立日期']
#     celue_total_list.loc[row_num, '最新净值日期'] = index_mingliang.loc[n, '最新净值日期']
#     celue_total_list.loc[row_num, '近一周收益率'] = index_mingliang.loc[n, '近一周收益率']
#     celue_total_list.loc[row_num, '近一周超额（除法）'] = index_mingliang.loc[n, '近一周超额（除法）']
#     celue_total_list.loc[row_num, '近四周收益率'] = index_mingliang.loc[n, '近四周收益率']
#     celue_total_list.loc[row_num, '近四周超额（除法）'] = index_mingliang.loc[n, '近四周超额（除法）']
#     celue_total_list.loc[row_num, '今年以来收益率'] = index_mingliang.loc[n, '今年以来收益率']
#     celue_total_list.loc[row_num, '今年以来超额（除法）'] = index_mingliang.loc[n, '今年以来超额（除法）']
#     celue_total_list.loc[row_num, '近三月收益率'] = index_mingliang.loc[n, '近三月收益率']
#     celue_total_list.loc[row_num, '近三月超额（除法）'] = index_mingliang.loc[n, '近三月超额（除法）']
#     celue_total_list.loc[row_num, '近六月收益率'] = index_mingliang.loc[n, '近六月收益率']
#     celue_total_list.loc[row_num, '近六月超额（除法）'] = index_mingliang.loc[n, '近六月超额（除法）']
#     celue_total_list.loc[row_num, '近一年收益率'] = index_mingliang.loc[n, '近一年收益率']
#     celue_total_list.loc[row_num, '近一年超额（除法）'] = index_mingliang.loc[n, '近一年超额（除法）']
#     celue_total_list.loc[row_num, '近两年收益率'] = index_mingliang.loc[n, '近两年收益率']
#     celue_total_list.loc[row_num, '近两年超额（除法）'] = index_mingliang.loc[n, '近两年超额（除法）']
#     celue_total_list.loc[row_num, '近三年收益率'] = index_mingliang.loc[n, '近三年收益率']
#     celue_total_list.loc[row_num, '近三年超额（除法）'] = index_mingliang.loc[n, '近三年超额（除法）']
#     n = n + 1
# =============================================================================

# 创建布尔索引，识别带有“排名分位”的行
has_ranking = celue_total_list['产品名称'].str.contains('排名分位')
# 从原始 DataFrame 中筛选出带有“排名分位”的行
ranking_rows = celue_total_list[has_ranking]
# 从原始 DataFrame 中删除带有“排名分位”的行
celue_total_list = celue_total_list[~has_ranking]
# 将带有“排名分位”的行添加到原始 DataFrame 的末尾
celue_total_list = pd.concat([celue_total_list, ranking_rows])

start_index = celue_total_list.columns.get_loc('成立以来收益率')
end_index = celue_total_list.columns.get_loc('近三年超额波动率') + 1
celue_total_list.iloc[:, start_index:end_index] = celue_total_list.iloc[:, start_index:end_index].apply(pd.to_numeric)     # 把列格式调整为数字

celue_total_list.to_sql(name='策略指数汇总表', con=engine, if_exists='replace', index=False)

# 关闭游标
cursor.close()

# 关闭连接
cnx.close()

logging.info('策略指数计算完成')