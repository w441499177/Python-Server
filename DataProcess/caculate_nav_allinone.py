import sys
sys.path.append('/home/ubuntu/market/dc-66-python-2.0/SelfModule')

from tradedate import *  # 自定义封装函数
from downloadindex import BenchmarkIndex_mapping, data_CSI1000 # 自定义封装函数
from selfdefinfunc import Locator_Len, Search_NavDate, BenchmarkIndex, Calculate_Reinvested_Nav, Process_Nav_Dataframe, get_precision_factor # 自定义封装函数
from fund_indicator_caculate import calculate_fund_metrics, Calculate_Fund_Metrics_With_Series, Calculate_Weekly_Indicator_Metrics # 自定义封装函数

import warnings
import mysql.connector
from sqlalchemy import create_engine
import numpy as np
import pandas as pd
from datetime import datetime, date, timedelta
import time
import os
import re
import pytz
import logging

# 设置日志
logging.basicConfig(filename='/home/ubuntu/market/dc-66-python-2.0/execution.log', 
                    level=logging.INFO, 
                    format='%(asctime)s - %(levelname)s - %(message)s', 
                    filemode='a')  # 设置为追加模式

warnings.filterwarnings('ignore')

# 环境变量读取数据库秘钥信息
host = os.environ.get('MYSQL_HOST')   
user = os.environ.get('MYSQL_USERNAME')  
password = os.environ.get('MYSQL_PASSWORD')
port = os.environ.get('MYSQL_PORT')

# 数据库存储操作
cnx = mysql.connector.connect(user=user, password=password,
                              host=host,  port=port, database='同业产品数据库')
engine = create_engine(f'mysql+mysqlconnector://{user}:{password}@{host}:{port}/同业产品数据库')
engine_index = create_engine(f'mysql+mysqlconnector://{user}:{password}@{host}:{port}/指数行情数据库')
cursor = cnx.cursor()

# 日期参数
parameter = pd.DataFrame([['本周五', time_this_Friday],
     ['上周五', time_last_Friday],
     ['上上周五', time_last_Friday_lag1],
     ['上周五四周前', time_last_Friday_lag4],
     ['上月底', time_last_month],
     ['上上月底', time_last_month_lag1],
     ['上年底', time_last_year],                 
     ['近一年', time_lag_year],
     ['近两年', time_lag2_year],
     ['近三年', time_lag3_year]])

# 周度和月度日期序列 #
weekly_date = data_CSI1000.drop('close', axis=1)
weekly_date = weekly_date.rename(columns = {'date':'净值日期'})
weekly_date['是否周末净值'] = ''
weekly_date['是否月末净值'] = ''
weekly_date['是否年末净值'] = ''
weekly_date.iloc[-1, 1] = '是'

# 比较这两个日期
if time_last_month_time > time_last_Friday_time:
    # 如果上月末的日期更大，那么就用上月末的日期来筛选数据
    weekly_date = weekly_date[weekly_date['净值日期'] <= time_last_month_time]
else:
    # 如果上周五的日期更大，那么就用上周五的日期来筛选数据
    weekly_date = weekly_date[weekly_date['净值日期'] <= time_last_Friday_time]

# 周度净值日期贴标签
for i in range(1, len(weekly_date)):
    if weekly_date.iloc[i, 0] - weekly_date.iloc[i-1, 0] != timedelta(days=1):
        weekly_date.iloc[i-1, 1] = '是'
    else:
        weekly_date.iloc[i-1, 1] = '否'
    if weekly_date.iloc[i, 0] == time_last_Friday_time:
        weekly_date.iloc[i, 1] = '是'
        
# 周中法定节假日周频净值剔除
for i in range(len(weekly_date) - 1):
    current_week = weekly_date.iloc[i, 0].strftime("%U")  # 获取当前日期所在周的周数
    next_week = weekly_date.iloc[i + 1, 0].strftime("%U")  # 获取下一个日期所在周的周数
    if current_week == next_week:
        weekly_date.iloc[i, 1] = '否'

# 月度净值日期贴标签
for i in range(1, len(weekly_date)):
    if weekly_date.iloc[i, 0].month - (weekly_date.iloc[i, 0] + timedelta(1)).month != 0:
        weekly_date.iloc[i, 2] = '是'
    elif weekly_date.iloc[i, 0].month - weekly_date.iloc[i-1, 0].month != 0:
        weekly_date.iloc[i-1, 2] = '是'
    else:
        weekly_date.iloc[i, 2] = '否'
        
# 年度净值日期贴标签
for i in range(len(weekly_date) - 1):
    weekly_date.iloc[i, 0].month
    if weekly_date.iloc[i+1, 0].month - weekly_date.iloc[i, 0].month == -11:
        weekly_date.iloc[i, 3] = '是'
    else:
        weekly_date.iloc[i, 3] = '否'

weekly_date_filter = weekly_date[(weekly_date['是否周末净值'] == '是') | (weekly_date['是否月末净值'] == '是')].reset_index(drop=True)

# 获取当前时间并转换为北京时间
def get_beijing_time():
    utc_now = datetime.utcnow()
    beijing_tz = pytz.timezone('Asia/Shanghai')
    beijing_now = utc_now.replace(tzinfo=pytz.utc).astimezone(beijing_tz)
    return beijing_now.strftime('%Y-%m-%d %H:%M:%S')

list_all = pd.read_sql_query("SELECT * FROM %s" % '所有产品爬虫信息表', engine)
list_self = pd.read_sql_query("SELECT * FROM %s" % '用户产品净值收集汇总表', engine)
summary_df = pd.read_sql_query("SELECT * FROM %s" % '所有产品计算指标汇总表', engine)
tracking_df = pd.read_sql_query("SELECT * FROM %s" % '用户产品标签要素汇总表', engine)

# 比较 tracking_df 与 list_self 和 list_all
# 如果 tracking_df 的 '产品名称' 字段在 list_self 和 list_all 中都没有，则删除该行
tracking_df = tracking_df[tracking_df['产品名称'].isin(list_self['产品名称']) | tracking_df['产品名称'].isin(list_all['产品名称'])]
# 如果 list_self 中有 tracking_df 里面没有出现的 '产品名称' 字段，则将这些字段对应的内容提取出来新添加到 tracking_df 里面
missing_products = list_self[~list_self['产品名称'].isin(tracking_df['产品名称'])]
if not missing_products.empty:
    new_rows = missing_products[['产品名称', 'user_name', '大类策略', '策略', '管理人', '最新净值日期']]
    # 添加新列 '是否平台净值产品' 并赋值为 '否'
    new_rows['是否平台净值产品'] = '否'
    tracking_df = pd.concat([tracking_df, new_rows], ignore_index=True)

# 删除 tracking_df 中 '产品名称' 和 'user_name' 同时重复的行
tracking_df = tracking_df.drop_duplicates(subset=['产品名称', 'user_name'], keep='first')

# 重置 tracking_df 的索引
tracking_df = tracking_df.reset_index(drop=True)

# 检查 summary_df 中的 '产品名称' 是否在 list_all 和 list_self 中存在
summary_df = summary_df[summary_df['产品名称'].isin(list_all['产品名称']) | summary_df['产品名称'].isin(list_self['产品名称'])]
# 重置 summary_df 的索引
summary_df = summary_df.reset_index(drop=True)

list_porfolio_manager = pd.read_sql_query("SELECT * FROM %s" % '基金经理信息对照表', engine)
list_fund_manager = pd.read_sql_query("SELECT * FROM %s" % '管理人信息对照表', engine)

list_all_updated = list_all.query('(更新净值日期 > 计算净值日期) or 计算净值日期.isna() or 更新净值日期.isna()').reset_index(drop=True)
list_self_updated = list_self.query('(更新净值日期 > 计算净值日期) or (更新净值日期.notna() and 计算净值日期.isna())').reset_index(drop=True)

for list_production in [list_all_updated, list_self_updated]:
    if list_production is list_all_updated:
        product_type = '平台产品'
    elif list_production is list_self_updated:
        product_type = '个人产品'
    
    for n in range(len(list_production)):
        chanpin_name = list_production.loc[n, '产品名称']
        type_strategy = list_production.loc[n, '策略']
        summary_row = pd.Series(index = summary_df.columns)
        
        # 平台净值的情况下
        if product_type == '平台产品':
            query = f"SELECT 净值日期, 单位净值, 累计净值, 复权净值 FROM {chanpin_name}"
            try:
                list_chanpin_info = pd.read_sql_query(query, engine)
            except:
                logging.info(chanpin_name + '净值表还未创建')
                continue
            list_chanpin_info = Process_Nav_Dataframe(list_chanpin_info)
            list_chanpin_info = list_chanpin_info.reset_index(drop=True)
            min_date = list_chanpin_info['净值日期'].min()
            # 匹配对标指数
            if not isinstance(BenchmarkIndex_mapping[type_strategy], str):
                list_chanpin_info = BenchmarkIndex(list_chanpin_info[['净值日期', '单位净值', '累计净值', '复权净值']], time_last_Friday_time, BenchmarkIndex_mapping[type_strategy])
            summary_row['是否平台净值产品'] = '是'
            summary_row['策略标签'] = list_production.loc[n, '策略标签']
            
            # 从 list_production 中获取当前记录的管理人和策略标签
            manager = list_production.loc[n, '管理人']
            strategy_label = list_production.loc[n, '策略标签']
            summary_row['同策略老产品'] = list_production.loc[n, '同策略老产品']

            # 从 list_fund_manager 中查找匹配的管理人代码
            manager_code = list_fund_manager.loc[list_fund_manager['管理人'] == manager, '管理人代码'].values
            if manager_code.size > 0:
                summary_row['管理人代码'] = manager_code[0]
            else:
                summary_row['管理人代码'] = None  # 或其他默认值
            
            # 检查大类策略是否为主观多头且策略标签有字符
            if list_production.loc[n, '大类策略'] == '主观多头' and isinstance(strategy_label, str) and strategy_label.strip():
                # 从 list_porfolio_manager 中查找匹配的基金经理代码
                portfolio_manager_code = list_porfolio_manager.loc[list_porfolio_manager['基金经理'] == strategy_label, '基金经理代码'].values
                if portfolio_manager_code.size > 0:
                    summary_row['基金经理代码'] = portfolio_manager_code[0]
                else:
                    summary_row['基金经理代码'] = None  # 或其他默认值
            else:
                summary_row['基金经理代码'] = None  # 或其他默认值
        
        # 个人净值的情况下
        elif product_type == '个人产品':
            query = f"SELECT 净值日期, 单位净值, 累计净值 FROM {chanpin_name}"
            try:
                list_chanpin_info = pd.read_sql_query(query, engine)
            except:
                logging.info(chanpin_name + '净值表还未创建')
                continue
            list_chanpin_info = Process_Nav_Dataframe(list_chanpin_info)
            list_chanpin_info = list_chanpin_info.reset_index(drop=True)
            min_date = list_chanpin_info['净值日期'].min()
            list_chanpin_info['复权净值'] = Calculate_Reinvested_Nav(list_chanpin_info, '单位净值', '累计净值')
            summary_row['是否平台净值产品'] = '否'
            summary_row['策略标签'] = tracking_df.loc[tracking_df['产品名称'] == chanpin_name, '策略标签'].values[0]
            benchmark_name = list_production.loc[n, '业绩基准']
            # 匹配对标指数
            if benchmark_name is None and not isinstance(BenchmarkIndex_mapping[type_strategy], str):
                list_chanpin_info = BenchmarkIndex(list_chanpin_info[['净值日期', '单位净值', '累计净值', '复权净值']], time_last_Friday_time, BenchmarkIndex_mapping[type_strategy])
            else:
                if type_strategy == '300指增' and benchmark_name == '沪深300':
                    benchmark_name = ''
                elif type_strategy == '500指增' and benchmark_name == '中证500':
                    benchmark_name = ''
                elif type_strategy == '1000指增' and benchmark_name == '中证1000':
                    benchmark_name = ''
                elif type_strategy == '空气指增' and benchmark_name == '国证2000':
                    benchmark_name = ''
                elif "亿元" in type_strategy and benchmark_name!= '中证800':
                    benchmark_name = ''
                else:
                    pass
        
                if benchmark_name:
                    query_benchmark = f"SELECT 日期, 收盘 FROM {benchmark_name} WHERE 日期 >= '{min_date}'"
                    benchmark_data = pd.read_sql_query(query_benchmark, engine_index)
                    benchmark_data.rename(columns={'日期': 'date', '收盘': 'close'}, inplace=True)
                    list_chanpin_info = BenchmarkIndex(list_chanpin_info[['净值日期', '单位净值', '累计净值', '复权净值']], time_last_Friday_time, benchmark_data)
                else:
                    if not isinstance(BenchmarkIndex_mapping[type_strategy], str):
                        list_chanpin_info = BenchmarkIndex(list_chanpin_info[['净值日期', '单位净值', '累计净值', '复权净值']], time_last_Friday_time, BenchmarkIndex_mapping[type_strategy])
        
        # 日期参数生成
        periods_dict = {
            '成立以来': (min_date, time_last_Friday_time),
            '近四周': (time_last_Friday_lag4_time, time_last_Friday_time),
            '今年以来': (time_last_year_time, time_last_Friday_time),
            '近三月': (time_lag3_month_time, time_last_Friday_time),
            '近六月': (time_lag6_month_time, time_last_Friday_time),
            '近一年': (time_lag_year_time, time_last_Friday_time),
            '近两年': (time_lag2_year_time, time_last_Friday_time),
            '近三年': (time_lag3_year_time, time_last_Friday_time)
        }

        fund_nav_caculate_df = Calculate_Fund_Metrics_With_Series(list_chanpin_info['净值日期'], list_chanpin_info['复权净值'], periods_dict, list_chanpin_info['对标指数'])
        list_chanpin_info = pd.concat([list_chanpin_info, fund_nav_caculate_df], axis=1)
        
        # 净值频率调整 # 
        list_chanpin_info = pd.merge(list_chanpin_info, weekly_date, on='净值日期', how='inner')
        list_chanpin_info.loc[0, ['是否周末净值', '是否月末净值', '是否年末净值']] = '是'
        
        list_chanpin_info = Calculate_Weekly_Indicator_Metrics(list_chanpin_info, min_date, weekly_date_filter)
        
        # 筛选出 '是否周末净值', '是否月末净值', '是否年末净值' 列值为 '是' 的所有行
        filtered_weekly_rows = list_chanpin_info[
            (list_chanpin_info['是否周末净值'] == '是') |
            (list_chanpin_info['是否月末净值'] == '是') |
            (list_chanpin_info['是否年末净值'] == '是')
        ]

        fund_weekly_nav_caculate_df = Calculate_Fund_Metrics_With_Series(filtered_weekly_rows['净值日期'], filtered_weekly_rows['复权净值'], periods_dict, filtered_weekly_rows['对标指数'])
        fund_weekly_nav_caculate_df = pd.concat([filtered_weekly_rows['净值日期'], fund_weekly_nav_caculate_df], axis=1)
        # 清空 list_chanpin_info 中包含 '回撤' 的字段内容
        withdrawal_columns = [col for col in list_chanpin_info.columns if '回撤' in col]
        list_chanpin_info[withdrawal_columns] = None

        # 用 fund_weekly_nav_caculate_df 的对应字段进行替代
        # 首先，确保 fund_weekly_nav_caculate_df 和 list_chanpin_info 的 '净值日期' 字段对齐
        merged_df = pd.merge(list_chanpin_info[['净值日期'] + withdrawal_columns], 
                            fund_weekly_nav_caculate_df[['净值日期'] + withdrawal_columns], 
                            on='净值日期', 
                            how='left')

        # 更新 list_chanpin_info 中的回撤字段
        for col in withdrawal_columns:
            list_chanpin_info.loc[merged_df.index, col] = merged_df[col + '_y']

        # 填充空缺值
        list_chanpin_info[withdrawal_columns] = list_chanpin_info[withdrawal_columns].fillna(method='ffill')

        # 对summary_row进行赋值
        summary_row['产品名称'] = chanpin_name
        summary_row['策略'] = type_strategy
        summary_row['大类策略'] = list_production.loc[n, '大类策略']
        summary_row['管理人'] = list_production.loc[n, '管理人']
        summary_row['管理人规模'] = list_production.loc[n, '管理人规模']
        summary_row['管理人规模区间'] = list_production.loc[n, '管理人规模区间']
        summary_row['成立日期'] = min_date
        summary_row['最新净值日期'] = list_chanpin_info['净值日期'].iloc[-1]
        summary_row['是否更新周报'] = '否'
        
        # 创建一个包含所有需要赋值的字段的序列
        filtered_list_chanpin_info = list_chanpin_info[list_chanpin_info['净值日期'] <= time_last_Friday_time]
        
        # 创建一个包含所有需要赋值的字段的序列
        summary_values = filtered_list_chanpin_info[['单位净值', '累计净值', '复权净值', '成立以来收益率', '对标指数成立以来收益率', '成立以来超额（除法）', '近四周收益率', '对标指数近四周收益率', '近四周超额（除法）', '今年以来收益率', '对标指数今年以来收益率', '今年以来超额（除法）', '近三月收益率', '对标指数近三月收益率', '近三月超额（除法）', '近六月收益率', '对标指数近六月收益率', '近六月超额（除法）', '近一年收益率', '对标指数近一年收益率', '近一年超额（除法）', '近两年收益率', '对标指数近两年收益率', '近两年超额（除法）', '近三年收益率', '对标指数近三年收益率', '近三年超额（除法）', '累计年化收益率', '累计年化超额收益率', '本周收益率', '本周超额', '夏普比率', '索提诺比率', '卡玛比率', '超额夏普比率', '超额索提诺比率', '超额卡玛比率', '周胜率', '月胜率', '超额周胜率', '超额月胜率']].iloc[-1]
        
        # 将这个序列赋值给 summary_row
        summary_row[summary_values.index] = summary_values
        
        # 最大回撤指标赋值
        summary_row['成立以来最大回撤'] = list_chanpin_info['成立以来最大回撤'].dropna().min()
        summary_row['成立以来最大回撤（超额）'] = list_chanpin_info['成立以来最大回撤（超额）'].dropna().min()
        summary_row['今年以来最大回撤'] = list_chanpin_info['今年以来最大回撤'].dropna().min()
        summary_row['今年以来最大回撤（超额）'] = list_chanpin_info['今年以来最大回撤（超额）'].dropna().min()
        summary_row['近三月最大回撤'] = list_chanpin_info['近三月最大回撤'].dropna().min()
        summary_row['近三月最大回撤（超额）'] = list_chanpin_info['近三月最大回撤（超额）'].dropna().min()
        summary_row['近六月最大回撤'] = list_chanpin_info['近六月最大回撤'].dropna().min()
        summary_row['近六月最大回撤（超额）'] = list_chanpin_info['近六月最大回撤（超额）'].dropna().min()
        summary_row['近一年最大回撤'] = list_chanpin_info['近一年最大回撤'].dropna().min()
        summary_row['近一年最大回撤（超额）'] = list_chanpin_info['近一年最大回撤（超额）'].dropna().min()
        summary_row['近两年最大回撤'] = list_chanpin_info['近两年最大回撤'].dropna().min()
        summary_row['近两年最大回撤（超额）'] = list_chanpin_info['近两年最大回撤（超额）'].dropna().min()
        summary_row['近三年最大回撤'] = list_chanpin_info['近三年最大回撤'].dropna().min()
        summary_row['近三年最大回撤（超额）'] = list_chanpin_info['近三年最大回撤（超额）'].dropna().min()

        # 波动率和超额波动率指标赋值
        if time_lag3_month_time >= min_date:
            summary_row['近三月波动率'] = np.std(filtered_list_chanpin_info.loc[filtered_list_chanpin_info['净值日期'] >= time_lag3_month_time, '本周收益率'].dropna()) * np.sqrt(52)
            summary_row['近三月超额波动率'] = np.std(filtered_list_chanpin_info.loc[filtered_list_chanpin_info['净值日期'] >= time_lag3_month_time, '本周超额'].dropna()) * np.sqrt(52)
        if time_lag6_month_time >= min_date:
            summary_row['近六月波动率'] = np.std(filtered_list_chanpin_info.loc[filtered_list_chanpin_info['净值日期'] >= time_lag6_month_time, '本周收益率'].dropna()) * np.sqrt(52)
            summary_row['近六月超额波动率'] = np.std(filtered_list_chanpin_info.loc[filtered_list_chanpin_info['净值日期'] >= time_lag6_month_time, '本周超额'].dropna()) * np.sqrt(52)
        if time_last_year_time >= min_date:
            summary_row['今年以来波动率'] = np.std(filtered_list_chanpin_info.loc[filtered_list_chanpin_info['净值日期'] >= time_last_year_time, '本周收益率'].dropna()) * np.sqrt(52)
            summary_row['今年以来超额波动率'] = np.std(filtered_list_chanpin_info.loc[filtered_list_chanpin_info['净值日期'] >= time_last_year_time, '本周超额'].dropna()) * np.sqrt(52)
        if time_lag_year_time >= min_date:
            summary_row['近一年波动率'] = np.std(filtered_list_chanpin_info.loc[filtered_list_chanpin_info['净值日期'] >= time_lag_year_time, '本周收益率'].dropna()) * np.sqrt(52)
            summary_row['近一年超额波动率'] = np.std(filtered_list_chanpin_info.loc[filtered_list_chanpin_info['净值日期'] >= time_lag_year_time, '本周超额'].dropna()) * np.sqrt(52)
        if time_lag2_year_time >= min_date:
            summary_row['近两年波动率'] = np.std(filtered_list_chanpin_info.loc[filtered_list_chanpin_info['净值日期'] >= time_lag2_year_time, '本周收益率'].dropna()) * np.sqrt(52)
            summary_row['近两年超额波动率'] = np.std(filtered_list_chanpin_info.loc[filtered_list_chanpin_info['净值日期'] >= time_lag2_year_time, '本周超额'].dropna()) * np.sqrt(52)
        if time_lag3_year_time >= min_date:   
            summary_row['近三年波动率'] = np.std(filtered_list_chanpin_info.loc[filtered_list_chanpin_info['净值日期'] >= time_lag3_year_time, '本周收益率'].dropna()) * np.sqrt(52)
            summary_row['近三年超额波动率'] = np.std(filtered_list_chanpin_info.loc[filtered_list_chanpin_info['净值日期'] >= time_lag3_year_time, '本周超额'].dropna()) * np.sqrt(52)

        # 获取净值小数点位数
        precision_factor = get_precision_factor(list_chanpin_info, '单位净值')
        # 对list_production进行赋值
        list_production.loc[n, '最新净值日期'] = list_chanpin_info['净值日期'].iloc[-1]
        list_production.loc[n, '净值小数点分位'] = precision_factor
        list_production.loc[n, '计算净值日期'] = get_beijing_time()
        list_production.loc[n, '成立日期'] = min_date
        # 把原始表中的最新净值日期也进行同步更新
        list_all.loc[list_all['产品名称'] == chanpin_name, '最新净值日期'] = list_chanpin_info['净值日期'].iloc[-1]
        
        # 检查 summary_df 中是否存在该产品名称
        if chanpin_name in summary_df['产品名称'].values:
            # 更新 summary_df 中已有的行
            summary_df.loc[summary_df['产品名称'] == chanpin_name, summary_row.index] = summary_row.values
        else:
            # 将新的 summary_row 添加到 summary_df 中
            summary_df = pd.concat([summary_df, summary_row.to_frame().T], ignore_index=True)
        
        
        list_chanpin_info.to_sql(name=chanpin_name, con=engine, if_exists='replace', index=False)
        print(chanpin_name + ':全量净值数据已更新')
        
# 将 '产品名称' 设置为索引
list_all = list_all.set_index('产品名称')
list_all_updated = list_all_updated.set_index('产品名称')

list_self = list_self.set_index('产品名称')
list_self_updated = list_self_updated.set_index('产品名称')

# 使用 update 方法逐行替代原始表中的记录
list_all.update(list_all_updated)
list_self.update(list_self_updated)

# 重置索引
list_all = list_all.reset_index()
list_self = list_self.reset_index()

# 更新 tracking_df 中与 list_all 和 list_self 有交集的行的 ['大类策略', '策略', '管理人', '最新净值日期'] 这几个字段
# 使用 list_all 更新 tracking_df
tracking_df = tracking_df.merge(list_all[['产品名称', '大类策略', '策略', '管理人', '最新净值日期']], on='产品名称', how='left', suffixes=('', '_new'))
tracking_df['大类策略'] = tracking_df['大类策略_new'].combine_first(tracking_df['大类策略'])
tracking_df['策略'] = tracking_df['策略_new'].combine_first(tracking_df['策略'])
tracking_df['管理人'] = tracking_df['管理人_new'].combine_first(tracking_df['管理人'])
tracking_df['最新净值日期'] = tracking_df['最新净值日期_new'].combine_first(tracking_df['最新净值日期'])
tracking_df = tracking_df.drop(columns=['大类策略_new', '策略_new', '管理人_new', '最新净值日期_new'])

# 使用 list_self 更新 tracking_df
tracking_df = tracking_df.merge(list_self[['产品名称', '大类策略', '策略', '管理人', '最新净值日期']], on='产品名称', how='left', suffixes=('', '_new'))
tracking_df['大类策略'] = tracking_df['大类策略_new'].combine_first(tracking_df['大类策略'])
tracking_df['策略'] = tracking_df['策略_new'].combine_first(tracking_df['策略'])
tracking_df['管理人'] = tracking_df['管理人_new'].combine_first(tracking_df['管理人'])
tracking_df['最新净值日期'] = tracking_df['最新净值日期_new'].combine_first(tracking_df['最新净值日期'])
tracking_df = tracking_df.drop(columns=['大类策略_new', '策略_new', '管理人_new', '最新净值日期_new'])

list_all.to_sql(name = '所有产品爬虫信息表', con=engine, if_exists='replace', index=False)
list_self.to_sql(name = '用户产品净值收集汇总表', con=engine, if_exists='replace', index=False)
summary_df.to_sql(name = '所有产品计算指标汇总表', con=engine, if_exists='replace', index=False)
tracking_df.to_sql(name = '用户产品标签要素汇总表', con=engine, if_exists='replace', index=False)

# 关闭游标
cursor.close()

# 关闭连接
cnx.close()