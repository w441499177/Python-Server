import warnings
import pandas as pd
import numpy as np
import time
import os
import mysql.connector
from sqlalchemy import create_engine

warnings.filterwarnings('ignore')

def get_db_connection():
    # 环境变量读取数据库秘钥信息
    host = os.environ.get('MYSQL_HOST')
    user = os.environ.get('MYSQL_USERNAME')
    password = os.environ.get('MYSQL_PASSWORD')
    port = os.environ.get('MYSQL_PORT')

    # 数据库存储操作
    cnx = mysql.connector.connect(user=user, password=password,
                                  host=host, port=port, database='衍盛产品净值数据库')
    engine = create_engine(f'mysql+mysqlconnector://{user}:{password}@{host}:{port}/衍盛产品净值数据库')
    cursor = cnx.cursor()
    
    return cursor, engine, cnx

def main():
    cursor, engine, cnx = get_db_connection()
    
    # 读取'配置产品申赎汇总表'
    query_summary = "SELECT * FROM 配置产品申赎汇总表"
    df_summary = pd.read_sql(query_summary, engine)
    
    # 读取'配置产品策略对照数据库'
    query_strategy = "SELECT * FROM 配置产品策略对照数据库"
    df_strategy = pd.read_sql(query_strategy, engine)
    
    # 保存原始的'份额名称'列到临时变量
    original_share_names = df_strategy['份额名称'].copy()
    
    # 删除'申赎份额名称'和'份额名称'中的'类'字符
    df_summary['申赎份额名称'] = df_summary['申赎份额名称'].str.replace('类', '')
    df_strategy['份额名称'] = df_strategy['份额名称'].str.replace('类', '')
    
    # 匹配'申赎份额名称'和'份额名称'
    merged_df = pd.merge(df_summary, df_strategy, left_on='申赎份额名称', right_on='份额名称', how='inner')
    
    # 处理'申赎份额名称'被包含在'份额名称'中的情况
    df_strategy['份额名称'] = df_strategy['份额名称'].fillna('')
    for index, row in df_summary.iterrows():
        matching_rows = df_strategy[df_strategy['份额名称'].str.contains(row['申赎份额名称'])]
        if not matching_rows.empty:
            for _, match_row in matching_rows.iterrows():
                new_row = pd.concat([row, match_row], axis=0).to_frame().T
                new_row.columns = merged_df.columns  # 确保列名一致
                merged_df = pd.concat([merged_df, new_row], ignore_index=True)
    
    # 筛选申购类型为'认购'或'申购'的行
    subscription_purchase_df = merged_df[merged_df['申购类型'].isin(['认购', '申购'])]
    subscription_purchase_dates = subscription_purchase_df.groupby('份额名称')['申购日期'].apply(lambda x: ','.join(x.astype(str))).reset_index()
    subscription_purchase_dates.rename(columns={'申购日期': '申购时点'}, inplace=True)
    
    # 筛选申购类型为'赎回'的行
    redemption_df = merged_df[merged_df['申购类型'] == '赎回']
    redemption_dates = redemption_df.groupby('份额名称')['申购日期'].apply(lambda x: ','.join(x.astype(str))).reset_index()
    redemption_dates.rename(columns={'申购日期': '赎回时点'}, inplace=True)
    
    # 将'申购时点'和'赎回时点'合并到df_strategy
    df_strategy = pd.merge(df_strategy, subscription_purchase_dates, on='份额名称', how='left')
    df_strategy['申购时点'] = df_strategy['申购时点_y'].combine_first(df_strategy['申购时点_x'])
    df_strategy.drop(columns=['申购时点_x', '申购时点_y'], inplace=True)
    
    df_strategy = pd.merge(df_strategy, redemption_dates, on='份额名称', how='left')
    df_strategy['赎回时点'] = df_strategy['赎回时点_y'].combine_first(df_strategy['赎回时点_x'])
    df_strategy.drop(columns=['赎回时点_x', '赎回时点_y'], inplace=True)
    
    # 处理重复的'产品名称'，合并'申购时点'和'赎回时点'
    df_strategy['申购时点'] = df_strategy['申购时点'].fillna('')
    df_strategy['赎回时点'] = df_strategy['赎回时点'].fillna('')
    
    # 创建一个新的 DataFrame 来处理重复的 '产品名称'
    df_strategy_processed = df_strategy.groupby('产品名称').agg({
        '份额名称': 'first',
        '申购时点': lambda x: ','.join(filter(None, set(','.join(x).split(',')))),
        '赎回时点': lambda x: ','.join(filter(None, set(','.join(x).split(','))))
    }).reset_index()
    
    # 将处理后的结果重新赋值回 df_strategy
    df_strategy = pd.merge(df_strategy, df_strategy_processed[['产品名称', '申购时点', '赎回时点']], on='产品名称', how='left')
    df_strategy['申购时点'] = df_strategy['申购时点_y'].combine_first(df_strategy['申购时点_x'])
    df_strategy['赎回时点'] = df_strategy['赎回时点_y'].combine_first(df_strategy['赎回时点_x'])
    df_strategy.drop(columns=['申购时点_x', '申购时点_y', '赎回时点_x', '赎回时点_y'], inplace=True)
    
    # 将'类'字符重新加回到'份额名称'中，仅对原本包含'类'的记录进行处理
    df_strategy['份额名称'] = df_strategy.apply(lambda row: row['份额名称'] + '类' if '类' in original_share_names.iloc[row.name] else row['份额名称'], axis=1)
    
    # 覆盖保存整个表
    df_strategy.to_sql('配置产品策略对照数据库', engine, if_exists='replace', index=False)
    
    # 提交更改并关闭连接
    cnx.commit()
    cursor.close()
    cnx.close()

if __name__ == "__main__":
    main()