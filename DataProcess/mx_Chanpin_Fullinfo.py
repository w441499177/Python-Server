import sys
sys.path.append('/home/ubuntu/market/dc-66-python-2.0/SelfModule')

from urllib.parse import unquote
import mysql.connector
from sqlalchemy import create_engine
import warnings
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from datetime import datetime, timedelta
from tradedate import *  # 自定义封装函数
from downloadindex import data_CSI1000  # 自定义封装函数
from chinese_calendar import is_workday
import os
import logging

# 设置日志
logging.basicConfig(filename='/home/ubuntu/market/dc-66-python-2.0/execution.log', 
                    level=logging.INFO, 
                    format='%(asctime)s - %(levelname)s - %(message)s', 
                    filemode='a')  # 设置为追加模式

warnings.filterwarnings('ignore')

# 环境变量读取数据库秘钥信息
host = os.environ.get('MYSQL_HOST')   
user = os.environ.get('MYSQL_USERNAME')  
password = os.environ.get('MYSQL_PASSWORD')
port = os.environ.get('MYSQL_PORT')

# 数据库存储操作
cnx = mysql.connector.connect(user=user, password=password,
                              host=host,  port=port, database='鸣熙产品净值数据库')
engine = create_engine(f'mysql+mysqlconnector://{user}:{password}@{host}:{port}/鸣熙产品净值数据库')
cursor = cnx.cursor()

# 周度和月度日期序列 #
weekly_date = data_CSI1000.drop('close', axis=1)
weekly_date = weekly_date.rename(columns = {'date':'净值日期'})
weekly_date['是否周末净值'] = ''
weekly_date['是否月末净值'] = ''
weekly_date['是否年末净值'] = ''
weekly_date.iloc[-1, 1] = '是'

# 比较这两个日期
if time_last_month_time > time_last_Friday_time:
    # 如果上月末的日期更大，那么就用上月末的日期来筛选数据
    weekly_date = weekly_date[weekly_date['净值日期'] <= time_last_month_time]
else:
    # 如果上周五的日期更大，那么就用上周五的日期来筛选数据
    weekly_date = weekly_date[weekly_date['净值日期'] <= time_last_Friday_time]

# 周度净值日期贴标签
for i in range(1, len(weekly_date)):
    if weekly_date.iloc[i, 0] - weekly_date.iloc[i-1, 0] != timedelta(days=1):
        weekly_date.iloc[i-1, 1] = '是'
    else:
        weekly_date.iloc[i-1, 1] = '否'
    if weekly_date.iloc[i, 0] == time_last_Friday_time:
        weekly_date.iloc[i, 1] = '是'
        
# 周中法定节假日周频净值剔除
for i in range(len(weekly_date) - 1):
    current_week = weekly_date.iloc[i, 0].strftime("%U")  # 获取当前日期所在周的周数
    next_week = weekly_date.iloc[i + 1, 0].strftime("%U")  # 获取下一个日期所在周的周数
    if current_week == next_week:
        weekly_date.iloc[i, 1] = '否'

# 月度净值日期贴标签
for i in range(1, len(weekly_date)):
    if weekly_date.iloc[i, 0].month - (weekly_date.iloc[i, 0] + timedelta(1)).month != 0:
        weekly_date.iloc[i, 2] = '是'
    elif weekly_date.iloc[i, 0].month - weekly_date.iloc[i-1, 0].month != 0:
        weekly_date.iloc[i-1, 2] = '是'
    elif weekly_date.iloc[i, 0] == time_last_month_time:
        weekly_date.iloc[i, 2] = '是'
    else:
        weekly_date.iloc[i, 2] = '否'
        
# 年度净值日期贴标签
for i in range(len(weekly_date) - 1):
    weekly_date.iloc[i, 0].month
    if weekly_date.iloc[i+1, 0].month - weekly_date.iloc[i, 0].month == -11:
        weekly_date.iloc[i, 3] = '是'
    elif i == len(weekly_date) - 2:
        weekly_date.iloc[i + 1, 3] = '是'
    else:
        weekly_date.iloc[i, 3] = '否'

weekly_date = weekly_date[(weekly_date['是否周末净值'] == '是') | (weekly_date['是否月末净值'] == '是')].reset_index(drop=True)

# weekly_date = pd.to_datetime(weekly_date['净值日期'])
# =============================================================================
# # 去掉最后一行的多余数据
# weekly_date = weekly_date.iloc[:-1]
# =============================================================================

# 确定筛选后的产品
list_weekly_summary = pd.read_sql_query("SELECT * FROM %s" % '产品净值周报汇总表', engine)
list_chanpin_strategy = pd.read_sql_query("SELECT * FROM %s" % '产品策略对照表', engine)
list_chanpin_strategy_weekly = selected_products = list_chanpin_strategy[
    (list_chanpin_strategy['资产规模'] > 0) &
    (list_chanpin_strategy['策略类型'].str.contains('中性|指增|CTA|其他', regex=True)) &
    ((datetime.now() - list_chanpin_strategy['最新净值日期']) <= timedelta(days=14)) &
    ((datetime.now() - pd.to_datetime(list_chanpin_strategy['成立日期'])).dt.days > 35)
].reset_index(drop=True)
for n in range(len(list_chanpin_strategy_weekly)):
    try:
        if list_weekly_summary.loc[list_weekly_summary['产品名称'] == list_chanpin_strategy_weekly.loc[n, '基金简称'], '最新净值日期'].values[0] != pd.Timestamp(time_last_Friday_time):
            list_chanpin_strategy.loc[list_chanpin_strategy['基金全称'] == list_chanpin_strategy_weekly.loc[n, '基金全称'], '是否更新周报'] = '否'
    except:
        pass
list_chanpin_strategy_weekly = selected_products = list_chanpin_strategy[
    (list_chanpin_strategy['资产规模'] > 0) &
    (list_chanpin_strategy['策略类型'].str.contains('中性|指增|CTA|其他', regex=True)) &
    ((datetime.now() - list_chanpin_strategy['最新净值日期']) <= timedelta(days=14)) &
    ((datetime.now() - pd.to_datetime(list_chanpin_strategy['成立日期'])).dt.days > 35)
].reset_index(drop=True)

for n in range(len(list_chanpin_strategy_weekly)):
    try:
        if list_weekly_summary.loc[list_weekly_summary['产品名称'] == list_chanpin_strategy_weekly.loc[n, '基金简称'], '最新净值日期'].values[0] != pd.Timestamp(time_last_Friday_time):
            list_chanpin_strategy.loc[list_chanpin_strategy['基金简称'] == list_chanpin_strategy_weekly.loc[n, '基金简称'], '是否更新周报'] = '否'
    except:
        if list_chanpin_strategy_weekly.loc[n, '最新净值日期'] < pd.Timestamp(time_last_Friday_time):
            continue
    if list_chanpin_strategy_weekly.loc[n, '是否更新周报'] == '是':
        continue
    if list_chanpin_strategy_weekly.loc[n, '最新净值日期'] < pd.Timestamp(time_last_Friday_time):
        continue
    try:
        # 使用参数化查询
        table_name = list_chanpin_strategy_weekly.loc[n, '基金全称']
        query = f"SELECT * FROM `{table_name}`"
        list_chanpin_info = pd.read_sql_query(query, engine)
        table_name = list_chanpin_strategy_weekly.loc[n, '基金全称'] + "(波动率)"
        query = f"SELECT * FROM `{table_name}`"
        list_chanpin_val_info = pd.read_sql_query(query, engine)
    except:
        n = n + 1
        logging.info('无产品信息' + list_chanpin_strategy_weekly.loc[n, '基金全称'])
        continue
    
    # 识别策略类型 # 
    type_strategy = list_chanpin_strategy_weekly.loc[n, '策略类型']
    list_chanpin_strategy_weekly.loc[n, '成立日期'] = list_chanpin_info.iloc[0, 0]
    
    # 产品成立日期格式转datetime #
    date_found_chanpin = list_chanpin_strategy_weekly.loc[n, '成立日期'].to_pydatetime()
    # 净值频率调整 # 
    first_row = pd.DataFrame(list_chanpin_info.iloc[0]).T
    list_chanpin_info = pd.merge(list_chanpin_info, weekly_date, on='净值日期', how='inner')
    if first_row['净值日期'].isin(list_chanpin_info['净值日期']).any() == False:
        list_chanpin_info = pd.concat([first_row, list_chanpin_info]).reset_index(drop=True)
    list_chanpin_info.loc[0, ['是否周末净值', '是否月末净值', '是否年末净值']] = '是'

    # 重新计算周频最大回撤序列 #
    for yeild_name, yeild_excess_name, drawdown_name, drawdown_excess_name in zip(['成立以来收益率', '今年以来收益率', '近三月收益率', '近六月收益率', '近一年收益率', '近两年收益率', '近三年收益率'],
                                         ['成立以来超额（除法）', '今年以来超额（除法）', '近三月超额（除法）', '近六月超额（除法）', '近一年超额（除法）', '近两年超额（除法）', '近三年超额（除法）'],
                                         ['成立以来最大回撤', '今年以来最大回撤', '近三月最大回撤', '近六月最大回撤', '近一年最大回撤', '近两年最大回撤', '近三年最大回撤'],
                                         ['成立以来最大回撤（超额）', '今年以来最大回撤（超额）', '近三月最大回撤（超额）', '近六月最大回撤（超额）', '近一年最大回撤（超额）', '近两年最大回撤（超额）', '近三年最大回撤（超额）']):
        # 通用策略 #
        nav_series = list_chanpin_info.loc[:, yeild_name]
        for d in range(1, len(nav_series)):
            try:
                list_chanpin_info.loc[nav_series.index[d], drawdown_name] = (nav_series.iloc[d] + 1)/(nav_series.iloc[0:d+1].max(skipna=True) + 1) - 1
                if list_chanpin_info.loc[nav_series.index[d], drawdown_name] > 0:
                    list_chanpin_info.loc[nav_series.index[d], drawdown_name] = 0  
            except:
                continue
        
        # 指增策略 #
        if type_strategy.find('指增') != -1:
            nav_series = list_chanpin_info.loc[:, yeild_excess_name]
            for d in range(1, len(nav_series)):
                try:
                    list_chanpin_info.loc[nav_series.index[d], drawdown_excess_name] = (nav_series.iloc[d] + 1)/(nav_series.iloc[0:d+1].max(skipna=True) + 1) - 1
                    if list_chanpin_info.loc[nav_series.index[d], drawdown_excess_name] > 0:
                        list_chanpin_info.loc[nav_series.index[d], drawdown_excess_name] = 0
                except:
                    continue
    
    # 筛选'是否周末净值'为是的行，并计算本周收益率
    list_chanpin_info.loc[list_chanpin_info['是否周末净值'] == '是', '本周收益率'] = \
        list_chanpin_info.loc[list_chanpin_info['是否周末净值'] == '是', '复权净值'] / \
        list_chanpin_info.loc[list_chanpin_info['是否周末净值'] == '是', '复权净值'].shift(1) - 1
    # 筛选'是否月末净值'为是的行，并计算本月收益率
    list_chanpin_info.loc[list_chanpin_info['是否月末净值'] == '是', '本月收益率'] = \
        list_chanpin_info.loc[list_chanpin_info['是否月末净值'] == '是', '复权净值'] / \
        list_chanpin_info.loc[list_chanpin_info['是否月末净值'] == '是', '复权净值'].shift(1) - 1
    # 筛选'是否年末净值'为是的行，并计算本年收益率
    list_chanpin_info.loc[list_chanpin_info['是否年末净值'] == '是', '本年收益率'] = \
        list_chanpin_info.loc[list_chanpin_info['是否年末净值'] == '是', '复权净值'] / \
        list_chanpin_info.loc[list_chanpin_info['是否年末净值'] == '是', '复权净值'].shift(1) - 1
    # 计算对应超额收益率
    if type_strategy.find('指增') != -1:
        list_chanpin_info.loc[list_chanpin_info['是否周末净值'] == '是', '本周超额'] = \
            (list_chanpin_info.loc[list_chanpin_info['是否周末净值'] == '是', '本周收益率'] + 1) / \
            (list_chanpin_info.loc[list_chanpin_info['是否周末净值'] == '是', '对标指数'] / \
            list_chanpin_info.loc[list_chanpin_info['是否周末净值'] == '是', '对标指数'].shift(1)) - 1
        list_chanpin_info.loc[list_chanpin_info['是否月末净值'] == '是', '本月超额'] = \
            (list_chanpin_info.loc[list_chanpin_info['是否月末净值'] == '是', '本月收益率'] + 1) / \
            (list_chanpin_info.loc[list_chanpin_info['是否月末净值'] == '是', '对标指数'] / \
            list_chanpin_info.loc[list_chanpin_info['是否月末净值'] == '是', '对标指数'].shift(1)) - 1
        list_chanpin_info.loc[list_chanpin_info['是否年末净值'] == '是', '本年超额'] = \
            (list_chanpin_info.loc[list_chanpin_info['是否年末净值'] == '是', '本年收益率'] + 1) / \
            (list_chanpin_info.loc[list_chanpin_info['是否年末净值'] == '是', '对标指数'] / \
            list_chanpin_info.loc[list_chanpin_info['是否年末净值'] == '是', '对标指数'].shift(1)) - 1
    else:
        list_chanpin_info.loc[:, '本周超额'] = np.nan  # 或者使用 None 或者其他默认值
        list_chanpin_info.loc[:, '本月超额'] = np.nan
        list_chanpin_info.loc[:, '本年超额'] = np.nan
                
    list_chanpin_info['净值日期'] = pd.to_datetime(list_chanpin_info['净值日期'])
    # 计算年化收益率
    list_chanpin_info.loc[list_chanpin_info['净值日期'] - date_found_chanpin < timedelta(365), '累计年化收益率'] = list_chanpin_info.loc[list_chanpin_info['净值日期'] - date_found_chanpin < timedelta(365), '成立以来收益率']
    list_chanpin_info.loc[list_chanpin_info['净值日期'] - date_found_chanpin >= timedelta(365), '累计年化收益率'] = \
        (list_chanpin_info.loc[list_chanpin_info['净值日期'] - date_found_chanpin >= timedelta(365), '成立以来收益率'] + 1) \
        **(timedelta(365)/(list_chanpin_info.loc[list_chanpin_info['净值日期'] - date_found_chanpin >= timedelta(365), '净值日期'] - date_found_chanpin)) - 1
    if type_strategy.find('指增') != -1:
        list_chanpin_info.loc[list_chanpin_info['净值日期'] - date_found_chanpin < timedelta(365), '累计年化超额收益率'] = list_chanpin_info.loc[list_chanpin_info['净值日期'] - date_found_chanpin < timedelta(365), '成立以来超额（除法）']
        list_chanpin_info.loc[list_chanpin_info['净值日期'] - date_found_chanpin >= timedelta(365), '累计年化超额收益率'] = \
            (list_chanpin_info.loc[list_chanpin_info['净值日期'] - date_found_chanpin >= timedelta(365), '成立以来超额（除法）'] + 1) \
            **(timedelta(365)/(list_chanpin_info.loc[list_chanpin_info['净值日期'] - date_found_chanpin >= timedelta(365), '净值日期'] - date_found_chanpin)) - 1
    else:
        list_chanpin_info.loc[:, '累计年化超额收益率'] = np.nan
    
    # 计算夏普比率、卡玛比率、索提诺
    for d in range(len(list_chanpin_info)):
        try:
            volatility_chanpin = np.std(list_chanpin_val_info.loc[list_chanpin_val_info['净值日期'] <= list_chanpin_info.loc[d, '净值日期'], '成立以来波动率'])*pow(52, 0.5)
            volatility_down_chanpin = np.std(list_chanpin_val_info.loc[(list_chanpin_val_info['净值日期'] <= list_chanpin_info.loc[d, '净值日期'])&(list_chanpin_val_info['成立以来波动率'] < 0), '成立以来波动率'])*pow(52, 0.5)
            list_chanpin_info.loc[d, '夏普比率'] = round(list_chanpin_info.loc[d, '累计年化收益率'] / volatility_chanpin, 2)
            list_chanpin_info.loc[d, '索提诺比率'] =  round(list_chanpin_info.loc[d, '累计年化收益率'] / volatility_down_chanpin, 2)
            list_chanpin_info.loc[d, '卡玛比率'] = round(list_chanpin_info.loc[d, '累计年化收益率'] / - list_chanpin_info.loc[0:d, '成立以来最大回撤'].min(skipna=True), 2)
            
            if type_strategy.find('指增') != -1:
                volatility_excess_chanpin = np.std(list_chanpin_val_info.loc[list_chanpin_val_info['净值日期'] <= list_chanpin_info.loc[d, '净值日期'], '成立以来超额波动率'])*pow(52, 0.5)
                volatility_excess_down_chanpin = np.std(list_chanpin_val_info.loc[(list_chanpin_val_info['净值日期'] <= list_chanpin_info.loc[d, '净值日期'])&(list_chanpin_val_info['成立以来超额波动率'] < 0), '成立以来超额波动率'])*pow(52, 0.5)
                list_chanpin_info.loc[d, '超额夏普比率'] = round(list_chanpin_info.loc[d, '累计年化超额收益率'] / volatility_excess_chanpin, 2)
                list_chanpin_info.loc[d, '超额索提诺比率'] = round(list_chanpin_info.loc[d, '累计年化超额收益率'] / volatility_excess_down_chanpin, 2)
                list_chanpin_info.loc[d, '超额卡玛比率'] = round(list_chanpin_info.loc[d, '累计年化超额收益率'] / - list_chanpin_info.loc[0:d, '成立以来最大回撤（超额）'].min(skipna=True), 2)
            else:
                list_chanpin_info.loc[:, '超额夏普比率'] = np.nan
                list_chanpin_info.loc[:, '超额索提诺比率'] = np.nan
                list_chanpin_info.loc[:, '超额卡玛比率'] = np.nan
                
        except:
            list_chanpin_info.loc[d, '夏普比率'] = np.nan
            list_chanpin_info.loc[d, '卡玛比率'] = np.nan
            list_chanpin_info.loc[d, '索提诺比率'] = np.nan
            list_chanpin_info.loc[d, '超额夏普比率'] = np.nan
            list_chanpin_info.loc[d, '超额卡玛比率'] = np.nan
            list_chanpin_info.loc[d, '超额索提诺比率'] = np.nan
                
    # 替换 -inf 为 NaN
    list_chanpin_info = list_chanpin_info.replace(to_replace=['', None, -np.inf, np.inf], value=float(np.nan), inplace=False).round(4)
    logging.info(list_chanpin_strategy_weekly.iloc[n, 0] + '：{:.2%}'.format((n+1)/len(list_chanpin_strategy_weekly)))
    # 导入数据到MySQL表
    list_chanpin_info.to_sql(name = list_chanpin_strategy_weekly.iloc[n, 0] + '净值周报', con=engine, if_exists='replace', index=False)

# 绩效指标汇总表
summary_df_columns = ['产品名称', '策略类型', '策略调整时点', '最新份额', '资产规模', '成立日期', '最新净值日期', '单位净值', '累计净值', '复权净值', '成立以来收益率', '对标指数成立以来收益率',
       '成立以来超额（除法）', '成立以来最大回撤', '成立以来最大回撤（超额）', '今年以来收益率',
       '对标指数今年以来收益率', '今年以来超额（除法）', '今年以来最大回撤', '今年以来最大回撤（超额）',
       '近三月收益率', '对标指数近三月收益率', '近三月超额（除法）', '近三月最大回撤', '近三月最大回撤（超额）',
       '近六月收益率', '对标指数近六月收益率', '近六月超额（除法）', '近六月最大回撤', '近六月最大回撤（超额）',
       '近一年收益率', '对标指数近一年收益率', '近一年超额（除法）', '近一年最大回撤', '近一年最大回撤（超额）',
       '近两年收益率', '对标指数近两年收益率', '近两年超额（除法）', '近两年最大回撤', '近两年最大回撤（超额）',
       '近三年收益率', '对标指数近三年收益率', '近三年超额（除法）', '近三年最大回撤', '近三年最大回撤（超额）',
       '近四周收益率', '对标指数近四周收益率', '近四周超额（除法）', 
       '本周收益率', '本周超额', '周胜率', '超额周胜率', '本月收益率', '本月超额', '月胜率', '超额月胜率', '累计年化收益率', '累计年化超额收益率', '夏普比率', '超额夏普比率', '索提诺比率', '超额索提诺比率', '卡玛比率', '超额卡玛比率']

summary_df = pd.DataFrame(columns = summary_df_columns)
for n in range(len(list_chanpin_strategy_weekly)):
    if list_chanpin_strategy_weekly.loc[n, '是否更新周报'] == '是':
        continue
    # 回填策略对照表周报更新值
    list_chanpin_strategy.loc[list_chanpin_strategy['基金全称'] == list_chanpin_strategy_weekly.iloc[n, 0], '是否更新周报'] = '是'
    try:
        table_name = list_chanpin_strategy_weekly.loc[n, '基金全称'] + '净值周报'
        query = f"SELECT * FROM `{table_name}`"
        list_chanpin_info = pd.read_sql_query(query, engine)
    except:
        continue
    summary_row = pd.Series(index = summary_df.columns)
    for col in summary_df.columns:
        if col in list_chanpin_info.columns:
            # 删除NaN值后的数据
            non_nan_data = list_chanpin_info[col].dropna()
            if not non_nan_data.empty:
                # 获取最后一行数据
                summary_row[col] = non_nan_data.iloc[-1]
            else:
                # 列为空的情况下将其设置为np.nan
                summary_row[col] = np.nan
    
    # 计算周胜率大于零的比率
    summary_row['周胜率'] = round(float((list_chanpin_info['本周收益率'] > 0).sum() / len(list_chanpin_info['本周收益率'].dropna())), 4)
    try:
        # 计算月胜率大于零的比率
        summary_row['月胜率'] = round(float((list_chanpin_info['本月收益率'] > 0).sum() / len(list_chanpin_info['本月收益率'].dropna())), 4)
    except:
        pass
    
    # 最大回撤单独计算
    try:
        summary_row['成立以来最大回撤'] = min(list_chanpin_info['成立以来最大回撤'].dropna())
        summary_row['近三月最大回撤'] = min(list_chanpin_info['近三月最大回撤'].dropna())
        summary_row['近六月最大回撤'] = min(list_chanpin_info['近六月最大回撤'].dropna())
        summary_row['近一年最大回撤'] = min(list_chanpin_info['近一年最大回撤'].dropna())
        summary_row['近两年最大回撤'] = min(list_chanpin_info['近两年最大回撤'].dropna())
        summary_row['近三年最大回撤'] = min(list_chanpin_info['近三年最大回撤'].dropna())
    except:
        pass
    try:
        summary_row['今年以来最大回撤'] = min(list_chanpin_info['今年以来最大回撤'].dropna())
    except:
        pass
    if list_chanpin_strategy_weekly.loc[n, '策略类型'].find('指增') != -1:
        try:
            summary_row['成立以来最大回撤（超额）'] = min(list_chanpin_info['成立以来最大回撤（超额）'].dropna())
            summary_row['近三月最大回撤（超额）'] = min(list_chanpin_info['近三月最大回撤（超额）'].dropna())
            summary_row['近六月最大回撤（超额）'] = min(list_chanpin_info['近六月最大回撤（超额）'].dropna())
            summary_row['近一年最大回撤（超额）'] = min(list_chanpin_info['近一年最大回撤（超额）'].dropna())
            summary_row['近两年最大回撤（超额）'] = min(list_chanpin_info['近两年最大回撤（超额）'].dropna())
            summary_row['近三年最大回撤（超额）'] = min(list_chanpin_info['近三年最大回撤（超额）'].dropna())
        except:
            pass
        try:
            summary_row['今年以来最大回撤（超额）'] = min(list_chanpin_info['今年以来最大回撤（超额）'].dropna())
        except:
            pass
        summary_row['超额周胜率'] = (list_chanpin_info['本周超额'] > 0).sum() / len(list_chanpin_info['本周超额'].dropna())
        try:
            summary_row['超额月胜率'] = (list_chanpin_info['本月超额'] > 0).sum() / len(list_chanpin_info['本月超额'].dropna())
        except:
            pass
        
    summary_row['产品名称'] = list_chanpin_strategy_weekly.loc[n, '基金简称']
    summary_row['策略类型'] = list_chanpin_strategy_weekly.loc[n, '策略类型']
    summary_row['成立日期'] = list_chanpin_strategy_weekly.loc[n, '成立日期']
    summary_row['最新净值日期'] = list_chanpin_strategy_weekly.loc[n, '最新净值日期']
    summary_row['资产规模'] = list_chanpin_strategy_weekly.loc[n, '资产规模']
    summary_row['最新份额'] = list_chanpin_strategy_weekly.loc[n, '最新份额']
    summary_row['策略调整时点'] = list_chanpin_strategy_weekly.loc[n, '策略调整时点']
    summary_df.loc[n] = summary_row

summary_df[["成立以来收益率", "对标指数成立以来收益率", "成立以来超额（除法）", "成立以来最大回撤", "成立以来最大回撤（超额）", "今年以来收益率", "对标指数今年以来收益率", "今年以来超额（除法）", "今年以来最大回撤", "今年以来最大回撤（超额）", "近三月收益率", "对标指数近三月收益率", "近三月超额（除法）", "近三月最大回撤", "近三月最大回撤（超额）", "近六月收益率", "对标指数近六月收益率", "近六月超额（除法）", "近六月最大回撤", "近六月最大回撤（超额）", "近一年收益率", "对标指数近一年收益率", "近一年超额（除法）", "近一年最大回撤", "近一年最大回撤（超额）", "近两年收益率", "对标指数近两年收益率", "近两年超额（除法）", "近两年最大回撤", "近两年最大回撤（超额）", "近三年收益率", "对标指数近三年收益率", "近三年超额（除法）", "近三年最大回撤", "近三年最大回撤（超额）", "近四周收益率", "对标指数近四周收益率", "近四周超额（除法）", "本周收益率", "本周超额", "周胜率", "超额周胜率", "月胜率", "超额月胜率", "累计年化收益率", "累计年化超额收益率", "夏普比率", "超额夏普比率", "索提诺比率", "超额索提诺比率", "卡玛比率", "超额卡玛比率"]] \
 = summary_df[["成立以来收益率", "对标指数成立以来收益率", "成立以来超额（除法）", "成立以来最大回撤", "成立以来最大回撤（超额）", "今年以来收益率", "对标指数今年以来收益率", "今年以来超额（除法）", "今年以来最大回撤", "今年以来最大回撤（超额）", "近三月收益率", "对标指数近三月收益率", "近三月超额（除法）", "近三月最大回撤", "近三月最大回撤（超额）", "近六月收益率", "对标指数近六月收益率", "近六月超额（除法）", "近六月最大回撤", "近六月最大回撤（超额）", "近一年收益率", "对标指数近一年收益率", "近一年超额（除法）", "近一年最大回撤", "近一年最大回撤（超额）", "近两年收益率", "对标指数近两年收益率", "近两年超额（除法）", "近两年最大回撤", "近两年最大回撤（超额）", "近三年收益率", "对标指数近三年收益率", "近三年超额（除法）", "近三年最大回撤", "近三年最大回撤（超额）", "近四周收益率", "对标指数近四周收益率", "近四周超额（除法）", "本周收益率", "本周超额", "周胜率", "超额周胜率", "月胜率", "超额月胜率", "累计年化收益率", "累计年化超额收益率", "夏普比率", "超额夏普比率", "索提诺比率", "超额索提诺比率", "卡玛比率", "超额卡玛比率"]].astype(float)
summary_df = pd.concat([list_weekly_summary, summary_df], ignore_index=True, sort=False)
summary_df = summary_df.drop_duplicates(subset='产品名称', keep='last').sort_values(by='成立日期')

# 将 '成立日期' 和 '净值日期' 列转换为 datetime 格式
summary_df['成立日期'] = pd.to_datetime(summary_df['成立日期'])
summary_df['最新净值日期'] = pd.to_datetime(summary_df['最新净值日期'])
# 获取'净值日期'列之后的所有列
cols_to_convert = summary_df.loc[:, '单位净值':].columns
# 将这些列转换为float类型
summary_df[cols_to_convert] = summary_df[cols_to_convert].astype(float)

summary_df.to_sql(name = '产品净值周报汇总表', con=engine, if_exists='replace', index=False)
list_chanpin_strategy.to_sql(name='产品策略对照表', con=engine, if_exists='replace', index=False)
